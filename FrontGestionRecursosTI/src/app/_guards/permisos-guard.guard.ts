import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class PermisosGuardGuard implements CanActivateChild {
  constructor(private router: Router)
  {}

  //Método para validar si el usuario tienen acceso  a ciertas rutas
  canActivateChild(
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      //Verifica que el usurio este logeado y si tiene permisos
      if (childRoute.data.permiso && !this.hasPermiso(childRoute.data.permiso)) {
        //Comprueba el usuario logeado
        if (localStorage.getItem('currentUser') != null) {
          //De no tener permiso a una ruta pero esta logeado un usuario se redirige a la pantalla principal
          
          /** En caso de que no tenga permiso para editar los mantenimeintos, se mostrará un mensaje de 
           * advertencia en donde se le indica al empleado que no puede acceder a determinada opcción */
          if(childRoute.data.permiso == "EDITAR MANTENIMIENTO")
          {
            Swal.fire({
              icon:'warning',
              title:'Atención',
              text:"¡No tienes permiso para editar los mantenimeintos!\nPonte en contacto con TI.",
              timer:2000
            })
          }
          
          this.router.navigate(['layout']);
          return false;
        } else {
          //Si no esta logeado el usuario siempre se redirigira al login
          this.router.navigate(['login']);
          return false;
        }
      } else {
        return true;
      }
  }

  //Metodo para verificar si un usuario tiene permiso en una ruta
  hasPermiso(permiso: string): boolean {
    let permisosU = JSON.parse(localStorage.getItem('permisos')!);

    if(permisosU.length>0)
    {
      for(let i=0; i<permisosU.length;i++)
      {
        if( permisosU[i].nombre == permiso )
        {
          return true
        }
      }
    }

    return false
  }
}
