import { TestBed } from '@angular/core/testing';

import { PermisosGuardGuard } from './permisos-guard.guard';

describe('PermisosGuardGuard', () => {
  let guard: PermisosGuardGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(PermisosGuardGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
