import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { DetalleRegistro } from 'src/app/administracion/modelos/Asignacion_Equipo/detalle-registro';
import { Equipo } from 'src/app/administracion/modelos/Inventario/Equipo/equipo';
import { Sucursal } from 'src/app/administracion/modelos/sucursal';
import { UsuarioService } from 'src/app/administracion/servicios';
import { DetalleRegistroService } from 'src/app/administracion/servicios/Asignacion_Equipo/detalle-registro.service';
import { RegistroAsignacionEquipoService } from 'src/app/administracion/servicios/Asignacion_Equipo/registro-asignacion-equipo.service';
import { EquipoService } from 'src/app/administracion/servicios/Inventario/Equipo/equipo.service';
import { UsuarioGlpiService } from 'src/app/administracion/servicios/Inventario/Usuario/usuario-glpi.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-remplazo-equipo',
  templateUrl: './remplazo-equipo.component.html',
  styleUrls: ['./remplazo-equipo.component.scss']
})
export class RemplazoEquipoComponent implements OnInit {

  /*PARÁMATROS */
  id_registro_general !: number
  id_empleado_asignac !: number
  nombre_empleado_asi !: string

  /**FORM CONTROL */
  input_observaciones: FormControl = new FormControl()
  input__tabla_select: FormControl = new FormControl()

  /**TABLA */
  dataSource          = new MatTableDataSource<Equipo>([])                          /* DATA DE LA TABLA                */
  @ViewChild('paginator', { static: true }) public paginator !: MatPaginator;       /* PAGINATOR DE LA TABLA           */
  encabezados     :string [] = ["OPCIONES","NOMBRE","NÚMERO DE SERIE","TIPO","MODELO","FABRICANTE"]  /* ENCABEZADOS GENÉRICOS           */
  opciones_select     :string [] = [] /* LISTA DE OPCIONES A SELECCIONAR */

  constructor(
              private detalleRegistroService         : DetalleRegistroService         ,
              private equipoService                  : EquipoService                  ,
              private activatedRoute                 : ActivatedRoute                 ,
              private registroAsignacionEquipoService: RegistroAsignacionEquipoService,
              private router                         : Router                         ,
              private usuarioService                 : UsuarioService                 ,
              private usuarioGlpiService             : UsuarioGlpiService
             ) { }

  ngOnInit()
  {
    this.parametros()
    this.opciones_select = this.equipoService.listaActivosAsignacion(3)
  }

  /* CAPTURAMOS LOS PARÁMETROS REIBIDOS DE LA URL */
  parametros()
  {
    this.activatedRoute.params.subscribe(params =>{
      this.id_registro_general   = params['id_registro_asignacion'] /* EL ID DEL REGISTRO     */
      this.id_empleado_asignac   = params['id_empleado_asignac']    /* EL ID DEL EMPLEADO     */
      this.nombre_empleado_asi   = params['nombre_empleado_asi']    /* EL NOMBRE DEL EMPLEADO */
    })
  }

  /*  EVENTO PARA ASIGNAR UN EQUIPO */
  clickSeleccionarEquipo(equipo:Equipo)
  {
    /* SOLICITAMOS CONFIRMACIÓN DE LA ASIGNACIÓN */
    Swal.fire({
      title:'¿Quieres asignar este equipo a ' + this.nombre_empleado_asi + '?',
      icon:'question',
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      showCancelButton:true,
      confirmButtonText:'SÍ',
      cancelButtonText: 'NO'
    }).then((result)=>{
      if(result.isConfirmed) /*CONFIRMADA LA ASIGNACIÓN */
      {
        /**ASIGNAMOS EL EQUIPO AL EMPLEADO EN GLPI */
        this.cambiarStatusEquipo(Number(equipo.id), (equipo.tabla+""), equipo)
      }
    })
  }
  

  /* CARGA LOS EQUIPOS DEL TIPO SELECCIONADO */
  clickSelectTabla(event:any)
  {
    
    /**DECLARAMOS UNA LISTA AUXILIAR PARA UTILIZARLA DESPUES*/
    let lista_aux_equipos: Equipo [] = []

    /**COMPARAMOS SI LA OPCCION QUE ELIGIÓ FUE UNA CPU */
    if(this.input__tabla_select.value == "CPU")
    {
      this.equipoService.todosLosEquipos("Computer").subscribe(lista_equipos=>{

        /**AHORA SOLO SELECCIONAMOS LOS EQUIPOS QUE SE ENCUENTREN DISPONIBLES O SIN ASIGNAR */
        lista_equipos.forEach(equipo=>{
          if(equipo.states_id == "Stock" || equipo.states_id == "No Asignado" || equipo.states_id == "Principal" || equipo.states_id == "Backup" || equipo.states_id == "" )
          {
            equipo.tipo = "COMPUTADORA"
            equipo.modelo = equipo.computermodels_id
            equipo.tabla = "Computer"
            if(equipo.name.includes("CPU")) /*REVISAMOS QUE SEA UNA CPU DISPONIBLE PARA AGREGARLOA  LA LISTA AUXILIAR*/
            {
              lista_aux_equipos.push(equipo)
            }
          }
        })
        this.dataSource.data = lista_aux_equipos
        this.dataSource.paginator = this.paginator

        /*VALIDAMOS SI ESTÁ VACÍO LA DATA DE LA TABLA PARA DESPLEGAR UN MENSAJE DE ERROR */
        if(this.dataSource.data.length==0)
        {
          Swal.fire({
            icon:'warning', 
            title:'¡No hay equipos disponibles!',
            text:'intentelo más tarde',
            showConfirmButton:false,
            allowEnterKey:false,
            allowEscapeKey:false,
            allowOutsideClick:false,
            timer:2000
          })
        }
      })
    }
    
    /**COMPARAMOS SI LA OPCCION QUE ELIGIÓ FUE UN MONITOR */
    else if(this.input__tabla_select.value == "MONITOR")
    {
      this.equipoService.todosLosEquipos("Monitor").subscribe(lista_equipos=>{
        
        /**AHORA SOLO SELECCIONAMOS LOS EQUIPOS QUE SE ENCUENTREN DISPONIBLES O SIN ASIGNAR */
        lista_equipos.forEach(equipo=>{
          if(equipo.states_id == "Stock" || equipo.states_id == "No Asignado" || equipo.states_id == "Principal" || equipo.states_id == "Backup" || equipo.states_id == "" )
          {
            equipo.tipo = "MONITOR"
            equipo.modelo = equipo.monitormodels_id
            equipo.tabla = "Monitor"
            lista_aux_equipos.push(equipo)
          }
        })
        this.dataSource.data = lista_aux_equipos
        this.dataSource.paginator = this.paginator

        /*VALIDAMOS SI ESTÁ VACÍO LA DATA DE LA TABLA PARA DESPLEGAR UN MENSAJE DE ERROR */
        if(this.dataSource.data.length==0)
        {
          Swal.fire({
            icon:'warning', 
            title:'¡No hay equipos disponibles!',
            text:'intentelo más tarde',
            showConfirmButton:false,
            allowEnterKey:false,
            allowEscapeKey:false,
            allowOutsideClick:false,
            timer:2000
          })
        }
      })
    }
    
    /**EN CASO QUE NO FUESE NINGUNA DE LAS ANTERIORES SOLAMENTE CARGAMOS LOS DISPOSITIVOS */
    else
    {
      this.equipoService.todosLosEquipos("Peripheral").subscribe(lista_equipos=>{
        
        /**AHORA SOLO SELECCIONAMOS LOS EQUIPOS QUE SE ENCUENTREN DISPONIBLES O SIN ASIGNAR */
        lista_equipos.forEach(equipo=>{
          if(equipo.states_id == "Stock" || equipo.states_id == "No Asignado" || equipo.states_id == "Principal" || equipo.states_id == "Backup" || equipo.states_id == "" )
          {
            
            /**COMPARAMOS SI LOS EQUIPOS PERTENECEN AL TIPO ELEGIDO */
            if((equipo.peripheraltypes_id+"").includes(this.input__tabla_select.value))
            {
              equipo.tipo = "DISPOSITIVO"
              equipo.modelo = equipo.peripheralmodels_id
              equipo.tabla = "Peripheral"
              lista_aux_equipos.push(equipo)
            }
          }
        })
        /*ENVIAMOS LA LISTA AULIAR A LA DATA DE LA TABLA */
        this.dataSource.data = lista_aux_equipos
        this.dataSource.paginator = this.paginator

        /*VALIDAMOS SI ESTÁ VACÍO LA DATA DE LA TABLA PARA DESPLEGAR UN MENSAJE DE ERROR */
        if(this.dataSource.data.length==0)
        {
          Swal.fire({
            icon:'warning', 
            title:'¡No hay equipos disponibles!',
            text:'intentelo más tarde',
            showConfirmButton:false,
            allowEnterKey:false,
            allowEscapeKey:false,
            allowOutsideClick:false,
            timer:2000
          })
        }
      })
    }    
  }

  /* CANCELAMOS ESTA ASIGNACION Y REGRESAMOS AL DETALLE DEL REGISTRO VISTO CON ANTERIORIDAD */
  clickCancelar(event:any)
  {
    Swal.fire({
      title:'¿Quieres cancelar esta asignación?',
      icon:'question',
      showCancelButton:true,
      confirmButtonText:'SÍ',
      cancelButtonText:'NO',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false
    }).then((result)=>{
      if(result.isConfirmed)
      {
        this.router.navigate(['layout/asignacionesEquipoUsuario/' + this.id_empleado_asignac])
      }
    })
  }

  /*  ASIGNA EL EQUIPO A UN EMPLEADO  */
  cambiarStatusEquipo(equipo_id:number, tabla:string,equipo:Equipo)
  {
    let sucursal_obj:Sucursal = new Sucursal() /* creamos un objeto para cuando la sucursal sea nula */
    sucursal_obj.nombreSucursal = "Sin sucursal a buscar"

    this.usuarioService.getAnEmpleado(this.id_empleado_asignac).subscribe(empleado_endpoint=>{
      /* Consultamos la sucursal en glpi*/
      if(empleado_endpoint.sucursal==null)/* valida si el empleado tiene el campo sucursal como nulo */
      {
        empleado_endpoint.sucursal = sucursal_obj /* De tenerlo solo agrega el valor por defecto */
      }

      this.equipoService.buscarSucursal(empleado_endpoint.sucursal.nombreSucursal).subscribe(sucursales=>{
  
        /* Consultamos al empleado en glpi*/
        this.usuarioGlpiService.allUsuariosSearch((empleado_endpoint.nombre),(empleado_endpoint.apellidoPat+" "+ empleado_endpoint.apellidoMat)).subscribe(val=>{

          if(sucursales.length>0)/* Validamos si la sucursal está en Glpi */
          {
            if(val.length>0)/* Validamos si el empleado está en Glpi */
            {
              /* CREAMOS UN BODY QUE VA A SER LA PETICION EN EL POST */
              let body   : string=""; 
              /* CONSTRUIMOS EL CUERPO DE LA PETICIÓN  */
              body = "{\"input\": {\"states_id\": \" " + 1 + +" \", \"contact\": \" " + (empleado_endpoint.puesto.nombrePuesto+"") +" \", \"users_id\": \" "+ val[0].id + " \", \"locations_id\": \" "    + sucursales[0].id + "\"}}";
              
              /* REALIZAMOS LA PETICIÓN */
              this.equipoService.actualizarItem(tabla + "",equipo_id,body).subscribe(data=>{})

              /**GENERAMOS EL DETALLE DE ASIGNACION PARA ESTE EQUIPO */
              this.crearDetalle(equipo)

              /**ACTUALIZAMOS EL REGISTRO GENERAL */
              this.actualizarResgistro()
            }
            else /* Si no encuentra al empleado retorna un mensaje de error */
            {
              this.mensajeDeError("El empleado seleccionado no se encuentra registrado en Glpi")
            }
          }
          else /* Si no encuentra la sucursal retorna un mensaje de error */
          {
            this.mensajeDeError("La sucursal a la que pertenece el empleado no se encuentra registrada en Glpi")
          }
        })
      })
    })
  }

  /* CREAMOS EL DETALLE DEL NUEVO EQUIPO ASIGNADO PARA AGREGARLE E ID DEL REGISTRO GENERAL */
  crearDetalle(equipo:Equipo)
  {
    if(equipo.comment!="" || equipo.comment!=undefined || equipo.comment!=null)
    {
      this.input_observaciones.setValue(equipo.comment)
    }
    else
    {
      this.input_observaciones.setValue("SIN COMENTARIOS")
    }
  
    /**GENERAMOS EL DETALLE */
    let detalleRegistro:DetalleRegistro     = new DetalleRegistro()
    detalleRegistro.equipo_id               = equipo.id
    detalleRegistro.observ_equipo           = this.input_observaciones.value
    detalleRegistro.status_detalle          = "ACTIVO"
    detalleRegistro.nombre_equipo           = equipo.name
    detalleRegistro.tipo_activo             = equipo.tipo

    /**GUARDAMOS EL DETALLE GENERADO CON EL ID DEL REGISTRO GENERAL CAPTURADO EN EL INICIO */
    this.detalleRegistroService.save(detalleRegistro, this.id_registro_general)
  }

  /* ACTUALIZAMOS EL REGISTRO GENERAL */
  actualizarResgistro()
  {
    this.registroAsignacionEquipoService.registroporId(this.id_registro_general).subscribe(registro=>{
      registro.status_registro = "ACTUALIZACION"
      this.registroAsignacionEquipoService.actualizacionRegistro(registro).subscribe(data=>
        {
          Swal.fire({
            title:'Asignación realizada',
            showConfirmButton:false,
            allowEnterKey:false,
            allowEscapeKey:false,
            allowOutsideClick:false,
            icon:'success',
            timer:2000       
          }).then((resu)=>{
            this.router.navigate(['layout/asignacionesEquipoUsuario/' + this.id_empleado_asignac])
          })
        })
    })
  }

  /* ABRE UNA VENTANA CON INFORMACION DE LA VISTA */
  clickMasInformacionR(event:any)
  {
    Swal.fire({
      title:'Seleccionar equipo de remplazo',
      html:
      
      '<p align="justify">Puedes elegir cualquiera de las piezas que componen a una computadora de escritorio:  </p>'    +
      
      '<p align="justify"> <b>1. </b>Selecciona el tipo de componente (CPU, TECLADO, MOUSE, REGULADOR O MONITOR). </p>'  +

      '<p align="justify"><b>2. </b>Ahora solo selecciona el equipo y confirma que uieres asignar este componente.</p>',

      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      confirmButtonColor: '#d33',
      confirmButtonText:'Cerrar'
    })
  }

  mensajeDeError(texto:string)
  {
    Swal.fire({
      title:'Error',
      text:texto,
      icon:'error',
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      showConfirmButton:false,
      timer:3000
    })
  }
}
