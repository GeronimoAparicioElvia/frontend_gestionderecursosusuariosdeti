import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RemplazoEquipoComponent } from './remplazo-equipo.component';

describe('RemplazoEquipoComponent', () => {
  let component: RemplazoEquipoComponent;
  let fixture: ComponentFixture<RemplazoEquipoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RemplazoEquipoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RemplazoEquipoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
