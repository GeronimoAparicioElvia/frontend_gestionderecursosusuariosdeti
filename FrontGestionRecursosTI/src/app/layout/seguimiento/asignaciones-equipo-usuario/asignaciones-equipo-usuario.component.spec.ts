import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignacionesEquipoUsuarioComponent } from './asignaciones-equipo-usuario.component';

describe('AsignacionesEquipoUsuarioComponent', () => {
  let component: AsignacionesEquipoUsuarioComponent;
  let fixture: ComponentFixture<AsignacionesEquipoUsuarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AsignacionesEquipoUsuarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignacionesEquipoUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
