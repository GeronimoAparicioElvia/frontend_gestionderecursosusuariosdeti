import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { RegistroAsignacionEquipo } from 'src/app/administracion/modelos/Asignacion_Equipo/registro-asignacion-equipo';
import { Empleado } from 'src/app/administracion/modelos/empleado';
import { Equipo } from 'src/app/administracion/modelos/Inventario/Equipo/equipo';
import { Sucursal } from 'src/app/administracion/modelos/sucursal';
import { SucursalService, UsuarioService } from 'src/app/administracion/servicios';
import { RegistroAsignacionEquipoService } from 'src/app/administracion/servicios/Asignacion_Equipo/registro-asignacion-equipo.service';
import { EquipoService } from 'src/app/administracion/servicios/Inventario/Equipo/equipo.service';
import { UsuarioGlpiService } from 'src/app/administracion/servicios/Inventario/Usuario/usuario-glpi.service';
import Swal from 'sweetalert2';
import * as pdfMake from "pdfmake/build/pdfmake";
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { DetalleRegistro } from 'src/app/administracion/modelos/Asignacion_Equipo/detalle-registro';
import { DetalleRegistroService } from 'src/app/administracion/servicios/Asignacion_Equipo/detalle-registro.service';
import { takeUntil } from 'rxjs/operators';
import { QueryValueType } from '@angular/compiler/src/core';

@Component({
  selector: 'app-asignaciones-equipo-usuario',
  templateUrl: './asignaciones-equipo-usuario.component.html',
  styleUrls: ['./asignaciones-equipo-usuario.component.scss']
})
export class AsignacionesEquipoUsuarioComponent implements OnInit {

  /*  PARÁMETROS */
  id_empleado!:number

  /*  DATOS DEL EMPLEADO */
  empleado!: Empleado

  /* ELEMENTOS DE VISTA */
  nombre: string = ""
  apellidos: string = ""
  selectTodos: boolean = false

  /* LISTAS A UTILIZAR EN LA RESPONSIVA GENERAL */
  listaColaboradores            :  Empleado [] = [];
  listaFiltradaColaboradores    !:  Observable<Empleado[]>;
  
  lista_sucursales        : Sucursal      []  =  []
  listaFiltradaSucursales : ReplaySubject<Sucursal[]> = new ReplaySubject<Sucursal[]>(1);

  /* ENCABEADOS DE LAS TABLAS */
  encabezadosP:string [] = ["OPCIONES","RAZON","FECHA ASIGNACIÓN","CUENTA CON SOLICITUD"]; /* PENDIENTES */
  encabezadosT:string [] = ["OPCIONES","STATUS DE LA ASIGNACIÓN","FECHA ASIGNACIÓN","FECHA MODIFICACIÓN","DESCRIPCIÓN DE LA ASIGNACIÓN"]; /* REGISTROS */
  encabezadosE:string [] = ["OPCIONES","NOMBRE","TIPO","MODELO","MARCA","NÚM. SERIE","LOCACION"];  /* EQUIPOS ASIGNADOS */

  /* DATA DE LAS TABLAS */
  dataSourceRP          = new MatTableDataSource<RegistroAsignacionEquipo>([]);    /* PENDIENTES            */
  dataSourceTR          = new MatTableDataSource<RegistroAsignacionEquipo>([]);    /* REGISTROS             */
  dataSourceTE          = new MatTableDataSource<Equipo>([]);                      /* EQUIPOS ASIGNADOS     */
  dataSourceES          = new MatTableDataSource<Equipo>([]);                      /* EQUIPOS POR SUCURSAL  */
  
  /* PAGINATORS */
  @ViewChild('paginatorRP', { static: true }) public paginatorTP !: MatPaginator;   /* PENDIENTES           */
  @ViewChild('paginatorTR', { static: true }) public paginatorTR !: MatPaginator;   /* REGISTROS            */
  @ViewChild('paginatorTE', { static: true }) public paginatorTE !: MatPaginator;   /* EQUIPOS ASIGNADOS    */
  @ViewChild('paginatorES', { static: true }) public paginatorES !: MatPaginator;   /* EQUIPOS POR SUCURSAL */

  listaTablas       : string    [] = ["Computer","Monitor","Networkequipment","Peripheral","Printer","Rack","Phone"]

  /* FORMULARIO DE LA RESPONSIVA GENERAL */
  input_responsiva_general_sucursal: FormControl = new FormControl() /* SUCURSAL EN DONDE SE VA A GENERAR LA SUCURSAL  */
  input__empleado__entrega__equipos: FormControl = new FormControl() /* EL EMPLEADO QUE EXTIENDE LA RESPONSIVA GENERAL */

  /* RESPONSIVA GENERAL */
  sucursales!: Sucursal[];
  empleado_entrega_equipos:Empleado= new Empleado()
  input_sucursal_nombre:FormControl =  new FormControl()
  select_sucursal:FormControl =  new FormControl()
  protected _onDestroy = new Subject<void>();
  
  constructor(private registroAsignacionEquipoService : RegistroAsignacionEquipoService , 
              private usuarioService                  : UsuarioService                  , 
              private usuarioGlpiService              : UsuarioGlpiService              , 
              private activatedRoute                  : ActivatedRoute                  ,
              private equipoService                   : EquipoService                   ,
              private router                          : Router                          ,
              private sucursalService                 : SucursalService                 ,
              private detalleRegistroService          : DetalleRegistroService          ,
              )
              {}

  ngOnInit()  
  {
    this.parametros()
    this.llenarDataVista1()
    this.llenarDataVista2()
    this.accionFiltrado()

    this.llenarListaSucursales()
    this.input_sucursal_nombre.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSucursal();
      });
  }

  /*LLENA LA LISTA DE LAS SUCURSALES */
  llenarListaSucursales()
  {
    this.sucursalService.getSucursales().subscribe(val=>{
      this.lista_sucursales=val;
    })
  }

  /*LLENA LA LISTA DE LOS COLABORADORES */
  llenarListColaboradores()
  {
    this.usuarioService.getAllEmpleados().subscribe(lista=>{
      lista.forEach(empleado=>{
        empleado.nomb_compl = empleado.nombre + " " + empleado.apellidoPat + " " + empleado.apellidoMat
      })
      this.listaColaboradores = lista
    })
  }

  /* CARGA LA DATA DE LA PRIMER PESTAÑA */
  llenarDataVista1()
  {
    /* CARGA LA DATA DE LOS PENDIENTES QUE TIENE EL USUARIO */
    let lista_Aux_P:RegistroAsignacionEquipo[] = []

    /*
       LLAMA A TODOS LOS REGISTROS QUE SEE ENCUENTREN EN CULAUQIERA DE LOS ESTADOS DE PENDIENTE: 
       
       1.REGISTRADO (QUE ESTA ASIGNADO PERO SIN RESPONSIVA)
       2.ACTUALIZACIÓN (SE NECESITA SUBIR LA RESPONSIVA ACTUALIADA)
       3.CANCELACIÓN (SE NECESITA SUBIR LA RESPONSIVA CANCELADA)
    */
    this.registroAsignacionEquipoService.todosLosRegistrosDeUnColaborador(this.id_empleado).subscribe(lista_registros=>{
      lista_registros.forEach(registro=>{
        if( registro.status_registro == "ACTUALIZACION" || registro.status_registro == "CANCELACION" || registro.status_registro == "REGISTRADO" )
        {
          if( registro.status_registro == "REGISTRADO"){registro.razon = "ASIGNACIÓN DE EQUIPO SIN REPSONSIVA"}
          else if( registro.status_registro == "ACTUALIZACION"){registro.razon = "ACTUALIZACIÓN DE RESPONSIVA"}
          else{registro.razon = "CANCELACIÓN DE ASIGNACIÓN"}
          if( registro.solicitud != null || registro.solicitud != undefined ){registro.cuenta_solicitud = "SÍ"}else{registro.cuenta_solicitud = "NO"}

          registro.fecha_asignacion_cadena = (registro.fecha_asignacion + "").substring(0,10)

          lista_Aux_P.push(registro)
          this.dataSourceRP.data = lista_Aux_P
          this.dataSourceRP.paginator = this.paginatorTP
        }
      })
    })
  }

  /*CARGA LA DATA DE LA SEGUNDA PESTAÑA */
  llenarDataVista2()
  {
    /*
       CARGA LA DATA DE LA SEGUNDA TABLA 
       LLAMA A TODOS LOS REGISTROS QUE HA EN LA BASE DE DATOS CON LOS DATOS DEL COLBAORADOR
    */
    this.registroAsignacionEquipoService.todosLosRegistrosDeUnColaborador(this.id_empleado).subscribe(lista_registros=>{
      lista_registros.forEach(registro=>{

        /* SE DA FORMAATO A LAS FECHAS */
        registro.fecha_asignacion_cadena = (registro.fecha_asignacion + "").substring(0,10)
        registro.fecha_modificacion_cadena = (registro.fecha_modificacion + "").substring(0,10)

             if(registro.status_registro=="REGISTRADO"   ){registro.status_registro="SIN RESPONSIVA"}
        else if(registro.status_registro=="ACTUALIZACION"){registro.status_registro="REQUIERE ACTUALIZAR LA RESPONSIVA"}
        else if(registro.status_registro=="CANCELACION"  ){registro.status_registro="EN PROCESO DE CANCELACIÓN"}


      })
      
      this.dataSourceTR.data = lista_registros
      this.dataSourceTR.paginator = this.paginatorTR
    })
  }

  /*CARGA LA DATA DE LA TERCER PESTAÑA */
  llenarDataVista3()
  {
    /*
       SE CARGA LA DATA DE LA TERCER TABLA BUSCANDO TODOS LOS EQUIPOS ASIGNADOS DEL COLABORADOR
    */
    let listaData: Equipo[]=[]

    /* SE BUSCA AL COLABORADOR EN GLPI CON SU NOMBRE Y APELLIDOS */
    this.usuarioGlpiService.allUsuariosSearch( this.nombre, this.apellidos ).subscribe(usuario=>{
      this.listaTablas.forEach(tabla=>{

        /* SE LLAMAN TODOS LOS EQUIPOS DE GLPI Y SE FULTRAN  SOLO LOS ASIGNADOS AL COLABORADOR*/
        this.equipoService.todosLosEquipos(tabla).subscribe(listaEquipos=>{
          listaEquipos.forEach(equipo=>{
            if(equipo.users_id == usuario[0].name)
            {
              if(equipo.locations_id=="CAS"){equipo.locations_id="CORPORATIVO"}
                   if (tabla =="Computer")        {equipo.tipo = "COMPUTADORA"       ; equipo.tabla = tabla; equipo.modelo = equipo.computermodels_id         ; listaData.push(equipo)}
              else if (tabla =="Networkequipment"){equipo.tipo = "DISPOSITIVO DE RED"; equipo.tabla = tabla; equipo.modelo = equipo.networkequipmentmodels_id ; listaData.push(equipo)}
              else if (tabla =="Printer")         {equipo.tipo = "IMPRESORA"         ; equipo.tabla = tabla; equipo.modelo = equipo.printermodels_id          ; listaData.push(equipo)}
              else if (tabla =="Monitor")         {equipo.tipo = "MONITOR"           ; equipo.tabla = tabla; equipo.modelo = equipo.monitormodels_id          ; listaData.push(equipo)}
              else if (tabla =="Peripheral")      {equipo.tipo = "DISPOSITIVO"       ; equipo.tabla = tabla; equipo.modelo = equipo.peripheralmodels_id       ; listaData.push(equipo)}
              else if (tabla =="Rack")            {equipo.tipo = "RACK"              ; equipo.tabla = tabla; equipo.modelo = equipo.rackmodels_id             ; listaData.push(equipo)}
              else if (tabla =="Phone")           {equipo.tipo = "TELEFONO"          ; equipo.tabla = tabla; equipo.modelo = equipo.phonemodels_id            ; listaData.push(equipo)}

              this.dataSourceTE.data = listaData
              this.dataSourceTE.paginator = this.paginatorTE
            }
          })
        })
      })
    })
  }

  /*CAPTURAMOS LOS PARÁMETROS */
  parametros()
  {
    /* SE CARGAN LOS PARÁMETROS */
    this.activatedRoute.params.subscribe(params =>{
      this.id_empleado   = params['id_empleado']
    })

    this.usuarioService.getAnEmpleado(this.id_empleado).subscribe(empleado_consulta=>{
      this.empleado = empleado_consulta;
      this.empleado.nomb_compl = this.empleado.nombre + " " +  this.empleado.apellidoPat  + " " + this.empleado.apellidoMat 
      
      this.nombre    = empleado_consulta.nombre+""
      this.apellidos = empleado_consulta.apellidoPat  + " " + empleado_consulta.apellidoMat 

      this.llenarDataVista3()

    })


    this.listaColaboradores.splice(0,this.listaColaboradores.length)
    this.usuarioService.getAllEmpleados().subscribe(lista=>{
      lista.forEach(empleado=>{
        empleado.nomb_compl = empleado.nombre + " " + empleado.apellidoPat + " " + empleado.apellidoMat
      })
      this.listaColaboradores = lista
    })
  }

  /* CANCELA LA ASINACION */
  clickCancelarAsignacionEquipo(id_equipo:number, tabla:string, tipo:string, nombre_equipo:string)
  {
    /* SE PREGUNTA SI SE QUIERE REMOVER EL EQUIPO  AL EMPLEADO */
    Swal.fire({
      title:'¿Quieres remover este equipo a '+ this.nombre + ' ' + this.apellidos +'?',
      icon:'question',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      showCancelButton:true,
      confirmButtonText:'Sí',
      cancelButtonText:'No'
    }).then( (result)=>{
      
      if(result.isConfirmed)
      {
        this.equipoService.buscarSucursal("CORPORATIVO").subscribe(sucursales=>{

          /* ACTUALIZA EL ESTADO DEL EQUIPO EN GLPI */
          let body = "{\"input\": {\"users_id\": \" "+ 2 + " \", \"states_id\": \" " + 2 + " \", \"locations_id\": \" " + sucursales[0].id +"\"}}";
          this.equipoService.actualizarItem(tabla, id_equipo,body).subscribe(data=>{})

            this.buscarDetalle(nombre_equipo)

          /* SE AVISA CON UNA VENTANA QUE SE CANCELÓ LA ASIGNACIÓN */
          Swal.fire({
            title:'Cancelación realizada',
            icon:'success',
            allowEnterKey:false,
            allowEscapeKey:false,
            allowOutsideClick:false,
            showConfirmButton:false,
            timer:2000
          }).then((result)=>{
            this.llenarDataVista1()
            this.llenarDataVista2()
            this.llenarDataVista3()
          })

        },
        err=>
        {
          this.mensajeDeError("Sin conexión con el servidor, intentelo más tarde")
        })        
      }
    })
  }

  /* REDIRECCIONA A OTRA VISTA EN DONDE MOSTRAR A DETALLE UNA REGISTRO DE ASIGNACIÓN */
  clickDetallAsignacion(id_administrador:number, id_colaborador:number, id_registro:number)
  {
    this.router.navigate(['layout/detalleAsignacionEquipoUsuario/' + id_administrador + '/' + id_colaborador + '/' + id_registro])
  }

  /* BRINDA INFORMACIÓN SOBRE LO QUE SE PUEDE REALIAR EN LA PRIMER PESTAÑA */
  clickMasInformacionV1(event:any)  
  {
    Swal.fire({
      title:'Pendientes de responsiva',
      html:
      
      '<p align="justify"> <b>ASIGNACIÓN DE EQUIPO SIN REPSONSIVA:</b> Son los proyectores y '+
      'computadoras asignadas recientemente que aún no cuentan con una repsonsiva en el sistema</p>'      +
  
      '<p align="justify"> <b>ACTUALIZACIÓN DE RESPONSIVA:</b> Cuando a una computadora de escritorio '+
      'se le cancela la asignación de un componente y se asigna otra en su lugar, se requiere subir una'+
      ' responsiva actualizada en donde se mencione todos los componente que conforman este equipo actualmente.</p>'                                +
      
      '<p align="justify"> <b>CANCELACIÓN DE ASIGNACIÓN:</b> Para las asignaciónes canceladas y que'+
      ' tengan una responsiva registrada en el sistema, es necesario subir la responsiva cancelada.</p>'                                               +

      '<p align="justify"> Al dar click en el botón derecho se cada fila se puede acceder al detalle de'  +
      ' la asignación.</p>',
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      confirmButtonColor: '#d33',
      confirmButtonText:'Cerrar'
    })
  }

  /* BRINDA INFORMACIÓN SOBRE LO QUE SE PUEDE REALIAR EN LA SEGUNDA PESTAÑA */
  clickMasInformacionV2(event:any)  
  {
    Swal.fire({
      title:'Todos los registros de asignación de un empleado',
      html:
      '<p align="justify">En la siguiente tabla se muestran todos los registros de asignación que tiene' +
      ' un empleado sin importar su estado actual.</p>'+

      '<p align="justify"> Al dar click en el botón derecho se cada fila se puede acceder al detalle de'  +
      ' la asignación.</p>',

      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      confirmButtonColor: '#d33',
      confirmButtonText:'Cerrar'
    })
  }

  /** BRINDA INFORMACIÓN SOBRE LO QUE SE PUEDE REALIAR EN LA TERCER PESTAÑA */
  clickMasInformacionV3(event:any)  
  {
    Swal.fire({
      title:'Todos los equipos asignados a un empleado',
      html:
      '<p align="justify">Se muestran todos los equipos que tiene asignado un empleado, se muestran' +
      ' datos generales como el nombre, la locación actual, modelo, tipo, además se puede cancelar la' +
      ' asignación con el botón rojo de cancelacion ubicado al inicio de cada fila de la tabla.</p>',
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      confirmButtonColor: '#d33',
      confirmButtonText:'Cerrar'
    })
  }

   /** BRINDA INFORMACIÓN SOBRE LO QUE SE PUEDE REALIAR EN LA CUARTA PESTAÑA */
   clickMasInformacionV4(event:any)  
   {
     Swal.fire({
       title:'Instrucciones',
       html:

       '<p align="justify"> - Ingresamos y seleccionamos el nombre de la sucursal de la que queremos generar la responsiva general </p>'+

       '<p align="justify"> - Ingresamos y seleccionamos el nombre del empleado quien a a entregar los equipos y la responsiva.</p>'+

       '<p align="justify"> - Al llenarse la tabla debemos seleccionar en los checkbox, los equipos que se van a incluir dentro de la responsiva.'+
       ' Si se prefiere, se cuenta con un check al inicio de los encabezados que selecciona todos los equipos</p>'+ 

       '<p align="justify"> - El botón azul tiene la función de descargar la responsiva.</p>' +

       '<p align="justify"> - El botón rosa tiene la función de subir y cargar la responsiva en el sistema.</p>'
       ,
       allowEnterKey:false,
       allowEscapeKey:false,
       allowOutsideClick:false,
       confirmButtonColor: '#d33',
       confirmButtonText:'Cerrar'
     })
   }
  
  /* SELECCIONA LA SUCURSAL  CARGA LOS EQUIPOS QUE ESTAN EXPUESTO EN ESA SUCURSAL */
  clickEquiposSucursal()
  {
    let listaData: Equipo [] = []
    let sucursal:string = this.input_responsiva_general_sucursal.value
    
    /* VALIDAMOS SI SELECCIONA EL COORPORATIVO PARA HACER EL CAMBIO AL VALOR DEFINIDO EN GLPI */
    if(sucursal == "CORPORATIVO"){ sucursal ="CAS"}

    this.listaTablas.forEach(tabla=>{

      this.equipoService.todosLosEquipos(tabla).subscribe(listaequipos=>{

        listaequipos.forEach(equipo=>{

          if(equipo.locations_id == sucursal)
          {
                 if (tabla =="Computer")        {equipo.tipo = this.buscarCoincidencia(equipo.computertypes_id+"")  ; equipo.tabla = tabla; equipo.modelo = equipo.computermodels_id        }
            else if (tabla =="Networkequipment"){equipo.tipo = equipo.networkequipmenttypes_id                      ; equipo.tabla = tabla; equipo.modelo = equipo.networkequipmentmodels_id}
            else if (tabla =="Printer")         {equipo.tipo = "IMPRESORA"                                         ; equipo.tabla = tabla; equipo.modelo = equipo.printermodels_id         }
            else if (tabla =="Monitor")         {equipo.tipo = "MONITOR"                                          ; equipo.tabla = tabla; equipo.modelo = equipo.monitormodels_id         }
            else if (tabla =="Peripheral")      {equipo.tipo = equipo.peripheraltypes_id                            ; equipo.tabla = tabla; equipo.modelo = equipo.peripheralmodels_id      }
            else if (tabla =="Rack")            {equipo.tipo = "RACK"                                               ; equipo.tabla = tabla; equipo.modelo = equipo.rackmodels_id            }
            else if (tabla =="Phone")           {equipo.tipo = "TELÉFONO"                                           ; equipo.tabla = tabla; equipo.modelo = equipo.phonemodels_id           }
            equipo.bandera = false
            listaData.push(equipo)
          }
          this.dataSourceES.data = listaData
          this.dataSourceES.paginator = this.paginatorES
        })
        
      })
    })

  }

  /*CREA UN PDF CON LOS DATOS DE LA RESPONSIVA GENERAL */
  async clickCrearResponsivaG(event:any)
  {
    var cuerpo   = [] /* Cuerpo y contenido de la responsiva general */

    let fila:string [] = [' ',' ',' ',' ',' ',' '] /* En cabeados para la tabla de la responsiva general */
    cuerpo.push(fila)

    this.dataSourceES.data.forEach(equipo=>{/* Recorre la lista de equqipos de la responsiva general */
      if(equipo.bandera) /* Contenido de un párrafao dentro del pdf a generar */
      {
        let fila:string [] = []
        fila.push(  equipo.tipo             + "")
        fila.push(  equipo.contact          + "")
        fila.push(  equipo.manufacturers_id + "")
        fila.push(  equipo.modelo           + "")
        fila.push(  equipo.serial           + "")
        fila.push(  equipo.name             + "")
        cuerpo.push(fila)
      }
    })

    /* fecha para la responsiva*/
    let fecha_aux:string=new Date().toISOString()
    let fecha:string = fecha_aux.substring(8,10) + " " + fecha_aux.substring(5,7) + " " + fecha_aux.substring(0,4)

    /* contenido del documento */
    const pdfDefinition: any = {
      /* se definen loss márgenes junto con el encabeado  membretado */
      pageMargins: [ 40 , 60 , 40 , 40 ],
      header:
      [
        {
          style: 'tableExample',
          table: {
            widths: ['auto', '*'],
            body: [
              [
                { /* Imagen membrete */
                  image: await this.getBase64ImageFromURL('/assets/img/sofipa.png'),
                  x:40,
                  width: 153,
                  height: 49,
                  border: [false, false, false, false],
                },
                { /* Titulo del documento */
                  text:'\nRESGUARDO GENERAL DE EQUIPO DE CÓMPUTO Y COMUNICACIONES\nPARA SOFIPA CORPORATION S.A.P.I. DE CV SOFOM E.N.R.',
                  style:'titulo',
                  border: [false, false, false, false],
                }
              ],
            ]
          }
        },
      ],
      /* Cuerpo y contenido del pdf */
      content:
      [
        /* DATOS DEL RESPONSABLE */
        {
          style: 'tableExample',
          table: {
            widths: ['auto', '*','20%'],
            body: [
              [
                {
                  text:'RESPONSABLE DEL EQUIPO:',
                  style:'label_texto_titulo',
                  border: [false, false, false, false],
                },
                {
                  text: this.empleado.nomb_compl,
                  style:'label_texto_info',
                  border: [false, false, false, true],
                },
                {
                  text:'',
                  style:'label_texto_info',
                  border: [false, false, false, false],
                }
              ],
            ]
          }
        
        },
        {
          style: 'tableExample',
          table: {
            widths: ['15%', '45%','10%','10%','20%'],
            body: [
              [
                {
                  text:'PUESTO:',
                  style:'label_texto_titulo',
                  border: [false, false, false, false],
                },
                {
                  text: this.empleado.puesto.nombrePuesto,
                  style:'label_texto_info',
                  border: [false, false, false, true],
                },
                {
                  text:'',
                  style:'label_texto_info',
                  border: [false, false, false, false],
                },
                {
                  text:'FECHA:',
                  style:'label_texto_titulo',
                  border: [false, false, false, false],
                },
                {
                  text: fecha,
                  style:'label_texto_info',
                  border: [false, false, false, true],
                },
              ],
            ]
          }
        },
        {
          style: 'tableExample',
          table: {
            widths: ['14.5%', '43.5%','*'],
            body: [
              [
                {
                  text:'SUCURSAL:',
                  style:'label_texto_titulo',
                  border: [false, false, false, false],
                },
                {
                  text: this.input_responsiva_general_sucursal.value,
                  style:'label_texto_info',
                  border: [false, false, false, true],
                },
                {
                  text:'',
                  style:'label_texto_info',
                  border: [false, false, false, false],
                },
              ],
            ]
          }
        },

        /**TABLA DE EQUIPOS */
        {
          text:' '
        },
        
        {
          style:'tableExample',
          fontSize:7,
          table: 
          {
            headerRows: 1,
            widths: [ '16.5%', '16.5%', '16.5%', '16.5%', '16.5%', '16.5%'],
            body:   
            [
              [
                {
                  text:'TIPO',
                  bold:true,
                  alignment: 'center',
                  color:'#ffffff',
                  fillColor: '#000000',
                },
                {
                  text:'USUARIO',
                  bold:true,
                  alignment: 'center',
                  color:'#ffffff',
                  fillColor: '#000000',
                },
                {
                  text:'MARCA',
                  bold:true,
                  alignment: 'center',
                  color:'#ffffff',
                  fillColor: '#000000',
                },
                {
                  text:'MODELO',
                  bold:true,
                  alignment: 'center',
                  color:'#ffffff',
                  fillColor: '#000000',
                },
                {
                  text:'SERIE',
                  bold:true,
                  alignment: 'center',
                  color:'#ffffff',
                  fillColor: '#000000',
                },
                {
                  text:'CÓDIGO DE INVENTARIO',
                  bold:true,
                  alignment: 'center',
                  color:'#ffffff',
                  fillColor: '#000000',
                },
              ],
            ],
          }
        },
        {
          style:'tableExample',
          fontSize:7,
          table: 
          {
            headerRows: 1,
            widths: [ '16.5%', '16.5%', '16.5%', '16.5%', '16.5%', '16.5%'],
            body:   cuerpo,
          },
          layout: 
          {
            fillColor: function (rowIndex:any) {
              return (rowIndex === 0) ? '#000000' : null ;
            },
          }
        },
        
        /**TÉRMINOS Y CONDICIONES */
        {
          style: 'tableExample',
          table: {
            widths: ['99.1%'],
            body: [
              [
                {
                  text:'Recibí de SOFIPA CORPORATION S.A.P.I DE C.V. SOFOM E.N.R. el equipo, accesorios, material, etc. Detallado, mismo que me es '+
                       'asignado para laborar en la empresa, el mismo deberá ser devuelto  en el momento que sea solicitado, en caso de reparación'+
                       ', pérdida parcial o total por descuido o negligencia, autorízo que sea descontado de mi sueldo el importe actualizado '+
                       'correspondiente al daño ocacionado.',
                  fontSize:8,
                  alignment: 'justify',
                },
              ],
            ]
          }
        },
        ,
        
        /**FIRMAS */
        {
          text:' '
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*','*','*'],
            body: [
              [
                {
                  text:'ENTREGADO POR',
                  style:'label_texto_titulo_centro',
                  border: [false, false, false, false],
                },

                {
                  text:' ',
                  border: [false, false, false, false],
                },

                {
                  text:'RECIBIDO POR',
                  style:'label_texto_titulo_centro',
                  border: [false, false, false, false],
                }
              ],

              [
                {
                  text:' ',
                  border: [false, false, false, false],
                },

                {
                  text:' ',
                  border: [false, false, false, false],
                },

                {
                  text:' ',
                  border: [false, false, false, false],
                }
              ],


              [
                {
                  text: this.empleado_entrega_equipos.nomb_compl,
                  style:'label_texto_titulo_centro',
                  border: [false, true, false, false],
                },

                {
                  text:' ',
                  border: [false, false, false, false],
                },

                {
                  text: this.empleado.nomb_compl,
                  style:'label_texto_titulo_centro',
                  border: [false, true, false, false],
                }
              ]
            ]
          }
        }
      ],
      /* Estilos s formatos utilizados */
      styles:
      {
        titulo: 
        {
          fontSize:11,
          bold:true,
          alignment: 'center',
        },
        texto: 
        {
          fontSize:11.5,
          bold:false,
          alignment: 'justify',
        },
        label_texto_titulo: 
        {
          fontSize:8.5,
          bold:true,
          alignment: 'left',
        },
        label_texto_titulo_centro: 
        {
          fontSize:8.5,
          bold:true,
          alignment: 'center',
        },
        label_texto_info: 
        {
          fontSize:8.5,
          bold:false,
          alignment: 'left',
        },
        label_texto_info_center: 
        {
          fontSize:8.5,
          bold:false,
          alignment: 'center',
        },
        label_texto_info_center_underline: 
        {
          fontSize:8.5,
          bold:false,
          alignment: 'center',
          decoration:'underline'
        },
      }
      
    }
    pdfMake.createPdf(pdfDefinition).download("RESPONSIVA GENERAL " + this.input_responsiva_general_sucursal.value + " " + this.empleado.nomb_compl);
  }

  /*  MÉTODO PARA SEREALIZAR UNA IMAGEN Y COLOCARLA DENTRO DEL PDF */
  getBase64ImageFromURL(url:string) {
    return new Promise((resolve, reject) => {
      var img = new Image();
      img.setAttribute("crossOrigin", "anonymous");

      img.onload = () => {
        var canvas = document.createElement("canvas");
        canvas.width = img.width;
        canvas.height = img.height;

        var ctx = canvas.getContext("2d");
        ctx?.drawImage(img, 0, 0);

        var dataURL = canvas.toDataURL("image/png");

        resolve(dataURL);
      };

      img.onerror = error => {
        reject(error);
      };

      img.src = url;
    });
  }

  /* CARGA AL EMPLEADO EN LA DATA PARA GENERAR LA RESPONSISVA GENERAL */
  clickColaboradorSeleccionado(empleado:Empleado)
  {
    this.empleado_entrega_equipos = this.input__empleado__entrega__equipos.value
    this.input__empleado__entrega__equipos.setValue(this.empleado_entrega_equipos.nomb_compl)
    this.llenarListColaboradores()
    this.accionFiltrado()
  }

  /* SUBE LA RESPSONSIVA GENERAL */
  async clickSubirResponsivaGeneral(event:any)
  {
    const { value: file } = await Swal.fire({
      title: 'Selecciona el archivo',
      input: 'file',
      inputAttributes: {
        'accept': 'application/pdf',
      },
      
    })
  

    if (file) /* valida que se subió algo y que el input file no está vacío */
    {
      if(file.type=='application/pdf')/* Valida que el archivo subido sea un pdf */
      {
        if(file.size < 1048576)/* Valida que el peso del archivo no sea superior al permitido 1 mb */
        {
          this.crearRegistroGeneral(file)  /* método que crea un registro general */
          this.llenarDataVista2()          /* método que carga los registros en la vista 2 */
          this.input__empleado__entrega__equipos.setValue("")
          this.input_responsiva_general_sucursal.setValue("")

          this.input_sucursal_nombre.setValue("")
          this.input__empleado__entrega__equipos.setValue("")

          let data: Equipo[] = this.dataSourceES.data
          data.splice(0,data.length)
          this.dataSourceES.data = data
          this.selectTodos = false
        }
        else
        {
          Swal.fire({
            icon:'error',
            title:'Error',
            text:'El archivo es muy grande para subirse.',
            showConfirmButton:false,
            allowEnterKey:false,
            allowEscapeKey:false,
            allowOutsideClick:false,
            timer:3000
          })  
        }
      }
      else /* En caso que se esuba otro tipo de archivo, retorna un mensaje de error */
      {
        Swal.fire({
          icon:'error',
          title:'Error',
          text:'Solo se permite subir archivos pdf.',
          showConfirmButton:false,
          allowEnterKey:false,
          allowEscapeKey:false,
          allowOutsideClick:false,
          timer:3000
        })
      }
    }
  }

  /* HABILITA EL BOTÓN GENERAL */
  habilitarbtnResponsivaGeneral()
  {
    let totalEquipos:number = 0
    
    this.dataSourceES.data.forEach(equipo=>{
      if(equipo.bandera)
      {
        totalEquipos++
      }
    })

    if(totalEquipos > 0 && this.input__empleado__entrega__equipos.value != null)
    {
      return true
    }
    return false
  }

  /*/* HABILITA EL CHECK SELECCIONAR TODOS */
  habilitarCheckAll()
  {
    if(this.dataSourceES.data.length > 0 )
    {
      return true
    }
    return false
  }

  /* SELECCIONA TODOS LOS EQUIPOS DE LA SUCURSAL PARA GENERA LA RESPONSIVA GENERAL*/
  checkAll(valor:boolean)
  {
    /* HABILITA LA CASILLA EN CADA FILA EN CASO QUE HABILIT EL CHECK DE TODOS */
    if(valor== true)
    {
      this.dataSourceES.data.forEach(equipo=>{
      
        equipo.bandera = true
        
      })
    }
    /* DESHABILITA LA CASILLA EN CADA FILA EN CASO QUE DESHABILITE EL CHECK DE TODOS */
    else
    {
      this.dataSourceES.data.forEach(equipo=>{
      
        equipo.bandera = false
        
      })
    }
  }

  /* BUSCA QUE LA CADENA COICIDA CON ALGUNA DE LAS OPCCIONES PARA REGRESAR SI ES COMPUTADORA DE ESCRITORIO O NO */
  buscarCoincidencia(cadena:string)
  {
    if( cadena.includes("Desktop") ||  cadena.includes("DESKTOP") )
    {
      return "COMPUTADORA DE ESCRITORIO"
    }
    else
    {
      return "LAPTOP"
    }
  }

  /* CREA UNA ASIGNACION PARA SUBIR LA RESPONSIVA GENERAL */
  async crearRegistroGeneral(archivo:File)
  {
    let registroAsignacionEquipo: RegistroAsignacionEquipo = new RegistroAsignacionEquipo();
    registroAsignacionEquipo.id_registro_asignacion        = 0
    registroAsignacionEquipo.status_registro               = "ACTIVO"
    registroAsignacionEquipo.colaborador_id                = this.empleado.idEmpleado
    registroAsignacionEquipo.fecha_asignacion              = new Date()
    registroAsignacionEquipo.fecha_modificacion            = new Date()
    
    /* SE ESPECIFICA LA RAON DE ESTE REGISTRO */
    registroAsignacionEquipo.observaciones                 = "RESPONSIVA GENERAL"
    this.registroAsignacionEquipoService.asignacionD(registroAsignacionEquipo).subscribe(registro=>{
      let detalleRegistro:DetalleRegistro     = new DetalleRegistro()
      detalleRegistro.equipo_id               = 0
      detalleRegistro.observ_equipo           = "RESPONSIVA GENERAL"
      detalleRegistro.status_detalle          = "ACTIVO"
      detalleRegistro.nombre_equipo           = "RESPONSIVA GENERAL"
      detalleRegistro.tipo_activo             = "RESPONSIVA GENERAL"
      
      /* SALVA EL DETALLE DE REGISTRO GENERADO, RETORNA EL ID DEL DETALLE GENERADO */
      this.detalleRegistroService.save2(detalleRegistro,Number(registro.id_registro_asignacion)).subscribe(detalleRegistro=>{
      
        /* SE CARGA EL ARCHIVO  DE ELA RESPONSIVA  SE ENVIA AL DETALLE RECIEN CREADO, ACTUALIZANDO EL REGISTRO */
        this.detalleRegistroService.subirResponsiva( Number(detalleRegistro.id_detalle_asignacion) , archivo ).subscribe(data=>{
          
          /* SE MUESTRA UN MENSAJE DE CONFIRMACIÓN */
          Swal.fire
          ({
            title:'Responsiva subida',
            icon:'success',
            allowEnterKey:false,
            allowEscapeKey:false,
            allowOutsideClick:false,
            showConfirmButton:false,
            timer:2000
          }).then(r=>{
            this.llenarDataVista2()
          })
        })
      })
    })
  }

  /* FILTRA LA DATA  DE COLABORADORES */
  accionFiltrado()
  {
    this.listaFiltradaColaboradores = this.input__empleado__entrega__equipos.valueChanges.pipe(
      startWith(''),
      map(value => this.filtrado(value)),
    );
  }

  /* FILTRA LA DATA DE LOS EMPLEADOS EN LA CUARTA PESTAÑA*/
  private filtrado(value: string): Empleado[] {
    const filterValue = value.toLowerCase();
    return this.listaColaboradores.filter(option => option.nomb_compl.toLowerCase().includes(filterValue));
    
  }

  /* ESTE METOO NOS VA A SERVIR PARA BUSCAR SI AL EQUIPO QUE SE CANCELA  LA ASIGNACIÓN TIENE DETALLE O NO */
  buscarDetalle(nombre_equipo:string)
  {

    /* REALIZAMOS LA CONSULTA */
    this.detalleRegistroService.detallePorNombreEquipo(nombre_equipo).subscribe(detalle_consulta=>{

      /* VALIDAMOS SI EXISTE */
      if(detalle_consulta)
      {
        /* EN CASO DE EXISTIR CANCELAMOS SU DETALLE */
        detalle_consulta.status_detalle = "CANCELADO"
        this.detalleRegistroService.update(detalle_consulta, Number(detalle_consulta.registro.id_registro_asignacion)).subscribe(data=>{console.log(data)})

        /* AHORA VALIDAMOS SI ES UNICO DETALLE O COMPARTE CON OTROS EL REGISTRO GENERAL */
        this.registroAsignacionEquipoService.registroporId(Number(detalle_consulta.registro.id_registro_asignacion)).subscribe(registro_C=>{
                    
          this.detalleRegistroService.listaDetallePorIdRegistro(Number(registro_C.id_registro_asignacion)).subscribe(lista_c=>{

           
             /* EN CASO DE NO SER EL UN ÚNICO REGISTRO, CAMBIA EL REGISTRO GENERAL AL ESTADO DE ACTUALIZACION */
            if(lista_c.length > 1)
            {
              registro_C.status_registro="ACTUALIZACION"
              this.registroAsignacionEquipoService.actualizacionRegistro(registro_C).subscribe(data=>{})
            }
             /* EN CASO DE SER UN ÚNICO REGISTRO VALIDAMOS SI TIENE O NO RESPONSIVA QUE ACTUALIZAR */
            else
            {
              /* EN CASO DE TENER RESPONSIVA AVISAMOS QUE DEBE VOVLER A SUBIRLA */
              if(detalle_consulta.archivo_responsiva!=undefined)
              {
                registro_C.status_registro="CANCELACION"
                this.registroAsignacionEquipoService.actualizacionRegistro(registro_C).subscribe(data=>{})
              }
              /* EN CASO DE NO TENER RESPONSIVA CANCELAMOS EL REGISTRO */
              else
              {
                registro_C.status_registro="CANCELADO"
                this.registroAsignacionEquipoService.actualizacionRegistro(registro_C).subscribe(data=>{})
              }
            }

          })
          
        })


        /* EN CASO DE O CONTAR CON RESPONSIVA SIMPLEMENTE CANCELAMOS TODO EL REGISTRO */
          this.registroAsignacionEquipoService.registroporId(Number(detalle_consulta.registro.id_registro_asignacion)).subscribe(registro_C=>{
            registro_C.status_registro="CANCELADO"

          })
        
      }
    },
    err=>{
      
    })
  }

  /* Filtro del data de equipos */
  filtroDataDourceTE(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceTE.filter = filterValue.trim().toLowerCase();
  }

  /* Filtro del data de registros */
  filtroDataDourceTR(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceTR.filter = filterValue.trim().toLowerCase();
  }

  /* CARGA EL OBJETO EMPLEADO QUE S EUTILIA PARA LA ASIGNACION */
  clickCargarSucursal(event:any)
  { 
    this.input_responsiva_general_sucursal.setValue(this.select_sucursal.value.nombreSucursal)
    this.clickEquiposSucursal()
  }
    
  /*Filtro de las sucursales en el select search */
  protected filterSucursal() {
    if (!this.lista_sucursales) {
      return;
    }
    /* Recuperamos el valor del input */
    let search = this.input_sucursal_nombre.value;
    if (!search) {
      this.listaFiltradaSucursales.next(this.lista_sucursales.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    /* Retornamos sla lista de eempleados que cumplen con las coincidencias */
    this.listaFiltradaSucursales.next(
      this.lista_sucursales.filter(sucursal => sucursal.nombreSucursal.toLowerCase().indexOf(search) > -1)
    );
  }

  mensajeDeError(texto:string)
  {
    Swal.fire({
      title:'Error',
      text:texto,
      icon:'error',
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      showConfirmButton:false,
      timer:3000
    })
  }
}
