import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleAsignacionEquipoUsuarioComponent } from './detalle-asignacion-equipo-usuario.component';

describe('DetalleAsignacionEquipoUsuarioComponent', () => {
  let component: DetalleAsignacionEquipoUsuarioComponent;
  let fixture: ComponentFixture<DetalleAsignacionEquipoUsuarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetalleAsignacionEquipoUsuarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleAsignacionEquipoUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
