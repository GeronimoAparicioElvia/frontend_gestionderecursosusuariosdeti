import { Component, OnInit, ViewChild } from '@angular/core';
import { DetalleRegistro } from 'src/app/administracion/modelos/Asignacion_Equipo/detalle-registro';
import { MatTableDataSource } from '@angular/material/table';
import { Empleado } from 'src/app/administracion/modelos/empleado';
import { DetalleRegistroService } from 'src/app/administracion/servicios/Asignacion_Equipo/detalle-registro.service';
import { EquipoService } from 'src/app/administracion/servicios/Inventario/Equipo/equipo.service';
import { UsuarioService } from 'src/app/administracion/servicios';
import { ActivatedRoute, Router } from '@angular/router';
import { Equipo } from 'src/app/administracion/modelos/Inventario/Equipo/equipo';
import { map, startWith } from 'rxjs/operators';
import * as pdfMake from "pdfmake/build/pdfmake";
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { RegistroAsignacionEquipo } from 'src/app/administracion/modelos/Asignacion_Equipo/registro-asignacion-equipo';
import { RegistroAsignacionEquipoService } from 'src/app/administracion/servicios/Asignacion_Equipo/registro-asignacion-equipo.service';
import Swal from 'sweetalert2';
import { ItemComponenteService } from 'src/app/administracion/servicios/Inventario/Item_Componente/item-componente.service';
import { MatDialog } from '@angular/material/dialog';
import { VisualizarResponsivaComponent } from '../../Dialogs/Mantenimiento/visualizar-responsiva/visualizar-responsiva.component';
import { ItemComponente } from 'src/app/administracion/modelos/Inventario/Item_Componente/item-componente';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { timeStamp } from 'console';
import { Perfil } from 'src/app/administracion/modelos/perfil';
import { Sucursal } from 'src/app/administracion/modelos/sucursal';
(<any>pdfMake).vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-detalle-asignacion-equipo-usuario',
  templateUrl: './detalle-asignacion-equipo-usuario.component.html',
  styleUrls: ['./detalle-asignacion-equipo-usuario.component.scss']
})
export class DetalleAsignacionEquipoUsuarioComponent implements OnInit {
  
  empleado_asignado:Empleado = new Empleado();
  empleado_gerente :Empleado = new Empleado();
  fecha_aux:string = new Date().toISOString().substring(0,10)
  input_domicilio_colaborador= new FormControl()
  lista__Equipos:Equipo [] = []

  /* Parámetros */
  id_registro     :number=0
  id_colaborador  :number=0
  id_administrador:number=0

  /*  Registro  lista de detalles */
  registroAsignacionEquipo:RegistroAsignacionEquipo = new RegistroAsignacionEquipo()
  lista_detalle_registro: DetalleRegistro[]=[]
  ietmComponente: ItemComponente = new ItemComponente()

  
  /* Empleado */
  input_gerente:FormControl  = new FormControl()
  select_gerente:FormControl = new FormControl()
  listaColaboradores            :   Empleado [] = [];
  listaFiltradaColaboradores    :   ReplaySubject<Empleado[]> = new ReplaySubject<Empleado[]>(1);
  equipos_asignados:string =""
  protected _onDestroy = new Subject<void>();

  /* Vista */
  dataSource = new MatTableDataSource<DetalleRegistro>([])                                            /*  Contenido de la tabla     */
  encabezados:string [] = ["botones","nombre del equipo","tipo de equipo","estatus","observavciones"] /*  Encabezados de las tablas */
  @ViewChild('paginator', { static: true }) public paginator !: MatPaginator;                         /*  Paginator de la tabla     */
  
  constructor(private detalleRegistroService:DetalleRegistroService, 
              private equipoService:EquipoService,
              private usuarioServicee: UsuarioService,
              private activatedRoute:ActivatedRoute,
              private registroAsignacionEquipoService: RegistroAsignacionEquipoService,
              private router:Router,
              public dialog: MatDialog,
              private itemComponenteService: ItemComponenteService,
              private usuarioService   : UsuarioService ,
             ) {}

  ngOnInit()
  {
    this.parametros()
    this.llenarData()
    this.llenarListaColaboradores()
    
    /*Comenzamos con el filtro de nombres de empleados */
    this.listaFiltradaColaboradores.next(this.listaColaboradores.slice());
    this.input_gerente.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterEmpleado();
      });
  }
  
  /* RECIBIMOS LOS PARAMETROS ENVIADOS POR MEDIO DE LA URL Y LOS ALMACENAMOS EN VARIABLES DECLARADAS AL INCIO DE LA CLASE */
  parametros()
  {
    this.activatedRoute.params.subscribe(params =>{
      this.id_colaborador   = params['id_colaborador'  ]
      this.id_administrador = params['id_administrador']
      this.id_registro      = params['id_registro'     ]
    })
  }

  /* Llena la lista de empleados del select search */
  llenarListaColaboradores()
  {
    /* Se consulta en el endpoint toos los empleados */
    this.usuarioService.getAllEmpleados().subscribe(
      /* se recorre la lista para construir el nombre completo */
      lista_empleados=>
      {
        lista_empleados.forEach(empleado=>{
          empleado.nomb_compl = empleado.nombre + " " + empleado.apellidoPat + " " + empleado.apellidoMat
        })
        this.listaColaboradores =lista_empleados;
      },
      /* En caso que exista un error al mandar la peticióna, retorna un mensaje de error */
      err=>
      {
        this.mensajeDeError() /* Se llama al método que despliega un mensaje de error */
      }
    )
  }

  /* CARGA LA DATA DE LA VISTA: LOS DETALLES DE ASIGNACIÓN */
  llenarData()
  {
    /* Limpiamos el contenido de la tabla de equipos */
    this.dataSource.data.splice(0,this.dataSource.data.length)
    /* Limpiamos la lista de equipos */
    this.lista__Equipos.splice(0,this.lista__Equipos.length)
    /*Cargamos la data del registro general y los detalles de registro*/
    this.detalleRegistroService.listaDetallePorIdRegistro(this.id_registro).subscribe(lista_detalles=>{
      this.lista_detalle_registro = lista_detalles
      /*Se cargan los datos generales de la asignación y se da formato a las fechas */
      this.registroAsignacionEquipo = lista_detalles[0].registro
      if(this.registroAsignacionEquipo.status_registro=="REGISTRADO"    ){this.registroAsignacionEquipo.status_registro="EN PROCESO DE ASIGNACIÓN (SIN RESPONSIVA)"}
      if(this.registroAsignacionEquipo.status_registro=="ACTUALIZACION" ){this.registroAsignacionEquipo.status_registro="ACTUALIZACIÓN DE RESPONSIVA"}
      if(this.registroAsignacionEquipo.status_registro=="CANCELACION"   ){this.registroAsignacionEquipo.status_registro="EN PROCESO DE CANCELACIÓN"}
      this.registroAsignacionEquipo.fecha_asignacion_cadena   = (this.registroAsignacionEquipo.fecha_asignacion+""  ).substring(8,10) +"-" +(this.registroAsignacionEquipo.fecha_asignacion+""  ).substring(5, 7) + "-" + (this.registroAsignacionEquipo.fecha_asignacion+""  ).substring(0, 4)
      this.registroAsignacionEquipo.fecha_modificacion_cadena = (this.registroAsignacionEquipo.fecha_modificacion+"").substring(8,10) +"-" +(this.registroAsignacionEquipo.fecha_modificacion+"").substring(5, 7) + "-" + (this.registroAsignacionEquipo.fecha_modificacion+"").substring(0, 4)

      /* Se hace una consulta a Glpi para traer los equipos pertenecientes a la lista de detalles*/
      this.lista_detalle_registro.forEach(detalle=>{
      /* Validamos que tipo de activo es el que tiene el detalle para consultarlo en Glpi */      
             if(detalle.tipo_activo=="COMPUTADORA"       ){ this.equipoService.unEquipoExtends("Computer"        , Number(detalle.equipo_id)).subscribe(equ=>{detalle.equipo = equ; equ.modelo = equ.computermodels_id         ;  equ.tipo = detalle.tipo_activo; detalle.equipo.tabla="Computer"         ; this.equipos_asignados = this.equipos_asignados + equ.computertypes_id         + ", MARCA " + equ.manufacturers_id + ", MODELO " + equ.computermodels_id         + ", SERIE " + equ.serial + ". "  ; this.lista__Equipos.push(equ)})}
        else if(detalle.tipo_activo=="DISPOSITIVO DE RED"){ this.equipoService.unEquipoExtends("Networkequipment", Number(detalle.equipo_id)).subscribe(equ=>{detalle.equipo = equ; equ.modelo = equ.networkequipmentmodels_id ;  equ.tipo = detalle.tipo_activo; detalle.equipo.tabla="Networkequipment" ; this.equipos_asignados = this.equipos_asignados + equ.networkequipmenttypes_id + ", MARCA " + equ.manufacturers_id + ", MODELO " + equ.networkequipmentmodels_id + ", SERIE " + equ.serial + ". "  ; this.lista__Equipos.push(equ)})}
        else if(detalle.tipo_activo=="IMPRESORA"         ){ this.equipoService.unEquipoExtends("Printer"         , Number(detalle.equipo_id)).subscribe(equ=>{detalle.equipo = equ; equ.modelo = equ.printermodels_id          ;  equ.tipo = detalle.tipo_activo; detalle.equipo.tabla="Printer"          ; this.equipos_asignados = this.equipos_asignados + equ.printertypes_id          + ", MARCA " + equ.manufacturers_id + ", MODELO " + equ.printermodels_id          + ", SERIE " + equ.serial + ". "  ; this.lista__Equipos.push(equ)})}
        else if(detalle.tipo_activo=="MONITOR"           ){ this.equipoService.unEquipoExtends("Monitor"         , Number(detalle.equipo_id)).subscribe(equ=>{detalle.equipo = equ; equ.modelo = equ.monitormodels_id          ;  equ.tipo = detalle.tipo_activo; detalle.equipo.tabla="Monitor"          ; this.equipos_asignados = this.equipos_asignados + equ.monitortypes_id          + ", MARCA " + equ.manufacturers_id + ", MODELO " + equ.monitormodels_id          + ", SERIE " + equ.serial + ". "  ; this.lista__Equipos.push(equ)})}
        else if(detalle.tipo_activo=="DISPOSITIVO"       ){ this.equipoService.unEquipoExtends("Peripheral"      , Number(detalle.equipo_id)).subscribe(equ=>{detalle.equipo = equ; equ.modelo = equ.peripheralmodels_id       ;  equ.tipo = detalle.tipo_activo; detalle.equipo.tabla="Peripheral"       ; this.equipos_asignados = this.equipos_asignados + equ.peripheraltypes_id       + ", MARCA " + equ.manufacturers_id + ", MODELO " + equ.peripheralmodels_id       + ", SERIE " + equ.serial + ". "  ; this.lista__Equipos.push(equ)})}
        else if(detalle.tipo_activo=="RACK"              ){ this.equipoService.unEquipoExtends("Rack"            , Number(detalle.equipo_id)).subscribe(equ=>{detalle.equipo = equ; equ.modelo = equ.rackmodels_id             ;  equ.tipo = detalle.tipo_activo; detalle.equipo.tabla="Rack"             ; this.equipos_asignados = this.equipos_asignados + equ.racktypes_id             + ", MARCA " + equ.manufacturers_id + ", MODELO " + equ.rackmodels_id             + ", SERIE " + equ.serial + ". "  ; this.lista__Equipos.push(equ)})}
        else if(detalle.tipo_activo=="TELEFONO"          ){ this.equipoService.unEquipoExtends("Phone"           , Number(detalle.equipo_id)).subscribe(equ=>{detalle.equipo = equ; equ.modelo = equ.phonemodels_id            ;  equ.tipo = detalle.tipo_activo; detalle.equipo.tabla="Phone"            ; this.equipos_asignados = this.equipos_asignados + equ.phonetypes_id            + ", MARCA " + equ.manufacturers_id + ", MODELO " + equ.phonemodels_id            + ", SERIE " + equ.serial + ". "  ; this.lista__Equipos.push(equ)})}
        
        /*valida que tenga responsiva en su espacio */
        if(detalle.archivo_responsiva){detalle.responsiva=true}

        /*valida que tenga responsiva en su espacio */
        if(detalle.status_detalle=="CANCELADO"){detalle.bandera=true}

        /*
              VALIDAMOS SI ENCUENTRA ALGUNA COMPUTADORA EN LA DATA SOURCE (YA SEA UNA LAPTOP O CPU)
              EN  CASO  QUE  ECNUNTRE  UNA COMPUTADORA, MANDARÁ UNA CONSULTA A GLPI PARA EXTRAER EL 
              SISTEMA OPERATIVO ASÍ COMO LA ARQUITECTURA DEL MISMO, TODO ESTO SERÁ PARA AÑADIRLO EN
              LA REPSONSIVA DE EQUIPO
        */
          if(detalle.tipo_activo=="COMPUTADORA" && detalle.status_detalle=="ACTIVO")
          {
            /* 
                MANDA HACER LA CONSULTA A GLPI PARA LLAMAR TODOS LOS SISTEMAS OPERATIVOS QUE ESTAN INSTALADOS EN LAS 
                DIFERENTES COMPUTADORAS  
            */
            this.itemComponenteService.allSistemasOperativos().subscribe(sistemas=>{

              /* DE LA CONSULTA RECIBIDA SE RECORRE CADA REGISTRO PARA BUSCAR EL SISTEMA QUE TIENE INSTALADO  ESTE 
                EQUIPO, ESTO LO REALIZA COMPARANDO EL NOMBRE DEL EQUIPO EN EL QUE ESTA INSTALADO EL SISTEMA CON EL 
                NOMBRE DEL EQUIPO QUE SE ENCUENTRA EN NUESTRO DETALLE DE ASIGNACIÓN 
              */
              for(let i=0; i<sistemas.length; i++)
              {
                if(sistemas[i].items_id == detalle.nombre_equipo)
                {
                  /* CUANDO ENCUENTRE EL SISTEMA LO SALVA EN UN OBJETO DECLARADO AL INICIO DE ESTA CLASE 
                    PARA SU POSTERIOR USO  Y TERMINA CON EL CICLO
                  */
                  this.ietmComponente = sistemas[i]
                  break
                }
              }
            })
          }
      })

      /*Carga la data de la tabla de detalles */
      this.dataSource.data = this.lista_detalle_registro
      this.dataSource.paginator = this.paginator

      /*Se cargan los datos del empleado */
      this.usuarioService.getAnEmpleado(Number(this.registroAsignacionEquipo.colaborador_id)).subscribe
      (
        data=>{ this.empleado_asignado = data}
      )
    })
  }

  /* ABRE UNA VENTANA CON INFORMACÍON ACERCA DE LA VISTA */
  clickInformacion(event:any)
  {
    Swal.fire({
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      title:'Instrucciones',
      html:
      '<p align="justify">El check añadir responsiva habilita un formulario para generar la responsiva de equipo correspondiente.</p>'+

      '<p align="justify">Para generar la responsiva de un equipo de cómputo se necesita que se llenen los campos del responsable que'+
      ' extiende la responsiva y el formulario donde se indican la dirección ip, mac y los programas instalados en el equipo '        +
      '(solo para las computadoras de escritorio y laptops).</p>'+

      '<p align="justify">Para generar la carata responsiva y anexo deberá rellenarse el campo nombre del responsable que'+
      ' extiende la responsiva y el formulario en donde indicamos el precio del equipo y el domicilio del colaborador.</p>'+

      '<p align="justify">Se puede guardar esta informaciòn haciendo click en el boton azul guardar, al igual que descargar '+
      'los documentos al habiitarse los botones que se encuentran a un lado del nombre del documento que genera.</p>'
      ,
      confirmButtonColor: '#d33',
      confirmButtonText:'Cerrar'
    })
  }

  /* RETORNA A LA VISTA EN DONDE SE NOS MUESTRA LAS ASIGANCIONES DEL COLABORADOR*/
  clickGoBack(event:any)
  {
    this.router.navigate(['layout/asignacionesEquipoUsuario/'+this.id_colaborador])
  }  

  /**MÉTODO PARA SEREALIZAR UNA IMAGEN Y COLOCARLA DENTRO DEL PDF */
  getBase64ImageFromURL(url:string) {
    return new Promise((resolve, reject) => {
      var img = new Image();
      img.setAttribute("crossOrigin", "anonymous");

      img.onload = () => {
        var canvas = document.createElement("canvas");
        canvas.width = img.width;
        canvas.height = img.height;

        var ctx = canvas.getContext("2d");
        ctx?.drawImage(img, 0, 0);

        var dataURL = canvas.toDataURL("image/png");

        resolve(dataURL);
      };

      img.onerror = error => {
        reject(error);
      };

      img.src = url;
    });
  }

  async clickSubirResponsiva(id_detalle:number)
  {
    /* Emerge un input para solicitar la resposniva a subir */
    const { value: file } = await Swal.fire({
      title: 'Selecciona el archivo',
      input: 'file',
      inputAttributes: {
        'accept': 'pdf/*',
      }
    })

    if (file) /* valida que se subió un archivo y que el input file no se encuenetre vacío */
    {
      if(file.type=='application/pdf')/* Valida que el archivo subido sea un pdf */
      {
        if(file.size < 1048576)/* Valida que el peso del archivo no sea superior al permitido 1 mb */
        {
          /* Guarda la responsisva subida parar el equipo en particular */
          this.detalleRegistroService.subirResponsiva( id_detalle , file ).subscribe(data=>{
            Swal.fire
            ({
              title:'Responsiva actualizada',
              icon:'success',
              allowEnterKey:false,
              allowEscapeKey:false,
              allowOutsideClick:false,
              showConfirmButton:false,
              timer:2000
            }).then(r=>{
              this.llenarData()
            })
          })  
        }
        /* En caso que el archivo sea más grande retorna un mensaje de advertencia */
        else 
        {
          Swal.fire({
            icon:'error',
            title:'Error',
            text:'El archivo es muy grande para subirse.',
            showConfirmButton:false,
            allowEnterKey:false,
            allowEscapeKey:false,
            allowOutsideClick:false,
            timer:3000
          })  
        }
      }
      /* En caso que se esuba otro tipo de archivo, retorna un mensaje de error */
      else 
      {
        Swal.fire({
          icon:'error',
          title:'Error',
          text:'Solo se permite subir archivos pdf.',
          showConfirmButton:false,
          allowEnterKey:false,
          allowEscapeKey:false,
          allowOutsideClick:false,
          timer:3000
        })
      }
    }
  }

  /* Carga la responsisva de forma general en  */
  async clickSubirResponsivaComputadora(event:any)
  {
    /* Emerge un input para solicitar la resposniva a subir */
    const { value: file } = await Swal.fire({
      title: 'Selecciona el archivo',
      input: 'file',
      inputAttributes: {
        'accept': 'pdf/*',
      }
    })

    if (file) /* valida que se subió un archivo y que el input file no se encuenetre vacío */
    {
      if(file.type=='application/pdf')/* Valida que el archivo subido sea un pdf */
      {
        if(file.size < 1048576)/* Valida que el peso del archivo no sea superior al permitido 1 mb */
        {
          /* Recorre todos los detalles de asignacion de este registro de asignación */
          this.dataSource.data.forEach(contenido=>{

            /* Valida que la responsiva se suba solo en los detalles activos */
            if(contenido.status_detalle=="ACTIVO")
            {
              /* Guarda la responsisva subida parar el equipo en particular */
              this.detalleRegistroService.subirResponsiva( Number(contenido.id_detalle_asignacion) , file ).subscribe(data=>{
                Swal.fire
                ({
                  title:'Responsiva actualizada',
                  icon:'success',
                  allowEnterKey:false,
                  allowEscapeKey:false,
                  allowOutsideClick:false,
                  showConfirmButton:false,
                  timer:2000
                }).then(r=>{
                  this.llenarData()
                })
              })
            }
          })
        }
        /* En caso que el archivo sea más grande retorna un mensaje de advertencia */
        else 
        {
          Swal.fire({
            icon:'error',
            title:'Error',
            text:'El archivo es muy grande para subirse.',
            showConfirmButton:false,
            allowEnterKey:false,
            allowEscapeKey:false,
            allowOutsideClick:false,
            timer:3000
          })  
        }
      }
      /* En caso que se esuba otro tipo de archivo, retorna un mensaje de error */
      else 
      {
        Swal.fire({
          icon:'error',
          title:'Error',
          text:'Solo se permite subir archivos pdf.',
          showConfirmButton:false,
          allowEnterKey:false,
          allowEscapeKey:false,
          allowOutsideClick:false,
          timer:3000
        })
      }
    }
  }

  /* ABRE UNA VENTANA PARA VISUALIZAR LA RESPONSIVA CORRESPONDIENTE */
  clickDescargarResponsiva(detalleRegistro:DetalleRegistro)
  {
    
    const base64Data = detalleRegistro.archivo_responsiva + "";
    const base64Response = `data:data:application/pdf;base64,${base64Data}`;
    const link = document.createElement("a");
    link.href = base64Response;
    link.download = `Responsiva de equipo.pdf`
    link.click();
  }

  /* Se cancela un detalle de reistro */
  clickCancelarDetalleDeRegistro(id_equipo:number, tabla:string, detalleRegistro:DetalleRegistro)
  {
    /**SE ABRE UNA VENTANA CON UN MENSAJE E CONFIRMACIÓN */
    Swal.fire({
      title:'¿Quieres cancelar la asignación de este equipo?',
      icon:'question',
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      showConfirmButton:true,
      showCancelButton:true,
      cancelButtonText:'NO',
      confirmButtonText:'SÍ',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33'
    }). then(result=>{
      /* Se valida que solo se realize lo siguiente si se confirmó la cancelación */
      if(result.isConfirmed)
      {
        /* Se cancela la asisgnación en Glpi */
        this.equipoService.buscarSucursal("CORPORATIVO").subscribe(sucursales=>{
          /* Se declara el body para realiar el put en Glpi*/
          let body:string= "{\"input\": {\"users_id\": \" "+ 2 + " \", \"states_id\": \" " + 2 + " \", \"locations_id\": \" " + sucursales[0].id + "\"}}";
          /* Se hace la petición request */
          this.equipoService.actualizarItem(tabla,id_equipo,body).subscribe(
            data=>
            {
              /* Despues de realizar la cancelación del equipo en Glpi se cancela el detalle de registro */
              detalleRegistro.status_detalle="CANCELADO"
              this.detalleRegistroService.update(detalleRegistro,Number(detalleRegistro.registro.id_registro_asignacion)).subscribe(
                detalle_registro_actualizado=>
                {
                  /* Ahora se valida si es un único equipo el asignado en este registro */
                  if(this.dataSource.data.length==1 && this.dataSource.data[0].archivo_responsiva== undefined)
                  {
                    detalleRegistro.registro.status_registro="CANCELADO"
                    this.registroAsignacionEquipoService.actualizacionRegistro(detalleRegistro.registro).subscribe(
                      registro_actualizado=>
                      {
                        this.mensajeDeExito() /* Se llama al método que despliega un mensaje de éxito */
                        this.llenarData() /* Cargamos la data del registro en la pantalla */
                      },
                      /* En caso que exista un error al mandar la peticióna, retorna un mensaje de error */
                      err=>
                      {
                        this.mensajeDeError() /* Se llama al método que despliega un mensaje de error */
                      }
                    )
                  }
                  /* Ahora se valida si es un único equipo el asignado en este registro */
                  else if(this.dataSource.data.length==1 && this.dataSource.data[0].archivo_responsiva!= undefined)
                  {
                    detalleRegistro.registro.status_registro="CANCELACION"
                    this.registroAsignacionEquipoService.actualizacionRegistro(detalleRegistro.registro).subscribe(
                      registro_actualizado=>
                      {
                        this.mensajeDeExito() /* Se llama al método que despliega un mensaje de éxito */
                        this.llenarData() /* Cargamos la data del registro en la pantalla */
                      },
                      /* En caso que exista un error al mandar la peticióna, retorna un mensaje de error */
                      err=>
                      {
                        this.mensajeDeError() /* Se llama al método que despliega un mensaje de error */
                      }
                    )
                  }
                  /* Si es parte de una computadora de escritorio solo retorna el mensaje de confirmación*/
                  else
                  {
                    detalleRegistro.registro.status_registro="ACTUALIZACION"
                    this.registroAsignacionEquipoService.actualizacionRegistro(detalleRegistro.registro).subscribe(
                      registro_actualizado=>
                      {
                        this.mensajeDeExito() /* Se llama al método que despliega un mensaje de éxito */
                        this.llenarData() /* Cargamos la data del registro en la pantalla */
                      },
                      /* En caso que exista un error al mandar la peticióna, retorna un mensaje de error */
                      err=>
                      {
                        this.mensajeDeError() /* Se llama al método que despliega un mensaje de error */
                      }
                    )
                  }
                },
                /* En caso que exista un error al mandar la peticióna, retorna un mensaje de error */
                err=>
                {
                  this.mensajeDeError() /* Se llama al método que despliega un mensaje de error */
                }
              )
            },
            /* En caso que exista un error al mandar la peticióna Glpi, retorna un mensaje de error */
            err=>
            {
              this.mensajeDeError() /* Se llama al método que despliega un mensaje de error */
            }
          )  
        })
      }
    })
  }

  /* REDIRIGE A OTRA VENTANA EN DONDE PODREMOS ESCOGER OTRO COMPONENTE EN CASO DE QUE UN COMPONENTE DEL EQUIPO DE ESCRITORIO SEA CANCELADO*/
  clickAddEquipo(event:any)
  {
    let id_empleado: number = Number(this.empleado_asignado.idEmpleado)
    let nombre_completo: string = this.empleado_asignado.nombre + " " + this.empleado_asignado.apellidoPat + " " + this.empleado_asignado.apellidoMat
    this.router.navigate(['layout/remplazoEquipo/'+this.id_registro+'/'+ id_empleado +'/'+nombre_completo])
  }

  /*  Cancela la asignación de una computadora de escritorio  */
  clickCancelarTodoElRegistro(event:any)
  {
    /* SE ABRE UNA VENTANA CON UN MENSAJE DE CONFIRMACIÓN */
    Swal.fire({
      title:'¿Quieres cancelar toda la asignación de este equipo?',
      icon:'question',
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      showConfirmButton:true,
      showCancelButton:true,
      cancelButtonText:'NO',
      confirmButtonText:'SÍ',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33'
    }).then(result=>{
      /* Se valida que solo se realize lo siguiente si se confirmó la cancelación */
      if(result.isConfirmed)
      {
        /* Se recorre todo el registro */
        this.dataSource.data.forEach(detalle=>{
          /* Se cancela la asisgnación en Glpi */
          this.equipoService.buscarSucursal("CORPORATIVO").subscribe(sucursales=>{
            /* Se declara el body para realiar el put en Glpi*/
            let body:string= "{\"input\": {\"users_id\": \" "+ 2 + " \", \"states_id\": \" " + 2 + " \", \"locations_id\": \" " + sucursales[0].id + "\"}}";
            /* Se hace la petición request */
            this.equipoService.actualizarItem((detalle.equipo.tabla+""),Number(detalle.equipo_id),body).subscribe(
              data=>
              {
                /* Despues de realizar la cancelación del equipo en Glpi se cancela el detalle de registro */
                detalle.status_detalle="CANCELADO"
                this.detalleRegistroService.update(detalle,Number(detalle.registro.id_registro_asignacion)).subscribe(data=>
                  {
                    /* Se valida que si el registro ya fue cancelado */
                    if(detalle.registro.status_registro!="CANCELADO")
                    {
                      /* Si no está cancelado, cancela el registro */
                      detalle.registro.status_registro="CANCELACION"
                      this.registroAsignacionEquipoService.actualizacionRegistro(detalle.registro).subscribe(
                        registro=>
                        {
                          this.llenarData()
                          this.mensajeDeExito() /* Se llama al método que despliega un mensaje de éxito */
                        },
                        /* En caso que exista un error al mandar la peticióna Glpi, retorna un mensaje de error */
                        err=>
                        {
                          this.mensajeDeError() /* Se llama al método que despliega un mensaje de error */
                        }
                      )
                    }
                    /* En caso de ya estar registrado se retorna un mensaje de confirmación */
                    else
                    {
                      this.mensajeDeExito() /* Se llama al método que despliega un mensaje de éxito */
                    }
                  }
                )
              },
              /* En caso que exista un error al mandar la peticióna Glpi, retorna un mensaje de error */
              err=>
              {
                this.mensajeDeError() /* Se llama al método que despliega un mensaje de error */
              }
            )
          })
        })
      }
    })
  }

  /* ABRE UNA VENTANA PARA VISUALIZAR LA RESPONSIVA CORRESPONDIENTE */
  clickVerResponsiva(detalleRegistro:DetalleRegistro)
  {
    const base64Data = detalleRegistro.archivo_responsiva + "";             /* guara el arhivo de la resposniva en uan constante */ 
    const base64Response = `data:data:application/pdf;base64,${base64Data}`;/* hace una conversion del blob a base 64 */ 

    const dialogRef = this.dialog.open(VisualizarResponsivaComponent,       /* Abre una ventana pasando todo en archivo en base 64 para visualiarlo */ 
    {
      width: "900px",
      position: {top: '7%'},
      data: { url: base64Response + "" },
    });
  }

   /*Filtro que busca coincidencias dentro del select search */
   protected filterEmpleado() {
    if (!this.listaColaboradores) {
      return;
    }
    /* Recuperamos el valor del input */
    let search = this.input_gerente.value;
    if (!search) {
      this.listaFiltradaColaboradores.next(this.listaColaboradores.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    /* Retornamos sla lista de eempleados que cumplen con las coincidencias */
    this.listaFiltradaColaboradores.next(
      this.listaColaboradores.filter(empleado => empleado.nomb_compl.toLowerCase().indexOf(search) > -1)
    );
  }

  /* AL DAR CLICK EL REGISTRO PASA DE UN STATUS DE EN CANCELACIÓN A CANCELADO */
  clickBtnTerminarCancelacion(event:any)
  {
    /**SE PREGUNTA AL USUARIO SI QUIERE DAR POR TERMINADO EL PROCESO DE CANCELACIÓN, ESTO SIGNIDICA SI YA SE SUBIÓ LA DOSUCMENTACION CORRESPONDIENTE  */
    Swal.fire
      ({
        title:'¿Marcar como terminado este pendiente de cancelación?',
        icon:'info',
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        showCancelButton:true,
        cancelButtonText:'NO',
        confirmButtonText:'SÍ',
        allowEnterKey:false,
        allowEscapeKey:false,
        allowOutsideClick:false,

      }).then(result=>{
        /* 
          EN CASO DE CONFIRMAR LA CANCELACIÓN SE PROCEDE A CAMBIAR EL STATUS A CANCELADO Y SE GUARDAN LOS CAMBIOS,
          ESTO HACE TAMBIEN QUE ESTE REGISTRO DEJE DE MOSTRARSE EN LOS PENDIENTES A ATENDER
          */
        if(result.isConfirmed)
        {
          this.registroAsignacionEquipo.status_registro = "CANCELADO"
          this.registroAsignacionEquipoService.actualizacionRegistro(this.registroAsignacionEquipo).subscribe(data=>{
            
            Swal.fire
            ({
              title:'Asignación cancelada',
              icon:'success',
              allowEnterKey:false,
              allowEscapeKey:false,
              allowOutsideClick:false,
              showConfirmButton:false,
              timer:2000
            }).then((r)=>{
                this.llenarData()
            })

          })
        }
      })
  }

  /* AL DAR CLICK EL REGISTRO PASA DE UN STATUS DE EN ACTUALIZACION/REGISTRADO A ACTIVO */
  clickBtnTerminarAsignacion(event:any)
  {
    Swal.fire
      ({
        title:'¿Marcar pendiente como terminado?',
        icon:'info',
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        showCancelButton:true,
        cancelButtonText:'NO',
        confirmButtonText:'SÍ',
        allowEnterKey:false,
        allowEscapeKey:false,
        allowOutsideClick:false,
      }).then(result=>{
        if(result.isConfirmed)
        {
          this.registroAsignacionEquipo.status_registro = "ACTIVO"
          this.registroAsignacionEquipoService.actualizacionRegistro(this.registroAsignacionEquipo).subscribe(data=>{

            Swal.fire
            ({
              title:'Asignación actualizada',
              icon:'success',
              allowEnterKey:false,
              allowEscapeKey:false,
              allowOutsideClick:false,
              showConfirmButton:false,
              timer:2000
            }).then((r)=>{

                this.llenarData()
            })

          })
        }
      })
  }

  /* Devuelve un mensaje emergente de error */
  mensajeDeError()
  {
    Swal.fire({
      title:'Error',
      text:'Sin conexión, vuelva a intentarlo más tarde',
      icon:'error',
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      showConfirmButton:false,
      timer:3000
    })
  }

  /* Devuelve un mensaje emergente de éxito */
  mensajeDeExito()
  {
    Swal.fire({
      title:'Asignación cancelada',
      icon:'success',
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      showConfirmButton:false,
      timer:3000
    })
  }

  habilitarBotonDescargarResponsiva()
  {
    if(this.empleado_asignado.apellidoPat != undefined && 
      this.empleado_asignado.apellidoPat  != ""        && 
      this.empleado_asignado.apellidoPat  != null)
    {
      return true
    }
    return false
  }

  habilitarBotonAnexoDescargarAndCartaResponsiva()
  {
    if(this.habilitarBotonDescargarResponsiva() && this.dataSource.data[0].direccion_colaborador!=undefined && this.dataSource.data[0].direccion_colaborador!=""
       && this.dataSource.data[0].precio!=undefined && this.dataSource.data[0].precio!=0)
       {
        return true
       }
       return false
  }

  habilitarBtnDescargarResponsiva()
  {
    /* Se valida que se escogio ya un empleado o gerente*/
    if(this.empleado_gerente.apellidoPat!=undefined){ return true}
    return false
  }

  habilitarBtnDescargarCartaYAnexo() /* Se valida que se escogio ya un empleado o gerente*/
  {
    if(this.empleado_gerente.apellidoPat!=undefined && this.dataSource.data[0].direccion_colaborador!=undefined && this.dataSource.data[0].precio!=undefined ){ return true}
    return false
  }

  habiilitarFormularioEquipoPortatil() /* Muestra el formulario para generar la carta  anexo */
  {
    if(this.dataSource.data.length==1){return true}
    return false
  }

  habiilitarFormularioComputadora() /* Muestra el formulario para generar la carta  anexo */
  {
    if(this.dataSource.data.length>1){return true}
    return false
  }

  /* HABILITA EL BOTON FINALIZAR ASIGNACIÓN */
  mostrarBtnTerminarAsignacion()
  {
    if(this.registroAsignacionEquipo.status_registro =="EN PROCESO DE ASIGNACIÓN (SIN RESPONSIVA)" || this.registroAsignacionEquipo.status_registro =="ACTUALIZACIÓN DE RESPONSIVA")
    {
     return true
    }
    return false
  }
  
  /* HABILITA EL BOTON FINALIZAR ASIGNACIÓN */
  mostrarBtnTerminarCancelacion()
  {
    if(this.registroAsignacionEquipo.status_registro =="EN PROCESO DE CANCELACIÓN")
    {
     return true
    }
    return false
  }

  /* Habilita paa vevr en pantala el botón rosa de la parte superior dela tabla*/
  mostrarBtnSubirResponsivaComputadoraYCancelarTodaLaAsignacion()
  {
    if(this.registroAsignacionEquipo.status_registro !="EN PROCESO DE CANCELACIÓN" && this.registroAsignacionEquipo.status_registro !="CANCELADO"  && this.dataSource.data.length > 1)
    {
     return true
    }
    return false
  }

  /* Habilita paa vevr en pantala el botón azul de la parte superior dela tabla*/
  mostrarBtnAgregarEquipo()
  {
    if(this.mostrarBtnSubirResponsivaComputadoraYCancelarTodaLaAsignacion())
    {
      let contador:number=0 /* Declaramos un contador para ver cuantos detalles estan activos */
      this.dataSource.data.forEach(detalle=> /* Recorremos todos los detalles de la asignacion */
        {
          if(detalle.status_detalle=="ACTIVO"){contador++}/* Por cada detalle activo se suma 1 al contador */
        }
      )
      if(contador>0 && contador < 5)/* Validamos que solo sean 4 los equipos asignados */
      {
        return true /* Devuele verdadera */
      }
    }
    return false /* Devuele falso */
  }
  
  /* Habilita paa vevr en pantala el botón rojo de la parte superior dela tabla*/
  mostrarBtnSubirCancelarTodaLaAsignacion()
  {
    if(this.registroAsignacionEquipo.status_registro !="EN PROCESO DE CANCELACIÓN")
    {
     return true
    }
    return false
  }

  /* AL DAR CLICK LA RESPONSIVA  CON BASE EN LOS DATOS QUE SE AGREGARON EN EL FORMULARIO DE LA RESPONSIVA */
  async clickCrearResponsivaG(event:any)
  {
    /*Construimos los nombres completos de los empleados */
    this.empleado_asignado.nomb_compl = this.empleado_asignado.nombre +  " " + this.empleado_asignado.apellidoPat + " " + this.empleado_asignado.apellidoMat
    this.empleado_gerente.nomb_compl = this.empleado_gerente.nombre +  " " + this.empleado_gerente.apellidoPat + " " + this.empleado_gerente.apellidoMat
    /* Cuerpo y contenido de la tabla de la responsiva */
    var cuerpo   = []
    this.lista__Equipos.forEach(equipo=>{
      let fila:string [] = []
      fila.push(  equipo.tipo             + "")
      fila.push(  equipo.manufacturers_id + "")
      fila.push(  equipo.modelo           + "")
      fila.push(  equipo.serial           + "")
      fila.push(  equipo.name             + "")
      fila.push(  equipo.comment          + "")
      cuerpo.push(fila)
    })
    /* Format de la tabla dentro del pdf */
    let fila:string [] = ['','','','','','']
    cuerpo.push(fila)
    let fecha:string = this.fecha_aux.substring(8,10) + "/" + this.fecha_aux.substring(5,7) + "/" + this.fecha_aux.substring(0,4)
    /* Contenido del pdf*/
    const pdfDefinition: any = {
      /* Margenes del documento */
      pageMargins: [ 40 , 60 , 40 , 40 ],
      header:
      [
        {
          style: 'tableExample',
          table: {
            widths: ['auto', '*'],
            body: [
              [/*Membrete superior del documento */
                {
                  image: await this.getBase64ImageFromURL('/assets/img/sofipa.png'),
                  x:40,
                  width: 153,
                  height: 49,
                  border: [false, false, false, false],
                },
                { /*Titulo del documento */
                  text:'\nRESPONSIVA DE EQUIPO DE CÓMPUTO\nSOFIPA CORPORATION S.A.P.I. DE CV SOFOM E.N.R.',
                  style:'titulo',
                  border: [false, false, false, false],
                }
              ],
            ]
          }
        },
      ],/* Pie de página */
      footer:
      [
        { /* Estilo del texto y formato del pie de página */
          fillColor:'#000000',
          color:'#ffffff',
          style: 'tableExample',
          table: {
            widths: ['*', '*', '*'],
            body: [
              [
                {
                  text:'www.sofipa.org.mx',
                  fontSize:12,
                  bold:true,
                  alignment: 'center',
                },
                {
                  text:'01 (951) 501 7100',
                  fontSize:12,
                  bold:true,
                  alignment: 'center',
                },
                {
                  text:'01 800 077 0946',
                  fontSize:12,
                  bold:true,
                  alignment: 'center',
                },
              ],
            ]
          }
        },
      ],
      /*Cuerpo del documento */
      content:
      [
        {
          style: 'tableExample',
          table: {
            widths: ['auto', '*','20%'],
            body: [
              [
                {
                  text:'RESPONSABLE DEL EQUIPO:',
                  style:'label_texto_titulo',
                  border: [false, false, false, false],
                },
                {
                  text: this.registroAsignacionEquipo.nombre_empleado_asignado,
                  style:'label_texto_info',
                  border: [false, false, false, true],
                },
                {
                  text:'',
                  style:'label_texto_info',
                  border: [false, false, false, false],
                }
              ],
            ]
          }
        },
        {
          style: 'tableExample',
          table: {
            widths: ['15%', '45%','10%','10%','20%'],
            body: [
              [
                {
                  text:'PUESTO:',
                  style:'label_texto_titulo',
                  border: [false, false, false, false],
                },
                {
                  text: this.empleado_asignado.puesto.nombrePuesto,
                  style:'label_texto_info',
                  border: [false, false, false, true],
                },
                {
                  text:'',
                  style:'label_texto_info',
                  border: [false, false, false, false],
                },
                {
                  text:'FECHA:',
                  style:'label_texto_titulo',
                  border: [false, false, false, false],
                },
                {
                  text: fecha,
                  style:'label_texto_info',
                  border: [false, false, false, true],
                },
              ],
            ]
          }
        },
        {
          style: 'tableExample',
          table: {
            widths: ['14.5%', '43.5%','*'],
            body: [
              [
                {
                  text:'SUCURSAL:',
                  style:'label_texto_titulo',
                  border: [false, false, false, false],
                },
                {
                  text: this.empleado_asignado.sucursal.nombreSucursal,
                  style:'label_texto_info',
                  border: [false, false, false, true],
                },
                {
                  text:'',
                  style:'label_texto_info',
                  border: [false, false, false, false],
                },
              ],
            ]
          }
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*'],
            body: [
              [
                {
                  text:' ',
                  border: [false, false, false, false],
                  fontSize:7
                },
              ],
              [
                {
                  text:'HARDWARE',
                  fontSize:8,
                  bold:true,
                  alignment: 'center',
                  color:'#ffffff',
                  fillColor: '#000000',
                }
              ],
            ]
          }
        },
        {
          
          style: 'tableExample',
          table: {
            border: [false, false, false, false],
            widths: ['*','*','*','*','*','*'],
            body: [
              [
                {
                  text:'TIPO',
                  fontSize:7,
                  bold:true,
                  alignment: 'center',
                  color:'#ffffff',
                  fillColor: '#000000',
                },
                {
                  text:'MARCA',
                  fontSize:7,
                  bold:true,
                  alignment: 'center',
                  color:'#ffffff',
                  fillColor: '#000000',
                },
                {
                  text:'MODELO',
                  fontSize:7,
                  bold:true,
                  alignment: 'center',
                  color:'#ffffff',
                  fillColor: '#000000',
                },
                {
                  text:'SERIE',
                  fontSize:7,
                  bold:true,
                  alignment: 'center',
                  color:'#ffffff',
                  fillColor: '#000000',
                },
                {
                  text:'NOMBRE',
                  fontSize:7,
                  bold:true,
                  alignment: 'center',
                  color:'#ffffff',
                  fillColor: '#000000',
                },
                {
                  text:'DESCRIPCIÓN',
                  fontSize:7,
                  bold:true,
                  alignment: 'center',
                  color:'#ffffff',
                  fillColor: '#000000',
                }
              ],
            ]
          }
        },
        {
          style:'tableExample',
          fontSize:7,
          table:
          
          {
            headerRows: 1,
            widths: [ '*', '*', '*', '*', '*', '*'],
            body:   cuerpo,
          },
          layout: 
            {
              fillColor: function (rowIndex:any) {
                return (rowIndex === cuerpo.length-1) ? '#000000' : null ;
              },
            }  
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*','*'],
            body: [
              [
                {
                  text:'RED',
                  fontSize:8,
                  bold:true,
                  alignment: 'center',
                  color:'#ffffff',
                  fillColor: '#000000',
                },
                {
                  text:'SISTEMA OPERATIVO',
                  fontSize:8,
                  bold:true,
                  alignment: 'center',
                  color:'#ffffff',
                  fillColor: '#000000',
                }
              ],
            ]
          }
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*','*','*','*'],
            body: [
              [
                {
                  text:'MAC ADDRES',
                  fontSize:8,
                  bold:true,
                },

                {
                  text:this.dataSource.data[0].direccion_mac,
                  fontSize:8,
                  bold:false,
                  alignment: 'center',
                },

                {
                  text:'VERSIÓN',
                  fontSize:8,
                  bold:true,
                },
              
                
                {
                  text: this.ietmComponente.operatingsystems_id,
                  fontSize:8,
                  bold:false,
                  alignment: 'center',
                }
              ],

              [
                {
                  text:'DIRECCIÓN IP',
                  fontSize:8,
                  bold:true,
                },

                {
                  text:this.dataSource.data[0].direccion_ip,
                  fontSize:8,
                  bold:false,
                  alignment: 'center',
                },

                {
                  text:'VERSIÓN (BITS)',
                  fontSize:8,
                  bold:true,
                },

                {
                  text: this.ietmComponente.operatingsystemarchitectures_id,
                  fontSize:8,
                  bold:false,
                  alignment: 'center',
                }
              ],
            ]
          }
        },
        {
          style: 'tableExample',
          table: {
            widths: ['33.5%','*'],
            body: [
              [
                {
                  text:'',
                  fontSize:7,
                  bold:true,
                  alignment: 'center',
                  color:'#ffffff',
                  fillColor: '#000000',
                },
                {
                  text:'SOFTWARE',
                  fontSize:7,
                  bold:true,
                  alignment: 'center',
                  color:'#ffffff',
                  fillColor: '#000000',
                }
              ],
              [
                {
                  text:'DESCRIPCIÓN',
                  fontSize:7,
                  bold:true,
                  alignment: 'center',
                  color:'#ffffff',
                  fillColor: '#000000',
                },
                {
                  text:'NÚMERO DE SERIE',
                  fontSize:7,
                  bold:true,
                  alignment: 'center',
                  color:'#ffffff',
                  fillColor: '#000000',
                }
              ],
            ]
          }
        },
        {
          border: [false, true, false, true],
          style: 'tableExample',
          table: {
            widths: ['10%','30%','*'],
            body: [
              [
                {
                  text:'OFFICE',
                  fontSize:7,
                  bold:true,
                },
                {
                  text: this.dataSource.data[0].version_office,
                  fontSize:7,
                },
                {
                  text: this.dataSource.data[0].licencia_office,
                  fontSize:7,
                }
              ]
            ]
          }
        },
        {
          style: 'tableExample',
          table: {
            widths: ['9.7%','*'],
            body: [
              [
                {
                  text:'OTROS',
                  fontSize:7,
                  bold:true,
                },
                {
                  text: this.dataSource.data[0].programas_instalados,
                  fontSize:7,
                },
              ]
            ]
          }
        },      
        {
          style: 'tableExample',
          table: {
            widths: ['10%','*','10%', '*'],
            body: [
              [
                {
                  text:' ',
                  border: [false, false, false, false],
                },
                {
                  text:' ',
                  border: [false, false, false, false],
                },
                {
                  text:' ',
                  border: [false, false, false, false],
                },
                {
                  text:' ',
                  border: [false, false, false, false],
                },
              ],
              [
                {
                  text:' ',
                  border: [false, false, false, false],
                },
                {
                  text:'ENTREGADO POR',
                  style:'label_texto_titulo_centro',
                  border: [false, false, false, false],
                },
                {
                  text:' ',
                  border: [false, false, false, false],
                },  
                {
                  text:'RECIBIDO POR',
                  style:'label_texto_titulo_centro',
                  border: [false, false, false, false],
                }
              ],
              [
                {
                  text:'NOMBRE:',
                  style:'label_texto_titulo_centro',
                  border: [false, false, false, false],
                },
                
                {
                  text:this.empleado_gerente.nomb_compl,
                  style:'label_texto_info_center',
                  border: [false, false, false, true],
                },

                {
                  text:'NOMBRE:',
                  style:'label_texto_titulo_centro',
                  border: [false, false, false, false],
                },
                
                {
                  text:this.registroAsignacionEquipo.nombre_empleado_asignado,
                  style:'label_texto_info_center',
                  border: [false, false, false, true],
                }
              ],
              [
                {
                  text:'FIRMA',
                  style:'label_texto_titulo_centro',
                  border: [false, false, false, false],
                },
                {
                  text: ' ',
                  border: [false, false, false, true],
                },
                {
                  text:'FIRMA',
                  style:'label_texto_titulo_centro',
                  border: [false, false, false, false],
                },
                {
                  text: ' ',
                  border: [false, false, false, true],
                },
              ],
            ]
          }
        },
        {
          style: 'tableExample',
          table: {
            widths: ['*'],
            body: [
              [
                {
                  text:'\n',
                  border: [false, false, false, false],
                }
              ],
              [
                {
                  text:'POLÍTICAS DE USO',
                  style:'label_texto_titulo_centro',
                  border: [true, true, true, false],
                }
              ],
              [
                {
                  text:
                  [
                    {
                      text:'\n-El equipo que se le entrega solamente debe ser utilizados para fines autorizados de negocio de SOFIPA CORPORATION S.A.P.I DE C.V. SOFM E.N.R.',
                      style:'label_texto_info'
                    },
                    {
                      text:'\n-Es único e intransferible, por lo que no se deberá permitir el uso a personas distintas al responsable, salvo que la operación de la empresa lo requiera y esté autorizado por su jefe inmediato',
                      style:'label_texto_info'
                    },
                    {
                      text:'\n-Todos los documentos creados, archivados o comunicados a través de equipo que se le entrega, son propiedad de SOFIPA CORPORATION S.A.P.I. DE C.V. SOFOM E.N.R.',
                      style:'label_texto_info'
                    },
                    {
                      text:'\n-Queda prohibido instalar juegos, juegos en línea o de azar y otros de naturaleza análoga, ajenos a lo requerido por el perfil de trabajo.',
                      style:'label_texto_info'
                    },
                    {
                      text:'\n-Queda estrictamente prohibido el uso del equipo que se le entrega para acceder a sitios públicos de entretenimiento (Youtube, Messenger, Hi5, Facebook, MSN, contenido pornográfico, etc.) y todo acceso con fines distintos al laboral.',
                      style:'label_texto_info'
                    },
                    {
                      text:'\n-Queda estrictamente prohibido la instalación de todo tipo de software, respetando el software previamente instalado.',
                      style:'label_texto_info'
                    },
                    {
                      text:'\n-No se permite guardar información personal como: Música, Fotos Personales, Documentos Personales, etc.',
                      style:'label_texto_info'
                    },
                    {
                      text:'\n-No se permite el ingreso a correo personal de cualquier dominio diferente al de ',
                      style:'label_texto_info'
                    },
                    {
                      text:'@sofipa.org.mx.',
                      style:'label_texto_info_negrita'
                    },
                    {
                      text:'\n-En su calidad de responsable del equipo que se le enctrega, deberá mantener las mayores medidas de seguridad respecto de su equipo.',
                      style:'label_texto_info'
                    },
                    {
                      text:'\n-EN caso de robo o extravío deberá el responsable aplicar la polìtica vigente.',
                      style:'label_texto_info'
                    },
                    {
                      text:'\n-Queda prohibido intercambiar un componente enlistado en esta responsiva y/o cambiar la ubicación física del equipo sin una ',
                      style:'label_texto_info'
                    },
                    {
                      text:'AUTORIZACIÓN',
                      style:'label_texto_info_negrita'
                    },
                    {
                      text:' de la ',
                      style:'label_texto_info'
                    },
                    {
                      text:'GERENCIA DE SISTEMAS.',
                      style:'label_texto_info_negrita'
                    },
                    {
                      text:'\n-El suscritor de esté documento reconoce que el equipo que se le entrega solo podrá ser utilizado para cumplir con las tareas que le encomiende la empresa en su calidad de patrón y que no podrá hacer uso del mismo para cuestión personal. Así mismo se compromete a emplear el equipo únicamente de acuerdo con las condiciones y especificaciones para que dichos efectos haga su conocimiento de la empresa, obligándose a no modificarlo ni en el hardware ni en el software, es decir no agregar ni suprimir ningún programa que se encuentren cargados originalmente sin el expreso conocimiento por escrito de la ',
                      style:'label_texto_info'
                    },
                    {
                      text:'GERENCIA DE SISTEMAS.',
                      style:'label_texto_info_negrita'
                    },
                    {
                      text:'\n\n-El que suscribe reconoce que los derechos sobre el equipo objeto de la presente corresponden exclusivamente a SOFIPA CORPORATION S.A.P.I. DE C.V. SOFOM E.N.R. mismo por lo que a la simple solicitud de la empresa se le obliga a devolver el equipo que se le entrega a la firma del presente y, en todo caso, al terminar su relación laboral con la empresa dejará de utilizar el mismo haciendo entrega de el al personal que se indique en el mismo estado que se haya recibido, salvo el deterioro debido al uso normal del equipo.',
                      style:'label_texto_info'
                    },
                    {
                      text:'\n\nSe anexa a la presente carta responsiva el pagaré por el mismo costo del equipo, el objetivo es proteger la devolución del equipo a la empresa SOFIPA una vez devuelto el equipo de raya el pagaré y usted es liberado de esta propiedad',
                      style:'label_texto_info'
                    },
                    
                  ],
                  border: [true, false, true, false],
                }
              ],
              [
                {
                  text:'\n',
                  border: [true, false, true, false],
                }
              ],
              [
                {
                  text:this.registroAsignacionEquipo.nombre_empleado_asignado,
                  style:'label_texto_info_center_underline',
                  border: [true, false, true, false],
                }
              ],
              [
                {
                  text:'NOMBRE Y FIRMA DE CONFORMIDAD',
                  style:'label_texto_info_center',
                  border: [true, false, true, false],
                }
              ],
              [
                {
                  text:'\n',
                  border: [true, false, true, true],
                }
              ]
            ]
          }
        },

        
      
      ],/* Estilos de texto formato del contenido del documento */
      styles:
      {
        titulo: 
        {
          fontSize:12,
          bold:true,
          alignment: 'center',
        },
        texto: 
        {
          fontSize:11.5,
          bold:false,
          alignment: 'justify',
        },
        label_texto_titulo: 
        {
          fontSize:8.5,
          bold:true,
          alignment: 'left',
        },
        label_texto_titulo_centro: 
        {
          fontSize:8.5,
          bold:true,
          alignment: 'center',
        },
        label_texto_info: 
        {
          fontSize:7,
          bold:false,
          alignment: 'left',
        },
        label_texto_info_negrita: 
        {
          fontSize:7,
          bold:true,
          alignment: 'left',
        },
        label_texto_info_center: 
        {
          fontSize:8.5,
          bold:false,
          alignment: 'center',
        },
        label_texto_info_center_underline: 
        {
          fontSize:8.5,
          bold:false,
          alignment: 'center',
          decoration:'underline'
        },
      }
      
    }
    pdfMake.createPdf(pdfDefinition).download("RESPONSIVA " + this.registroAsignacionEquipo.nombre_empleado_asignado);
  }

  /* AL DAR CLICK GENERA LA CARTA RESPONSIVA  CON BASE EN LOS DATOS QUE SE AGREGARON EN EL FORMULARIO DE LA CARTA RESPONSIVA */
  async clickCrearCartaResponsiva(event:any)
  {
  /* Elementos para generar la fecha */
  let meses: string[]=["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Noviembre","Noviembre","Diciembre"]
  let fecha:string = this.fecha_aux.substring(8,10) + " " + meses[Number(this.fecha_aux.substring(5,7))-1] + " " + this.fecha_aux.substring(0,4)
  /*Hace la conversion de cantidad numerica a escrita */   
  this.dataSource.data[0].precio_nombre == this.conversor(Number(this.dataSource.data[0].precio))
  /* Nombre del empleado que tiene asignado el equipo */
  this.empleado_asignado.nomb_compl = this.empleado_asignado.nombre +  " " + this.empleado_asignado.apellidoPat + " " + this.empleado_asignado.apellidoMat
  /* Contenido del pdf */
  this.validarEmpleado(this.empleado_asignado)
  this.validarEmpleado(this.empleado_gerente)
  const pdfDefinition: any = 
  {
    /* Se agregan los margenes para el contenido del pdf */
    pageMargins: [ 40, 90, 40, 40 ],
    header:
    [
      {
        table: {
          widths: ['auto', '*'],
          body: [
            [
              {
                /* Se agrega el membrete del pdf de la cabeza */
                image: await this.getBase64ImageFromURL('/assets/img/logoSofipa.png'),
                x:40,
                width: 153,
                height: 85, 
                border: [false, false, false, false],
              },
            ],
          ]
        }
      },
    ],
    /* Se agrega el membrete del pdf del pie */
    footer:
    [
      {/* Formato del pie de página  */
        fillColor:'#000000',
        color:'#ffffff',
        style: 'tableExample',
        table: {/*Contenido del pie de página */
          widths: ['*', '*', '*'],
          body: [
            [
              {
                text:'www.sofipa.org.mx',
                fontSize:12,
                bold:true,
                alignment: 'center',
              },
              {
                text:'01 (951) 501 7100',
                fontSize:12,
                bold:true,
                alignment: 'center',
              },
              {
                text:'01 800 077 0946',
                fontSize:12,
                bold:true,
                alignment: 'center',
              },
            ],
          ]
        }
      },
    ],
    content: 
    [
      {
        text:'CARTA RESPONSIVA ',
        style:'titulo'
      },
      /** PRIMER PÁRRAFO */
      {
        text: '\nEn la ciudad de Oaxaca de Juárez, Oaxaca, siendo las ' + (new Date() + "").substring(16,21) +' horas del día ' + fecha + 
              ' ' + 'reunidos en el Carlos Gracida No. 1 3er Piso Col. 24 de Febrero 6ª Sección San Antonio de la Cal, Oaxaca, C. '     +
              this.registroAsignacionEquipo.nombre_empleado_asignado + ' quien se identifica con el IFE  ' + this.empleado_asignado.folioIne +' y quie'+
              'n funge con el puesto de ' + this.empleado_asignado.puesto.nombrePuesto + ', con domicilio ' + this.dataSource.data[0].direccion_colaborador + 
              ', en lo sucesivo el “COLABORADOR” y por otra parte SOFIPA CORPORATION S.A.P.I DE C.V.  ' + 'SOFOM E.N.R. en lo sucesivo '+
              '“SOFIPA” con domicilio en  Carlos Gracida No. 1 3er Piso Col. 24 de Febrero 6ª Sección San Antonio de la Cal, Oaxaca en '+
              'este caso representado por el C. ' + this.empleado_gerente.nomb_compl +' quien acredita su personalidad con el númer'+
              'o de instrumento 2909 (Dos mil novecientos nueve) Volumen número 49 (cuarenta y nueve), para firmar la siguiente carta r'+
              'esponsiva al tenor de las siguientes Cláusulas ver anexo 1:',
        style: 'texto'
      },
      /** SEGUNDO PÁRRAFO */
      {
        text:
        [
          {
            text: '\n   PRIMERA.- ',
            style: 'negrita'
          },
          {
            text: '“SOFIPA” a través de su representante, hace entrega en este acto al colaborador el siguiente equipo de c'+
              'ómputo con la siguiente característica: ',
            style: 'texto'
          },
          {
            text: this.equipos_asignados,
            style: 'negrita_subrayado'
          },
          {
            text: ' Para uso único y exclusivo del trabajo que de sempeñará para ”SOFIPA” en el puesto de ',
            style: 'texto'
          },
          {
            text:  this.empleado_asignado.puesto.nombrePuesto,
            style: 'negrita'
          },
        ]
      },
      /** TERCER PÁRRAFO */
      {
        text:
        [
          {
            text: '\n   SEGUNDA.- ',
            style: 'negrita'
          },
          {
            text: 'El “COLABORADOR” después de haber recibido el “EQUIPO” con las características que se mencionan, la recibe '+
              'en perfectas condiciones para su uso; manifestando en este mismo acto hacerse responsable del uso y cuidados que ello '+
              'requiera, así mismo manifiesta que es de su conocimiento que el “EQUIPO” no es una prestación laboral, ni de su absolu'+
              'ta propiedad y que en caso de no seguir laborando para “SOFIPA” el “EQUIPO” deberá ser devuelto en las condiciones de '+
              'satisfacción de “SOFIPA”.',
            style: 'texto'
          }
        ]
      },
      /** CUARTO PÁRRAFO */
      {
        text:
        [
          {
            text: '\nTERCERA.- ',
            style: 'negrita'
          },
          {
            text: 'El “COLABOADOR” se compromete a no utilizar para uso personal el “EQUIPO” que se le está entregando en este'+
            ' acto, mismo que en caso de dar por terminada, rescindida o suspendida la relación laboral, el “EQUIPO” deberá ser entr'+
            'egado al representante legal de “SOFIPA” o quien este designe, con toda la información contenida en el mismo, la cual '+
            'no deberá ser copiada, borrada, modificada o eliminada aun cuando esta sea personal; siendo cualquier violación a esta'+
            ' cláusula un elemento para que se pueda proceder de forma penal, civil o laboral en contra del “COLABORADOR” por las r'+
            'esponsabilidades que esto conlleve.',
            style: 'texto'
          }
        ]
      },
      /** QUINTO PÁRRAFO */
      {
        text:
        [
          {
            text: '\nCUARTA.- ',
            style: 'negrita'
          },
          {
            text: 'Toda la información que maneje el equipo es exclusiva y propiedad de “SOFIPA” por cual deberá el “COLABORAOR”'+
              ' mantenerla de forma confidencial, así mismo, si el colaborador llegará a divulgar la información, hiciera mal uso del '+
              '“EQUIPO” por cual se llegara a dañar, sustrajera la información de cualquier índole, extraviara o le  robara el “EQUIPO'+
              '”, extraiga piezas, las cambien o le de mantenimiento en un lugar distinto al autorizado por “SOFIPA” sin consentimient'+
              'o del Gerente General de Sistemas, instale o desinstale programas sin autorización, guarde o mantenga información que a'+
              'tente contra la moral y las buenas costumbres o se llegara a descomponer  por causas imputables al “COLABORADOR”, tendr'+
              'á que responder por la reparación del daño ocasionado o en su caso la reposición total del “EQUIPO” con las característ'+
              'icas descritas en la CLÁUSULA PRIMERA, independientemente de la aplicación del artículo 47 de la ley federal de trabajo'+
              ' según sea el caso o de las leyes que pudieran ser aplicables en el ámbito de incumplimiento a este apartado por parte '+
              'del “COLABORADOR”.',
            style: 'texto'
          }
        ]
      },
      /**SEXTO PÁRRAFO */
      {
        text:
        [
          {
            text: '\nQUINTA.- ',
            style: 'negrita'
          },
          {
            text: 'Si el “EQUIPO” se llegara a descomponer y se demuestra que fue por causas no imputables al “COLABORADOR”, '+ 
            '“SOFIPA” se hará responsable de repararla, así mismo “SOFIPA” se compromete en darle el soporte técnico necesario '  +
            'para efectos de que esté funcionando en óptimas condiciones.',
            style: 'texto'
          }
        ]
      },
      /** SEPTIMO PÁRRAFO */
      {
        text:
        [
          { 
            text:'\nSEXTA.- ',
            style: 'negrita'
          },
          { 
            text:'Se anexa a la presente carta responsiva el siguiente pagare por el mismo costo del equipo, el objetivo es '  +
              'proteger la devolución del equipo a la empresa SOFIPA según lo estipulado en la presente responsiva, una vez devuelto'+
              'el equipo se raya el pagare y usted es liberado de esta responsabilidad.',
            style: 'texto'
          }
        ]
      },
      {
        text:" "
      },
      {
        text:" "
      },
      /**PAGARÉ */
      {
        style: 'tableExample',
        table: {
          widths: ['*'],
          body: 
          [
            [
              {
                border: [true, true, true,false],
                style: 'tableExample',
                table: {
                  widths: ['*','*','*'],
                  body: [
                    [
                      {
                        border: [false, false, false, false],
                        text:'PAGARE 1/1',
                        fontSize:11.5,
                        bold:true,
                        alignment: "left"
                      },
                      {
                        border: [false, false, false, false],
                        text:' ',
                        fontSize:8,
                      },
                      {
                        border: [false, false, false, false],
                        text:'BUENO POR: $' + this.dataSource.data[0].precio,
                        fontSize:11.5,
                        alignment:"rigth"
                      }
                    ],
                  ]
                }
              }
            ],
            [
              {
                border: [true, false, true,false],
                style: 'tableExample',
                table: {
                  widths: ['10%','10%','*'],
                  body: [
                    [
                      { 
                        border: [false, false, false, false],
                        text:' ',
                        fontSize:8,
                      },
                      {
                        border: [false, false, false, false],
                        text:' ',
                        fontSize:8,
                      },
                      {
                        border: [false, false, false, false],
                        text:'Oaxaca de Júarez, Oaxaca A ' + fecha,
                        fontSize:11.5,
                        alignment:"rigth"
                      }
                    ],
                  ]
                }
              }
            ],
            [
              {
                border: [true, false, true, false],
                text:
                [
                  {
                    border: [false, false, false, false],
                    text: "Debo y pagaré incondicionalmente a la orden de ",
                    fontSize:11.5
                  },
                  {
                    border: [false, false, false, false],
                    text: "SOFIPA CORPORATION S.A.P.I DE CV SOFOM E.N.R. ",
                    style:"negrita_subrayado_pagare"
                  },
                  {
                    border: [false, false, false, false],
                    text: "en Carlos Gracilda No.1 3er. Piso Col. 24 de Febrero 6a Sección San Antonio de la Cal, Oaxaca. La cantidad de",
                    fontSize:11.5
                  },
                  {
                    border: [false, false, false, false],
                    text: " $ " + this.dataSource.data[0].precio + " (" + this.conversor(Number(this.dataSource.data[0].precio)) + " pesos 00/100 M.N) ",
                    style:"subrayado_pagare"
                  },
                ]
              }
            ],
            [
              
              {
                border: [true, false, true, false], 
                text:  "valor recibido a  mi entera satisfaccion, en este causara interes a razon del 10% mensual desde la fecha de vencimiento hasta su total liquidación, pagadero conjuntamente con el principal",
                fontSize:11.5
              }
            ],
            [
              {
                text:' ',
                border: [true, false, true, false], 
              }
            ],

            [
              {
                text:
                [
                  {
                    text:'Nombre: C. ',
                    fontSize:11.5,
                    bold:true,
                    alignment: 'center',
                  },
                  {
                    text:this.registroAsignacionEquipo.nombre_empleado_asignado,
                    fontSize:11.5,
                    alignment: 'center',
                  }
                ],
                border: [true, false, true, false], 
              }
            ],
            [
              {
                text:
                [
                  {
                    text:'Domicilio: ',
                    fontSize:11.5,
                    bold:true,
                    alignment: 'center',
                  },
                  {
                    text:this.input_domicilio_colaborador.value,
                    fontSize:11.5,
                    alignment: 'center',
                  }
                ],
                border: [true, false, true, false], 
              }
            ],
            [
              {
                text:
                [
                  {
                    text:'INE: ',
                    fontSize:11.5,
                    bold:true,
                    alignment: 'center',
                  },
                  {
                    text:this.empleado_asignado.folioIne+"",
                    fontSize:11.5,
                    alignment: 'center',
                  }
                ],
                border: [true, false, true, true], 
              }
            ],

          ]
        }
      },
      {
        text:" "
      },
      {
        text:
        [
          {
            text: '\n   SEPTIMA.- ',
            style: 'negrita'
          },
          {
            text: 'El “COLABORADOR” recibe en su entera satisfacción el "EQUIPO" con las '+
                  'caracteristicas que menciona la cláusura primera y acepta la responsabilidad civil, penal y en '+
                  'su caso la rescisión del contrato laboral deacuerdo a la Ley Federal de Trabajo conforme el '+
                  'artículo 47 fracción V, VI, Y XI según se trate.',
            style: 'texto'
          }
        ]
      },
      {
        text:" "
      },
      {
        text:"Ambas partes manifiestan su absoluta conformidad con las cláusulas, firmando al calce para constancia los que intervienen.",
        style: 'texto'
      },
      
      {
        table: 
        {
          widths: ['*', '*', '*'],
          body: 
          [
            [
              {
                border: [false, false, false, false],
                table:
                {
                  widths: ['*'],
                  body: 
                  [
                    [{text:" ", style : 'label_texto_negrita_centro', border: [false, false, false, false]}],
                    [{text:" ", style : 'label_texto_negrita_centro', border: [false, false, false, false]}],
                    [{text:" ", style : 'label_texto_negrita_centro', border: [false, false, false, false]}],
                    [{text: "C. " + this.registroAsignacionEquipo.nombre_empleado_asignado, style : 'firma_negrita',border: [false, true, false, false],}],
                    [{text:this.empleado_gerente.puesto.nombrePuesto, style : 'firma_negrita', border: [false, false, false, false]}],
                  ]
                }
              },

              {text:' ',style:'titulo',border: [false, false, false, false],},

              {
                border: [false, false, false, false],
                table:
                {
                  widths: ['*'],
                  body: 
                  [
                    
                    [{text:" ", style : 'label_texto_negrita_centro', border: [false, false, false, false]}],
                    [{text:" ", style : 'label_texto_negrita_centro',border: [false, false, false, false],}],
                    [{text:" ", style : 'label_texto_negrita_centro',border: [false, false, false, false],}],
                    [{text:"SOFIPA CORPORATION S.A.P.I. DE C.V., SOFOM E.N.R. a travez de su representante.", style : 'firma_negrita', border: [false, true, false, false]}],
                    [{text:this.empleado_gerente.puesto.nombrePuesto, style : 'firma_negrita',border: [false, false, false, false],}],
                  ]
                }
              }
            ]
          ]
        } 
      },
      {
        table: 
        {
          widths: ['*', '*', '*'],
          body: 
          [
            [
              {
                border: [false, false, false, false],
                table:
                {
                  widths: ['*'],
                  body: 
                  [
                    [{text:" ", style : 'label_texto_negrita_centro', border: [false, false, false, false]}],
                    [{text:" ", style : 'label_texto_negrita_centro', border: [false, false, false, false]}],
                    [{text:" ", style : 'label_texto_negrita_centro', border: [false, false, false, false]}],
                    [{text: "TESTIGO", style : 'firma_negrita',border: [false, true, false, false],}],
                    
                  ]
                }
              },

              {text:' ',style:'titulo',border: [false, false, false, false],},

              {
                border: [false, false, false, false],
                table:
                {
                  widths: ['*'],
                  body: 
                  [
                    
                    [{text:" ", style : 'label_texto_negrita_centro', border: [false, false, false, false]}],
                    [{text:" ", style : 'label_texto_negrita_centro',border: [false, false, false, false],}],
                    [{text:" ", style : 'label_texto_negrita_centro',border: [false, false, false, false],}],
                    [{text:"TESTIGO", style : 'firma_negrita', border: [false, true, false, false]}],
                    
                  ]
                }
              }
            ]
          ]
        } 
      },
    ],
    styles:
    {
      titulo: 
      {
        fontSize:12,
        bold:true,
        alignment: 'center',
      },
      texto: 
      {
        fontSize:11.5,
        bold:false,
        alignment: 'justify',
      },
      negrita: 
      {
        fontSize:11.5,
        bold:true,
        alignment: 'justify',
      },
      negrita_subrayado: 
      {
        fontSize:11.5,
        bold:true,
        alignment: 'justify',
        decoration:'underline',
        italics: true
      },
      subrayado_pagare: 
      {
        fontSize:11.5,
        alignment: 'justify',
        decoration:'underline',
      },
      negrita_subrayado_pagare: 
      {
        fontSize:11.5,
        bold:true,
        alignment: 'justify',
        decoration:'underline',
      },
      firma_negrita: 
      {
        fontSize:11,
        bold:true,
        alignment: 'center',
      },
    }
  }
  pdfMake.createPdf(pdfDefinition).download("CARTA RESPONSIVA " + this.registroAsignacionEquipo.nombre_empleado_asignado);
  } 

  /* GENERA EL ANEXO DE LA CARTA RESPONSIVA  CON BASE EN LOS DATOS QUE SE AGREGARON EN EL FORMULARIO DE LA CARTA RESPONSIVA */
  async clickCrearAnexoCartaResponsiva(event:any)
  {
    this.validarEmpleado(this.empleado_asignado)
    this.validarEmpleado(this.empleado_gerente)
    /* Elementos para generar la fecha */
    let meses: string[]=["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Noviembre","Noviembre","Diciembre"]
    let fecha:string = this.fecha_aux.substring(8,10) + " " + meses[Number(this.fecha_aux.substring(5,7))-1] + " " + this.fecha_aux.substring(0,4)
    /* Define la razon de la carta responsiva */
    let tipo_activo_:string = ""
    if(this.dataSource.data[0].tipo_activo=="COMPUTADORA")
    {
      tipo_activo_="ENTREGA DE NOTEBOOK"
    }
    else if(this.dataSource.data[0].tipo_activo=="DISPOSITIVO")
    {
      tipo_activo_="ENTREGA DE " + this.dataSource.data[0].equipo.peripheraltypes_id
    }
    else if(this.dataSource.data[0].tipo_activo=="DISPOSITIVO DE RED")
    {
      tipo_activo_="ENTREGA DE " + this.dataSource.data[0].equipo.networkequipmenttypes_id
    }
    else
    {
      tipo_activo_="ENTREGA DE " + this.dataSource.data[0].tipo_activo
    }

    /* Se define el contenido del pdf */
    const pdfDefinition: any = {

      /* Se asignan margenes para el contenido de la información */
      pageMargins: [ 40 , 60 , 40 , 40 ],
      header:
      [
        {
          style: 'tableExample',
          table: {
            widths: ['auto', '*'],
            /* Se agrega membrete a la hoja */
            body: [
              [
                {
                  image: await this.getBase64ImageFromURL('/assets/img/sofipa.png'),
                  x:40,
                  width: 153,
                  height: 49,
                  border: [false, false, false, false],
                },
              ],
            ]
          }
        },
      ],
      /* Se agrega contenido en el pdf */
      content:
      [
        {text:' ',style:'titulo'},{text:' ',style:'titulo'},{text:' ',style:'titulo'},{text:' ',style:'titulo'},
        
        { /* Titulo del documento */
          text:'ANEXO A LA CARTA RESPONSIVA',
          style:'titulo'
        },

        {text:' ',style:'titulo'},{text:' ',style:'titulo'},{text:' ',style:'titulo'},{text:' ',style:'titulo'},

        {/*Lugar  fecha */
          text  : 'Oaxaca de Juarez, ' + fecha,
          style : 'texto_derecha'
        },
        { /* Destinatario del oficio */
          text  : 'C. ' + this.registroAsignacionEquipo.nombre_empleado_asignado + "\n " 
                  + this.empleado_asignado.puesto.nombrePuesto + 
                  "\nSOFIPA CORPORATION S.A.P.I DE C.V., SOFOM E.N.R \n "+
                  "P R E S E N T E",
          style : 'label_texto_negrita'
        },

        {/* Razon del documento */
          text  : 'ASUNTO: ' + tipo_activo_,
          style : 'texto_negrita_derecha'
        },

        {text:' ',style:'titulo'},{text:' ',style:'titulo'},

        {/* Contenido del documento */
          text: 
          [
            {
              text:'El que suscribe C. ' + this.empleado_gerente.nombre + " " + this.empleado_gerente.apellidoPat + " " +
                    this.empleado_gerente.apellidoMat + " " + this.empleado_gerente.puesto.nombrePuesto + " de SOFIPA, "+
                    "por medio de la presente le ",
              style:'texto'
            },
            
            {
              text:
              [  
                {
                  text  : 'HAGO ENTREGA DEL SIGUIENTE EQUIPO: ',
                  style : 'label_texto_negrita'
                },
                {
                  text: this.equipos_asignados + " ",
                  style : 'label_texto_negrita_subrayado'
                },
                {
                  text:'Por otra parte para dar uso correcto al activo entregado deberá observar lo siguiente:',
                  style : 'label_texto_negrita'
                },
              ]
            },
            
          ]
        },
      
        {text:' ',style:'titulo'},{text:' ',style:'titulo'},

        {
          text:'*  Hacer uso correcto de la información cuidando siempre la confidencialidad de la misma.',
          style:'texto'
        },
        {
          text:'*  No proporcionar la información de la empresa para uso personal, con terceras personas o divulgación de la misma ',
          style:'texto'
        },
        {
          text:'*  La información contenida en el equipo de computo es propiedad de la empresa.',
          style:'texto'
        },
        {
          text:'*  Si llegara a dañarse el equipo o averiarse a consecuencia del mal cuidado o uso será el colaborador el responsable'+
               ' de pagar losdaños que llegara a ocasionar.',
          style:'texto'
        },


        {text:' ',style:'titulo'},{text:' ',style:'titulo'},


        {
          table: 
          {
            widths: ['*', '*', '*'],
            body: 
            [
              [
                {
                  border: [false, false, false, false],
                  table:
                  {
                    widths: ['*'],
                    body: 
                    [
                      [{text:"Atentamente:", style : 'label_texto_negrita_centro', border: [false, false, false, false]}],
                      [{text:" ", style : 'label_texto_negrita_centro', border: [false, false, false, false]}],
                      [{text:" ", style : 'label_texto_negrita_centro', border: [false, false, false, false]}],
                      [{text:"C. "+this.empleado_gerente.nomb_compl, style : 'label_texto_negrita_centro', border: [false, true, false, false]}],
                      [{text:this.empleado_gerente.puesto.nombrePuesto, style : 'label_texto_negrita_centro', border: [false, false, false, false]}],
                    ]
                  }
                },

                {text:' ',style:'titulo',border: [false, false, false, false],},

                {
                  border: [false, false, false, false],
                  table:
                  {
                    widths: ['*'],
                    body: 
                    [
                      
                      [{text:"Recibí", style : 'label_texto_negrita_centro',border: [false, false, false, false],}],
                      [{text:" ", style : 'label_texto_negrita_centro',border: [false, false, false, false],}],
                      [{text:" ", style : 'label_texto_negrita_centro',border: [false, false, false, false],}],
                      [{text: "C. " + this.registroAsignacionEquipo.nombre_empleado_asignado, style : 'label_texto_negrita_centro',border: [false, true, false, false],}],
                      [{text:this.empleado_asignado.puesto.nombrePuesto, style : 'label_texto_negrita_centro',border: [false, false, false, false],}],
                    ]
                  }
                }
              ]
            ]
          } 
        }

      ],
      /* Estilos y formato del teto utilizado */
      styles:
      {
        titulo: 
        {
          fontSize:12,
          bold:true,
          alignment: 'center',
        },
        texto: 
        {
          fontSize:11.5,
          bold:false,
          alignment: 'justify',
        },
        texto_derecha: 
        {
          fontSize:11.5,
          bold:false,
          alignment: 'right',
        },
        texto_negrita_derecha: 
        {
          fontSize:11.5,
          bold:true,
          alignment: 'right',
        },
        label_texto_negrita: 
        {
          fontSize:11.5,
          bold:true,
        },
        label_texto_negrita_centro: 
        {
          fontSize:10,
          bold:true,
          alignment: 'center',
        },
        label_texto_negrita_subrayado: 
        {
          fontSize:11.5,
          bold:true,
          decoration:'underline'
        },
      }
      
    }
    pdfMake.createPdf(pdfDefinition).download("ANEXO DE CARTA RESPONSIVA " + this.registroAsignacionEquipo.nombre_empleado_asignado);    
  }

  /* AL DAR CLICK TODO LO INGRESADO EN LOS FORMULARIOS SE GUARDA DENTRO DEL DETALLE DE LA RESPONSIVA */
  clickActualizarDatosResponsiva( event:any )
  {
    /* PEDIMOS CONFIRMACIÓN */
    Swal.fire({
      icon:'question',
      text:'¿Quieres guardar la información agregada?',
      showCancelButton:true,
      showConfirmButton:true,
      cancelButtonText:'NO',
      confirmButtonText:'SÍ',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      allowEnterKey:false,
      allowEscapeKey: false,
      allowOutsideClick:false,
    }).then((result=>{

      /* EN CASO DE TENER CONFIRMACIÓN */
      if(result.isConfirmed)
      {
        /* ACTUALIZAMOS LOS CAMPOS PERTENECIENTES A ESTE DETALLE DE ASIGNACIÓN */
        this.detalleRegistroService.update(this.dataSource.data[0],Number(this.dataSource.data[0].registro.id_registro_asignacion)).subscribe(
          resultado=>{
            /* LANZAMOS UN MENSAJE DE REALIZADO Y VOLVEMOS A CARGAR LOS DATOS DEL DETALLE */
            Swal.fire({
              icon:'success',
              text:'Datos guardados',
              showCancelButton:false,
              showConfirmButton:false,
              allowEnterKey:false,
              allowEscapeKey: false,
              allowOutsideClick:false,
              timer:2000
            }).then(()=>{
      
              /* FINALEMETE CARGAMOS TODO DE NUEVO */
              this.llenarData()
            })
          },/* En caso de existir algun error de conexión se realiza lo siguiente */
          err=>
          {
            this.mensajeDeError() /* Retornamos un mensaje de errir */
          }
        )
      }
    }))
  }

  checkbandera:boolean=false/* Elementos del check */
  bandera:boolean=false     /* Elemento para habilitar todo el formulario */
  onCheckboxChange(event:any) /* Metodo que muestra u oculta el formularos*/
  {
    if( this.bandera == false)
    {
      this.bandera = true
    }
    else
    {
      this.bandera = false
    }
  }

  /* CARGA LOS DATOS DEL GERENTE QUE EXTIENDE LA RESPONSIVA */
  clickSelectResponsableExtiendeResponsiva(event:any)
  {
    let puesto:Perfil = new Perfil()
    puesto.nombrePuesto=""
    this.empleado_gerente = this.select_gerente.value/* Valida que el puesto no sea nulo o vacío */
    if(this.empleado_gerente.puesto==null){this.empleado_gerente.puesto=puesto} /* de ser vacío o nulo agrega un valor por defecto */
    this.llenarListaColaboradores()
  }

  /* RETORNA EL VALOR DE LA CANTIDAD INGESADA EN UNIDADES  */
  private unidad(unidad: number) 
  {
         if(unidad == 0){return ""}
    else if(unidad == 1){ return "UNO"}
    else if(unidad == 2){ return "DOS"}
    else if(unidad == 3){ return "TRES"}
    else if(unidad == 4){ return "CUATRO"}
    else if(unidad == 5){ return "CINCO"}
    else if(unidad == 6){ return "SESIS"}
    else if(unidad == 7){ return "SIETE"}
    else if(unidad == 8){ return "OCHO"}
    else if(unidad == 9){ return "NUEVE"}
    else{ return "" }
  }

  /* RETORNA EL VALOR DE LA CANTIDAD INGESADA EN DECENAS  */
  private decena(decena:number, unidad: number) 
  {
    /* VALIDA SI SON CANTIDADES CERRADAS */
         if( decena == 0){return ""}
    else if( decena == 1 && unidad == 0){ return "DIEZ "}
    else if( decena == 2 && unidad == 0){ return "VEITE "    }
    else if( decena == 3 && unidad == 0){ return "TREINTA "  }
    else if( decena == 4 && unidad == 0){ return "CUARENTA " }
    else if( decena == 5 && unidad == 0){ return "CINCUENTA "}
    else if( decena == 6 && unidad == 0){ return "SESENTA "  }
    else if( decena == 7 && unidad == 0){ return "SETENTA "  }
    else if( decena == 8 && unidad == 0){ return "OCHENTA "  }
    else if( decena == 9 && unidad == 0){ return "NOVENTA "  }
     
    /** VALIDAMOS SI SE ENCUENTRA DELL RANGO DE 11 A 19 */
    else if( decena == 1 && unidad == 1){ return "ONCE"      }
    else if( decena == 1 && unidad == 2){ return "DOCE"      }
    else if( decena == 1 && unidad == 3){ return "TRECE"     }
    else if( decena == 1 && unidad == 4){ return "CATORCE"   }
    else if( decena == 1 && unidad == 5){ return "QUINCE"    }
    else if( decena == 1 && unidad >= 6){ return "DIECI"     }
    
    /** VALIDAMOS QUE SE ENCUENTRE DE 20 A 90 */
    else if( decena == 2 && unidad >= 0){ return "VEINTI"      }
    else if( decena == 3 && unidad >= 0){ return "TREINTA y "  }
    else if( decena == 4 && unidad >= 0){ return "CUARENTA y " }
    else if( decena == 5 && unidad >= 0){ return "CINCUENTA y "}
    else if( decena == 6 && unidad >= 0){ return "SESENTA y "  }
    else if( decena == 7 && unidad >= 0){ return "SETENTA y "  }
    else if( decena == 8 && unidad >= 0){ return "OCHENTA y "  }
    else if( decena == 9 && unidad >= 0){ return "NOVENTA y "  }
    else{ return "" }
  }

  /* RETORNA EL VALOR DE LA CANTIDAD INGESADA EN CENTENAS  */
  private centena(centena:number, decena:number, unidad:number)
  {

    /** PRIMERO VALIDAMOS SI ES 100 CERRADO */
    if(centena == 1 && decena ==0 && unidad == 0){ return "CIEN"}

    /** VALIDAMOS SI ES MÁS DE 100 HASTA 900 */
    else if( centena == 1 && decena > 0 || 
             centena == 1 && unidad > 0 || 
             centena == 1 && decena > 0 && decena > 0  ){return "CIENTO "}
    else if(centena == 2){ return "DOSCIENTOS "   }
    else if(centena == 3){ return "TRESCIENTOS "  }
    else if(centena == 4){ return "CUATROCIENTOS "}
    else if(centena == 5){ return "QUINIENTOS "   }
    else if(centena == 6){ return "SEISCIENTOS "  }
    else if(centena == 7){ return "SETECIENTOS "  }
    else if(centena == 8){ return "OCHOCIENTOS "  }
    else if(centena == 9){ return "NOVECIENTOS "  }
    else if( decena > 0){ return this.decena(decena, unidad)} 
    else if( unidad > 0){ return this.unidad(unidad)} 
    else { return ""}
  }

  /* RETORNA EL VALOR DE LA CANTIDAD INGESADA EN UNIDADES DE MILLAR  */
  private unidad_de_millar(unidad_millar: number) 
  {
         if(unidad_millar == 1){ return "MIL "        }
    else if(unidad_millar == 2){ return "DOS MIL "    }
    else if(unidad_millar == 3){ return "TRES MIL "   }
    else if(unidad_millar == 4){ return "CUATRO MIL " }
    else if(unidad_millar == 5){ return "CINCO MIL "  }
    else if(unidad_millar == 6){ return "SESIS MIL "  }
    else if(unidad_millar == 7){ return "SIETE MIL "  }
    else if(unidad_millar == 8){ return "OCHO MIL "   }
    else if(unidad_millar == 9){ return "NUEVE MIL "  }
    else{ return "" }
  }

  /* RETORNA EL VALOR DE LA CANTIDAD INGESADA EN DECENAS DE MILLAR  */
  private decena_de_millar(decena:number, unidad: number) 
  {
        /** VALIDAMOS SI SON CANTIDADES CERRADAS */
         if( decena == 1 && unidad == 0){ return "DIEZ MIL "}
    else if( decena == 2 && unidad == 0){ return "VEITE MIL "    }
    else if( decena == 3 && unidad == 0){ return "TREINTA MIL "  }
    else if( decena == 4 && unidad == 0){ return "CUARENTA MIL " }
    else if( decena == 5 && unidad == 0){ return "CINCUENTA MIL "}
    else if( decena == 6 && unidad == 0){ return "SESENTA MIL "  }
    else if( decena == 7 && unidad == 0){ return "SETENTA MIL "  }
    else if( decena == 8 && unidad == 0){ return "OCHENTA MIL "  }
    else if( decena == 9 && unidad == 0){ return "NOVENTA MIL "  }
     
    /** VALIDAMOS SI PERTENECEN AL RANDO DE 11 A 19 */
    else if( decena == 1 && unidad == 1){ return "ONCE MIL "         }
    else if( decena == 1 && unidad == 2){ return "DOCE MIL "         }
    else if( decena == 1 && unidad == 3){ return "TRECE MIL "        }
    else if( decena == 1 && unidad == 4){ return "CATORCE MIL "      }
    else if( decena == 1 && unidad == 5){ return "QUINCE MIL "       }
    else if( decena == 1 && unidad == 6){ return "DIECISEIS MIL "    }
    else if( decena == 1 && unidad == 7){ return "DIECISIETE MIL "   }
    else if( decena == 1 && unidad == 8){ return "DIECIOCHOMIL MIL " }
    else if( decena == 1 && unidad == 8){ return "DIECINUEVE MIL "   }
    
    /** VALIDAMOS QUE PERTENESCA AL RANGO DE 21, 31, . . .91 */
    else if( decena == 2 && unidad == 1){ return "VEINTIUN MIL "      }
    else if( decena == 3 && unidad == 1){ return "TREINTA Y UN MIL "   }
    else if( decena == 4 && unidad == 1){ return "CUARENTA Y UN MIL "  }
    else if( decena == 5 && unidad == 1){ return "CINCUENTA Y UN MIL " }
    else if( decena == 6 && unidad == 1){ return "SESENTA Y UN MIL "   }
    else if( decena == 7 && unidad == 1){ return "SETENTA Y UN MIL "   }
    else if( decena == 8 && unidad == 1){ return "OCHENTA Y UN MIL "   }
    else if( decena == 9 && unidad == 1){ return "NOVENTA Y UN MIL "   }

    /** VALIDAMOS QUE PERTENESCA AL RANGO DE 22 - 90 */
    else if( decena == 2 && unidad >= 1){ return "VEINTI"     + this.unidad(unidad) + " MIL "}
    else if( decena == 3 && unidad >= 1){ return "TREINTA y "  + this.unidad(unidad) + " MIL "}
    else if( decena == 4 && unidad >= 1){ return "CUARENTA y " + this.unidad(unidad) + " MIL "}
    else if( decena == 5 && unidad >= 1){ return "CINCUENTA y "+ this.unidad(unidad) + " MIL "}
    else if( decena == 6 && unidad >= 1){ return "SESENTA y "  + this.unidad(unidad) + " MIL "}
    else if( decena == 7 && unidad >= 1){ return "SETENTA y "  + this.unidad(unidad) + " MIL "}
    else if( decena == 8 && unidad >= 1){ return "OCHENTA y "  + this.unidad(unidad) + " MIL "}
    else if( decena == 9 && unidad >= 1){ return "NOVENTA y "  + this.unidad(unidad) + " MIL "}

    else{ return "" }
  }

  /** RETORNAMOS EL VALOR REPRESENTADO EN LETRAS */
  conversor(precio:number)
  {
    /** DECLARAMOS UNA CADENA QUE VAA A CONTENER EL PRECIO EN LETRA */
    let precio_letra :string = ""

    /** ALBERGAMOS LA CANTIDAD EN UNA CADENA PARA COMPRAR SU TAMAÑO EN CARACTERES. */
    let auxiliar:string = precio+""

    if(auxiliar.length == 1)
    {
      precio_letra = this.unidad(precio)
    }
    else if(auxiliar.length == 2)
    {
      if(Number(auxiliar[0])==1 && Number(auxiliar[1])<6)
      {
        precio_letra = this.decena( Number(auxiliar[0]),Number(auxiliar[1]))
      }
      else
      {
        precio_letra = this.decena( Number(auxiliar[0]),Number(auxiliar[1])) + this.unidad( Number(auxiliar[1]) )
      }
    }
    else if(auxiliar.length == 3)
    {
      if(Number(auxiliar[1])==1 && Number(auxiliar[2])<6)
      {
        precio_letra = this.centena( Number(auxiliar[0]),Number(auxiliar[1]),Number(auxiliar[2]) ) + 
                       this.decena(  Number(auxiliar[1]),Number(auxiliar[2]))
      }
      else
      {
        precio_letra = this.centena( Number(auxiliar[0]),Number(auxiliar[1]),Number(auxiliar[2]) ) + 
                       this.decena(  Number(auxiliar[1]),Number(auxiliar[2]))                     +  
                       this.unidad(  Number(auxiliar[2]))
      }
    }
    else if(auxiliar.length == 4 )
    {
      if(Number(auxiliar[2])==1 && Number(auxiliar[3])<6)
      {
        precio_letra = this.unidad_de_millar( Number(auxiliar[0]) )                                +  
                     this.centena( Number(auxiliar[1]),Number(auxiliar[2]),Number(auxiliar[3]) ) + 
                     this.decena(  Number(auxiliar[2]),Number(auxiliar[3]) )
      }
      else
      {
        precio_letra = this.unidad_de_millar( Number(auxiliar[0]) )                                +  
                      this.centena( Number(auxiliar[1]),Number(auxiliar[2]),Number(auxiliar[3]) ) + 
                      this.decena(  Number(auxiliar[2]),Number(auxiliar[3]) )                     +  
                      this.unidad(  Number(auxiliar[3]) )
      }
    }
    else if(auxiliar.length == 5)
    {
      if(Number(auxiliar[3])==1 && Number(auxiliar[4])<6)
      {
        precio_letra = this.decena_de_millar(  Number(auxiliar[0]),Number(auxiliar[1]) )           +  
                     this.centena( Number(auxiliar[2]),Number(auxiliar[3]),Number(auxiliar[4]) ) + 
                     this.decena(  Number(auxiliar[3]),Number(auxiliar[4]) )
      }
      else
      {
        precio_letra = this.decena_de_millar(  Number(auxiliar[0]),Number(auxiliar[1]) )           +  
                     this.centena( Number(auxiliar[2]),Number(auxiliar[3]),Number(auxiliar[4]) ) + 
                     this.decena(  Number(auxiliar[3]),Number(auxiliar[4]) )                     +  
                     this.unidad(  Number(auxiliar[4]) )
      }
    }


    return precio_letra
  }

  /* Con esté método revisamos que el empleado cuente con todos los dataos necesarios para generar las responsivas 
  y en caso de no poseer un dato  se pueda asignar un valor por defecto */
  validarEmpleado(empleado:Empleado)
  {
    let oficina:Sucursal = new Sucursal()
    let puesto :Perfil   = new Perfil()

    oficina.nombreSucursal = " "
    puesto.nombrePuesto    = " "

    if(empleado.puesto==null || empleado.puesto.nombrePuesto==undefined    ){empleado.puesto=puesto}
    if(empleado.sucursal==null || empleado.sucursal.nombreSucursal==undefined){empleado.sucursal=oficina}
    if(empleado.folioIne==undefined){empleado.folioIne=""}
  }

}