import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Empleado } from 'src/app/administracion/modelos/empleado';
import { Perfil } from 'src/app/administracion/modelos/perfil';
import { Sucursal } from 'src/app/administracion/modelos/sucursal';
import { UsuarioService } from 'src/app/administracion/servicios';
import { RegistroAsignacionEquipoService } from 'src/app/administracion/servicios/Asignacion_Equipo/registro-asignacion-equipo.service';
import   Swal from 'sweetalert2';

@Component({
  selector: 'app-control-seguimiento-asignacion-equipos',
  templateUrl: './control-seguimiento-asignacion-equipos.component.html',
  styleUrls: ['./control-seguimiento-asignacion-equipos.component.scss']
})
export class ControlSeguimientoAsignacionEquiposComponent implements OnInit {

  /*Tabla de empleados */
  encabezados:string [] = ["NOTIFICACION","OPCIONES","NOMBRE DEL COLABORADDOR","CARGO","OFICINA"]; /* Encabezados genéricos */
  dataSource          = new MatTableDataSource<Empleado>([]);                 /* Data de la tabla      */
  @ViewChild('paginator', { static: true }) public paginator !: MatPaginator; /* Paginator de la tabla */

  lista_Empleados_N : Empleado [] = []; /*Empleados sin pendientes*/
  lista_Empleados_P : Empleado [] = []; /*Empleados con pendientes*/

  constructor(private registroAsignacionEquipoService : RegistroAsignacionEquipoService , /* Conexión con la tabla registro asignación de la base de datos */
              private usuarioService                  : UsuarioService                  , /* Conexión con el endpoint usuario                              */
              private router                          : Router
             )
             {}

  ngOnInit()
  {
    this.llenarLista()
  }

  /* CARGA LA LISTA DE COLABORADORES PROVENIENTES DEL END POINT */
  llenarLista()
  {
    /* CREAMOS DOS CLASES PARA USARLAS EN CASO QUE TENGA NULO LOS CAMPOS DE SUCURSAL Y PUESTO EL EMPLEADO */
    let sucursal : Sucursal = new Sucursal()
    let perfil   : Perfil   = new Perfil()

    /* REALIZAMOS UN LLAMADO A LOS EMPLEADOS CON PENDIENTES EN LA BASE DE DATOS */
    this.registroAsignacionEquipoService.seguimiento().subscribe(lista_id_consulta=>{
      this.usuarioService.getAllEmpleados().subscribe(lista_todos=>{
        this.lista_Empleados_N = lista_todos

        /* RECORREMOS ESA LISTA TRAIDA DE LA BASE DE DATOS  */
        this.lista_Empleados_N.forEach(empleado=>{  

          /* CONSTRUIMOS EL NOMBRE COMPLETO DEL EMPLEADO */
          empleado.nomb_compl = empleado.nombre + " " + empleado.apellidoPat + " " + empleado.apellidoMat

          if(empleado.sucursal  == null || empleado.sucursal  == undefined){ empleado.sucursal = sucursal}
          if(empleado.puesto    == null || empleado.puesto    == undefined){ empleado.puesto   = perfil  }

          /* RECORREMOS LA LISTA TARIDA POR PARTE DEL ENDPOINT */
          for(let i = 0; i < lista_id_consulta.length; i++)
          {
            /*
                COMPARAMOS LOS ID's Y EN CASO DE TENER PENDIENTES LOS REMOVEMOS DE LA LISTA DEL ENPOINT Y 
                LOS COLOCAMOS AL PRINCIPIO DE LA LISTA 
            */
            if(empleado.idEmpleado == lista_id_consulta[i])
            {
              empleado.bandera = true
              this.lista_Empleados_N.splice(this.lista_Empleados_N.indexOf(empleado),1)
              this.lista_Empleados_P.push(empleado)
              lista_id_consulta.splice(i,1)
              break
            }
          }    
        })
        
        this.dataSource.data = this.lista_Empleados_P.concat(this.lista_Empleados_N)
        this.dataSource.paginator= this.paginator
      })
    })
  }
  
  /* NOS REDIRIGE A LA VISTA DE DETALLE DE TODOS LOS REGISTROS Y EQUIPOS QUE TIENE EL EMPLEADO */
  clickRegistros(id_empleado:number)
  {
    this.router.navigate(['layout/asignacionesEquipoUsuario/'+id_empleado])
  }

  /* FILTRA LOS EMPLEADOS POR MEDIO DE SU NOMBRE COMPLETO */
  filtroDataDource(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  /*MUESTRA UNA VENTANA CON INFORMACIÓN SOBRE LA PANTALLA EN CURSO */
  clickMasInformacion(event:any)  
  {
    Swal.fire({
      title:'Pendientes de responsiva',
      html:
      '<p align="justify">En la siguiente tabla se muestran a todos los colaboradores de la empresa, re'  +
      'saltando con un punto rojo al inicio de su fila (ademas de estar siempre al inicio de los demas '  +
      'colaboradores) aquellos que tengan pendientes aún (requiere subir nuevamente la responsiva o sub'  +
      'irla por primera vez).</p>'                                                                        +
      
      '<p align="justify"> <b>FILTRO </b> Se cueta con una barra de búsqueda por la cual se podra filtr'  +
      'ar la data de la tabla ya sea por el nombre del colaborador, su cargo o su oficina para hacer un'  +
      'a búsqueda más rápida.</p>'                                                                        +
      
      '<p align="justify"> <b>BOTÓN</b> El botón que se encuentra al princicipio de cada fila nos sirve'  +
      ' para ver los pendientes, las asignaciones y los equipos que tiene esta persona.</p>'              ,

      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      confirmButtonColor: '#d33',
      confirmButtonText:'Cerrar'
    })
  }
}