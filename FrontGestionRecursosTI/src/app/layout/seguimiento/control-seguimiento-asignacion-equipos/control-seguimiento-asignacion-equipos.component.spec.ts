import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlSeguimientoAsignacionEquiposComponent } from './control-seguimiento-asignacion-equipos.component';

describe('ControlSeguimientoAsignacionEquiposComponent', () => {
  let component: ControlSeguimientoAsignacionEquiposComponent;
  let fixture: ComponentFixture<ControlSeguimientoAsignacionEquiposComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ControlSeguimientoAsignacionEquiposComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlSeguimientoAsignacionEquiposComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
