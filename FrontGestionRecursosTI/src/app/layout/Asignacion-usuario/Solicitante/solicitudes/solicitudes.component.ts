import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Empleado } from 'src/app/administracion/modelos/empleado';
import { Perfil } from 'src/app/administracion/modelos/perfil';
import { Sucursal } from 'src/app/administracion/modelos/sucursal';
import { SucursalService, UsuarioService } from 'src/app/administracion/servicios';
import { TipoServicio } from 'src/app/administracion/modelos/Control_Usuario/tipo-servicio';
import { TipoSolicitud } from 'src/app/administracion/modelos/Control_Usuario/tipo-solicitud';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { TipoServicioService } from 'src/app/administracion/servicios/Control_Usuario/tipo-servicio.service';
import { SolicitudService } from 'src/app/administracion/servicios/Control_Usuario/solicitud.service';
import { TipoSolicitudService } from 'src/app/administracion/servicios/Control_Usuario/tipo-solicitud.service';
import { DetalleSolicitudService } from 'src/app/administracion/servicios/Control_Usuario/detalle-solicitud.service';
import { MatTableDataSource } from '@angular/material/table';
import { ServicioUsuario } from 'src/app/administracion/modelos/Control_Usuario/servicio-usuario';
import { Solicitud } from 'src/app/administracion/modelos/Control_Usuario/solicitud';
import { ElemServicioService } from 'src/app/administracion/servicios/Control_Usuario/elem-servicio.service';
import { MatPaginator } from '@angular/material/paginator';

/** PDF */
import * as pdfMake from "pdfmake/build/pdfmake";
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { environment } from 'src/environments/environment.prod';
(<any>pdfMake).vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-solicitudes',
  templateUrl: './solicitudes.component.html',
  styleUrls: ['./solicitudes.component.scss']
})
export class SolicitudesComponent implements OnInit {
  /* Paginators */
  @ViewChild(MatPaginator) paginator  !: MatPaginator

  dataSource          = new MatTableDataSource<ServicioUsuario>([]);
  encabezadostabla     : String [] = ["c1","c2","c3","c4"];
  cabeceraCheck        : String ="";
  tipoAlta       = "ALTA DE SERVICIOS";
  tipoActivacion = "ACTIVACIÓN DE PRIVILEGIOS";
  tipoSuspension = "SUSPENSIÓN DE SERVICIOS";
  fechaMinima : Date = new Date();

  /**Datos para el PDF de la solicitud */
  colaborador : Empleado= new Empleado();
  sustituye   : Empleado= new Empleado();
  supervisor  : Empleado= new Empleado();
  sucursal2   : Sucursal= new Sucursal();
  puesto2     : Perfil  = new Perfil();
  fecha       : Date = new Date();
  fechaSoli   : string="";
  fecha_plan_asig : string="";
  regionSolicitante: string="";
  fecha_alta2 : string="";
  fecha_baja2 : string="";
  private urlFirmasDig:string = environment.urlFirmasDig+"/";


  /** Listas a utilizar */
  listaMeses          :  string   []=[" DE ENERO DE ", " DE FEBRERO DE ", " DE MARZO DE ", " DE ABRIL DE ", " DE MAYO DE ", " DE JUNIO DE ", " DE JULIO DE ", " DE AGOSTO DE ", " DE SEPTIEMBRE DE ", " DE OCTUBRE DE ", " DE NOVIEMBRE DE ", " DE DICIEMBRE DE ", "ERROR"]
  listaReemplazo      :  string   []=["SI","NO"];
  listaColaboradores  :  Empleado []=[];
  listaCargos!        :  Perfil   [];
  listaSucursales!    :  Sucursal [];
  listaServicios1     :  TipoServicio []=[];
  listaServicios      :  TipoServicio []=[];
  listatipoSolicitudes1  :  TipoSolicitud[]=[];
  listatipoSolicitudes   :  TipoSolicitud[]=[];
  servSolicitados        :  TipoServicio[]=[];
  /**Listas para los select del colboarador, sustituye_a y del suspervisor */
  listaFiltradaEmp      :   ReplaySubject<Empleado[]> = new ReplaySubject<Empleado[]>(1);
  listaFiltradaSust     :   ReplaySubject<Empleado[]> = new ReplaySubject<Empleado[]>(1);
  listaFiltradaSup      :   ReplaySubject<Empleado[]> = new ReplaySubject<Empleado[]>(1);

  protected onDestroy  = new Subject<void>();

  private solicitante_id  : number = JSON.parse(localStorage.getItem('empleado') || '{}').id;
  private solicitante     : Empleado= new Empleado();

  constructor(
              private router                :Router,
              private sucursalService       : SucursalService,
              private usuarioService        : UsuarioService ,
              private tipoServicioService   : TipoServicioService,
              private solicitudService      : SolicitudService,
              private tipoSolicitudService  : TipoSolicitudService,
              private detalleSolicitudService : DetalleSolicitudService,
              private elemServicioService   : ElemServicioService,
              private formBuilder           : FormBuilder,
  ) { }
  /**Form Control de los empleados */
  colaboradorFilter = new FormControl(); /**select- Colaborador al que se le asigna el servicio */
  inputColaboradorF = new FormControl(); /**input */

  sustituyeFilter   = new FormControl();
  inputSustituyeF   = new FormControl();

  supervisorFilter  = new FormControl();
  inputSupervisorF  = new FormControl();

   /**Campos del formulario */
  formSolicitud = this.formBuilder.group({
    reemplazo           : ['',[]],
    tipoSolicitud       : ['',[Validators.required]],
    fecha_planeada      : ['',[Validators.required]],//ALTA DE SERVICIOS
    tipo                : ['',[]],                   //ACTIVACIÓN Y SUSPENSIÓN
    comentarios         : ['',[Validators.required]],//ALTA Y ACTIVACIÓN
    puesto_a_cubrir     : ['',[Validators.required]],//ACTIVACIÓN
    sucursal_a_cubrir   : ['',[Validators.required]],//ACTIVACIÓN
    // No se incluyen en el post para agregar una solicitud
    puesto              : [{value: '', disabled: true}],
    sucursal            : [{value: '', disabled: true}],
    tipo_servicio       : ['',[Validators.required]],
  })
  /**Campos del rango de fechas */
  range= this.formBuilder.group({
    fecha_alta  : ['',[Validators.required]],//TEMPORAL
    fecha_baja  : ['',[Validators.required]] //TEMPORAL
  })

  /**En este método se llenan los datos para el pdf de la solicitud */
  ngOnInit(): void {
    //this.solicitante_id=1957
    /**Llena los datos para el pdf de la solicitud */
    this.usuarioService.getAnEmpleado(this.solicitante_id).subscribe(solicit=>{
      this.solicitante=solicit;
      if(solicit?.sucursal?.region?.nombreRegion!=undefined){
        this.regionSolicitante= this.solicitante?.sucursal.region?.nombreRegion+""
      }

      /**Asigna al solicitante un nombre completo */
      this.solicitante.nomb_compl=solicit.nombre+" "+solicit.apellidoPat+" "+solicit.apellidoMat;

      /*Consulta que obtiene el nombre de usuario del solicitante*/
      this.usuarioService.getAll().subscribe(usuarios=>{
        usuarios.forEach(usuario=>{
          /**localiza alusuario que le corresponde al empleado */
          if(usuario.empleado.idEmpleado==this.solicitante.idEmpleado){

            /**Verfica que exista una firma registrada en el endpoint, de lo contrario asignara una imagen como marca de agua */
            this.usuarioService.getFirmaDigital(usuario.username+"").subscribe(firma=>{
              //console.log(firma+"MARTINEZ") ;
              this.urlFirmasDig= this.urlFirmasDig+usuario.username;
            }, err=>{
              this.urlFirmasDig="/assets/img/firmaNoEncontrada.png";
              console.log("La firma no fue encontrada dentro del endpoint"+err);
            });
          }
        });
      });
    });

    /**Asigna la fecha actual a la variable */
    this.fechaSoli= this.fecha.toISOString().substring(8,10)+" "+this.listaMeses[Number(this.fecha.toISOString().substring(5,7))-1]+" "+this.fecha.toISOString().substring(0,4);

    this.llenarListas();
    this.llenarListaColab();

    /**Escucha los cambios del valor de búsqueda del colaborador*/
    this.listaFiltradaEmp.next(this.listaColaboradores.slice());
    this.inputColaboradorF.valueChanges.pipe(takeUntil(this.onDestroy)).subscribe(() => {
        this.filterColaborador();
    });

    /**Escucha los cambios del valor de búsqueda del que sustituye*/
    this.listaFiltradaSust.next(this.listaColaboradores.slice());
    this.inputSustituyeF.valueChanges.pipe(takeUntil(this.onDestroy)).subscribe(() => {
        this.filterSustituye();
    });

    /**Escucha los cambios del valor de búsqueda del que sustituye*/
    this.listaFiltradaSup.next(this.listaColaboradores.slice());
    this.inputSupervisorF.valueChanges.pipe(takeUntil(this.onDestroy)).subscribe(() => {
        this.filterSupervisor();
    });
  }

  /**Se llenan las listas para las sucursales, puestos, tipos de servicio y tipos de solicitud */
  private llenarListas(){
    this.sucursalService.getSucursales  ().subscribe( val => { this.listaSucursales = val   ;} );
    this.usuarioService .getAllPuesto   ().subscribe( val => { this.listaCargos = val       ;} );
    //Se llenan las listas por medio de la BD local
    this.tipoServicioService.todosLosServicios().subscribe(val=>{
      this.listaServicios1 = val;
      for(let i=0;i<this.listaServicios1.length ;i++){
        /**Inserta solo datos activos en la lista */
        if (this.listaServicios1[i].status_servicio=="ACTIVO"){
          this.listaServicios.push(this.listaServicios1[i]);
        }
      }
    });
    this.tipoSolicitudService.todosLosTiposSolicitud().subscribe(val=>{
      this.listatipoSolicitudes1 = val;
      for(let i=0;i<this.listatipoSolicitudes1.length ;i++){
        /**Inserta solo datos activos en la lista */
        if (this.listatipoSolicitudes1[i].status_solicitud=="ACTIVO"){
          this.listatipoSolicitudes.push(this.listatipoSolicitudes1[i]);
        }
      }
    });
  }

  /**Llena lista de colaboradores */
  llenarListaColab(){
    this.usuarioService .getAllEmpleados().subscribe( val => {
      val.forEach(emp=>{
        emp.nomb_compl= emp.nombre+" "+ emp.apellidoPat+" "+emp.apellidoMat
      });
      this.listaColaboradores = val;
    } );
  }

  /**Realiza la búsqueda para el colaborador a quien se le solicitan los servicios */
  protected filterColaborador(){
    if(!this.listaColaboradores){
      return;
    }
    /**Se optiene el valor del input para el colaborador*/
    let search = this.inputColaboradorF.value;
    if (!search) {
      this.listaFiltradaEmp.next(this.listaColaboradores.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    /**Retorna la lista de los colaboradores que cumplen con la busqueda */
    this.listaFiltradaEmp.next( this.listaColaboradores.filter(empleado => empleado.nomb_compl.toLowerCase().indexOf(search) > -1) );
  }

  /**Realiza la búsqueda para el colaborador que va a sustituir */
  protected filterSustituye(){
    if(!this.listaColaboradores){
      return;
    }
    /**Se optiene el valor del input para el colaborador*/
    let search = this.inputSustituyeF.value;
    if (!search) {
      this.listaFiltradaSust.next(this.listaColaboradores.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    /**Retorna la lista de los colaboradores que cumplen con la busqueda */
    this.listaFiltradaSust.next( this.listaColaboradores.filter(empleado => empleado.nomb_compl.toLowerCase().indexOf(search) > -1) );
  }

  /**Realiza la búsqueda para el supervisor */
  protected filterSupervisor(){
    if(!this.listaColaboradores){
      return;
    }
    /**Se optiene el valor del input para el colaborador*/
    let search = this.inputSupervisorF.value;
    if (!search) {
      this.listaFiltradaSup.next(this.listaColaboradores.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    /**Retorna la lista de los colaboradores que cumplen con la busqueda */
    this.listaFiltradaSup.next( this.listaColaboradores.filter(empleado => empleado.nomb_compl.toLowerCase().indexOf(search) > -1) );
  }


  /**Botón que muestra la información de la vista de solicitudes*/
  botonInformacion(){
    Swal.fire({
      position: 'top-end',
      icon: 'info',
      title: 'SOLICITUD DE SERVICIOS',
      showConfirmButton: false,
      html: '<p align="justify"> Llene los campos correctamente según sea el tipo de solicitud que requiera</p>'
           +'<b>SOLICITUD DE ALTA DE SERVICIOS</b>'
           +'<p align="justify"> Para este tipo de solicitud deberá elegir los servicios que requiere dar de alta</p>'
           +'<b>SOLICITUD DE ACTIVACIÓN DE PRIVILEGIOS</b>'
           +'<p align="justify" >No es necesario seleccionar servicios, sólo llenar los datos correspondientes</p>'
           +'<b>SOLICITUD DE SUSPENSIÓN DE SERVICIOS</b>'
           +'<p align="justify">Debe seleccionar todos los servicios de la sucursal o sucursales de donde los desea suspender</p>',
           confirmButtonColor: '#3085d6',
           cancelButtonColor: '#d33',
           confirmButtonText: 'Sí'
    })
  }

  /**limpia los campos de la solicitud */
  cont =0;
  limpiarCampos(){
    let fechap= new Date();

    this.formSolicitud.controls['reemplazo'].setValue("");
    this.formSolicitud.controls['fecha_planeada'].setValue("");
    this.formSolicitud.controls['tipo'].setValue("Permanente");
    this.formSolicitud.controls['comentarios'].setValue("");
    this.formSolicitud.controls['puesto_a_cubrir'].setValue("");
    this.formSolicitud.controls['sucursal_a_cubrir'].setValue("");
    this.formSolicitud.controls['puesto'].setValue("");
    this.formSolicitud.controls['sucursal'].setValue("");
    this.formSolicitud.controls['tipo_servicio'].setValue("");
    this.colaboradorFilter.setValue("");
    this.sustituyeFilter.setValue("");
    this.supervisorFilter.setValue("");
    /**Habilita el campo del colaborador */
    this.colaboradorFilter.enable();

    if(this.formSolicitud.value.tipo=="Permanente" ){
      this.range.controls['fecha_alta'].setValue("0000-00-00");
      this.range.controls['fecha_baja'].setValue("0000-00-00");
    }

    let empl = new Empleado();
    if(this.tipoSoliSeleccionada(this.tipoAlta)){
      this.cont++;
      console.log(this.cont);
      console.log("alta");
      //this.formSolicitud.controls['tipo'].setValue("*");
      this.formSolicitud.controls['puesto_a_cubrir'].setValue("*");
      this.formSolicitud.controls['sucursal_a_cubrir'].setValue("*");

    }else
    if(this.tipoSoliSeleccionada(this.tipoActivacion)){
      console.log("activacion");
      this.formSolicitud.controls['fecha_planeada'].setValue(fechap);
      this.formSolicitud.controls['tipo_servicio'].setValue(["null", "null"]);
      this.supervisorFilter.setValue(empl);

   }else
   if(this.tipoSoliSeleccionada(this.tipoSuspension)){
      console.log("suspension");
      this.formSolicitud.controls['fecha_planeada'].setValue(fechap);
      this.formSolicitud.controls['puesto_a_cubrir'].setValue(" ");
      this.formSolicitud.controls['sucursal_a_cubrir'].setValue(" ");
      //this.formSolicitud.controls['comentarios'].setValue(" ");
      this.formSolicitud.controls['tipo_servicio'].setValue(["null", "null"]);
      this.supervisorFilter.setValue(" ");
    }

  }
  /**Carga los datos del colaborador, sucursal y puesto */
  cargarDatosColaborador(event:any){
    this.colaborador = this.colaboradorFilter.value;
    /**Borramos el contenido que tengan los campos de cargo y sucursal */
    this.formSolicitud.controls['puesto'].setValue("");
    this.formSolicitud.controls['sucursal'].setValue("");
    this.sucursal2=this.colaborador.sucursal;
    this.puesto2=this.colaborador.puesto;
    /** Cargo - Si el puesto fue localizado se le asigna ese valor*/
    if(this.puesto2 != null){ this.formSolicitud.controls['puesto'].setValue(this.puesto2?.nombrePuesto);}
    /** Sucursal - Si la sucursal fue localizado se le asigna ese valor*/
    if(this.sucursal2 != null){ this.formSolicitud.controls['sucursal'].setValue(this.sucursal2?.nombreSucursal);}
  }

  /**Datos del campo sustituye_a */
  cargarDatosSustituye(event : any){
    this.sustituye= this.sustituyeFilter.value;
  }

  /**Datos del campo sustituye_a */
  cargarDatosSupervisor(event : any){
    this.supervisor=this.supervisorFilter.value;
  }

  /**llena la tabla con los servicios que el colaborador tiene activados */
  llenarTablaActivados(event : any){
    if(this.tipoSoliSeleccionada("SUSPENSIÓN DE SERVICIOS")){
      this.dataSource.data=[];
      let idColaborador= Number(this.colaboradorFilter.value.idEmpleado);

      let listaServicios: ServicioUsuario[]=[];
      this.elemServicioService.listaElemServicioForColaborador(idColaborador).subscribe(elementos=>{
        elementos.forEach(elemento=>{
          if(elemento.status_registro=="Activado" && elemento.servicio_usuario.archivo_responsiva!=null){
            let regUsuario : ServicioUsuario=new ServicioUsuario();
            regUsuario=elemento.servicio_usuario;
            this.sucursalService.getAnSucursal(Number(elemento?.sucursal_id)).subscribe(sucursal=>{
              regUsuario.nomSucursalCol=sucursal?.nombreSucursal;
            });
            /**Se inserta el id del elemento de servicio que se decea modificar */
            regUsuario.detalle_solicitud_id=elemento.idElemServicio;
            regUsuario.nombre_servicio= elemento.servicio_usuario.detalleSolicitud.tipo_servicio.nombre_servicio;
            listaServicios.push(regUsuario);
            this.dataSource.data=listaServicios;
            this.dataSource.paginator=this.paginator;
          }
        });
        if(listaServicios.length<=0){
          Swal.fire({
            icon: 'info',
            title: 'Tabla vacía',
            text: 'Este colaborador no cuenta con servicios activados',
            showConfirmButton: false,
            timer: 4000
          });
        }
      }, err=>{console.log(err)});
    }
  }

  /**Retorna true o false dependiendo de la solicitud que se le pasa como parámetro */
  tipoSoliSeleccionada(tipoSoli : string): boolean{
    if(tipoSoli == this.formSolicitud.value.tipoSolicitud.tipo_solicitud){
      //console.log(tipoSoli+"tipo soli");
      //console.log(this.formSolicitud.value.tipoSolicitud.tipo_solicitud+" tipo soli- metodo");
      //console.log("true--");
      return true;
    }
    return false;
  }

  /** Retorna true o false dependiendo del tipo que se le pasa como parámetro, ya sea temporal o permanente*/
  tipoSeleccionado(tipo:string){
    if(tipo==this.formSolicitud.value.tipo)
      return true;
    return false;
  }

  /**Permite habilitar el boton para enviar la solicitud */
  habilitarBotonSolicitud(){
    if(this.colaboradorFilter.value != null ){
       return true}
    return false
  }

  /**Filtro para la tabla de servicios  a suspender*/
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  /**Método que obtiene todos los datos del formulario, según sea el tipo de solicitud elegida y genera un a solicitud de ese tipo */
  enviarSolicitud(){//&& this.colaboradorFilter.value!="" && this.supervisorFilter.value!=""
    if(this.formSolicitud.valid && this.colaboradorFilter.value!="" && this.supervisorFilter.value!=""  ){

      let reemplazoB = false
      let fecha1="0000-00-00";
      let fecha2="0000-00-00";
      let fecha3="0000-00-00";
      let tipoSoli=this.formSolicitud.value.tipoSolicitud.id_tipo_solicitud_usuario;
      let nomTipoSoli: TipoSolicitud=this.formSolicitud.value.tipoSolicitud
      /**Valida que el que un usuario no haya realizado una solicitud del mismo tipo seguidamente con el mismo solicitante */
      this.solicitudService.ultimaSolicitud(tipoSoli).subscribe(objeto=>{
        let colaboradorId=Number(this.colaborador.idEmpleado)
        if( objeto==null || (objeto.colaboradorId!=colaboradorId) ){

          if (this.formSolicitud.value.reemplazo=="SI"){
              reemplazoB=true
          }
          if(this.formSolicitud.value.reemplazo=="NO"){
                      reemplazoB==false
          }

          if(this.formSolicitud.value.tipo=="Temporal" ){
            //Fecha de alta
            let fechaAux2: Date =  this.range.value.fecha_alta;
            this.fecha_alta2= fechaAux2.toISOString().substring(0,10);
            fecha2= this.fecha_alta2;
            //Fecha de baja
            let fechaAux3: Date =  this.range.value.fecha_baja;
            this.fecha_baja2 = fechaAux3.toISOString().substring(0,10);
            fecha3=this.fecha_baja2;
          } else if(this.formSolicitud.value.tipo=="Permanente"){
            this.range.controls['fecha_alta'].setValue("0000-00-00");
            this.range.controls['fecha_alta'].setValue("0000-00-00");
          }
          if(this.formSolicitud.value.tipoSolicitud.tipo_solicitud==this.tipoAlta){

            let fechaAux: Date =  this.formSolicitud.value.fecha_planeada;
            this.fecha_plan_asig= fechaAux.toISOString();
            this.fecha_plan_asig = this.fecha_plan_asig.substring(0,10);
            fecha1=this.fecha_plan_asig;
          }

          let timer :any
          Swal.fire({
            title: 'Espere por favor!',
            html: 'Registrando...',
            timerProgressBar: true,
            didOpen: () => {
              Swal.showLoading()

            let solicitud : Solicitud = new Solicitud();
            solicitud.reemplazo=reemplazoB;
            solicitud.colaboradorId= this.colaborador.idEmpleado;
            solicitud.tipo_solicitud_usuario_id= tipoSoli;
            solicitud.fecha_planeada_act=fecha1;
            solicitud.fecha_alta=this.range.value.fecha_alta//fecha2;
            solicitud.fecha_baja=this.range.value.fecha_baja;//fecha3;
            solicitud.sustituye_a=this.sustituye.nomb_compl;
            solicitud.nom_supervisor=this.supervisor.nomb_compl;
            solicitud.tipo=this.formSolicitud.value.tipo;
            solicitud.comentarios= this.formSolicitud.value.comentarios;
            solicitud.puesto_a_cubrir=this.formSolicitud.value.puesto_a_cubrir.idPuesto;
            solicitud.sucursal_id=this.formSolicitud.value.sucursal_a_cubrir.idSucursal;

            this.solicitudService.agregarSolicitud(solicitud,tipoSoli).subscribe(data=>{
              let ultimoId = data.id_solicitud;
              this.generarDetalleSolicitud(Number(ultimoId));
              /**Validaciones para generar el PDF de la solicitud correspondiente al tipo de solicitud */
              if(this.tipoSoliSeleccionada(this.tipoAlta)){ this.createSolicitudAltaPDF(); }
              if(this.tipoSoliSeleccionada(this.tipoActivacion)){ this.createSolicitudActivacionPDF(); }
              if(this.tipoSoliSeleccionada(this.tipoSuspension)){ this.createSolicitudSuspPDF(); }

              this.router.navigate(['layout/gestionSolicitudes/' ]);

              Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Registro de solicitud',
                text: ' La solicitud se registró exitosamente',
              });

            }, err => {
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: 'Registro de solicitud',
                text: 'El registro no se realizó correctamente debido a que no hay conexión con el servidor, inténtelo más tarde o contacte al área de T.I. ',
              })
            }
            );
            //cierre de espera
          },
          willClose: () => {
            clearInterval(timer)
          }
          }).then((result) => {
            /* Read more about handling dismissals below */
            if (result.dismiss === Swal.DismissReason.timer) {}
          })

      }else if(objeto.status_solicitud=="Espera") {
        Swal.fire({
          position: 'center',
          icon: 'error',
          title: 'Registro de solicitud',
          text: 'Este colaborador ya cuenta con una solicitud previa del tipo: '+nomTipoSoli.tipo_solicitud+' Verifique la solicitud o espere a que le sea permitido'
        })
      }
     });
    }
    console.log("servicio");
    Swal.fire({
      icon: 'error',
      title: 'Faltan campos',
      text: 'Es necesario que todos los campos esten debidamente requisitados',
      showConfirmButton: false,
      timer: 4000
    });
  }

  //Método para generar el detalle de la solicitud
  generarDetalleSolicitud(ultimoid:number){
    let listaSU = this.dataSource.data;
        /**Corresponde a una solicitud de alta de servicios o cualquier otra que no sea de activacion o suspensión */
        if(this.tipoSoliSeleccionada('ACTIVACIÓN DE PRIVILEGIOS')==false && this.tipoSoliSeleccionada('SUSPENSIÓN DE SERVICIOS')==false){
          this.servSolicitados=this.formSolicitud.value.tipo_servicio;
          this.servSolicitados.map(val=>{
            this.detalleSolicitudService.generarDetalleSolicitud(ultimoid,Number(val.id_tipo_servicio), 0).subscribe(data=>{
              console.log(data);
            }, err=>{console.log(err)});
          });
          /**Corresponde a una solicitud de activacion o suspencion para el cual si es necesario pasarle el servicio_usaurio_id que va  a modificar */
        }else if(this.tipoSoliSeleccionada('ACTIVACIÓN DE PRIVILEGIOS') || this.tipoSoliSeleccionada('SUSPENSIÓN DE SERVICIOS')){
                listaSU.forEach(val=>{
                  if(val.status_reg?.toString()=="true"){
                    this.detalleSolicitudService.generarDetalleSolicitud( ultimoid,Number(val.detalleSolicitud.tipo_servicio.id_tipo_servicio), Number(val.detalle_solicitud_id)).subscribe(data=>{
                      console.log(data);
                    }, err=>{console.log(err)});
                  }
                });
              }
  }

  /**Método que genera parte del pdf con los datos del colaborador */
  datosColaboradorPDF(){
    return {
      table: {
        widths: [130,'*'],
        body: [
          [
            {text: ' °  NOMBRE COMPLETO:    ',    fontSize: 12, bold: true},
            {border: [true, true, true, true], fillColor: '#eeffee', text:  this.colaborador.nomb_compl }
          ],[{text: ' ', fontSize: 4},{text: ' ', fontSize: 4}],//space
          [
            {text: ' °  PUESTO:             ', fontSize: 12, bold: true},
            {border: [true, true, true, true], fillColor: '#eeffee', text: this.puesto2?.nombrePuesto }
          ],[{text: ' ', fontSize: 4},{text: ' ', fontSize: 4}],//space
          [
            {text: ' °  SUCURSAL:           ', fontSize: 12, bold: true},
            {border: [true, true, true, true], fillColor: '#eeffee', text: this.sucursal2?.nombreSucursal}
          ],[{text: ' ', fontSize: 4},{text: ' ', fontSize: 4}],//space
        ]
      }, layout: { defaultBorder: false }
    }
  }

  /**Método que genera parte del pdf con los datos del solicitante */
  datosSolicitantePDF(){
    return {
      table: {
        widths: [150,'*'],
        body: [
          [{text: ' ', fontSize: 4},{text: ' ', fontSize: 4}],//space
          [
            {text: 'COMENTARIOS:           ', fontSize: 12, bold: true},
            {border: [true, true, true, true], fillColor: '#eeffee', text: this.formSolicitud.value.comentarios}
          ],[{text: ' ', fontSize: 4},{text: ' ', fontSize: 4}],//space
          /**Separador negro */
          [{ colSpan:2, fillColor: '#000000', text: ' ', fontSize: 4 },''],[{text: ' ', fontSize: 4},{text: ' ', fontSize: 4}],//space
          [
            {colSpan:2, text: 'DATOS DEL SOLITANTE    ',    fontSize: 13, bold: true},''
          ],[{text: ' ', fontSize: 4},{text: ' ', fontSize: 4}],//space
          [
            {text: ' °  NOMBRE COMPLETO:    ',    fontSize: 12, bold: true},
            {border: [true, true, true, true], fillColor: '#eeffee', text: this.solicitante.nomb_compl }
          ],[{text: ' ', fontSize: 4},{text: ' ', fontSize: 4}],//space
          [
            {text: ' °  PUESTO:             ', fontSize: 12, bold: true},
            {border: [true, true, true, true], fillColor: '#eeffee', text: this.solicitante.puesto.nombrePuesto }
          ],[{text: ' ', fontSize: 4},{text: ' ', fontSize: 4}],//space
          [
            {text: ' °  REGIÓN:           ', fontSize: 12, bold: true},
            {border: [true, true, true, true], fillColor: '#eeffee', text: this.regionSolicitante}
          ],[{text: ' ', fontSize: 4},{text: ' ', fontSize: 4}],//space
          [
            {text: ' °  CORREO ELECTRÓNICO: ', fontSize: 12, bold: true},
            {border: [true, true, true, true], fillColor: '#eeffee', text: this.solicitante.correoInstitucional}
          ],['\n\n','\n\n'],//space
        ]
      }, layout: { defaultBorder: false }
    };
  }

  /**Método que genera el PDF de la solicitud de alta de servicios*/
  async createSolicitudAltaPDF(){
      const pdfDefinition: any={
        header: [
          { columns:[
              {
                image: await this.getBase64ImageFromURL("/assets/img/sofipa.png"),
                fit: [150, 150]
              },
              { text:[
                  { alignment: 'right', text: 'GTI-FO-ALTATI V1      ', fontSize: 11},
                  { text: '\nSOLICITUD DE ALTA DE SERVICIOS DE TI     ', style: 'header', fontSize: 13.5, bold:true }
              ]},
          ]}
        ],
        content: [
          /**DATOS DEL COLABORADOR */
          {table: {
            widths:['*'], body:[['\n'],[{ fillColor: '#000000', text: ' ', fontSize: 4 }]]
          },layout: { defaultBorder: false }},
          { columns: [

              {width:200, text: '\nDATOS DEL COLABORADOR\n\n',   fontSize: 13, bold: true},
              {alignment: 'right',  text: '\nFECHA DE SOLICITUD:  '+this.fechaSoli+"\n\n" }
            ]
          },
          this.datosColaboradorPDF(),
          {
            table: {
              widths: [130,'*'],
              body: [
                [
                  {text: ' °  SUSTITUYE A:        ', fontSize: 12, bold: true},
                  {border: [true, true, true, true], fillColor: '#eeffee', text: this.sustituye.nomb_compl}
                ],[{text: ' ', fontSize: 4},{text: ' ', fontSize: 4}],//space
                [
                  {text: ' °  SUPERVISOR:         ', fontSize: 12, bold: true},
                  {border: [true, true, true, true], fillColor: '#eeffee', text: this.supervisor.nomb_compl}
                ],[{text: ' ', fontSize: 4},{text: ' ', fontSize: 4}],//space
                /**Separador negro */
                [{ colSpan:2, fillColor: '#000000', text: ' ', fontSize: 4 },''],[{text: ' ', fontSize: 4},{text: ' ', fontSize: 4}],
                [
                  { colSpan: 2,
                    text: [
                    {text: 'ALTA DE SERVICIOS SOLICITADOS',   fontSize: 13, bold: true},
                    {text: '\n'+this.leerServSolicitados()+'\n'}
                    ]
                },''
                ],
                /**Separador negro */
                [{ colSpan:2, fillColor: '#000000', text: ' ', fontSize: 4 },''], [{text: ' ', fontSize: 4},{text: ' ', fontSize: 4}],//space
              ]
            },layout: { defaultBorder: false }
          },
          /**FECHAS DE ACTIVACIÓN Y CONTRATACIÓN */
          {table:{
            widths: ['*',20,'*'],
              body: [
                [
                  {text: 'FECHA DE CONTRATACIÓN: ',   fontSize: 12, bold: true},'',
                  {text: 'FECHA DE ACTIVACIÓN: ',   fontSize: 12, bold: true},              ],
                [
                  {border: [true, true, true, true], fillColor: '#eeffee', text: this.colaborador.fechaIngreso},
                  '',
                  {border: [true, true, true, true], fillColor: '#eeffee', text: this.fecha_plan_asig}
                ]
              ]
            }, layout: { defaultBorder: false }
          },
          this.datosSolicitantePDF(),
          /**NOMBRE Y FIRMA DEL SOLICITANTE */
          { alignment: 'center', text:'NOMBRE Y FIRMA DEL SOLICITANTE\n' },
          { alignment: 'center',
            image: await this.getBase64ImageFromURL(this.urlFirmasDig),
            fit: [140, 140]
          },
          { alignment: 'center', text: ''+this.solicitante.nomb_compl +'\n________________________________________' },
        ],
      }
      console.log(this.solicitante.nombre+"entra al pdf"+this.solicitante_id);
      const pdf = pdfMake.createPdf(pdfDefinition)
      pdf.download("SOLICITUD-ALTATI "+this.colaborador.nomb_compl);
  }

  /**Método que genera el PDF de la solicitud de alta de servicios*/
  async createSolicitudActivacionPDF(){
      const pdfDefinition: any={
        header: [
          { columns:[
              {
                image: await this.getBase64ImageFromURL("/assets/img/sofipa.png"),
                fit: [150, 150]
              },
              { text:[
                  { alignment: 'right', text: 'GTI-FO-ACTTI V1      ', fontSize: 11},
                  { text: '\nSOLICITUD DE ACTIVACIÓN DE PRIVILEGIOS     ', style: 'header', fontSize: 13.5, bold:true }
              ]},
          ]}
        ],
        content: [
          /**DATOS DEL COLABORADOR */
          {table: {
            widths:['*'], body:[['\n'],[{ fillColor: '#000000', text: ' ', fontSize: 4 }]]
          },layout: { defaultBorder: false }},
          { columns: [
              {width:200, text: '\nDATOS DEL COLABORADOR\n\n',   fontSize: 13, bold: true},
              {alignment: 'right', text: '\nFECHA DE SOLICITUD:  '+this.fechaSoli+"\n\n" }
            ]
          },
          this.datosColaboradorPDF(),
          {
            table: {
              widths: [140,'*'],
              body: [
                /**Separador negro */
                [{ colSpan:2, fillColor: '#000000', text: ' ', fontSize: 4 },''],[{text: ' ', fontSize: 4},{text: ' ', fontSize: 4}],
                [{ colSpan: 2,
                    text: 'DATOS GENERALES',   fontSize: 13, bold: true
                  },''
                ],[{text: ' ', fontSize: 4},{text: ' ', fontSize: 4}],//space
                [
                  {text: ' °  CARGO A CUBRIR:        ', fontSize: 12, bold: true},
                  {border: [true, true, true, true], fillColor: '#eeffee', text: this.formSolicitud.value.puesto_a_cubrir.nombrePuesto}
                ],[{text: ' ', fontSize: 4},{text: ' ', fontSize: 4}],//space
                [
                  {text: ' °  SUCURSAL A CUBRIR:         ', fontSize: 12, bold: true},
                  {border: [true, true, true, true], fillColor: '#eeffee', text: this.formSolicitud.value.sucursal_a_cubrir.nombreSucursal}
                ],[{text: ' ', fontSize: 4},{text: ' ', fontSize: 4}],//space
                [
                  {text: ' °  TIPO DE ACTIVACIÓN:         ', fontSize: 12, bold: true},
                  {border: [true, true, true, true], fillColor: '#eeffee', text: this.formSolicitud.value.tipo}
                ],[{text: ' ', fontSize: 4},{text: ' ', fontSize: 4}],//space
              ]
            },layout: { defaultBorder: false }
          },
          /**FECHAS DE ALTA Y BAJA */
          {table:{
            widths: ['*',20,'*'],
              body: [
                [
                  {text: 'FECHA DE ALTA: ',   fontSize: 12, bold: true},'',
                  {text: 'FECHA DE BAJA: ',   fontSize: 12, bold: true},              ],
                [
                  {border: [true, true, true, true], fillColor: '#eeffee', text: this.fecha_alta2},'',
                  {border: [true, true, true, true], fillColor: '#eeffee', text: this.fecha_baja2}
                ]
              ]
            }, layout: { defaultBorder: false }
          },
          this.datosSolicitantePDF(),
          /**NOMBRE Y FIRMA DEL SOLICITANTE */
          { alignment: 'center', text:'\n\nNOMBRE Y FIRMA DEL SOLICITANTE\n' },
          { alignment: 'center',
            image: await this.getBase64ImageFromURL(this.urlFirmasDig),
            fit: [140, 140]
          },
          { alignment: 'center', text: ''+this.solicitante.nomb_compl +'\n________________________________________' },
        ],
      }
      console.log(this.solicitante.nombre+"entra al pdf"+this.solicitante_id);
      const pdf = pdfMake.createPdf(pdfDefinition)
      pdf.download("SOLICITUD-ACTTI "+this.colaborador.nomb_compl);
    //}
  }
   /**Método que genera el PDF de la solicitud de alta de servicios*/
   async createSolicitudSuspPDF(){
      const pdfDefinition: any={
        header: [
          { columns:[
              { width: 250,
                image: await this.getBase64ImageFromURL("/assets/img/sofipa.png"),
                fit: [150, 150]
              },
              { text:[
                  { alignment: 'right', text: 'GTI-FO-BAJATI V1      ', fontSize: 11},
                  { text: '\nSOLICITUD DE SUSPENSIÓN DE SERVICIOS DE TI     ', style: 'header', fontSize: 13.5, bold:true }
              ]},
          ]}
        ],
        content: [
          /**DATOS DEL COLABORADOR */
          {table: {
            widths:['*'], body:[['\n'],[{ fillColor: '#000000', text: ' ', fontSize: 4 }]]
          },layout: { defaultBorder: false }},
          { columns: [
              {width:200, text: '\nDATOS DEL COLABORADOR\n\n',   fontSize: 13, bold: true},
              {alignment: 'right', text: '\nFECHA DE SOLICITUD:  '+this.fechaSoli+"\n\n" }
            ]
          },
          this.datosColaboradorPDF(),
          {
            table: {
              widths: [165,'*'],
              body: [
                [
                  {text: ' °  CUENTA CON REEMPLAZO:        ', fontSize: 12, bold: true},
                  {border: [true, true, true, true], fillColor: '#eeffee', text: this.formSolicitud.value.reemplazo}
                ],[{text: ' ', fontSize: 4},{text: ' ', fontSize: 4}],//space
                 /**Separador negro */
                [{ colSpan:2, fillColor: '#000000', text: ' ', fontSize: 4 },''],[{text: ' ', fontSize: 4},{text: ' ', fontSize: 4}],
                [
                  { colSpan: 2,
                    text: [
                    {text: 'BAJA DE SERVICIOS SOLICITADOS',   fontSize: 13, bold: true},
                    {text: '\n'+this.leerServSuspension()+'\n'}
                    ]
                },''
                ],
                /**Separador negro */
                [{ colSpan:2, fillColor: '#000000', text: ' ', fontSize: 4 },''], [{text: ' ', fontSize: 4},{text: ' ', fontSize: 4}],//space
                [
                  {text: ' °  TIPO DE SUSPENSIÓN:         ', fontSize: 12, bold: true},
                  {border: [true, true, true, true], fillColor: '#eeffee', text: this.formSolicitud.value.tipo}
                ],[{text: ' ', fontSize: 4},{text: ' ', fontSize: 4}],//space
              ]
            },layout: { defaultBorder: false }
          },
          /**FECHAS DE INICIO Y FIN  */
          {table:{
            widths: ['*',20,'*'],
              body: [
                [
                  {text: 'FECHA DE INICIO: ',   fontSize: 12, bold: true},'',
                  {text: 'FECHA DE FINAL: ',   fontSize: 12, bold: true},              ],
                [
                  {border: [true, true, true, true], fillColor: '#eeffee', text: this.fecha_alta2},
                  '',
                  {border: [true, true, true, true], fillColor: '#eeffee', text: this.fecha_baja2}
                ]
              ]
            }, layout: { defaultBorder: false }
          },
          this.datosSolicitantePDF(),
          /**NOMBRE Y FIRMA DEL SOLICITANTE */
          { alignment: 'center', text:'NOMBRE Y FIRMA DEL SOLICITANTE\n' },
          { alignment: 'center',
            image: await this.getBase64ImageFromURL(this.urlFirmasDig),
            fit: [140, 140]
          },
          { alignment: 'center', text: ''+this.solicitante.nomb_compl +'\n________________________________________' },
        ],
      }
      console.log(this.solicitante.nombre+"entra al pdf"+this.solicitante_id);
      const pdf = pdfMake.createPdf(pdfDefinition)
      pdf.download("SOLICITUD-BAJATI "+this.colaborador.nomb_compl);
  }

  /**Lee los servicios solicitados */
  leerServSolicitados(): string{
    this.servSolicitados=this.formSolicitud.value.tipo_servicio;
    let nomServicios="";
    let contador=1;
    this.servSolicitados.forEach(servicio=>{
      nomServicios= nomServicios+"    "+contador+".- "+ servicio.nombre_servicio+"\n"
      contador++;
    });
    return nomServicios
  }

  /**leé servicios solicitados para suspension */
  leerServSuspension(): string{
    let listaSU = this.dataSource.data;
    let nomServicios="";
    let contador=1;
    listaSU.forEach(servicioU=>{
      if(servicioU.status_reg?.toString()=="true"){
        nomServicios= nomServicios+contador+".- "+servicioU.detalleSolicitud.tipo_servicio.nombre_servicio+"  _______   "+servicioU.nomSucursalCol+"\n";
        contador++;
      }
    });
    return nomServicios;
  }

  /**Método que convierte a base 64 a la ruta o url de la imagen */
  getBase64ImageFromURL(url: string) {
    //console.log(url+"url de la imagen");
    //if(url!=""){
      return new Promise((resolve, reject) => {
      var img = new Image();
      img.setAttribute("crossOrigin", "anonymous");
            img.onload = () => {
                var canvas = document.createElement("canvas");
                canvas.width = img.width;
                canvas.height = img.height;

                var ctx = canvas.getContext("2d");
                ctx?.drawImage(img, 0, 0);

                var dataURL = canvas.toDataURL("image/png");
                resolve(dataURL);
            };
            img.onerror = error => {
                reject(error);
            };
            img.src = url;
        })
  }

  /**Selecciona o deselecciona todos los servicios que el empleado tiene activado, en caso de ser una solicitud de suspension  */
  selectAllCheck(accion : boolean){
    if(accion==true){
      /**Selecciona todos los servicios */
      this.dataSource.filteredData.forEach(servicio=>{
        servicio.status_reg=true;
      });
    }else{
      /**Deshace todas las selecciones de los servicios */
      this.dataSource.filteredData.forEach(servicio2=>{
        servicio2.status_reg=false;
      });
    }
  }

  /**Salida: Pregunta si está seguro de salir y si es asi redirige al inicio */
  goBack(){
    Swal.fire({
      icon: 'question',
      title: '¿Está seguro de salir?',
      text: "Al salir no se guardará la información ingresada",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí'
    }).then((result) => {
      if (result.isConfirmed) {
        this.router.navigate(['layout/home/']);
      }
    })
  }

}
