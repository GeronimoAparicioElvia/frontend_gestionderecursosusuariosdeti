import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { SucursalService, UsuarioService } from 'src/app/administracion/servicios';
import { TipoServicio } from 'src/app/administracion/modelos/Control_Usuario/tipo-servicio';
import { ActivatedRoute, Router } from '@angular/router';
import { DetalleSolicitud } from 'src/app/administracion/modelos/Control_Usuario/detalle-solicitud';
import { MatTableDataSource } from '@angular/material/table';
import { SolicitudService } from 'src/app/administracion/servicios/Control_Usuario/solicitud.service';
import { DetalleSolicitudService } from 'src/app/administracion/servicios/Control_Usuario/detalle-solicitud.service';
import { TipoServicioService } from 'src/app/administracion/servicios/Control_Usuario/tipo-servicio.service';
@Component({
  selector: 'app-detalle-solicitud',
  templateUrl: './detalle-solicitud.component.html',
  styleUrls: ['./detalle-solicitud.component.scss']
})
export class DetalleSolicitudComponent implements OnInit {

  dataSource          = new MatTableDataSource<DetalleSolicitud>([]);
  /**Listas utilizadas */
  listaServicios!     :  TipoServicio [];
  servSolicitado!     :  TipoServicio[];
  encabezadostabla     : String [] = ["c1","c2"];

  tipo : string ="";
  tipoSolicitud : string="";
  constructor(
              private router                :Router,
              private sucursalService       : SucursalService,
              private usuarioService        : UsuarioService ,
              private tipoServicioService   : TipoServicioService,
              private solicitudService      : SolicitudService,
              private detalleSolicitudService : DetalleSolicitudService,
              private formBuilder           : FormBuilder,
              private activeRouter          : ActivatedRoute
  ) {
      this.llenarListas();
      this.cargarDatosSolicitud();
    }
    colaborador : number = 0;
    idSolicitud: number = 0;
  /**Datos de la solicitud */
  formDetSolicitud = this.formBuilder.group({
    reemplazo           : [{value: ''}],
    colaborador         : [{value: ''}],
    solicitante         : [{value: ''}],
    tipoSolicitud       : [{value: ''}],
    fecha_planeada      : [{value: ''}],
    sustituye_a         : [{value: ''}],
    supervisor          : [{value: ''}],
    tipo                : [{value: ''}],
    comentarios         : [{value: ''}],
    puesto_a_cubrir     : [{value: ''}],
    sucursal_a_cubrir   : [{value: ''}],
    puesto              : [{value: ''}],
    sucursal            : [{value: ''}],
    puesto_solicitante  : [{value: ''}],
    sucursal_solicitante : [{value: ''}],
    email_solicitante    : [{value: ''}],
    tipo_servicio       : ['',[]],
    tipo_servicio2      : ['',[]],
  })
  /**Campos para la fecha de la solicitud */
  range= this.formBuilder.group({
    fecha_alta: [{value: ''}],
    fecha_baja: [{value: ''}],
  })

  ngOnInit(): void {
  }
  /**Llena la lista de los tipos de servicios */
  private llenarListas(){
    this.tipoServicioService.todosLosServicios().subscribe(val=>{ this.listaServicios = val ;});
  }

  /**Carga los datos de la solicicitud seleccionada */
  cargarDatosSolicitud(){
    this.activeRouter.params.subscribe(params=>{
      this.idSolicitud = params['idSolicitud'];
      //this.idSolicitud=5;
      this.solicitudService.unaSolicitud(this.idSolicitud).subscribe(val=>{
        this.tipo=val.tipo+"";
        this.tipoSolicitud=val?.tipo_solicitud_usuario?.tipo_solicitud+"";
        //Datos del colaborador
        this.formDetSolicitud.controls['tipoSolicitud'].setValue(val?.tipo_solicitud_usuario?.tipo_solicitud);
        this.usuarioService.getAnEmpleado(Number(val.colaboradorId)).subscribe(valor=>{
          this.formDetSolicitud.controls['colaborador'].setValue(valor?.nombre+" "+valor?.apellidoPat+" "+valor?.apellidoMat);
          this.formDetSolicitud.controls['puesto'].setValue(valor?.puesto?.nombrePuesto);
          this.formDetSolicitud.controls['sucursal'].setValue(valor?.sucursal?.nombreSucursal);
        });
        //Datos del solicitante
        this.usuarioService.getAnEmpleado(Number(val.solicitanteId)).subscribe(valor=>{
          this.formDetSolicitud.controls['solicitante'].setValue(valor?.nombre+" "+valor?.apellidoPat+" "+valor?.apellidoMat);
          this.formDetSolicitud.controls['puesto_solicitante'].setValue(valor?.puesto?.nombrePuesto);
          this.formDetSolicitud.controls['sucursal_solicitante'].setValue(valor?.sucursal?.nombreSucursal);
          this.formDetSolicitud.controls['email_solicitante'].setValue(valor?.correoInstitucional);
        });
        //Sucursal a cubrir
        this.sucursalService.getAnSucursal(Number(val.sucursal_id)).subscribe(valor=>{
          this.formDetSolicitud.controls['sucursal_a_cubrir'].setValue(valor?.nombreSucursal);
        });
        //Puesto a cubrir
        this.usuarioService.getAnPuesto(Number(val.puesto_a_cubrir)).subscribe(valor=>{
          this.formDetSolicitud.controls['puesto_a_cubrir'].setValue(valor?.nombrePuesto);
        });
        this.formDetSolicitud.controls['sustituye_a'].setValue(val?.sustituye_a);
        this.formDetSolicitud.controls['supervisor'].setValue(val?.nom_supervisor);
        if(val?.reemplazo+""=="true"){
          this.formDetSolicitud.controls['reemplazo'].setValue("SI");
        }else if(val?.reemplazo+""=="false"){
                this.formDetSolicitud.controls['reemplazo'].setValue("NO");
               }
        this.formDetSolicitud.controls['tipo'].setValue(val.tipo);
        this.formDetSolicitud.controls['comentarios'].setValue(val?.comentarios);
        this.formDetSolicitud.controls['fecha_planeada'].setValue(val?.fecha_planeada_act?.substring(0,10));
        this.range.controls['fecha_alta'].setValue(val?.fecha_alta);
        this.range.controls['fecha_baja'].setValue(val?.fecha_baja);
      });
      this.llenarTablaServicios();
    });
  }

  /**Llena la tabla con los servicios correspondientes a esa solicitud y muestra el status */
  llenarTablaServicios(){
    let listaDetSolicitudes: DetalleSolicitud[]=[];
    this.detalleSolicitudService.listaDetSolicitudPorSolicitudId(this.idSolicitud).subscribe(valor=>{
      valor.forEach(element=>{
        let detSolicitud : DetalleSolicitud=new DetalleSolicitud();
        detSolicitud.nombre_servicio=element.tipo_servicio.nombre_servicio;
        detSolicitud.status_serv=element.status_serv;
        listaDetSolicitudes.push(detSolicitud);
        this.dataSource.data=listaDetSolicitudes;
      });
    });
  }

  /**Retorna true o false dependiendo de la solicitud que se le pasa como parámetro */
  tipoSoliSeleccionada(tipoSoli : string): boolean{
    if(tipoSoli == this.tipoSolicitud){
      return true;
    }
    return false;
  }

  /** Retorna true o false dependiendo del tipo que se le pasa como parámetro, ya sea temporal o permanente*/
  tipoSeleccionado(tipo:string){
    if(tipo==this.tipo)
      return true;
    return false;
  }
  /** Permite regresar a la gestion de solicitudes*/
  goBack(){
    this.router.navigate(['layout/gestionSolicitudes/' ]);
  }

}
