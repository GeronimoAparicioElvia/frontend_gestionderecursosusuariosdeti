import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Solicitud } from 'src/app/administracion/modelos/Control_Usuario/solicitud';
import { TipoSolicitud } from 'src/app/administracion/modelos/Control_Usuario/tipo-solicitud';
import { Empleado } from 'src/app/administracion/modelos/empleado';
import { UsuarioService } from 'src/app/administracion/servicios';
import { DetalleSolicitudService } from 'src/app/administracion/servicios/Control_Usuario/detalle-solicitud.service';
import { SolicitudService } from 'src/app/administracion/servicios/Control_Usuario/solicitud.service';
import { TipoSolicitudService } from 'src/app/administracion/servicios/Control_Usuario/tipo-solicitud.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-gestion-solicitudes',
  templateUrl: './gestion-solicitudes.component.html',
  styleUrls: ['./gestion-solicitudes.component.scss']
})
export class GestionSolicitudesComponent implements OnInit {
  /**Tablas utilizadas, Espera, Atendido, Cancelado */
  dataSourceE         = new MatTableDataSource<Solicitud>([]);
  dataSourceA         = new MatTableDataSource<Solicitud>([]);
  dataSourceC         = new MatTableDataSource<Solicitud>([]);
  statusSolicitud="";

   /* Paginators */
   @ViewChild('paginatorE', { static: true }) public paginatorE !: MatPaginator;
   @ViewChild('paginatorA', { static: true }) public paginatorA !: MatPaginator;
   @ViewChild('paginatorC', { static: true }) public paginatorC !: MatPaginator;

  /**Listas utilizadas */
  listaEmpleados       : Empleado[]=[];
  listatipoSolicitudes : TipoSolicitud[]=[];
  encabezadostabla     : String [] = ["opciones","nomColaborador","nomPuesto","name_tipo_solicitud","nomSolicitante","fechaSolicitud"];
  encabezadostablaAten : String [] = ["opciones","nomColaborador","nomPuesto","name_tipo_solicitud","nomSolicitante","fechaSolicitud", "fechaModificacion"];
  nomEncabezadoFecha   : String="";
  /**ngModels de busqueda */
  datoBuscar           : String="";
  datoBuscar2          : String="";
  datoBuscar3          : String="";

  constructor(
    private router                : Router,
    private solicitudService      : SolicitudService,
    private detalleSolicitudService : DetalleSolicitudService,
    private tipoSolicitudService  : TipoSolicitudService,
    private usuarioService        : UsuarioService
  ) { this.llenarListas();
  }

  ngOnInit(): void {
    this.llenarTabla(0);
  }

  /**Llena las listas de empleados y tipos de solicitud */
  llenarListas(){
    this.usuarioService.getAllEmpleados().subscribe(val=>{this.listaEmpleados=val;});
    this.tipoSolicitudService.todosLosTiposSolicitud().subscribe(val=>{this.listatipoSolicitudes = val;});
  }

  /**Botón que muestra información del funcionamiento de esta vista */
  botonInformacion(){
    Swal.fire({
      position: 'top-end',
      icon: 'info',
      title: 'SOLICITUDES REALIZADAS',
      showConfirmButton: false,
      html:
           '<b>SOLICITUDES PENDIENTES</b>'
           +'<p align="justify"> Podrá visualizar o cancelar las solicitudes que aun no han sido atendidas</p>'
           +'<b>SOLICITUDES ATENDIDAS O CANCELADAS</b>'
           +'<p align="justify" >Sólo podrá visualizar a detalle cada solicitud</p>'
    })
  }

  /**Método para llenar la tabla de datos */
  llenarTabla(index : number){
    let listaSolicitud: Solicitud[]=[];

    if(index==0){this.statusSolicitud="Espera";}
    if(index==1){this.statusSolicitud="Atendido"; this.nomEncabezadoFecha="FECHA DE ATENCIÓN" }
    if(index==2){this.statusSolicitud="Cancelado"; this.nomEncabezadoFecha="FECHA DE CANCELACIÓN"}
    //Se realiza la busqueda de los elementos
    this.solicitudService.todasLasSolicitudes().subscribe(response=>{
      response.forEach(element=>{
        if(element.status_solicitud==this.statusSolicitud){
          let solicitud : Solicitud=new Solicitud();
          solicitud.id_solicitud=element.id_solicitud;
          this.usuarioService.getAnEmpleado(Number(element.colaboradorId)).subscribe(val=>{ solicitud.name_colaborador_id=val?.nombre+" "+val?.apellidoPat+" "+ val?.apellidoMat; solicitud.name_cargo_colaborador= val?.puesto?.nombrePuesto;});
          this.usuarioService.getAnEmpleado(Number(element.solicitanteId)).subscribe(val=>{solicitud.name_solicitante_id=val?.nombre+" "+val?.apellidoPat+" "+ val?.apellidoMat;});
          this.listatipoSolicitudes.forEach(val=>{
            if(val.id_tipo_solicitud_usuario==element.tipo_solicitud_usuario.id_tipo_solicitud_usuario){
              element.name_tipo_solicitud=val.tipo_solicitud;
              solicitud.name_tipo_solicitud=val.tipo_solicitud;
            }
            solicitud.fechaSolicitud= element.fechaSolicitud?.substring(0,10);
            solicitud.fechaModificacion=element.fechaModificacion?.substring(0,10);
          });
          listaSolicitud.push(solicitud);
        }
      });
      if(index==0 && this.dataSourceE.data.length==0){ this.dataSourceE.data=listaSolicitud;        this.dataSourceE.paginator=this.paginatorE;
      }//else
      if(index==1 && this.dataSourceA.data.length==0){this.dataSourceA.data=listaSolicitud;   this.dataSourceA.paginator=this.paginatorA;
      }
      //else
      if(index==2 && this.dataSourceC.data.length==0){      this.dataSourceC.data=listaSolicitud;     this.dataSourceC.paginator=this.paginatorC;
            }
    }, err=>{
      Swal.fire({
        position: 'center',
        icon: 'error',
        title: 'Error de conexión',
        text: 'Lo sentimos las solicitudes no se cargaron correctamente debido a que no hay conexión con el servidor, inténtelo más tarde o contacte al área de T.I. ',
      })
    });
  }

  /**Metodos que filtran las busqueda en la tabla de solicitudes en ESPERA */
  applyFilterE(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceE.filter = filterValue.trim().toLowerCase();
  }

    /**Metodos que filtran las busqueda en la tabla de solicitudes ANTENDIDAS */
  applyFilterA(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceA.filter = filterValue.trim().toLowerCase();
  }

  /**Metodos que filtran las busqueda en la tabla de solicitudes CANCELADAS */
  applyFilterC(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceC.filter = filterValue.trim().toLowerCase();
  }

  /**Método para cancelar una solicitud */
  async cancelarSolicitud(idSoli:number){
    await Swal.fire({
      title: '¿Estás seguro de que deseas cancelar esta solicitud?',
      text: "¡Ya no podrá revertir esta acción!",
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'No, regresar',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, cancelar solicitud'
    }).then((result) => {
      if (result.isConfirmed) {
        this.solicitudService.actualizarStatusSolicitud(idSoli,"Cancelado","")/*.subscribe(resul=>{
          this.llenarTabla(2);
        });*/
        this.detalleSolicitudService.listaDetSolicitudPorSolicitudId(idSoli).subscribe(detalles=>{
          detalles.forEach(detalle=>{
            this.detalleSolicitudService.actualizarStatusDetSolicitud(Number(detalle.id_detalle_solicitud), "Cancelado")/*.subscribe( response=>{
              console.log(response+"Actualizado con exito");
            });*/
          });
           Swal.fire(
            '¡Solicitud cancelada!',
            'La solicitud ha sido cancelada',
            'success'
         );
         this.llenarTabla(2);
        });
      }
    });
  }

  /**Redirecciona al detalle de la solicitud */
  detalleSolicitud(idSolicitud: number){
    this.router.navigate(['layout/DetalleSolicitudSolicitante/' + idSolicitud]);
  }
}
