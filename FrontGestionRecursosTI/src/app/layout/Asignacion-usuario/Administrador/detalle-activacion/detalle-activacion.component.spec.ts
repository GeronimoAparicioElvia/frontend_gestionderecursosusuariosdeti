import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleActivacionComponent } from './detalle-activacion.component';

describe('DetalleActivacionComponent', () => {
  let component: DetalleActivacionComponent;
  let fixture: ComponentFixture<DetalleActivacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetalleActivacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleActivacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
