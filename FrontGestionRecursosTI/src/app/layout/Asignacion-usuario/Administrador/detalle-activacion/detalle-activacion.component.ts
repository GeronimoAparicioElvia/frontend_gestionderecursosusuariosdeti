import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { SucursalService, UsuarioService } from 'src/app/administracion/servicios';
import { TipoServicio } from 'src/app/administracion/modelos/Control_Usuario/tipo-servicio';
import { ActivatedRoute, Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { ServicioUsuario } from 'src/app/administracion/modelos/Control_Usuario/servicio-usuario';
import Swal from 'sweetalert2';
import { TipoServicioService } from 'src/app/administracion/servicios/Control_Usuario/tipo-servicio.service';
import { SolicitudService } from 'src/app/administracion/servicios/Control_Usuario/solicitud.service';
import { DetalleSolicitudService } from 'src/app/administracion/servicios/Control_Usuario/detalle-solicitud.service';
import { ServicioUsuarioService } from 'src/app/administracion/servicios/Control_Usuario/servicio-usuario.service';
import { DetalleSolicitud } from 'src/app/administracion/modelos/Control_Usuario/detalle-solicitud';
import { Empleado } from 'src/app/administracion/modelos/empleado';
import { ElemServicioService } from 'src/app/administracion/servicios/Control_Usuario/elem-servicio.service';
import { Sucursal } from 'src/app/administracion/modelos/sucursal';


@Component({
  selector: 'app-detalle-activacion',
  templateUrl: './detalle-activacion.component.html',
  styleUrls: ['./detalle-activacion.component.scss']
})
export class DetalleActivacionComponent implements OnInit {

  dataSource          = new MatTableDataSource<ServicioUsuario>([]);
  dataSourceAcSus     = new MatTableDataSource<ServicioUsuario>([]);
  dataActivados       = new MatTableDataSource<ServicioUsuario>([]);
  dataAtendidos       = new MatTableDataSource<DetalleSolicitud>([]);
  /**Llena la tabla que corresponde a los servicios activados de esta solicitud */
  dataServAct         = new MatTableDataSource<ServicioUsuario>([]);
  /**Listas utilizadas */
  listaServicios      :  TipoServicio []=[];
  listaSucursales     :  Sucursal[]=[];
  listaSUxUsuario     :  ServicioUsuario []=[];
  serviciosAdd        :  TipoServicio[]=[];
  servSolicitados     :  ServicioUsuario[]=[];
  servSolicitadosAct  :  ServicioUsuario[]=[];
  /**Encabezados de las tablas */
  encabezadostabla    :  String [] = ["c1","c2","c3","c4","c5"];
  encabezadostabla6   :  String [] = ["c1","c2","c3","c4","c5","c6"];
  encabezadostabla4   :  String [] = ["c1","c2","c3","c4"];
  encabezadostabAtendidos : String [] = ["c1","c2"];

  cabeceraCheck       : String ="";
  statusComparar      : String ="";
  sucursal_colab      : String ="";
  sucursal_cubrir     : String ="";
  colaborador         : Empleado= new Empleado();
  banderaAct          : boolean  =true;/**Indica que los servicios de alta ya estan bien si se puede reallizar la activación */
  bandera             : boolean =true;/**bandera que establece sí se puede enviar la solicitud o  no*/
  errorGmail      : boolean=false;//Indica si hay error en el gmail
  /**Expresion regular para validar el correo electrónico */
  expReg           : RegExp=/^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/


  tipo : string ="";
  tipoSolicitud : string="";
  statusSolicitud : string="";
  constructor(
              private router                :Router,
              private sucursalService       : SucursalService,
              private usuarioService        : UsuarioService ,
              private tipoServicioService   : TipoServicioService,
              private solicitudService      : SolicitudService,
              private detalleSolicitudService : DetalleSolicitudService,
              private servicioUsuarioService : ServicioUsuarioService,
              private elemServicioService    : ElemServicioService,
              private formBuilder           : FormBuilder,
              private activeRouter          : ActivatedRoute
  ) {
      this.cargarDatosSolicitud();
      this.llenarTablaServicios();
      this.llenarListas();
    }
    idSolicitud: number = 0;
    banderaE   : string="";

    /**Campos de la solicitud realizada */
  formDetSolicitud = this.formBuilder.group({
    colaborador         : [{value: '' }],
    solicitante         : [{value: ''}],
    tipoSolicitud       : [{value: ''}],
    fecha_planeada      : [{value: ''}],
    tipo                : [{value: ''}],
    comentarios         : [{value: ''}],
    puesto_a_cubrir     : [{value: ''}],
    sucursal_a_cubrir   : [{value: ''}],
    puesto              : [{value: ''}],
    sucursal            : [{value: ''}],
    puesto_solicitante  : [{value: ''}],
    sucursal_solicitante : [{value: ''}],
    email_solicitante    : [{value: ''}],
    tipo_servicio       : ['',[]],
    nom_usuario         : ['',[]],
    contrasenia         : ['',[]]

  })
  /**Campos utilizados para el rango de fechas */
  range= this.formBuilder.group({
    fecha_alta: [{value: ''}],
    fecha_baja: [{value: ''}],
  })

  ngOnInit(): void {
  }
  /**Metódo para ordenar las sucursales por nombre e ignorar signos de puntuación */
  SortArray(x: Sucursal, y: Sucursal){
    return x.nombreSucursal.localeCompare(y.nombreSucursal, 'fr', {ignorePunctuation: true});
  }
  /**Llena las listas de sucursales y tipo de servicio */
  private llenarListas(){
    this.sucursalService.getSucursales().subscribe(sucursales=>{ this.listaSucursales=sucursales.sort(this.SortArray);  });
    /**Llena el select que el usuario utiliza para seleccionar los servicios  para los cuales requiere activación */
    this.tipoServicioService.todosLosServicios().subscribe(tipoServicios=>{
      this.listaServicios =[];
      tipoServicios.forEach(tipoServ=>{
        if (tipoServ.status_servicio=="ACTIVO"){
          let comparar=this.dataSourceAcSus.data.find(element=>element.detalleSolicitud.tipo_servicio.id_tipo_servicio==tipoServ.id_tipo_servicio);
          let comparar2=this.dataSource.data.find(element=>element.detalleSolicitud.tipo_servicio.id_tipo_servicio==tipoServ.id_tipo_servicio);
          if(comparar==undefined && comparar2==undefined){    this.listaServicios.push(tipoServ); }
        }
      });
    });
  }
  /**Llena una lista de los servicios dados de alta  */
  listaSerAlta(idColab: number){
    this.servicioUsuarioService.ListaServicioUPorColaboradorId(Number(idColab)).subscribe(servicios=>{ this.listaSUxUsuario=servicios;});
  }
  /**Carga los datos de la solicitud y se los asigna al formulario*/
  cargarDatosSolicitud(){
    this.activeRouter.params.subscribe(params=>{
      this.idSolicitud = params['idSolicitud'];
      this.banderaE    = params['banderaE'];
      //this.idSolicitud=75;//144/19

      this.solicitudService.unaSolicitud(this.idSolicitud).subscribe(val=>{
        this.tipo=val.tipo+"";
        this.statusSolicitud= val.status_solicitud+"";
        this.tipoSolicitud=val?.tipo_solicitud_usuario?.tipo_solicitud+"";
        //Datos del colaborador
        this.formDetSolicitud.controls['tipoSolicitud'].setValue(val?.tipo_solicitud_usuario?.tipo_solicitud);
        this.usuarioService.getAnEmpleado(Number(val.colaboradorId)).subscribe(valor=>{
          this.formDetSolicitud.controls['colaborador'].setValue(valor?.nombre+" "+valor?.apellidoPat+" "+valor?.apellidoMat);
          this.formDetSolicitud.controls['puesto'].setValue(valor?.puesto?.nombrePuesto);
          this.formDetSolicitud.controls['sucursal'].setValue(valor?.sucursal?.nombreSucursal);
          this.sucursal_colab=valor?.sucursal?.nombreSucursal+"";
          this.colaborador=valor;
        });
        //Datos del solicitante
        this.usuarioService.getAnEmpleado(Number(val.solicitanteId)).subscribe(valor=>{
          this.formDetSolicitud.controls['solicitante'].setValue(valor?.nombre+" "+valor?.apellidoPat+" "+valor?.apellidoMat);
          this.formDetSolicitud.controls['puesto_solicitante'].setValue(valor?.puesto?.nombrePuesto);
          this.formDetSolicitud.controls['sucursal_solicitante'].setValue(valor?.sucursal?.nombreSucursal);
          this.formDetSolicitud.controls['email_solicitante'].setValue(valor?.correoInstitucional);
        });
        //Sucursal a cubrir
        this.sucursalService.getAnSucursal(Number(val.sucursal_id)).subscribe(valor=>{
          this.formDetSolicitud.controls['sucursal_a_cubrir'].setValue(valor?.nombreSucursal);
          this.sucursal_cubrir=valor?.nombreSucursal+"";
        });
        //Puesto a cubrir
        this.usuarioService.getAnPuesto(Number(val.puesto_a_cubrir)).subscribe(valor=>{
          this.formDetSolicitud.controls['puesto_a_cubrir'].setValue(valor?.nombrePuesto);
        });

        this.formDetSolicitud.controls['tipo'].setValue(val.tipo);
        this.formDetSolicitud.controls['comentarios'].setValue(val?.comentarios);
        this.range.controls['fecha_alta'].setValue(val?.fecha_alta);
        this.range.controls['fecha_baja'].setValue(val?.fecha_baja);
        this.llenarTablaAtendidos(this.idSolicitud);
        this.llenarTablaServAct();

        //Ejecutar metodo para llenar la tabla segun se necesite- activación
                this.cabeceraCheck="ACTIVAR ";
                this.statusComparar="Alta";
                this.listaSerAlta(Number(val.colaboradorId));
                //this.llenarTablaActivados(Number(val.colaboradorId), Number(val.sucursal_id) );
      });
    });
  }
  /*
  /**Llena la tabla con los servicios que el colaborador ya tiene activados para la sucursal que solicitó *
  llenarTablaActivados(colaborador_id: number, sucursal_cubrir_id: number){
    this.dataActivados.data=[];

    let listaServicios2: ServicioUsuario[]=[];
    this.elemServicioService.listaElemServicioForColaboradorSucursal(colaborador_id, sucursal_cubrir_id).subscribe(elementos=>{
      elementos.forEach(elemento=>{
        if(elemento.status_registro=="Activado"){
          let regUsuario : ServicioUsuario=new ServicioUsuario();
          regUsuario=elemento.servicio_usuario;
          listaServicios2.push(regUsuario);
          this.dataActivados.data=listaServicios2;
        }
      });
    }, err=>{console.log(err)});
  }*/
  /**Llena ambas datas para los servicios que requieren solo activacion y para los que requieren alta y activación */
  llenarTablaServicios(){
    //limpiar tabla
    this.dataSource.data=[];
    let listaServicioUsuario: ServicioUsuario[]=[];
    let listaServicioUsuarioAct: ServicioUsuario[]=[];
    this.detalleSolicitudService.listaDetSolicitudPorSolicitudId(this.idSolicitud).subscribe(valor=>{
      valor.forEach(element=>{//element==detalle
        //Llena la tabla para los servicios que requieren alta y activación
        if(element.servicio_usuario_id==null){
          let regUsuario : ServicioUsuario=new ServicioUsuario();
           /**Expresion regular en caso de ser gmail */
          if(element.tipo_servicio.nombre_servicio?.includes("GMAIL")){
            /**Valida que el correo ingresado sea válido */
            regUsuario.expresionRegular="^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$";
          }else{
            regUsuario.expresionRegular="";
          }
          regUsuario.detalleSolicitud=element;
          regUsuario.nombre_servicio=element.tipo_servicio.nombre_servicio;
          regUsuario.status_serv=element.status_serv;
          regUsuario.status_reg=true;
          regUsuario.detalle_solicitud_id=element.id_detalle_solicitud;
          listaServicioUsuario.push(regUsuario);
          this.dataSource.data=listaServicioUsuario;
        }else{
          //Llena la tabla para los servicios que requieren sólo activación
          let regUsuarioAct : ServicioUsuario=new ServicioUsuario();
          this.servicioUsuarioService.unServicioUsuario(Number(element.servicio_usuario_id)).subscribe(servicio=>{
            regUsuarioAct.id_servicio_usuario=servicio.id_servicio_usuario;
            regUsuarioAct.usuario= servicio.usuario;
            regUsuarioAct.contrasenia=servicio.contrasenia;
            regUsuarioAct.detalleSolicitud=element;
            regUsuarioAct.status_reg=true;
            listaServicioUsuarioAct.push(regUsuarioAct);
            this.dataSourceAcSus.data=listaServicioUsuarioAct;
          });
        }
      });
      this.llenarListas();
    });
  }

  /**Agregar detalle de alta de servicios que añade el administrador */
  generarDetalleSolicitudAlta(){
    /**Corresponde a una solicitud de activacion y genera los detalles para dar de alta servicios*/
    this.serviciosAdd=this.formDetSolicitud.value.tipo_servicio;
    this.serviciosAdd.map(val=>{
      const compara  = this.listaSUxUsuario.find(element=>element.detalleSolicitud.tipo_servicio.id_tipo_servicio==val.id_tipo_servicio);
      if (compara!=undefined){
        this.detalleSolicitudService.generarDetalleSolicitud(this.idSolicitud, Number(val.id_tipo_servicio), Number(compara.id_servicio_usuario)).subscribe(data=>{
          console.log(data);
          this.llenarTablaServicios();
        }, err =>{console.log(err)});
      }else{
        /**GENERA LOS DETALLES DE UNA SOLICITUD DE ACTIVACIÓN  */
        this.detalleSolicitudService.generarDetalleSolicitud(this.idSolicitud,Number(val.id_tipo_servicio), 0).subscribe(data=>{
          console.log("detalle generado");
          this.llenarTablaServicios();
        });
      }
    });
    this.formDetSolicitud.controls['tipo_servicio'].setValue([]);
  }
  /**Llena la tabla al momento de mostrar una solicitud atendida */
  llenarTablaAtendidos(idSolicitud: number){
    let listaDetSolicitudes: DetalleSolicitud[]=[];
    this.detalleSolicitudService.listaDetSolicitudPorSolicitudId(idSolicitud).subscribe(valor=>{
      valor.forEach(element=>{
        let detSolicitud : DetalleSolicitud=new DetalleSolicitud();
        detSolicitud.nombre_servicio=element.tipo_servicio.nombre_servicio;
        detSolicitud.status_serv=element.status_serv;
        listaDetSolicitudes.push(detSolicitud);
        this.dataAtendidos.data=listaDetSolicitudes;
      });
    });
  }
  /**Llena la tabla al momento de mostrar una solicitud atendida con las credenciales de los servicios que se le fueron otorgadas */
  llenarTablaServAct(){
    let listaServAct : ServicioUsuario[]=[];
    this.elemServicioService.listaElemServicioIdSolicitudAct(this.idSolicitud).subscribe(elementos=>{
      elementos.forEach(elemento=>{
        let servicio : ServicioUsuario = new ServicioUsuario();
        servicio=elemento.servicio_usuario;
        this.sucursalService.getAnSucursal(Number(elemento.sucursal_id)).subscribe(sucursal=>{
          servicio.nomSucursalCol=sucursal.nombreSucursal;
        });
        listaServAct.push(servicio);
      });
      this.dataServAct.data= listaServAct;
    });
  }
  /**Método para rechazar una solicitud y todos sus servicios */
  async rechazarSolicitud(){
    await Swal.fire({
      title: '¿Está seguro de que desea rechazar esta solicitud?',
      text: "¡Ya no podrá revertir esta acción!",
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'No, cancelar',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, rechazar solicitud'
    }).then(async result => {
      if (result.isConfirmed) {
        const { value: text } = await Swal.fire({
          title: 'Razón por la cual rechaza la solicitud: ',
          input: 'text',
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'Aceptar',
          allowEnterKey:false,
          allowEscapeKey:false,
          allowOutsideClick:false
        })
        if (text){/**valida que se haya ingresado una razón */
          //Editamos el status de la solicitud
          let comentarios= "Motivo del rechazo de la solicitud: "+text;
          //Editamos el status de la solicitud
          this.solicitudService.actualizarStatusSolicitud(this.idSolicitud,"Atendido", comentarios);
          //Se edita el status de los detalles de la solicitud
          this.detalleSolicitudService.listaDetSolicitudPorSolicitudId(this.idSolicitud).subscribe(valor=>{
            valor.forEach(element=>{
              this.detalleSolicitudService.actualizarStatusDetSolicitud(Number(element.id_detalle_solicitud),"Rechazado")
            });
          });
          this.router.navigate(['layout/gestionSolicitudesAdmin/'+this.tipoSolicitud ]);
          //mensaje  de confirmación
          Swal.fire(
            '¡Solicitud rechazada!',
            'La solicitud ha sido rechazada',
            'success'
          );
        }else{
          Swal.fire({
            title: 'No se rechazó la solicitud, es necesario insertar un motivo por el cual se desea rechazar.',
            icon:'warning',
            timer:3000,
            showConfirmButton:false,
            allowEnterKey:false,
            allowEscapeKey:false,
            allowOutsideClick:false
          })
        }
      }
    });
  }
  /**Método que genera y registra el alta y activación de los servicios que fueron solicitados */
  generarAltaActServicio( ){
    this.bandera=true;
    this.banderaAct=true;
    let listaSU: ServicioUsuario[]=[];
    //let co =0
    this.servSolicitados=[];
      listaSU=this.dataSource.data;
    let cont=0;
    listaSU.forEach(valor=>{
      if(valor.status_reg==true){ //valor que el usuario seleccionó //valor.status_reg?.toString()=="true"
        cont++;
        if(valor.sucursales.length>0){
          /**Verifica que si haya ingresado usuario y contraseña */
          if(valor.usuario!="" && valor.contrasenia!="" && valor.usuario!=null && valor.contrasenia!=null){
            /**Preguntra si el servicio es gmail, ya que debe validarlo */
            if(valor.detalleSolicitud.tipo_servicio.nombre_servicio?.includes("GMAIL")){
              /**valida que el dato ingresado en el usuario sea un correo electrónico válido */
              if(this.expReg.test(valor.usuario)){
                /**Si es asi lo agrega a la lista y establece que ya no hay error en el gmail */
                this.errorGmail=false;
                this.servSolicitados.push(valor);
              }else{
                /**En caso de ser erróneo el correo electrónico este emitira un mensaje de error */
                /**Indica que el gmail es incorrecto */
                this.errorGmail=true;
                Swal.fire({
                  icon: 'info',
                  title: 'Correo electrónico inválido',
                  text: 'El valor ingresado en GMAIL no es válido. Porfavor ingrese el dato con formato de correo correcto.',
                  showConfirmButton: false,
                  timer: 3000
                });
              }
            }else{
              /**Agrega el servicio  */
              if(this.errorGmail==false){
                this.servSolicitados.push(valor);
              }
            }
          }else{
            /**bandera que establece que aun no se puede enviar la solicitud */
            this.bandera=false;
            this.banderaAct=false;
            Swal.fire({
              icon: 'info',
              title: 'Faltan campos',
              text: 'Es necesario que le asigne "Usuario" y "Contraseña" a todos los servicios aprobados',
              showConfirmButton: false,
              timer: 3000
            });
          }
        }else{
          /**bandera que establece que aun no se puede enviar la solicitud */
          this.bandera=false;
          Swal.fire({
            icon: 'info',
            title: 'Faltan campos',
            text: 'Debe seleccionar al menos una sucursal de cada servicio aprobado',
            showConfirmButton: false,
            timer: 3000
          });
        }
      }else{
        /**Rechaza los servicios no seleccionados de la solicitud */
        this.detalleSolicitudService.actualizarStatusDetSolicitud(Number(valor.detalle_solicitud_id),"Rechazado")
      }
    });
    if(cont==0){//Si no hay servicios para dar de alta
      let nSerActivos=this.validarSucursalesAct();
      if(nSerActivos>0){//Valida que se haya elegido al menos un servicio para aprobar
      //console.log(aprobarAct+"cuando no agrega altas aprobarAct ");
        console.log(this.bandera+"banderadeba alt");
        if(this.bandera==true){
          /**Genera la activación de los servicios que solo requieren activación, sin necesidad de darlos de alta  */
          this.generarActivacionServicio();
          if(this.banderaAct==true){//Ya puede activar los servicios que fueron activados
            this.solicitudService.actualizarStatusSolicitud(this.idSolicitud,"Atendido","");
            this.router.navigate(['layout/gestionSolicitudesAdmin4/'+this.tipoSolicitud+'/'+this.idSolicitud]);
          }
        }
      }else{
        this.bandera=false;
        Swal.fire({
          icon: 'info',
          title: 'No ha seleccionado ningun servicio',
          text: 'Debe seleccionar el o los servicios que desea aprobar',
          showConfirmButton: false,
          timer: 3000
        });
      }
    }else{
          console.log(this.bandera+"bandera de solicitud" +this.errorGmail +"error gmail ");
          /**Permite validar que los servicios selecionados para activación tengan al menos una sucursal asignada */
          this.validarSucursalesAct();
          //console.log (aprobarAct+"aprobar");
          //guardar
          if(this.bandera==true && this.errorGmail==false){//Sí todos los servicios seleccionados ya cuentan con usuario y contraseña y no hay error en el gmail
            //Editamos el status de la solicitud se realizara al final de los dos metodos
            //this.solicitudService.actualizarStatusSolicitud(this.idSolicitud,"Atendido");

            //Se crea un registro de servicio usuario
              this.servSolicitados.forEach(val=>{
                let usuario= val.usuario+"";
                let contrasenia= val.contrasenia+"";
                let id_detalle=Number(val.detalle_solicitud_id);
                this.detalleSolicitudService.actualizarStatusDetSolicitud(Number(val.detalle_solicitud_id),"Aceptado");
                //se insertaba un status de alta
                this.servicioUsuarioService.guardarServicioUsuario(usuario, contrasenia,id_detalle).subscribe(servicioU=>{
                  val.sucursales.forEach(sucursalSelect=>{
                    this.elemServicioService.agregarElemServicio(Number(servicioU.id_servicio_usuario),Number(val.detalle_solicitud_id),Number(sucursalSelect.idSucursal) ).subscribe(data=>{
                      Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Solicitud aprobada',
                        text: ' Los servicios fueron registrados y dados de alta correctamente',
                        });
                    }, err=>{ console.log(err+"error en el elemento");
                      Swal.fire({
                      position: 'center',
                      icon: 'error',
                      title: 'Aprobar solicitud',
                      text: ' Los servicios no fueron registrados correctamente, por favor intentelo más tarde',
                      })

                    });
                  });
                }/*, err => { console.log(err+"error al guardar el servicio");
                  Swal.fire({
                  position: 'center',
                  icon: 'error',
                  title: 'Aprobar solicitud',
                  text: ' Los servicios no fueron registrados correctamente, por favor intentelo más tarde',
                  })
               }*/);
              });
              /**Genera la activación de los servicios que solo requieren activacion  */
              this.generarActivacionServicio();
              if(this.banderaAct==true){/**Los servicios de alta ya estan bien */
                this.solicitudService.actualizarStatusSolicitud(this.idSolicitud,"Atendido","");
                this.router.navigate(['layout/SeguimientoControlU/'+this.idSolicitud]);
              }
          }
        }
  }
  /**genera la activacion de los servicios que fueron solicitados y sólo requieren activación*/
  generarActivacionServicio(){
    let listaSUAct: ServicioUsuario[]=[];
    listaSUAct=this.dataSourceAcSus.data;
    let cont=0;
    listaSUAct.forEach(valor=>{
      if(valor.status_reg==true){ //valor que el usuario seleccionó //valor.status_reg?.toString()=="true"
        cont++;
        this.servSolicitadosAct.push(valor);
      }else{//Se rechazan las que no fueron seleccionadas
        this.detalleSolicitudService.actualizarStatusDetSolicitud(Number(valor.detalleSolicitud.id_detalle_solicitud),"Rechazado")
      }
    });
    if(cont==0){ //No selecciono ninguna de activacion
    }else
      if(this.banderaAct==true){
        //Editamos el status de los detalles de la solicitud que solo requieren activación
        this.servSolicitadosAct.forEach(val=>{
          this.detalleSolicitudService.actualizarStatusDetSolicitud(Number(val.detalleSolicitud.id_detalle_solicitud),"Aceptado");
          let id_servUsu=Number(val.id_servicio_usuario);
          val.sucursales.forEach(sucursalSelect=>{
            this.elemServicioService.agregarElemServicio(id_servUsu,Number(val.detalleSolicitud.id_detalle_solicitud),Number(sucursalSelect.idSucursal)).subscribe(data=>{
              console.log(data);
              Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Solicitud atendida',
                text: ' Los servicios aprobados fueron activados correctamente',
                })
            }, err => {
              Swal.fire({
              position: 'center',
              icon: 'error',
              title: 'Aprobar solicitud',
              text: ' Los servicios no fueron activados correctamente, por favor intentelo más tarde',
              })
            });
          });
        });
      }
  }
  /**Valida el servicio aceptado que solo va a ser activado, cuente con al menos una sucursal seleccionada */
  validarSucursalesAct(){
    let listaA: ServicioUsuario[]=[];
    listaA=this.dataSourceAcSus.data;
    let cont=0;//Indica la cantidad de servicios que seleccionó
    listaA.forEach(serAct=>{
      if(serAct.status_reg==true){
        cont++
        if(serAct.sucursales.length>0){
          //return true;
        }else{
          /**bandera que establece que aun no se puede enviar la solicitud */
          this.bandera=false;
          console.log(this.bandera+"validar sucu");
          Swal.fire({
            icon: 'info',
            title: 'Faltan campos',
            text: 'Debe seleccionar al menos una sucursal de cada servicio aprobado',
            showConfirmButton: false,
            timer: 3000
          });
          //return false;
        }
      }
    });
    return cont;
  }
  /**Leé las sucursales de la lista y las regresa en una cadena sepraradas por " - "  */
  leerSucursales(sucursales : Sucursal[]): string{
    let nomSucursales="";
    sucursales.forEach(sucursal=>{
      nomSucursales=nomSucursales+sucursal.nombreSucursal+" - ";
    });
    return nomSucursales;
  }
  /**Valida cuando ya no hay mas servicios para mostrar y ya todos fueron seleccionados */
  dataVacia(){
    if(this.listaServicios.length==0){
      return false;
    }else {
      return true;
    }
  }
  tipoSeleccionado(tipo:string){
    if(tipo==this.tipo)
      return true;
    return false;
  }
  /**Salida */
  goBack(){
    if(this.banderaE=="true"){
      this.router.navigate(['layout/Expedientes2/'+this.colaborador.idEmpleado ]);
    }else{
      this.router.navigate(['layout/gestionSolicitudesAdmin/'+this.tipoSolicitud ]);
    }
  }
}
