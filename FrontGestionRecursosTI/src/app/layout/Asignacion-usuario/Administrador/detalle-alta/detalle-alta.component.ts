import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {  SucursalService, UsuarioService } from 'src/app/administracion/servicios';
import { TipoServicio } from 'src/app/administracion/modelos/Control_Usuario/tipo-servicio';
import { ActivatedRoute, Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { ServicioUsuario } from 'src/app/administracion/modelos/Control_Usuario/servicio-usuario';
import Swal from 'sweetalert2';
import { TipoServicioService } from 'src/app/administracion/servicios/Control_Usuario/tipo-servicio.service';
import { SolicitudService } from 'src/app/administracion/servicios/Control_Usuario/solicitud.service';
import { DetalleSolicitudService } from 'src/app/administracion/servicios/Control_Usuario/detalle-solicitud.service';
import { ServicioUsuarioService } from 'src/app/administracion/servicios/Control_Usuario/servicio-usuario.service';
import { DetalleSolicitud } from 'src/app/administracion/modelos/Control_Usuario/detalle-solicitud';
import { Empleado } from 'src/app/administracion/modelos/empleado';
import { Sucursal } from 'src/app/administracion/modelos/sucursal';
import { ElemServicioService } from 'src/app/administracion/servicios/Control_Usuario/elem-servicio.service';




@Component({
  selector: 'app-detalle-alta',
  templateUrl: './detalle-alta.component.html',
  styleUrls: ['./detalle-alta.component.scss']
})
export class DetalleAltaComponent implements OnInit {

  dataSource          = new MatTableDataSource<ServicioUsuario>([]);
  dataAtendidos       = new MatTableDataSource<DetalleSolicitud>([]);
  dataServAlta        = new MatTableDataSource<ServicioUsuario>([]);
 /**Listas utilizadas */
  listaServicios      :  TipoServicio []=[];
  serviciosAdd        :  TipoServicio []=[];
  servSolicitados     :  ServicioUsuario[]=[];
  listaSucursales     :  Sucursal[]=[];
  /**Encabezados de la tabla */
  encabezadostabla        :  String [] = ["c1","c2","c3","c4","c5","c6"];
  encabezadostabla4       :  String [] = ["c1","c2","c3","c4"];
  encabezadostabAtendidos : String [] = ["c1","c2"];

  cabeceraCheck       : String ="";
  statusComparar      : String ="";
  sucursal_cubrir     : String ="";
  colaborador         : Empleado = new Empleado();


  tipo : string ="";
  tipoSolicitud   : string="ALTA DE SERVICIOS";
  statusSolicitud : string="";
  banderaE        : string="";
  bandera         : boolean=true;//Indica si se puede enviar o no la solicitud
  errorGmail      : boolean=false;//Indica si hay error en el gmail
  /**Expresion regular para validar el correo electrónico */
  expReg           : RegExp=/^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/



  constructor(
              private router                :Router,
              private usuarioService        : UsuarioService ,
              private tipoServicioService   : TipoServicioService,
              private solicitudService      : SolicitudService,
              private detalleSolicitudService : DetalleSolicitudService,
              private servicioUsuarioService : ServicioUsuarioService,
              private elemServicioService   : ElemServicioService,
              private sucursalService       : SucursalService,
              private formBuilder           : FormBuilder,
              private activeRouter          : ActivatedRoute
  ) {
      this.cargarDatosSolicitud();
      this.llenarTablaServicios();
      this.llenarListas();
    }
    idSolicitud: number = 0;

  /**Campos de la solicitud realizada */
  formDetSolicitud = this.formBuilder.group({
    colaborador         : [{value: ''}],
    solicitante         : [{value: ''}],
    tipoSolicitud       : [{value: ''}],
    fecha_planeada      : [{value: ''}],
    sustituye_a         : [{value: ''}],
    supervisor          : [{value: ''}],
    comentarios         : [{value: ''}],
    puesto              : [{value: ''}],
    sucursal            : [{value: ''}],
    puesto_solicitante  : [{value: ''}],
    sucursal_solicitante : [{value: ''}],
    email_solicitante    : [{value: ''}],
    tipo_servicio       : ['',[]],
    tipo_servicio2      : ['',[]],
    nom_usuario         : ['',[]],
    contrasenia         : ['',[]]
  })


  ngOnInit(): void {}

  /**Metódo para ordenar las sucursales por nombre e ignorar signos de puntuación */
  SortArray(x: Sucursal, y: Sucursal){
    return x.nombreSucursal.localeCompare(y.nombreSucursal, 'fr', {ignorePunctuation: true});
  }

  /**Llena las listas de sucursales y servicios */
  private llenarListas(){
    this.sucursalService.getSucursales().subscribe(sucursales=>{ this.listaSucursales=sucursales.sort(this.SortArray);  });
    this.tipoServicioService.todosLosServicios().subscribe(tipoServicios=>{
      this.listaServicios=[];
      tipoServicios.forEach(tipoServ=>{
        if (tipoServ.status_servicio=="ACTIVO"){
          this.detalleSolicitudService.listaDetSolicitudPorSolicitudId(this.idSolicitud).subscribe(solicitudes=>{
            let comparar= solicitudes.find(element=>element.tipo_servicio.id_tipo_servicio==tipoServ.id_tipo_servicio)
            let comparar2=this.listaServicios.find(element=>element.id_tipo_servicio==tipoServ.id_tipo_servicio);
            if(comparar==undefined && comparar2==undefined){ this.listaServicios.push(tipoServ); }
          });
        }
      });
    });
  }

  /**Carga los datos de la solicitud y se los asigna al formulario*/
  cargarDatosSolicitud(){
    this.activeRouter.params.subscribe(params=>{
      this.idSolicitud = params['idSolicitud'];
      this.banderaE    = params['banderaE']+"";
      //this.idSolicitud= 49;
      console.log(this.banderaE+"bandera e");
      //this.idSolicitud=225;//144/80

      this.solicitudService.unaSolicitud(this.idSolicitud).subscribe(val=>{
        this.tipo=val.tipo+"";
        this.statusSolicitud= val.status_solicitud+"";
        this.tipoSolicitud=val?.tipo_solicitud_usuario?.tipo_solicitud+"";
        //Datos del colaborador
        this.formDetSolicitud.controls['tipoSolicitud'].setValue(val?.tipo_solicitud_usuario?.tipo_solicitud);
        this.usuarioService.getAnEmpleado(Number(val.colaboradorId)).subscribe(valor=>{
          this.formDetSolicitud.controls['colaborador'].setValue(valor?.nombre+" "+valor?.apellidoPat+" "+valor?.apellidoMat);
          this.formDetSolicitud.controls['puesto'].setValue(valor?.puesto?.nombrePuesto);
          this.formDetSolicitud.controls['sucursal'].setValue(valor?.sucursal?.nombreSucursal);
          this.colaborador= valor;
        });
        //Datos del solicitante
        this.usuarioService.getAnEmpleado(Number(val.solicitanteId)).subscribe(valor=>{
          this.formDetSolicitud.controls['solicitante'].setValue(valor?.nombre+" "+valor?.apellidoPat+" "+valor?.apellidoMat);
          this.formDetSolicitud.controls['puesto_solicitante'].setValue(valor?.puesto?.nombrePuesto);
          this.formDetSolicitud.controls['sucursal_solicitante'].setValue(valor?.sucursal?.nombreSucursal);
          this.formDetSolicitud.controls['email_solicitante'].setValue(valor?.correoInstitucional);
        });

        this.formDetSolicitud.controls['sustituye_a'].setValue(val?.sustituye_a);
        this.formDetSolicitud.controls['supervisor'].setValue(val?.nom_supervisor);

        this.formDetSolicitud.controls['comentarios'].setValue(val?.comentarios);
        this.formDetSolicitud.controls['fecha_planeada'].setValue(val?.fecha_planeada_act?.substring(0,10));
        this.llenarTablaAtendidos(this.idSolicitud);
        this.llenarTablaServAlta();

        //Ejecutar metodo para llenar la tabla segun se necesite
          this.cabeceraCheck="APROBAR ";
          this.llenarTablaServicios();
      });
    });
  }

  /** llena la tabla servicios con los detalles de solicitudes realizadas */
  llenarTablaServicios(){
    //limpiar tabla
    this.dataSource.data=[];
    let listaServicioUsuario: ServicioUsuario[]=[];
    this.detalleSolicitudService.listaDetSolicitudPorSolicitudId(this.idSolicitud).subscribe(valor=>{
      valor.forEach(element=>{
        let regUsuario : ServicioUsuario=new ServicioUsuario();
        /**Expresion regular en caso de ser gmail */
        if(element.tipo_servicio.nombre_servicio?.includes("GMAIL")){
          /**Valida que el correo ingresado sea válido */
          regUsuario.expresionRegular="^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$";
        }else{
          regUsuario.expresionRegular="";
        }

        regUsuario.detalleSolicitud=element;
        regUsuario.nombre_servicio=element.tipo_servicio.nombre_servicio;
        regUsuario.status_serv=element.status_serv;
        regUsuario.detalle_solicitud_id=element.id_detalle_solicitud;
        listaServicioUsuario.push(regUsuario);
        this.dataSource.data=listaServicioUsuario;

      });
      this.llenarListas();
    });

  }
   //Método para generar el detalle de la solicitud
  generarDetalleSolicitud(){
   /**Corresponde a una solicitud de alta de servicios o cualquier otra que no sea de activacion o suspensión */
    this.serviciosAdd=this.formDetSolicitud.value.tipo_servicio;
    this.serviciosAdd.map(val=>{
      this.detalleSolicitudService.generarDetalleSolicitud(this.idSolicitud,Number(val.id_tipo_servicio), 0).subscribe(data=>{
        console.log("detalle generado");
        this.llenarTablaServicios();
      });
    });
    this.formDetSolicitud.controls['tipo_servicio'].setValue([]);
  }

  /**Llena la tabla al momento de mostrar una solicitud atendida */
  llenarTablaAtendidos(idSolicitud: number){
    let listaDetSolicitudes: DetalleSolicitud[]=[];
    this.detalleSolicitudService.listaDetSolicitudPorSolicitudId(idSolicitud).subscribe(valor=>{
      valor.forEach(element=>{
        let detSolicitud : DetalleSolicitud=new DetalleSolicitud();
        detSolicitud.nombre_servicio=element.tipo_servicio.nombre_servicio;
        detSolicitud.status_serv=element.status_serv;
        listaDetSolicitudes.push(detSolicitud);
        this.dataAtendidos.data=listaDetSolicitudes;
      });
    });
  }

  /**Llena la tabla al momento de mostrar una solicitud atendida con las credenciales de los servicios que se le fueron otorgadas */
  llenarTablaServAlta(){
    let listaServicioUsuario: ServicioUsuario[]=[];
    this.elemServicioService.listaElemServicioIdSolicitudAct(this.idSolicitud).subscribe(elementos=>{
      elementos.forEach(elem=>{
        if (elem.detalle_solicitudAct.status_serv=="Aceptado"){
          let servicioU: ServicioUsuario= new ServicioUsuario();
          servicioU=elem.servicio_usuario
          this.sucursalService.getAnSucursal(Number(elem.sucursal_id)).subscribe(sucursal=>{
            servicioU.nomSucursalCol=sucursal.nombreSucursal;
          });
          listaServicioUsuario.push(servicioU);
        }
      });
      this.dataServAlta.data=listaServicioUsuario;
    });
  }

  /**Método para rechazar una solicitud y todos sus servicios */
  async rechazarSolicitud(){
    await Swal.fire({
      title: '¿Está seguro de que desea rechazar esta solicitud?',
      text: "¡Ya no podrá revertir esta acción!",
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'No, cancelar',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, rechazar solicitud'
    }).then(async result => {
      if (result.isConfirmed) {
        const { value: text } = await Swal.fire({
          title: 'Razón por la cual rechaza la solicitud: ',
          input: 'text',
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'Aceptar',
          allowEnterKey:false,
          allowEscapeKey:false,
          allowOutsideClick:false
        })
        if (text){/**valida que se haya ingresado una razón */

          let comentarios= "Motivo del rechazo de la solicitud: "+text;
         //Editamos el status de la solicitud
          this.solicitudService.actualizarStatusSolicitud(this.idSolicitud,"Atendido",comentarios);
          //Se edita el status de los detalles de la solicitud
          this.detalleSolicitudService.listaDetSolicitudPorSolicitudId(this.idSolicitud).subscribe(valor=>{
            valor.forEach(element=>{
              this.detalleSolicitudService.actualizarStatusDetSolicitud(Number(element.id_detalle_solicitud),"Rechazado")
            });
          });
          this.router.navigate(['layout/gestionSolicitudesAdmin/'+this.tipoSolicitud ]);
          //mensaje  de confirmación
          Swal.fire(
            '¡Solicitud rechazada!',
            'La solicitud ha sido rechazada',
            'success'
          );
        }else{
          Swal.fire({
            title: 'No se rechazó la solicitud, es necesario insertar un motivo por el cual se desea rechazar.',
            icon:'warning',
            timer:3000,
            showConfirmButton:false,
            allowEnterKey:false,
            allowEscapeKey:false,
            allowOutsideClick:false
          })
        }
      }
    });
  }

  /**Agrega los servicios que fueron solicitados y aprobados */
  agregarServicio( ){
    this.bandera=true;
    let listaSU: ServicioUsuario[]=[];
    listaSU=[];
    this.servSolicitados=[];
    listaSU=this.dataSource.data;
    //Número de servicios seleccionados
    let cont=0;
    listaSU.forEach(valor=>{
      console.log(valor.detalleSolicitud.tipo_servicio.nombre_servicio+"nombre sdel servicio");
      if(valor.status_reg?.toString()=="true"){ //valor que el usuario seleccionó
        cont++;
        if(valor.sucursales.length>0){
          /**Verifica que si haya ingresado usuario y contraseña */
          if(valor.usuario!="" && valor.contrasenia!="" && valor.usuario!=null && valor.contrasenia!=null){
            /**Preguntra si el servicio es gmail, ya que debe validarlo */
            if(valor.detalleSolicitud.tipo_servicio.nombre_servicio?.includes("GMAIL")){
              /**valida que el dato ingresado en el usuario sea un correo electrónico válido */
              if(this.expReg.test(valor.usuario)){
                /**Si es asi lo agrega a la lista y establece que ya no hay error en el gmail */
                this.errorGmail=false;
                this.servSolicitados.push(valor);
              }else{
                /**En caso de ser erróneo el correo electrónico este emitira un mensaje de error */
                /**Indica que el gmail es incorrecto */
                this.errorGmail=true;
                Swal.fire({
                  icon: 'info',
                  title: 'Correo electrónico inválido',
                  text: 'El valor ingresado en GMAIL no es válido. Porfavor ingrese el dato con formato de correo correcto.',
                  showConfirmButton: false,
                  timer: 3000
                });
              }
            }else{
              /**Agrega el servicio  */
              if(this.errorGmail==false){
                this.servSolicitados.push(valor);
              }
            }
          }else{
            /**bandera que establece que aun no se puede enviar la solicitud */
            this.bandera=false;
            Swal.fire({
              icon: 'info',
              title: 'Faltan campos',
              text: 'Es necesario que le asigne "Usuario" y "Contraseña" a todos los servicios aprobados',
              showConfirmButton: false,
              timer: 3000
            });
          }
        }else{
          /**bandera que establece que aun no se puede enviar la solicitud */
          this.bandera=false;
          Swal.fire({
            icon: 'info',
            title: 'Faltan campos',
            text: 'Debe seleccionar al menos una sucursal de cada servicio aprobado',
            showConfirmButton: false,
            timer: 3000
          });
        }
      }else{
        console.log(valor.detalle_solicitud_id+"id del detalle soli rechazar");
        this.detalleSolicitudService.actualizarStatusDetSolicitud(Number(valor.detalle_solicitud_id),"Rechazado")
      }
    });
    if(cont==0){
      //this.rechazarSolicitud();
      Swal.fire({
        icon: 'info',
        title: 'No ha seleccionado ningun servicio',
        text: 'Debe seleccionar el o los servicios que desea aprobar',
        showConfirmButton: false,
        timer: 3000
      });
    }else
          //guardar
          if(this.bandera==true && this.errorGmail==false){
            //Editamos el status de la solicitud
            this.solicitudService.actualizarStatusSolicitud(this.idSolicitud,"Atendido","");

            //Se crea un registro de servicio usuario
              this.servSolicitados.forEach(val=>{
                let usuario= val.usuario+"";
                let contrasenia= val.contrasenia+"";
                let id_detalle=Number(val.detalle_solicitud_id);
                this.detalleSolicitudService.actualizarStatusDetSolicitud(Number(val.detalle_solicitud_id),"Aceptado");
                //se insertaba un status de alta
                this.servicioUsuarioService.guardarServicioUsuario(usuario, contrasenia,id_detalle).subscribe(servicioU=>{
                  val.sucursales.forEach(sucursalSelect=>{
                    console.log(sucursalSelect.nombreSucursal+"nombre de la sucursal"+ sucursalSelect.idSucursal);
                    //Genera la activación del servicio
                    this.elemServicioService.agregarElemServicio(Number(servicioU.id_servicio_usuario),Number(val.detalle_solicitud_id),Number(sucursalSelect.idSucursal) ).subscribe(data=>{
                      Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Solicitud aprobada',
                        text: ' Los servicios fueron registrados y dados de alta correctamente',
                        });
                    }, err=>{ console.log(err+"error en el elemento");
                      Swal.fire({
                      position: 'center',
                      icon: 'error',
                      title: 'Aprobar solicitud',
                      text: ' Los servicios no fueron registrados correctamente, por favor intentelo más tarde',
                      })

                    });
                  });
                }, err => {
                  Swal.fire({
                  position: 'center',
                  icon: 'error',
                  title: 'Aprobar solicitud',
                  text: ' Los servicios fueron no registrados correctamente, por favor intentelo más tarde',
                  })
               }
                );
              });
              this.router.navigate(['layout/SeguimientoControlU/'+this.idSolicitud]);
          }
  }

  /**Leé las sucursales  y retorna una lista de sucursales separadas por un - */
  leerSucursales(sucursales : Sucursal[]): string{
    let nomSucursales="";
    sucursales.forEach(sucursal=>{
      nomSucursales=nomSucursales+sucursal.nombreSucursal+" - ";
    });
    return nomSucursales;
  }

  validarFormu(){
    /*var input = document.getElementById('usuario');
    input?.oninvalid
    //input.oninvalid = function(event) { event.target.setCustomValidity('Username should only contain lowercase letters. e.g. john'); }
    input*/

  }

  /**salida */
  goBack(){
    if(this.banderaE=="true"){
      this.router.navigate(['layout/Expedientes2/'+this.colaborador.idEmpleado ]);
    }else{
      this.router.navigate(['layout/gestionSolicitudesAdmin/'+this.tipoSolicitud ]);
    }
  }
}
