import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleAltaComponent } from './detalle-alta.component';

describe('DetalleAltaComponent', () => {
  let component: DetalleAltaComponent;
  let fixture: ComponentFixture<DetalleAltaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetalleAltaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleAltaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
