import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Solicitud } from 'src/app/administracion/modelos/Control_Usuario/solicitud';
import { TipoSolicitud } from 'src/app/administracion/modelos/Control_Usuario/tipo-solicitud';
import { Empleado } from 'src/app/administracion/modelos/empleado';
import { UsuarioService } from 'src/app/administracion/servicios';
import { DetalleSolicitudService } from 'src/app/administracion/servicios/Control_Usuario/detalle-solicitud.service';
import { SolicitudService } from 'src/app/administracion/servicios/Control_Usuario/solicitud.service';
import { TipoSolicitudService } from 'src/app/administracion/servicios/Control_Usuario/tipo-solicitud.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-gestion-solicitudes-admin',
  templateUrl: './gestion-solicitudes-admin.component.html',
  styleUrls: ['./gestion-solicitudes-admin.component.scss']
})
export class GestionSolicitudesAdminComponent implements OnInit {
  /**Creación de las tablas utilizadas */
  dataSourceE         = new MatTableDataSource<Solicitud>([]);
  dataSourceA         = new MatTableDataSource<Solicitud>([]);
  dataSourceC         = new MatTableDataSource<Solicitud>([]);
  tipoSolicitud="";
   /* Paginators */
   @ViewChild('paginatorE', { static: true }) public paginatorE !: MatPaginator;
   @ViewChild('paginatorA', { static: true }) public paginatorA !: MatPaginator;
   @ViewChild('paginatorC', { static: true }) public paginatorC !: MatPaginator;

  listaEmpleados       : Empleado[]=[];
  listatipoSolicitudes : TipoSolicitud[]=[];
  encabezadostabla     : String [] = ["opciones","nomColaborador","nomPuesto","name_sucursalCol","name_tipo_solicitud","nomSolicitante","fechaSolicitud","fechaModificacion","status_solicitud"];

  constructor(
    private router                : Router,
    private activateRouter        : ActivatedRoute,
    private solicitudService      : SolicitudService,
    private tipoSolicitudService  : TipoSolicitudService,
    private detalleSolicitudService : DetalleSolicitudService,
    private usuarioService        : UsuarioService,
  ) { this.llenarListas();
  }

  ngOnInit(): void {

    this.capturarParametro();
    this.llenarTabla(0);
  }
  /**Captura los parametros que le son enviados */
  capturarParametro(){
    this.activateRouter.params.subscribe(params=>{
      this.tipoSolicitud = params['tipoSolicitud'];
      //this.tipoSolicitud="ALTA DE SERVICIOS";
    });
  }
  /**Se llenan las listas de empleados y tipos de solicitud */
  llenarListas(){
    this.usuarioService.getAllEmpleados().subscribe(val=>{this.listaEmpleados=val;});
    this.tipoSolicitudService.todosLosTiposSolicitud().subscribe(val=>{this.listatipoSolicitudes = val;});
  }

  /**Metodo para llenar la tabla de datos */
  llenarTabla(index : number){
    let statusSolicitud="";
    let listaSolicitud: Solicitud[]=[];
    /**Asigna valores dependiendo de la acción elegida */
    if(index==0){statusSolicitud="Espera";}
    if(index==1){statusSolicitud="Atendido";}
    if(index==2){statusSolicitud="Cancelado";}
    //Se realiza la búsqueda de los elementos
    this.solicitudService.todasLasSolicitudes().subscribe(response=>{
      response.forEach(element=>{
        /**Busca por el estatus elegido */
        if(element.status_solicitud==statusSolicitud){
          if(element.tipo_solicitud_usuario.tipo_solicitud==this.tipoSolicitud){
            let solicitud : Solicitud=new Solicitud();
            solicitud.id_solicitud=element.id_solicitud;
            this.usuarioService.getAnEmpleado(Number(element.colaboradorId)).subscribe(val=>{
              solicitud.name_colaborador_id=val?.nombre+" "+val?.apellidoPat+" "+ val?.apellidoMat;
              solicitud.name_cargo_colaborador= val?.puesto?.nombrePuesto;
              solicitud.name_sucursalCol=val?.sucursal?.nombreSucursal;
            });
            this.usuarioService.getAnEmpleado(Number(element.solicitanteId)).subscribe(val=>{ solicitud.name_solicitante_id=val?.nombre+" "+val?.apellidoPat+" "+ val?.apellidoMat; });
            this.listatipoSolicitudes.forEach(val=>{
              if(val.id_tipo_solicitud_usuario==element.tipo_solicitud_usuario.id_tipo_solicitud_usuario){
                element.name_tipo_solicitud=val.tipo_solicitud;
                solicitud.name_tipo_solicitud=val.tipo_solicitud;
              }
            solicitud.fechaSolicitud= element.fechaSolicitud?.substring(0,10);
            solicitud.fechaModificacion=element.fechaModificacion?.substring(0,10);
            solicitud.status_solicitud=element.status_solicitud;
            });
            listaSolicitud.push(solicitud);
          }
        }
      });

      /**Llena la tabla segun la opción elegida */
      if(index==0 && this.dataSourceE.data.length==0){ this.dataSourceE.data=listaSolicitud;        this.dataSourceE.paginator=this.paginatorE;
        }else if(index==1 && this.dataSourceA.data.length==0){this.dataSourceA.data=listaSolicitud;   this.dataSourceA.paginator=this.paginatorA;
            }else if(this.dataSourceC.data.length==0){      this.dataSourceC.data=listaSolicitud;     this.dataSourceC.paginator=this.paginatorC;
              }
    }, err=>{
      Swal.fire({
        position: 'center',
        icon: 'error',
        title: 'Error de conexión',
        text: 'Lo sentimos las solicitudes no se cargaron correctamente debido a que no hay conexión con el servidor, por favor  inténtelo más tarde.',
      })
    });
  }

  /**Metodos que filtran las busqueda en la tabla de solicitudes en ESPERA */
  applyFilterE(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceE.filter = filterValue.trim().toLowerCase();
  }

  /**Metodos que filtran las busqueda en la tabla de solicitudes ANTENDIDAS */
  applyFilterA(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceA.filter = filterValue.trim().toLowerCase();
  }

  /**Metodos que filtran las busqueda en la tabla de solicitudes CANCELADAS */
  applyFilterC(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceC.filter = filterValue.trim().toLowerCase();
  }

  /**Permite rechazar la solicitud y todos los servicios que esta contiene */
  async rechazarSolicitud(id:number){
    await Swal.fire({
      title: '¿Está seguro de que desea rechazar todos los servicios de esta solicitud?',
      text: "¡Ya no podrá revertir esta acción!",
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'No, regresar',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, rechazar solicitud'
    }).then(async result => {
      if (result.isConfirmed) {
        const { value: text } = await Swal.fire({
          title: 'Razón por la cual rechaza la solicitud: ',
          input: 'text',
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'Aceptar',
          allowEnterKey:false,
          allowEscapeKey:false,
          allowOutsideClick:false
        })
        if (text){/**valida que se haya ingresado una razón */

          let comentarios= "Motivo del rechazo de la solicitud: "+text;
          //Editamos el status de la solicitud
          this.solicitudService.actualizarStatusSolicitud(id,"Atendido",comentarios);
          //Se edita el status de los detalles de la solicitud
          this.detalleSolicitudService.listaDetSolicitudPorSolicitudId(id).subscribe(valor=>{
            valor.forEach(element=>{
              this.detalleSolicitudService.actualizarStatusDetSolicitud(Number(element.id_detalle_solicitud),"Rechazado")
            });
          });
          this
          Swal.fire(
            '¡Solicitud rechazada!',
            'La solicitud ha sido rechazada',
            'success'
          );
          this.llenarTabla(0);
        }else{
          Swal.fire({
            title: 'No se rechazó la solicitud, es necesario insertar un motivo por el cual se desea rechazar.',
            icon:'warning',
            timer:3000,
            showConfirmButton:false,
            allowEnterKey:false,
            allowEscapeKey:false,
            allowOutsideClick:false
          })
        }
      }
    });
  }

  /**Redirecciona al detalle de la solicitud */
  detalleSolicitud(idSolicitud: number){
    if(this.tipoSolicitud=="ALTA DE SERVICIOS"){
      this.router.navigate(['layout/DetalleAdminAlta/' + idSolicitud]);
    }else if(this.tipoSolicitud=="ACTIVACIÓN DE PRIVILEGIOS"){
      this.router.navigate(['layout/DetalleAdminActivacion/' + idSolicitud]);
    }else{
      this.router.navigate(['layout/DetalleAdminSuspension/' + idSolicitud]);
    }
  }
}
