import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionSolicitudesAdminComponent } from './gestion-solicitudes-admin.component';

describe('GestionSolicitudesComponent', () => {
  let component: GestionSolicitudesAdminComponent;
  let fixture: ComponentFixture<GestionSolicitudesAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionSolicitudesAdminComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionSolicitudesAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
