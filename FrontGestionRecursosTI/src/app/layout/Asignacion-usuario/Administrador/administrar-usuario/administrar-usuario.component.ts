import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ServicioUsuario } from 'src/app/administracion/modelos/Control_Usuario/servicio-usuario';
import { Empleado } from 'src/app/administracion/modelos/empleado';
import { SucursalService, UsuarioService } from 'src/app/administracion/servicios';
import { ElemServicioService } from 'src/app/administracion/servicios/Control_Usuario/elem-servicio.service';
import { ServicioUsuarioService } from 'src/app/administracion/servicios/Control_Usuario/servicio-usuario.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-administrar-usuario',
  templateUrl: './administrar-usuario.component.html',
  styleUrls: ['./administrar-usuario.component.scss']
})
export class AdministrarUsuarioComponent implements OnInit {
  sucursal_a_filtrar :string ='';
  dataSourceT          = new MatTableDataSource<ServicioUsuario>([]);
  dataSourceAl        = new MatTableDataSource<ServicioUsuario>([]);
  dataSourceAc        = new MatTableDataSource<ServicioUsuario>([]);
  dataSourceS         = new MatTableDataSource<ServicioUsuario>([]);
   /* Paginators */
   @ViewChild(MatPaginator) paginator  !: MatPaginator
   @ViewChild('paginatorT',  { static: true }) public paginatorT !: MatPaginator;
   @ViewChild('paginatorAl', { static: true }) public paginatorAl !: MatPaginator;
   @ViewChild('paginatorAc', { static: true }) public paginatorAc !: MatPaginator;
   @ViewChild('paginatorS',  { static: true }) public paginatorS !: MatPaginator;

  listaEmpleados       : Empleado[]=[];
  encabezadostabla        : String [] = ["nomColaborador","nomPuesto","nomSucursalCol","nombre_servicio","status_serv","fechaRegistro","fecha_mod_reg"];
  encabezadostablaAlta    : String [] = ["nomColaborador","nomPuesto","nombre_servicio","status_serv","fechaRegistro","fecha_mod_reg"];
  listaServAlta        : ServicioUsuario[]=[];
  listaServAct         : ServicioUsuario[]=[];
  listaServSus         : ServicioUsuario[]=[];
  contador :number=0;

  constructor(
    private servicioUsuarioService : ServicioUsuarioService,
    private elemServicioService   : ElemServicioService,
    private usuarioService        : UsuarioService,
    private sucursalService       : SucursalService
  ) {
    this.llenarListas();
    this.llenarTablaAl();
    this.llenarTablaAS(2);
    this.llenarTablaAS(3);
  }

  ngOnInit(): void {
  }
  /**Llena las listas con los datos de la BD */
  llenarListas(){
    this.usuarioService.getAllEmpleados().subscribe(val=>{this.listaEmpleados=val;});
  }

  /**Método que llena la tabla de servicios dados de alta correspondietes a la tabla servicio_usuario */
  llenarTablaAl(){
    let listaAlta: ServicioUsuario[]=[];
    this.servicioUsuarioService.todosLosServiciosUsuario().subscribe(servicios=>{
      servicios.forEach(servicio=>{
        if(servicio.archivo_responsiva!=null){
          servicio.status_serv="Alta";
          this.usuarioService.getAnEmpleado(Number(servicio.detalleSolicitud.solicitud.colaboradorId)).subscribe(val=>{
            servicio.nomColaborador=val?.nombre+" "+val?.apellidoPat+" "+ val?.apellidoMat;
            servicio.nomPuesto= val?.puesto?.nombrePuesto;
            servicio.nomSucursalCol="Sin asignar"//val?.sucursal?.nombreSucursal;
          });
          servicio.nombre_servicio=servicio.detalleSolicitud.tipo_servicio.nombre_servicio;
          listaAlta.push(servicio);
        }
      });
      this.dataSourceAl.data=listaAlta;
      this.dataSourceAl.paginator=this.paginatorAl;
      if(this.listaServAlta.length<=0){this.listaServAlta=listaAlta; }
    });
  }
  /**Llena la tabla segun la seleccion elegida, activado o suspendido con la tabla elem_servicio*/
  llenarTablaAS(index: number){
    let lista: ServicioUsuario[]=[];
    let status="";
    if (index==2){ status="Activado"
    }else if(index==3) { status="Suspendido"}

    this.elemServicioService.listaElemServicios().subscribe(elementos=>{
      //Se recorre la lista de elementos de servicio
      elementos.forEach(elem=>{
        let servicioU : ServicioUsuario= new ServicioUsuario();
        /**Comprueba que el servicio activado o suspendido ya cuente con su responsiva, para ser tomado encuenta*/
        if(elem.status_registro==status && elem.servicio_usuario.archivo_responsiva!=null){
          this.usuarioService.getAnEmpleado(Number(elem.detalle_solicitudAct.solicitud.colaboradorId)).subscribe(val=>{
            servicioU.nomColaborador=val?.nombre+" "+val?.apellidoPat+" "+ val?.apellidoMat;
            servicioU.nomPuesto= val?.puesto?.nombrePuesto;//Si proviene de una solicitud de alta, muestra el puesto del colaborador
          });
            //si proviene de una solicitud de activación mostrará el puesto a cubrir
            this.usuarioService.getAnPuesto(Number(elem.detalle_solicitudAct.solicitud.puesto_a_cubrir)).subscribe(puesto=>{servicioU.nomPuesto=puesto.nombrePuesto;});
            this.sucursalService.getAnSucursal(Number(elem.sucursal_id)).subscribe(sucu=>{servicioU.nomSucursalCol=sucu?.nombreSucursal;});

          servicioU.detalleSolicitud=elem.detalle_solicitudAct;
          servicioU.status_serv=elem.status_registro;
          servicioU.fecha_mod_reg=elem.fecha_modificacion;
          servicioU.fechaRegistro= elem.fecha_registro;
          servicioU.usuario=elem.servicio_usuario.usuario;
          servicioU.contrasenia=elem.servicio_usuario.contrasenia;
          servicioU.nombre_servicio=elem.detalle_solicitudAct.tipo_servicio.nombre_servicio;
          lista.push(servicioU);
        }
      });
      if(status=="Activado"){
        this.listaServAct=lista;
        if(this.dataSourceAc.data.length==0){ this.dataSourceAc.data=lista;        this.dataSourceAc.paginator=this.paginatorAc;}
      }else if(status=="Suspendido"){
        this.listaServSus=lista;
        if(this.dataSourceS.data.length==0){this.dataSourceS.data=lista;     this.dataSourceS.paginator=this.paginatorS; }
      }
    }, err=>{
      Swal.fire({
        position: 'center',
        icon: 'error',
        title: 'Error de conexión',
        text: 'Lo sentimos los usuarios no se cargaron correctamente debido a que no hay conexión con el servidor, por favor  inténtelo más tarde.',
      })
    });
  }
  /**Método que llena la tabla con todos los servicios existentes, ya sea de alta, activación y suspensión */
  llenarTablaTodos(){
    let listaT  : ServicioUsuario[]=[];
    //Elije la opción para visualizar todos
    if(this.contador==0){
      this.listaServAlta.forEach(servAlta=>{  listaT.push(servAlta);  });
      this.listaServAct.forEach(servAct=>{    listaT.push(servAct);   });
      this.listaServSus.forEach(servSus=>{    listaT.push(servSus);   });
      this.contador++;
      this.dataSourceT.data=listaT;
      this.dataSourceT.paginator= this.paginatorT;
    }
  }
  /**Metodos que filtran las busqueda en la tabla de servicios en general */
  applyFilterT(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceT.filter = filterValue.trim().toLowerCase();
  }
  /**Metodos que filtran las busqueda en la tabla de SERVICIOS DADOS DE ALTA*/
  applyFilterAl(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceAl.filter = filterValue.trim().toLowerCase();
  }
    /**Metodos que filtran las busqueda en la tabla de SERVICIOS ACTIVOS */
  applyFilterAc(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceAc.filter = filterValue.trim().toLowerCase();
  }
    /**Metodos que filtran las busqueda en la tabla de SERVICIOS SUSPENDIDOS */
  applyFilterS(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceS.filter = filterValue.trim().toLowerCase();
  }
  /**Muestra al usuario una ventana emergente con la inforación del módulo */
  botonInformacion(){
    Swal.fire({
      position: 'top-end',
      icon: 'info',
      title: 'VISUALIZAR USUARIOS',
      showConfirmButton: false,
      html: '<b>ALTA</b>'
           +'<p align="justify"> Se muestran todos los servicios dados de alta de los colaboradores.</p>'
           +'<b>ACTIVADOS</b>'
           +'<p align="justify" >Se observan todos los servicios activados de los colaboradores y la sucursal en la que fue activado.</p>'
           +'<b>SUSPENDIDOS</b>'
           +'<p align="justify">Se listan los servicios suspendidos de los colaboradores y la sucursal en la que fue suspendido.</p>'
           +'<b>TODOS</b>'
           +'<p align="justify">Se despliega una lista general de todos los servicios de los colaboradores, sin importar el estado en el que se encuentren.</p>',

           confirmButtonColor: '#3085d6',
           cancelButtonColor: '#d33',
           confirmButtonText: 'Sí'
    })
  }
}
