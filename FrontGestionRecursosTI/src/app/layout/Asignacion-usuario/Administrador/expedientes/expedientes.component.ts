import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { map, startWith, takeUntil } from 'rxjs/operators';
import { Solicitud } from 'src/app/administracion/modelos/Control_Usuario/solicitud';
import { TipoSolicitud } from 'src/app/administracion/modelos/Control_Usuario/tipo-solicitud';
import { Empleado } from 'src/app/administracion/modelos/empleado';
import { UsuarioService } from 'src/app/administracion/servicios';
import { ServicioUsuarioService } from 'src/app/administracion/servicios/Control_Usuario/servicio-usuario.service';
import { SolicitudService } from 'src/app/administracion/servicios/Control_Usuario/solicitud.service';
import { TipoSolicitudService } from 'src/app/administracion/servicios/Control_Usuario/tipo-solicitud.service';
import { VerResponsivaServicioComponent } from 'src/app/layout/Seguimiento-servicios/ver-responsiva-servicio/ver-responsiva-servicio.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-expedientes',
  templateUrl: './expedientes.component.html',
  styleUrls: ['./expedientes.component.scss']
})
export class ExpedientesComponent implements OnInit {
  /**Form control para el select del colaborador */
  colaboradorFiltrar = new FormControl();
  inputColaboradorF  = new FormControl();

  colaborador   : Empleado = new Empleado();
  dataSourceSolicitud = new MatTableDataSource<Solicitud>([]);
  dataSourceFilter   :string="";
  statusVista : string ="";
   /* Paginators */
   @ViewChild(MatPaginator) paginator  !: MatPaginator

  /**Listas utilizadas */
  listaColaboradores   : Empleado[]=[];
  listatipoSolicitudes : TipoSolicitud[]=[];
  solicitudesxId!      : Solicitud[];
  encabezadostabla     : String [] = ["visualizarSoli","id_solicitud","name_tipo_solicitud","status_solicitud","nomColaborador","nomPuesto","name_sucursalCol","fechaSolicitud", "fechaModificacion", "responsiva"]
  /**lista para el select del colaborador */
  listaFiltradaEmp      :   ReplaySubject<Empleado[]> = new ReplaySubject<Empleado[]>(1);

  protected onDestroy  = new Subject<void>();

  constructor(
    private router                : Router,
    private activeRouter          : ActivatedRoute,
    private tipoSolicitudService  : TipoSolicitudService,
    private servicioUsuarioService : ServicioUsuarioService,
    private usuarioService        : UsuarioService,
    private solicitudService      : SolicitudService,
    public  dialog                : MatDialog
  ) {  }

  /**En este metódo se capturan los parametros enviados */
  ngOnInit(): void {
    this.llenarListas();

    let emp :Empleado= new Empleado();
    this.activeRouter.params.subscribe(params=>{
      emp.idEmpleado= params['idColaborador'];
      this.llenarTablaExpSolicitudes(emp);
    })
    /**Escucha los cambios del valor de búsqueda del colaborador*/
    this.listaFiltradaEmp.next(this.listaColaboradores.slice());
    this.inputColaboradorF.valueChanges.pipe(takeUntil(this.onDestroy)).subscribe(() => {
        this.filterColaborador();
    });
  }
  /**Se llenan las listas de colaboradores, y de los tipos de solicitud */
  llenarListas(){
    this.usuarioService .getAllEmpleados().subscribe( val => {
      val.forEach(emp=>{
        emp.nomb_compl= emp.nombre+" "+ emp.apellidoPat+" "+emp.apellidoMat
      });
      this.listaColaboradores = val;
    }, err=>{
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
      })
      Toast.fire({
        icon: 'error',
        title: 'Error de conexión intentelo más tarde',
      })
    });
    ;

    this.tipoSolicitudService.todosLosTiposSolicitud().subscribe(val=>{this.listatipoSolicitudes = val;});
  }

   /**Realiza la búsqueda para el colaborador a quien se le solicitan los servicios */
   protected filterColaborador(){
    if(!this.listaColaboradores){
      return;
    }
    /**Se optiene el valor del input para el colaborador*/
    let search = this.inputColaboradorF.value;
    if (!search) {
      this.listaFiltradaEmp.next(this.listaColaboradores.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    /**Retorna la lista de los colaboradores que cumplen con la busqueda */
    this.listaFiltradaEmp.next( this.listaColaboradores.filter(empleado => empleado.nomb_compl.toLowerCase().indexOf(search) > -1) );
  }

  /**Recibe un empleado y lo asigna a un colaborador */
  cargarDatosColaborador(event : any){
    this.colaborador = this.colaboradorFiltrar.value;
  }

  /**Método que llena toda la tabla de las solicitudes por colaborador */
  llenarTablaExpSolicitudes(event : any){
    let listaSolicitudes: Solicitud[]=[];
    this.dataSourceSolicitud.data =listaSolicitudes; //vacia la tabla
    this.solicitudService.solicitudesPorColaborador(Number(this.colaboradorFiltrar.value.idEmpleado)).subscribe(solicitudes=>{
      solicitudes.forEach(solicitud=>{
        solicitud.banderaResponsiva=true;
        solicitud.servicio_usuario_id=0; // Será igual a 0 siempre que sea una alta o activación cancelada o sin servicios de usuario
        this.usuarioService.getAnEmpleado(Number(solicitud.colaboradorId)).subscribe(val=>{
          solicitud.name_colaborador_id=val?.nombre+" "+val?.apellidoPat+" "+ val?.apellidoMat;
          solicitud.name_cargo_colaborador=val?.puesto?.nombrePuesto;
          solicitud.name_sucursalCol=val?.sucursal?.nombreSucursal;
        });
        solicitud.name_tipo_solicitud=solicitud.tipo_solicitud_usuario?.tipo_solicitud;
        this.servicioUsuarioService.ListaServicioUPorSolicitudId(Number(solicitud.id_solicitud)).subscribe(servicios=>{
          servicios.forEach(servicio=>{
            if(servicio.archivo_responsiva!= null){
              solicitud.servicio_usuario_id=servicio.id_servicio_usuario;
            }else if(servicio.archivo_responsiva==null){
              //si esta condición se cumple se debe mostrar el botón de reemplazar solamente
              solicitud.servicio_usuario_id=servicio.id_servicio_usuario;
              solicitud.banderaResponsiva=false;
              console.log("false responsiva null"+solicitud.id_solicitud);
            }
          });
        });
      });
      listaSolicitudes= solicitudes;
      this.dataSourceSolicitud.data=listaSolicitudes;
      this.dataSourceSolicitud.paginator=this.paginator;
      if(listaSolicitudes.length<=0){
        Swal.fire({
          icon: 'info',
          title: 'Tabla vacía',
          text: 'Este colaborador no cuenta con solicitudes',
          showConfirmButton: false,
          timer: 4000
        });
      }
    }, err=>{
      Swal.fire({
        position: 'center',
        icon: 'error',
        title: 'Error de conexión',
        text: 'Lo sentimos no hay conexión con el servidor, por favor  inténtelo más tarde.',
      })
    });
  }
  /**Realiza el filtro por la búsqueda que realiza el usuario en la tabla*/
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceSolicitud.filter = filterValue.trim().toLowerCase();
  }
  /** Descarga la responsiva de la solicitud elegida*/
  descargarResponsiva(id_servicio:number){
    this.servicioUsuarioService.unServicioUsuario(id_servicio).subscribe(async servicio=>{
      const base64Data= servicio.archivo_responsiva+"";
      const base64Response = `data:data:application/pdf;base64,${base64Data}`;
        const downloadLink = document.createElement("a");
        const fileName = "Responsiva.pdf";
        downloadLink.href = base64Response;
        downloadLink.download = fileName;
        downloadLink.click();
    });
  }
   /**Muestra la responsiva en pantalla de la soliciud seleccionada */
  async mostrarResponsiva(id_servicio:number){
    this.servicioUsuarioService.unServicioUsuario(id_servicio).subscribe(async servicio=>{
        const base64Data= servicio.archivo_responsiva+"";
        const base64Response = `data:data:application/pdf;base64,${base64Data}`;
          const dialogRef =   this.dialog.open(VerResponsivaServicioComponent,
            /**Dimensiones de la solicitud mostrada */
            {
              height: '90%',
              width:  '70%',
              position: {top: '5%'},
              data: { url: base64Response + "" },
            });
    });
  }
  /**Permite subir la responsiva en la solicitud correspondiente, la carga en cada registro de "servicio_usuario" perteneciente a la solicitud */
  async subirResponsiva(idSolicitud:number, idColaborador: number){
    const { value: file } = await Swal.fire({
      title: 'Selecciona el PDF de la responsiva firmada',
      input: 'file',
      inputAttributes: {
        'accept': '.pdf',
        'aria-label': 'Subir responsiva'
      }
    })
    /**Valida que se haya subido un archivo */
    if (file) {
      /**Valida que el archivo subido sea un pdf */
      if(file.type=='application/pdf'){
        /**Valida que el peso del archivo no sea superior al permitido 1048576 bytes */
          if(file.size < 1048576){
          const reader = new FileReader()
          console.log(file.size+"tamaño");
          this.servicioUsuarioService.todosLosServiciosUsuario().subscribe(servicios=>{
            servicios.forEach(servicio=>{
              if(servicio.detalleSolicitud.solicitud.id_solicitud== idSolicitud){
                  this.servicioUsuarioService.cargarResponsiva(Number(servicio.id_servicio_usuario),file).subscribe(val=>{
                    let emp : Empleado= new Empleado();
                    console.log(val+ idColaborador+"id ");
                    emp.idEmpleado= idColaborador;
                    Swal.fire({
                      position: 'center',
                      icon: 'success',
                      title: 'Archivo subido',
                      text: 'La responsiva ha sido guardada correctamente ',
                    })
                    /**Actualizacion de la pantalla */
                    this.llenarTablaExpSolicitudes(emp);
                  /**Validación en caso de que la consulta no se realice */
                  }, err => {
                      console.log(err);
                      Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'Cargar responsiva',
                        text: 'La responsiva no se cargó correctamente debido a un problema con el servidor, por favor inténtelo más tarde o contacte al área de T.I. ',
                      })
                    }
                  );
              }
            });
          }, err => {
            console.log(err);
            Swal.fire({
              position: 'center',
              icon: 'error',
              title: 'Cargar responsiva',
              text: 'La responsiva no se cargó correctamente debido a un problema con el servidor, por favor inténtelo más tarde o contacte al área de T.I. ',
            })
          }
          );
          reader.readAsDataURL(file);
         /**Cierre de validación para el tamaño de la responsiva */
        }else{
          Swal.fire({
            position: 'center',
            icon: 'error',
            title: 'Tamaño de archivo no permitido',
            text: 'El archivo que intenta subir supera el tamaño permitido, por favor verifique su documento.',
          })
        }
      /**Cierre de validación para que permita solo pdfs*/
      }else{
        Swal.fire({
          position: 'center',
          icon: 'error',
          title: 'El archivo no es un PDF',
          text: 'Lo sentimos, el archivo que intenta subir no corresponde a un formato PDF ',
        })
      }
    }
  }
  /**Metodo que valida que se muestren o no los botones de subir, mostrar y descargar responsiva  */
  validarBoton(nBoton: number, tipoSoli : string, servicioUId: number, banderaResponsiva : boolean){
    if(tipoSoli=="SUSPENSIÓN DE SERVICIOS" || servicioUId ==0){
      return false;
    }else if(nBoton!=3 && banderaResponsiva==false){
      return false;
    }
    return true;
  }
  /**Redirecciona al detalle de la solicitud */
  visualizarSolicitud(idSoli : number){
    let banderaE=true;
    this.solicitudService.unaSolicitud(idSoli).subscribe(solicitud=>{
      if(solicitud.tipo_solicitud_usuario.tipo_solicitud=="ALTA DE SERVICIOS"){
        this.router.navigate(['layout/DetalleAdminAltaExp/' + idSoli+'/'+banderaE]);
      }else if(solicitud.tipo_solicitud_usuario.tipo_solicitud=="ACTIVACIÓN DE PRIVILEGIOS"){
              this.router.navigate(['layout/DetalleAdminActivacionExp/' + idSoli+'/'+banderaE]);
            }else{
              this.router.navigate(['layout/DetalleAdminSuspensionExp/' + idSoli+'/'+banderaE]);
            }
    });
  }
  /**Botón que muestra la información de la vista de solicitudes y la simboogía de colores*/
  botonInformacion(){
    Swal.fire({
      position: 'top-end',
      icon: 'info',
      title: 'EXPEDIENTE DE COLABORADOR',
      showConfirmButton: false,
      html: '<p align="justify"> Para poder visualizar todas las solicitudes primero debe elegir un colaborador</p>'
           +'<b>DESCRIPCIÓN DE LOS BOTONES POR SU COLOR</b> <p></p>'
           +'<p align="justify">'
           +'<button style="background-color:  #00bd39c2;"   >1</button>'
           +' Visualizar solicitud a detalle</p>'
           +'<p align="justify">'
           +'<button style="background-color: #0804e9;"   >2</button>'
           +' Descargar responsiva</p>'
           +'<p align="justify">'
           +'<button style="background-color:  #ff3bde;"  >3</button>'
           +' Visualizar responsiva</p>'
           +'<p align="justify">'
           +'<button style="background-color: #1d9be4;"   >4</button>'
           +' Reemplazar o cargar responsiva</p>',
          confirmButtonColor: '#3085d6',
           cancelButtonColor: '#d33',
           confirmButtonText: 'Sí'
    })
  }
}
