import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder} from '@angular/forms';
import { SucursalService, UsuarioService } from 'src/app/administracion/servicios';
import { ActivatedRoute, Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { ServicioUsuario } from 'src/app/administracion/modelos/Control_Usuario/servicio-usuario';
import Swal from 'sweetalert2';
import { SolicitudService } from 'src/app/administracion/servicios/Control_Usuario/solicitud.service';
import { DetalleSolicitudService } from 'src/app/administracion/servicios/Control_Usuario/detalle-solicitud.service';
import { ElemServicioService } from 'src/app/administracion/servicios/Control_Usuario/elem-servicio.service';
import { DetalleSolicitud } from 'src/app/administracion/modelos/Control_Usuario/detalle-solicitud';
import { MatPaginator } from '@angular/material/paginator';
import { Empleado } from 'src/app/administracion/modelos/empleado';


@Component({
  selector: 'app-detalle-suspension',
  templateUrl: './detalle-suspension.component.html',
  styleUrls: ['./detalle-suspension.component.scss']
})
export class DetalleSuspensionComponent implements OnInit {
  /**Tablas utilizadas */
  dataSourceSusp      = new MatTableDataSource<ServicioUsuario>([]);
  dataSourceActivados = new MatTableDataSource<ServicioUsuario>([]);
  dataAtendidos       = new MatTableDataSource<DetalleSolicitud>([]);
  /**Llena la tabla que corresponde a los servicios suspendidos de esta solicitud */
  dataServSusp        = new MatTableDataSource<ServicioUsuario>([]);

   /* Paginators */
   @ViewChild(MatPaginator) paginator  !: MatPaginator
  /**Encabezados */
  encabezadostabla        :  String [] = ["c1","c2","c3","c4","c5"];
  encabezadostabla4       :  String [] = ["c1","c2","c3","c4"];
  encabezadostabAtendidos : String [] = ["c1","c2"];
  listaServSolicitados    : DetalleSolicitud[]=[];
  listaServiciosAdd       : ServicioUsuario[]=[];
  cabeceraCheck       : String ="";
  statusComparar      : String ="";
  sucursal_colab      : String ="";
  sucursal_cubrir     : String ="";

  tipo : string ="";
  tipoSolicitud : string="";
  statusSolicitud : string="";
  banderaE        : string="";

  constructor(
              private router                :Router,
              private sucursalService       : SucursalService,
              private usuarioService        : UsuarioService ,
              private solicitudService      : SolicitudService,
              private detalleSolicitudService : DetalleSolicitudService,
              private elemServicioService   : ElemServicioService,
              private formBuilder           : FormBuilder,
              private activeRouter          : ActivatedRoute
  ) {
      this.cargarDatosSolicitud();
      this.llenarTablaServSolicitados();
    }
    colaborador :   Empleado= new Empleado();
    idSolicitud : number = 0;
    banderaAct  : boolean  =true;

    /**Campos de la solicitud realizada */
  formDetSolicitud = this.formBuilder.group({
    reemplazo           : [{value: ''}],
    colaborador         : [{value: ''}],
    solicitante         : [{value: ''}],
    tipoSolicitud       : [{value: ''}],
    tipo                : [{value: ''}],
    comentarios         : [{value: ''}],
    puesto              : [{value: ''}],
    sucursal            : [{value: ''}],
    puesto_solicitante  : [{value: ''}],
    sucursal_solicitante : [{value: ''}],
    email_solicitante    : [{value: ''}],
    nom_usuario         : ['',[]],
    contrasenia         : ['',[]]

  })
  /**Campos del rango de fechas */
  range= this.formBuilder.group({
    fecha_alta: [{value: ''}],
    fecha_baja: [{value: ''}],
  })

  ngOnInit(): void {}

  /**Carga los datos de la solicitud */
  cargarDatosSolicitud(){
    this.activeRouter.params.subscribe(params=>{
      this.idSolicitud = params['idSolicitud'];
      this.banderaE    = params['banderaE'];
      //this.idSolicitud=25//144/19

      this.solicitudService.unaSolicitud(this.idSolicitud).subscribe(val=>{
        this.tipo=val.tipo+"";
        this.statusSolicitud= val.status_solicitud+"";
        this.tipoSolicitud=val?.tipo_solicitud_usuario?.tipo_solicitud+"";
        //Datos del colaborador
        this.formDetSolicitud.controls['tipoSolicitud'].setValue(val?.tipo_solicitud_usuario?.tipo_solicitud);
        this.usuarioService.getAnEmpleado(Number(val.colaboradorId)).subscribe(valor=>{
          this.formDetSolicitud.controls['colaborador'].setValue(valor?.nombre+" "+valor?.apellidoPat+" "+valor?.apellidoMat);
          this.formDetSolicitud.controls['puesto'].setValue(valor?.puesto?.nombrePuesto);
          this.formDetSolicitud.controls['sucursal'].setValue(valor?.sucursal?.nombreSucursal);
          this.sucursal_colab=valor?.sucursal?.nombreSucursal+"colab1";
          this.colaborador=valor;
        });
        //Datos del solicitante
        this.usuarioService.getAnEmpleado(Number(val.solicitanteId)).subscribe(valor=>{
          this.formDetSolicitud.controls['solicitante'].setValue(valor?.nombre+" "+valor?.apellidoPat+" "+valor?.apellidoMat);
          this.formDetSolicitud.controls['puesto_solicitante'].setValue(valor?.puesto?.nombrePuesto);
          this.formDetSolicitud.controls['sucursal_solicitante'].setValue(valor?.sucursal?.nombreSucursal);
          this.formDetSolicitud.controls['email_solicitante'].setValue(valor?.email);
        });

        if(val?.reemplazo+""=="true"){
          this.formDetSolicitud.controls['reemplazo'].setValue("SI");
        }else if(val?.reemplazo+""=="false"){
                this.formDetSolicitud.controls['reemplazo'].setValue("NO");
               }
        this.formDetSolicitud.controls['tipo'].setValue(val.tipo);
        this.formDetSolicitud.controls['comentarios'].setValue(val?.comentarios);
        this.range.controls['fecha_alta'].setValue(val?.fecha_alta);
        this.range.controls['fecha_baja'].setValue(val?.fecha_baja);
        //Llenamos la tabla con los servicios de una solicitud atendida para mostarle su detalle
        this.llenarTablaAtendidos(this.idSolicitud);
        console.log(val.colaboradorId);
        this.llenarTablaActivados(Number(val.colaboradorId));
        this.llenarTablaServSusp();
        //Ejecutar metodo para llenar la tabla segun se necesite
                      this.cabeceraCheck="SUSPENDER ";
                      this.statusComparar="Activado";
      });
    });
  }
  /**Filtro para buscar en la tabla de los servicios activos del colaborador */
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceActivados.filter = filterValue.trim().toLowerCase();
  }

  /**Selecciona o deselecciona todos los servicios que el empleado tiene activado, en caso de ser una solicitud de suspension  */
  selectAllCheck(accion : boolean){
    console.log(accion+"accion");
    if(accion==true){
      /**Selecciona todos los servicios */
      this.dataSourceActivados.filteredData.forEach(servicio=>{
        servicio.status_reg=true;
      });
    }else{
      /**Deshace todas las selecciones de los servicios */
      this.dataSourceActivados.filteredData.forEach(servicio2=>{
        servicio2.status_reg=false;
      });
    }
  }

  /**Llena la tabla con los servicios que pidio suspender el colaborador */
  llenarTablaServSolicitados(){
    this.dataSourceSusp.data=[];
    let listaSus: ServicioUsuario[]=[];
    this.detalleSolicitudService.listaDetSolicitudPorSolicitudId(this.idSolicitud).subscribe(detalles=>{
      this.listaServSolicitados=detalles;
      detalles.forEach(detalle=>{
        this.elemServicioService.elemServicio(Number(detalle.servicio_usuario_id)).subscribe(elem=>{
          let servicio : ServicioUsuario = new ServicioUsuario();
          servicio=elem.servicio_usuario;
          servicio.detalleSolicitud=detalle;
          servicio.status_reg=true;
          servicio.nombre_servicio= elem.servicio_usuario.detalleSolicitud.tipo_servicio.nombre_servicio;

          this.sucursalService.getAnSucursal(Number(elem?.sucursal_id)).subscribe(sucursal=>{
            servicio.nomSucursalCol=sucursal?.nombreSucursal;
          });
          listaSus.push(servicio);
          this.dataSourceSusp.data=listaSus;
        });
      });
    });
  }

  /**Llena la tabla con todos los servicios que el colaborador tiene activados  */
  llenarTablaActivados(idColaborador: number){
    this.dataSourceActivados.data=[];
      let listaServicios: ServicioUsuario[]=[];
      this.elemServicioService.listaElemServicioForColaborador(idColaborador).subscribe(elementos=>{
        elementos.forEach(elemento=>{
          let compararId= this.listaServSolicitados.find(element=>element.servicio_usuario_id==elemento.idElemServicio);
          if(elemento.status_registro=="Activado" && compararId==undefined && elemento.servicio_usuario.archivo_responsiva!=null){
            let regUsuario : ServicioUsuario=new ServicioUsuario();
            regUsuario=elemento.servicio_usuario;
            regUsuario.nombre_servicio=elemento.servicio_usuario.detalleSolicitud.tipo_servicio.nombre_servicio;
            this.sucursalService.getAnSucursal(Number(elemento?.sucursal_id)).subscribe(sucursal=>{
              regUsuario.nomSucursalCol=sucursal?.nombreSucursal;
            });
            /**Se inserta el id del elemento de servicio que se decea modificar */
            regUsuario.detalle_solicitud_id=elemento.idElemServicio;
            listaServicios.push(regUsuario);
            this.dataSourceActivados.data=listaServicios;
            this.dataSourceActivados.paginator=this.paginator;
          }
        });
      }, err=>{console.log(err)});
  }

/**Llena la tabla con los servicios para una solicitud que ya fue atendida */
  llenarTablaAtendidos(idSolicitud: number){
    let listaDetSolicitudes: DetalleSolicitud[]=[];
    this.detalleSolicitudService.listaDetSolicitudPorSolicitudId(idSolicitud).subscribe(valor=>{
      valor.forEach(element=>{
        let detSolicitud : DetalleSolicitud=new DetalleSolicitud();
        detSolicitud.nombre_servicio=element.tipo_servicio.nombre_servicio;
        detSolicitud.status_serv=element.status_serv;
        listaDetSolicitudes.push(detSolicitud);
        this.dataAtendidos.data=listaDetSolicitudes;
      });
    });
  }

  /**Llena la tabla con los servicios de una solicitud atendida y en este caso muestra usuario, contraseña y la sucursa a al que pertenece */
  llenarTablaServSusp(){
    let listaServSusp : ServicioUsuario[]=[];
    this.elemServicioService.listaElemServicioIdSolicitudSusp(this.idSolicitud).subscribe(elementos=>{
      elementos.forEach(elemento=>{
        let servicio : ServicioUsuario = new ServicioUsuario();
        servicio=elemento.servicio_usuario;
        this.sucursalService.getAnSucursal(Number(elemento.sucursal_id)).subscribe(sucursal=>{
          servicio.nomSucursalCol=sucursal.nombreSucursal;
        });
        listaServSusp.push(servicio);
      });
      this.dataServSusp.data= listaServSusp;
    });
  }

  /**Método para rechazar una solicitud y todos sus servicios, es decir rechazar la solicitud de suspensión */
  async rechazarSolicitud(){
    await Swal.fire({
      title: '¿Está seguro de que desea rechazar esta solicitud?',
      text: "¡Ya no podrá revertir esta acción!",
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'No, cancelar',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, rechazar solicitud'
    }).then(async result => {
      if (result.isConfirmed) {
        const { value: text } = await Swal.fire({
          title: 'Razón por la cual rechaza la solicitud: ',
          input: 'text',
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'Aceptar',
          allowEnterKey:false,
          allowEscapeKey:false,
          allowOutsideClick:false
        })
        if (text){/**valida que se haya ingresado una razón */

          let comentarios= "Motivo del rechazo de la solicitud: "+text;
          //Editamos el status de la solicitud
          this.solicitudService.actualizarStatusSolicitud(this.idSolicitud,"Atendido",comentarios);
          //Se edita el status de los detalles de la solicitud
          this.detalleSolicitudService.listaDetSolicitudPorSolicitudId(this.idSolicitud).subscribe(valor=>{
            valor.forEach(element=>{
              this.detalleSolicitudService.actualizarStatusDetSolicitud(Number(element.id_detalle_solicitud),"Rechazado")
              //tengo el id del cada detalle
            });
          });
          this.router.navigate(['layout/gestionSolicitudesAdmin/'+this.tipoSolicitud ]);
          //mensaje  de confirmación
          Swal.fire(
            '¡Solicitud rechazada!',
            'La solicitud ha sido rechazada',
            'success'
          );
        }else{
          Swal.fire({
            title: 'No se rechazó la solicitud, es necesario insertar un motivo por el cual se desea rechazar.',
            icon:'warning',
            timer:3000,
            showConfirmButton:false,
            allowEnterKey:false,
            allowEscapeKey:false,
            allowOutsideClick:false
          })
        }
      }
    });
  }

  /**Agregar detalle de suspension de servicios que selecciona el administrador */
  generarDetalleSolicitudSusp(){
    let listaSusp        : ServicioUsuario[]= [];
    let listaSolicitados : ServicioUsuario[]= [];
    listaSusp           = this.dataSourceActivados.data;
    listaSolicitados    = this.dataSourceSusp.data;
    /**Edita el elem_servicio de los detalles que corresponden a los servicios que solicitó el solicitante (servSolicitado.detalleSolicitud.servicio_usuario_id==id_elem_servicio)*/
    listaSolicitados.forEach(servSolicitado=>{
      console.log(servSolicitado.status_reg+"status de"+servSolicitado.id_servicio_usuario);
      if(servSolicitado.status_reg==true){
          this.elemServicioService.actualizarStatusElemServicio(Number(servSolicitado.detalleSolicitud.servicio_usuario_id), "Suspendido",Number(servSolicitado.detalleSolicitud.id_detalle_solicitud) );
          this.detalleSolicitudService.actualizarStatusDetSolicitud(Number(servSolicitado.detalleSolicitud.id_detalle_solicitud),"Aceptado");
      }else {
        this.detalleSolicitudService.actualizarStatusDetSolicitud(Number(servSolicitado.detalleSolicitud.id_detalle_solicitud),"Rechazado");
      }
    });

    /**Genera los detalles de solicitud y edita el elem_servicio que le corresponde de los servicios recientemente seleccionados */
    listaSusp.forEach(servicio=>{
      if(servicio.status_reg==true){
        console.log(servicio.status_reg+"status de"+servicio.id_servicio_usuario);

        this.detalleSolicitudService.generarDetalleSolicitud(this.idSolicitud,Number(servicio.detalleSolicitud.tipo_servicio.id_tipo_servicio), Number(servicio.detalle_solicitud_id)).subscribe(ultimoDetalle=>{
          //console.log(ultimoDetalle+"detalle generado");
          this.elemServicioService.actualizarStatusElemServicio(Number(servicio.detalle_solicitud_id), "Suspendido",Number(ultimoDetalle.id_detalle_solicitud) );
          this.detalleSolicitudService.actualizarStatusDetSolicitud(Number(ultimoDetalle.id_detalle_solicitud),"Aceptado");

        }, err =>{console.log(err)});
      }else{
        this.detalleSolicitudService.actualizarStatusDetSolicitud(Number(servicio.detalleSolicitud.id_detalle_solicitud),"Rechazado");
      }
    });
    this.solicitudService.actualizarStatusSolicitud(this.idSolicitud, "Atendido","");
    this.router.navigate(['layout/gestionSolicitudesAdmin/'+this.tipoSolicitud ]);
  }

  /**Verifica el tipo de suspensión que se le pasa como parametro y devuelve un true o false,  */
  tipoSeleccionado(tipo:string){
    if(tipo==this.tipo)
      return true;
    return false;
  }

  /**salida - Dependiendo de la ventana en la que se haya encontrado redireccionará a ese componente*/
  goBack(){
    if(this.banderaE=="true"){
      this.router.navigate(['layout/Expedientes2/'+this.colaborador.idEmpleado ]);
    }else{
      this.router.navigate(['layout/gestionSolicitudesAdmin/'+this.tipoSolicitud ]);
    }
  }
}
