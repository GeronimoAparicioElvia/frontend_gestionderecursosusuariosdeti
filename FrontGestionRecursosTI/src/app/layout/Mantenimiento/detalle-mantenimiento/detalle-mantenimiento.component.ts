import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { ActividadMan } from 'src/app/administracion/modelos/Mantenimiento/actividad-man';
import { MantenimientoEquipo } from 'src/app/administracion/modelos/Mantenimiento/mantenimiento-equipo';
import { RegistroMantenimiento } from 'src/app/administracion/modelos/Mantenimiento/registro-mantenimiento';
import { TipoMan } from 'src/app/administracion/modelos/Mantenimiento/tipo-man';
import { SucursalService, UsuarioService } from 'src/app/administracion/servicios';
import { EquipoService } from 'src/app/administracion/servicios/Inventario/Equipo/equipo.service';
import { ElementMantenimientoService } from 'src/app/administracion/servicios/Mantenimiento/element-mantenimiento.service';
import { MantenimientoEquipoService } from 'src/app/administracion/servicios/Mantenimiento/mantenimiento-equipo.service';
import { RegistroMantenimientoService } from 'src/app/administracion/servicios/Mantenimiento/registro-mantenimiento.service';
import { TipoManService } from 'src/app/administracion/servicios/Mantenimiento/tipo-man.service';
import * as pdfMake from "pdfmake/build/pdfmake";
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import { map, startWith } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { Empleado } from 'src/app/administracion/modelos/empleado';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { Perfil } from 'src/app/administracion/modelos/perfil';
(<any>pdfMake).vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-detalle-mantenimiento',
  templateUrl: './detalle-mantenimiento.component.html',
  styleUrls: ['./detalle-mantenimiento.component.scss']
})
export class DetalleMantenimientoComponent implements OnInit {

  /** P A R Á M E T R O S   D E   E N T R A D A */
  id_registro_mantenimiento!:number
  status_registro!:string

  /**O B J E C T S */
  registro_mantenimiento!: RegistroMantenimiento

  /** T A B L A    E Q U I P O S */
  listaEncabezados:string[] = ["NOMBRE DEL EQUIPO","TIPO DE EQUIPO","COMENTARIOS DE USUARIO","OBSERVACIONES","TIPO DE MANTENIMIENTO","ACTIVIDADES REALIZADAS"]
  dataSource           = new MatTableDataSource<MantenimientoEquipo>([]);
  @ViewChild('paginator', { static: true }) public paginator !: MatPaginator;

  /** T A B L A    A C T I V I D A D E S */
  listaEncabezadosA:string[] = ["c1","c2"]
  dataSourceA          = new MatTableDataSource<ActividadMan>([]);
  @ViewChild('paginatorA', { static: true }) public paginatorA !: MatPaginator;

  /** L I S T A S */
  lista_ActividadesMan : ActividadMan [] = []
  lista_Tipo_de_Manten : TipoMan      [] = [] 

  /* VISTA */
  fecha_programada   : string = ""
  fecha_modificacion : string = ""
  nombre_sucursal    : string = ""

  /** B A N D E R A */
  tipo_mantenimiento!:string
  bandera_fallo :boolean=false
  problema_dete!:string
  nombre_equipo:string=""
  tipo___equipo:string=""
  actividades_realizadas:string=""
  tipo_de_mant_realizado:string=""

  /* M  I N U T A */
  num_mant_correctivos:number = 0
  num_mant_preventivos:number = 0
  minuta_actividades_realizadas:string=""
  minuta_tipo_de_mant_realizado:string=""
  minuta_lista_actividades_Tipo!:MantenimientoEquipo []
  nombre_empleado_avala:string=""

  /* ELEMENTOS PARA EL INPUT CON AUTOCOMPLETADO DEL EMPLEADO A ASIGNAR EL EQUIPO */
  colaborador            = new FormControl();
  select_empleado : Empleado = new Empleado();
  listaColaboradores            :  Empleado [] = [];
  listaFiltradaColaboradores    !:  Observable<Empleado[]>;

  /* CUERPO DE LA TABLA DEL PDF */
  cuerpo:any [] = []

  constructor(private activeRouter                  : ActivatedRoute              ,
              private registro_mantenimeintoService : RegistroMantenimientoService,
              private elementMantenimientoService   : ElementMantenimientoService,
              private mantenimeintoEquipoService    : MantenimientoEquipoService  ,
              private router                        : Router,
              private sucursalService               : SucursalService,
              private usuarioService                : UsuarioService
             ){}

  ngOnInit()
  {
    this.parametros()
    this.llenarData()
    this.llenarDataColaboradores()
    this.accionFiltrado()    
  }

  /** CAPTURAMOS LOS PARÁMETROS DE LA URL */
  parametros()
  {
    this.activeRouter.params.subscribe(params=>{   
      this.id_registro_mantenimiento  = params['id_registro_mantenimiento']
      this.status_registro            = params['status_registro'          ]
    });

  }

  /* CARGA LA DATA DE LA VISTA */
  llenarData()
  {
    this.registro_mantenimeintoService.registroDeMantenimientoPorId(this.id_registro_mantenimiento).subscribe(registro=>{
      this.registro_mantenimiento = registro

      /* CARGAMOS LOS DATOS DE LOS EMPLEADOS QUE  REGISTRÓ Y REALIZÓ EL MANTENIMIENTO */
      if(registro.id_empleado_realiza!=undefined)
      {
        this.usuarioService.getAnEmpleado(Number(registro.id_empleado_realiza)).subscribe(data=>{ registro.obj_empleado_realiza = data})
      }
      else
      {
        let obj_aux: Empleado = new Empleado()
        obj_aux.nombre = "- - - - - - - - - "
        obj_aux.apellidoPat = ""
        obj_aux.apellidoMat = ""
        registro.obj_empleado_realiza = obj_aux
      }
      
      this.usuarioService.getAnEmpleado(Number(registro.id_empleado_registra)).subscribe(data=>{ 
        registro.obj_empleado_registra = data

        if(registro.obj_empleado_registra.folioIne==null)
        {
          registro.obj_empleado_registra.folioIne = ""
        }
      })

      this.fecha_programada   = (this.registro_mantenimiento.fecha_agendada      +"").substring(8,10) + "-" +
                                (this.registro_mantenimiento.fecha_agendada      +"").substring(5,7) + "-" +
                                (this.registro_mantenimiento.fecha_agendada      +"").substring(0,4)

      this.fecha_modificacion = (this.registro_mantenimiento.fecha_mod      +"").substring(8,10) + "-" +
                                (this.registro_mantenimiento.fecha_mod      +"").substring(5,7) + "-" +
                                (this.registro_mantenimiento.fecha_mod      +"").substring(0,4)

      this.sucursalService.getSucursal(Number(registro.sucursal_id)).subscribe(sucursal=>{
        this.nombre_sucursal = sucursal.nombreSucursal
      })      
    })

    
    this.mantenimeintoEquipoService.mantenimientosEquipoPorIdRegistro(this.id_registro_mantenimiento).subscribe(lista_mantenimientos=>{

      lista_mantenimientos.forEach(mantenimiento=>{

        if(mantenimiento.comentarios_usuario=="CANCELADO")
        {
          mantenimiento.tipo_mantenimiento_rea = "CANCELADO"
          mantenimiento.actividades_realizadas = "CANCELADO"
        }
        else
        {
          mantenimiento.actividades_realizadas = ""
          this.elementMantenimientoService.listaActividadesPorIdMantenimientoEquipo(Number(mantenimiento.id_mantenimiento_equipo)).subscribe(actividades=>{

            mantenimiento.tipo_mantenimiento_rea = actividades[0].nombre + ""

            actividades.forEach(actividad=>{
              mantenimiento.actividades_realizadas = mantenimiento.actividades_realizadas + 
                                                    actividad.nombre_act + ": " + actividad.descripcion + ". \n"
            })

          })
        }
        

      })
      this.dataSource.data = lista_mantenimientos
      this.dataSource.paginator = this.paginator


    })

  }

  /* CARGA LA DATA DE LOS COLABORADORES */
  llenarDataColaboradores()
  {
    this.listaColaboradores.splice(0,this.listaColaboradores.length)
    this.usuarioService.getAllEmpleados().subscribe(lista=>{
      lista.forEach(empleado=>{
        empleado.nomb_compl = empleado.nombre + " " + empleado.apellidoPat + " " + empleado.apellidoMat
      })
      this.listaColaboradores = lista
    })
  }
  
  /* REGRESA A LA VISTA DE CONTROL DE MANTENIMEINTOS */
  btnAtras(event:any)
  {
    this.router.navigate(['layout/controlDeMantenimiento'])
  }
  
  /*   DESCARGA LA MINUTA */
  async clickDescargarMinuta(event:any)
  {
    let fecha_agendada:string =""
    let agendado___por:string =""


    if(this.registro_mantenimiento.descripcion!="EVENTUAL")
    {
      agendado___por  = this.registro_mantenimiento.obj_empleado_registra.nombre      + " " +
                        this.registro_mantenimiento.obj_empleado_registra.apellidoPat + " " +
                        this.registro_mantenimiento.obj_empleado_registra.apellidoMat
      fecha_agendada  = (this.registro_mantenimiento.fecha_agendada+"").substring(0,10)
    }
    else
    {
      fecha_agendada="Fué un mentenimiento no programado"
      agendado___por="Fué un mentenimiento no programado"
    }


    /* DATOS A UTILIZAR PARA MOSTRAR LOS RESULTADOS DEL MANTENIMIENTO */
    this.num_mant_correctivos = 0;
    this.num_mant_preventivos = 0;
    this.cuerpo.splice(0,this.cuerpo.length)

    /* ENCABEZADOS DE LA TABLA */
    let encabezados:string [] = ['NOMBRE','TIPO DE EQUIPO','TIPO DE MANTENIMIENTO','ACTIVIDADES REALIZADAS']
    this.cuerpo.push(encabezados)
    let lista_mantenimiento_Equipo : MantenimientoEquipo [] = this.dataSource.data

    for(let i = 0; i< lista_mantenimiento_Equipo.length; i++)
    {

      if(lista_mantenimiento_Equipo[i].tipo_mantenimiento_rea=="CORRECTIVO"){ this.num_mant_correctivos++ }
      if(lista_mantenimiento_Equipo[i].tipo_mantenimiento_rea=="PREVENTIVO"){ this.num_mant_preventivos++ }

      let fila:string []     = []
      fila.push(lista_mantenimiento_Equipo[i].nombre_equipo           +"")
      fila.push(lista_mantenimiento_Equipo[i].tipo_activo             +"")
      fila.push(lista_mantenimiento_Equipo[i].tipo_mantenimiento_rea  +"")
      fila.push(lista_mantenimiento_Equipo[i].actividades_realizadas  +"")
      this.cuerpo.push(fila)
    }

    const pdfDefinition: any = {

      pageMargins: [ 40 , 60 , 40 , 40 ],
      header:
      [
        {
          image: await this.getBase64ImageFromURL('/assets/img/logoSofipa.png'),
          x:40,
          width: 121,
          height: 64,
          border: [false, false, false, false],
        },
      ],
      content: 
      [ 
        {
          text:'MINUTA DE MANTENIMIENTO',
          style:'titulo'
        },
        
        { text:" \n", style:"texto" },
        { text:" \n", style:"texto" },
        { text:" \n", style:"texto" },

        {
          table:
          {
            border: [false, false, false, false],
            withs:['*','*','*'],
            body:
            [ 
              /* PRIMER FILA */
              [
                /* CAMPO 1 */
                {
                  border: [false, false, false, false],
                  text:
                  [
                    {
                      text:
                      [
                        {
                          style: "texto_negrita",
                          text: "MANTENIMIENTO: "
                        },
                        {
                          style:"texto",
                          text: this.registro_mantenimiento.descripcion
                        }
                      ]
                    }
                  ]
                },
                /* CAMPO 2 */
                {
                  border: [false, false, false, false],
                  text:
                  [
                    {
                      text:
                      [
                        {
                          style: "texto_negrita",
                          text: "SUCURSAL: "
                        },
                        {
                          style:"texto",
                          text: this.nombre_sucursal
                        }
                      ]
                    }
                  ]
                },
                /* CAMPO 3 */
                {
                  border: [false, false, false, false],
                  text:
                  [
                    {
                      text:
                      [
                        {
                          style: "texto_negrita",
                          text: "AGENDÓ: "
                        },
                        {
                          style:"texto",
                          text: agendado___por
                        }
                      ]
                    }
                  ]
                }
              ],

              /* SEGUNDA FILA */
              [
                {
                  border: [false, false, false, false],
                  text:
                  [
                    {
                      text:
                      [
                        {
                          style: "texto_negrita",
                          text: "FECHA PROGRAMADA: "
                        },
                        {
                          style:"texto",
                          text: fecha_agendada//(this.registro_mantenimiento.fecha_agendada+"").substring(0,10)
                        }
                      ]
                    }
                  ]
                },

                {
                  border: [false, false, false, false],
                  text:
                  [
                    {
                      text:
                      [
                        {
                          style: "texto_negrita",
                          text: "FECHA REALIZACIÓN: "
                        },
                        {
                          style:"texto",
                          text: (this.registro_mantenimiento.fecha_mod+"").substring(0,10)
                        }
                      ]
                    }
                  ]
                },

                {
                  border: [false, false, false, false],
                  text:
                  [
                    {
                      style: "texto_negrita",
                      text:'OBSERVACIONES GENERALES: '
                    },
                    {
                      style:"texto",
                      text: this.registro_mantenimiento.observ_generales + ""
                    }
                  ]
                }
              ],

              [
                /* CAMPO 1 */
                {
                  border: [false, false, false, false],
                  text:
                  [
                    {
                      text:
                      [
                        {
                          style: "texto_negrita",
                          text: "TOTAL DE EQUIPOS: "
                        },
                        {
                          style:"texto",
                          text: this.dataSource.data.length
                        }
                      ]
                    }
                  ]
                },
                /* CAMPO 2 */
                {
                  border: [false, false, false, false],
                  text:
                  [
                    {
                      text:
                      [
                        {
                          style: "texto_negrita",
                          text: "MANTENIMIENTOS PREVENTIVOS: "
                        },
                        {
                          style:"texto",
                          text: this.num_mant_preventivos
                        }
                      ]
                    }
                  ]
                },
                /* CAMPO 3 */
                {
                  border: [false, false, false, false],
                  text:
                  [
                    {
                      text:
                      [
                        {
                          style: "texto_negrita",
                          text: "MANTENIMIENTOS CORRECTIVOS: "
                        },
                        {
                          style:"texto",
                          text: this.num_mant_correctivos
                        }
                      ]
                    }
                  ]
                }
              ],
            ]
          }
        },
        
        

        { text:" \n", style:"texto" },
        { text:" \n", style:"texto" },
        { text:" \n", style:"texto" },

        {
          style:"texto",
          table:
          {
            withs:['*','*','*','*'],
            body: this.cuerpo

          }
        },
        
        { style:'texto', text:' ' },
        { style:'texto', text:' ' },
        { style:'texto', text:' ' },
        { style:'texto', text:' ' },
        { style:'texto', text:' ' },
        { style:'texto', text:' ' },

        {
          style: 'tableExample',
          table: {
            withs:['30%', 'auto' ,'30%', 'auto' ,'30%'],
            body:
            [
              [
                { text: this.registro_mantenimiento.nombre_empleado_realiza, style:'firmas',border: [false, false, false, true],},
                { text: " ", style:'firmas',border: [false, false, false, false],},
                { text: this.registro_mantenimiento.sucursal_Tabla, style:'firmas',border: [false, false, false, true],},
                { text: " ", style:'firmas',border: [false, false, false, false],},
                { text: this.colaborador.value, style:'firmas',border: [false, false, false, true],},
              ],
              [
                { text: "REALIZADO POR", style:'firmas',border: [false, false, false, false],},
                { text: " ", style:'firmas',border: [false, false, false, false],},
                { text: "SELLO DE LA SUCURSAL", style:'firmas',border: [false, false, false, false],},
                { text: " ", style:'firmas',border: [false, false, false, false],},
                { text: "AVALADO POR", style:'firmas',border: [false, false, false, false],},
              ],
            ]
          }
        }
        
      ],
      styles:
      {
        titulo: 
        {
          fontSize:14,
          bold:true,
          alignment: 'center',
        },
        texto: 
        {
          fontSize:10,
          bold:false,
        },
        texto_negrita: 
        {
          fontSize:10,
          bold:true,
        },
        firmas: 
        {
          fontSize:10,
          alignment: 'center',
        },
      }
    }
    pdfMake.createPdf(pdfDefinition).download("Minuta de mantenimiento");
  }

  /* SERIALZIA IMAGENES PARA AGREGARLAS EN UN PDF */
  getBase64ImageFromURL(url:string) {
    return new Promise((resolve, reject) => {
      var img = new Image();
      img.setAttribute("crossOrigin", "anonymous");

      img.onload = () => {
        var canvas = document.createElement("canvas");
        canvas.width = img.width;
        canvas.height = img.height;

        var ctx = canvas.getContext("2d");
        ctx?.drawImage(img, 0, 0);

        var dataURL = canvas.toDataURL("image/png");

        resolve(dataURL);
      };

      img.onerror = error => {
        reject(error);
      };

      img.src = url;
    });
  }

  /* FILTRA LA DATA DE LOS EMPLEADOS EN BASE AL NOMBRE QUE SE ESTÁ INGRESANDO */
  accionFiltrado()
  {
    this.listaFiltradaColaboradores = this.colaborador.valueChanges.pipe(
      startWith(''),
      map(value => this.filtrado(value)),
    );
  }

  private filtrado(value: string): Empleado[] {
    const filterValue = value.toLowerCase();
    return this.listaColaboradores.filter(option => option.nomb_compl.toLowerCase().includes(filterValue));
    
  }

  /* ABRE UNA VENTANA QUE DESCRIBE LO QUE SE VE Y PUEDE HACER EN LA VISTA */
  clickMasInformacionR(event:any)
  {
    Swal.fire({
      title:'MANTENIMIENTO REALIZADO',
      html:
      
      '<p align="justify">En la parte superior podras conocer datos generales del mantenimiento como'   +
      ' son las fechas de agenda y realización del mantenimiento, la sucursal en la que se realizó el ' +
      'mantenimiento, el responsable del mantenimiento así como sus observaciones o comentarios.</p>'   +
      
      '<p align="justify">Con el botón azul descargar se podrá generar y descargar la minuta '+
      'del mantenimiento correspondiente</p>' +

      '<p align="justify">En la tabla inferior se exponen los equipos que tomaron parte del mantenimiento'+
      ' , así como el tipo de equipo, las observaciones, los comentarios, el tipo de mantenimiento y las '+
      'actividades que se le realizaron.</p>',

      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      confirmButtonColor: '#d33',
      confirmButtonText:'Cerrar'
    })
  }

  /* ABRE UNA VENTANA QUE DESCRIBE LO QUE SE VE Y PUEDE HACER EN UN REGISTRO CANCELADO */
  clickMasInformacionC(event:any)
  {
    Swal.fire({
      title:'MANTENIMIENTO CANCELADO',
      html:
      
      '<p align="justify">En la parte superior podras conocer datos generales del mantenimiento como' +
      ' son la fecha de agenda del mantenimiento, la sucursal en la que se realizaría, la razón y fe' +
      'cha de la cancelación.</p>'   +

      '<p align="justify">En la tabla inferior se exponen los equipos que tomarían parte del manteni' +
      'miento con la leyenda de <b>CANCELADO</b>.</p>',

      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      confirmButtonColor: '#3085d6',
      confirmButtonText:'Cerrar'
    })
  }

  /* VUELVE A CARGAR LA DATA DE TODA LA VISTA */
  clickCargarDatosColaborador( empleado:Empleado )
  {
    this.colaborador.setValue(empleado.nombre+" "+empleado.apellidoPat+""+empleado.apellidoMat+" ")

    this.llenarDataColaboradores()
    this.accionFiltrado()
  }

  /* HABILITA EL CAMPO PARA OBSERVAR PORQUE SE CANCELÓ EL MANTENIMEINTO */
  mostrarRazonCancelacion()
  {
    if(this.status_registro == "CANCELADO")
    {
      return true
    }
    return false
  }

  /* HABILITA EL BOTON PARA DESCARGAR LA MINUTA D EMANTENIMIENTO */
  habilitarbotonDescargar()
  {
    if(this.nombre_empleado_avala!="")
    {
      return true
    }
    return false
  }
  
  /* HABILITA CUANDO SE PUEDE DESCARGAR LA MINUTA DEL MANTENIMEINTO */
  habilitarBotonDescargarMinuta()
  {
    if(this.colaborador.value!=undefined && this.colaborador.value!=null){ return true}
    return false
  }

  /* HABILITA CUANDO SE PUEDE DESCARGAR LA MINUTA DEL MANTENIMEINTO */
  habilitarlabels()
  {
    if(this.registro_mantenimiento.descripcion != "EVENTUAL"){ return true}
    return false
  }
}
