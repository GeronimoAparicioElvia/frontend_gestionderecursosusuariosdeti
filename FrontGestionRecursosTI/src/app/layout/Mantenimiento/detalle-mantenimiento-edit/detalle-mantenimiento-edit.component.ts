import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Equipo } from 'src/app/administracion/modelos/Inventario/Equipo/equipo';
import { ActividadMan } from 'src/app/administracion/modelos/Mantenimiento/actividad-man';
import { MantenimientoEquipo } from 'src/app/administracion/modelos/Mantenimiento/mantenimiento-equipo';
import { RegistroMantenimiento } from 'src/app/administracion/modelos/Mantenimiento/registro-mantenimiento';
import { TipoMan } from 'src/app/administracion/modelos/Mantenimiento/tipo-man';
import { SucursalService } from 'src/app/administracion/servicios';
import { EquipoService } from 'src/app/administracion/servicios/Inventario/Equipo/equipo.service';
import { ActividadManService } from 'src/app/administracion/servicios/Mantenimiento/actividad-man.service';
import { ElementMantenimientoService } from 'src/app/administracion/servicios/Mantenimiento/element-mantenimiento.service';
import { MantenimientoEquipoService } from 'src/app/administracion/servicios/Mantenimiento/mantenimiento-equipo.service';
import { RegistroMantenimientoService } from 'src/app/administracion/servicios/Mantenimiento/registro-mantenimiento.service';
import { TipoManService } from 'src/app/administracion/servicios/Mantenimiento/tipo-man.service';
import   Swal from 'sweetalert2';

@Component({
  selector: 'app-detalle-mantenimiento-edit',
  templateUrl: './detalle-mantenimiento-edit.component.html',
  styleUrls: ['./detalle-mantenimiento-edit.component.scss']
})
export class DetalleMantenimientoEditComponent  implements OnInit {

  /**P A R Á M E T R O S   D E   E N T R A D A */
  id_registro_mantenimiento!:number
  
  /**O B J E C T S */
  registro_mantenimiento!: RegistroMantenimiento
  mantenimiento___equipo!: MantenimientoEquipo

  /** T A B L A    E Q U I P O S */
  encabezados:string[] = ["OPCIONES","EQUIPO","TIPO DE EQUIPO"]
  dataSource           = new MatTableDataSource<MantenimientoEquipo>([]);
  @ViewChild('paginator', { static: true }) public paginator !: MatPaginator;

  /**L I S T A S */
  lista_ActividadesMan : ActividadMan [] = []
  lista_Tipo_de_Manten : TipoMan      [] = [] 
  lista____equipos: Equipo [] = []
  lista_equipos_filtrada!: Observable<Equipo[]>;
    
  /**E L E M E N T O S   D E   L A   V I S T A */
  fecha_actual:string= (new Date().toISOString() + "").substring(0,10)
  nombre_equipo!:string

  constructor(private activeRouter                  : ActivatedRoute                ,
              private registro_mantenimeintoService : RegistroMantenimientoService  ,
              private actividadManService           : ActividadManService           ,
              private elementMantenimientoService   : ElementMantenimientoService   ,
              private mantenimeintoEquipoService    : MantenimientoEquipoService    ,
              private equipoService                 : EquipoService                 ,
              private router                        : Router                        ,
              private tipoManService                : TipoManService                ,
              private sucursalService               : SucursalService               ,
              private formBuilder: FormBuilder,
             ){}
  
  /** F O R M U L A R I O */
  formulario = this.formBuilder.group({
    tipo_Mantenim:             [{value: '', disabled: true},[]] ,
    actividad_Man:             [{value: '', disabled: true},[]] ,
    Observaciones:             [{value: '', disabled: true},[]] ,
    comentarios_U:             [{value: '', disabled: true},[]] ,
    problema_dete:             [{value: '', },[]] ,  
  });
  
  /** F O R M   C O N T R O L */
  input_equipo_sucursal: FormControl = new FormControl()
  comentarios_generales: FormControl = new FormControl()
  input___Equipo: FormControl = new FormControl()
  input_Observ_G: FormControl = new FormControl()

  verCampoProblema:boolean=true
  
  ngOnInit()
  {
    this.parametros()
    this.llenarTablaMantenimientos()
    this.accionFiltrado()
  }
  
  /** D A T O S   D E   I N I C I O */
  parametros()
  {
    this.activeRouter.params.subscribe(params=>{   
      this.id_registro_mantenimiento = params['id_registro_mantenimiento']
    });
  }

  llenarTablaMantenimientos()
  {
    this.registro_mantenimeintoService.registroDeMantenimientoPorId(this.id_registro_mantenimiento).subscribe(registro=>{
      this.registro_mantenimiento = registro
      this.registro_mantenimiento.fecha_agendad_tabla = (this.registro_mantenimiento.fecha_agendada+"").substring(0,10)
      this.registro_mantenimiento.fecha_modific_tabla = (this.registro_mantenimiento.fecha_mod+"").substring(0,10)

      this.sucursalService.getSucursal(Number(registro.sucursal_id)).subscribe(sucursal=>{
        this.registro_mantenimiento.sucursal_Tabla = sucursal.nombreSucursal
        this.llenarListaEquiposDispo()
      })      
    })


    let tabla:string=""
    this.mantenimeintoEquipoService.mantenimientoEquipoPorIdRegistroSinEditar(this.id_registro_mantenimiento).subscribe(lista_manteniminetos=>{
      lista_manteniminetos.forEach(mantenimiento=>{
        
             if(  mantenimiento.tipo_activo =="COMPUTADORA"         ){tabla="Computer"        }
        else if(  mantenimiento.tipo_activo =="DISPOSITIVO DE RED"  ){tabla="Networkequipment"}
        else if(  mantenimiento.tipo_activo =="IMPRESORA"           ){tabla="Printer"         }
        else if(  mantenimiento.tipo_activo =="MONITOR"             ){tabla="Monitor"         }
        else if(  mantenimiento.tipo_activo =="DISPOSITIVO"         ){tabla="Peripheral"      }
        else if(  mantenimiento.tipo_activo =="RACK"                ){tabla="Rack"            }
        else if(  mantenimiento.tipo_activo =="TELEFONO"            ){tabla="Phone"           }

        this.equipoService.unEquipo(tabla, Number(mantenimiento.equipo_id)).subscribe(equipo=>{
          mantenimiento.equipo = equipo
        })

      })
      this.dataSource.data = lista_manteniminetos
      this.dataSource.paginator = this.paginator
    })
  }
  
  /** E V E N T O S   D E   S E L E C T */
  select_Tipo_Man()
  {
    /* Se habilita el campo actividades realizadas para cargar la lista de actividades que pertenecen a ese tipo de mantenimiento*/
    this.formulario.controls['actividad_Man'].enable()
    this.formulario.controls['actividad_Man'].setValue(null)
    let tipo_Man: TipoMan = this.formulario.value.tipo_Mantenim
    /* Se carrga todas as actividades de mantenimeinto pertenecientes a ese tipo de mamntenimiento */
    this.actividadManService.allActividadesDeMantenimientoActivasPorTipoMantenimiento( Number(tipo_Man.id_tipo_mant) ).subscribe(
      lista_act=> /* Se cargan las actividades en este campo */
      {
        this.lista_ActividadesMan = lista_act
      },/* Si hay un error al consusltar las sactividades retornamos un mensaje de error */
      err=>
      {
        this.mensajeDeError() /* Mensaje de error */
      }
    )

    if(tipo_Man.nombre == "CORRECTIVO")
    {
      this.formulario.controls['problema_dete'].setValue(null)
      this.verCampoProblema = false
    }
    else
    {
      this.verCampoProblema = true
      this.formulario.controls['problema_dete'].setValue(null)
    }
  }

  /** E V E N T O S   D E   C L I C K */

  /* EN CASO QUE SE TENGA QUE CANCELAR TODO EL REGISTRO SE SOLICITA CONFIRMACIÓN */
  clickCancelarMantenimiento(event:any)
  {
    Swal.fire({
      icon:'question',
      title:'¿Quieres cancelar este mantenimiento agendado?',
      text:'Una vez realizado el cambio no podrá revertirse',
      showCancelButton:true,
      showConfirmButton:true,
      confirmButtonText:'Sí',
      cancelButtonText:'No',
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false
    }).then( async (result)=>{

      /* CUANDO SE CONFIRME SE SOLICITA UNA RAZON O CAUSA PARA CANCELARLO (ESTO ES OBLIGATORIO) */
      if(result.isConfirmed)
      {

        const { value: text } = await Swal.fire({
          input: 'textarea',
          inputLabel: 'Causa de la cancelación',
          inputAttributes: {'aria-label': 'Type your message here' },
          showCancelButton: true,
          allowEnterKey:false,
          allowEscapeKey:false,
          allowOutsideClick:false
        })

        /* VALIDA QUE EN EFECTO AGREGUE UNA RAZON PARA ACTUALIZAR EL REGISTRO */
        if (text) {


          let id_empleado_realiza    :number   = JSON.parse(localStorage.getItem('empleado') || '{}').id;
          let nombre_empleado_realiza:string   = JSON.parse(localStorage.getItem("empleado") || '{}').nombreEmpleado + " " + JSON.parse(localStorage.getItem("empleado") || '{}').apellidoPat + " " + JSON.parse(localStorage.getItem("empleado") || '{}').apellidoMat
          this.registro_mantenimiento.id_empleado_realiza = id_empleado_realiza
          this.registro_mantenimiento.nombre_empleado_realiza=nombre_empleado_realiza
          this.registro_mantenimiento.status_registro="CANCELADO"
          this.registro_mantenimiento.observ_generales = text

          this.registro_mantenimeintoService.updateRegistrosMantenimiento(this.registro_mantenimiento).subscribe(data=>{
            Swal.fire({
              icon:'success',
              title:'Mantenimiento cancelado',
              showCancelButton:false,
              showConfirmButton:false,
              allowEnterKey:false,
              allowEscapeKey:false,
              allowOutsideClick:false,
              timer:2000
            }).then((result)=>{

              /* DAMOS POR CANCELADO TODOS DETALLES DE LOS EQUIPOS A REALIZAR SU MANTENIMIENTO */
              this.dataSource.data.forEach( mantenimiento=>{
                this.mantenimeintoEquipoService.updateMantenimientoEquipo( Number(mantenimiento.id_mantenimiento_equipo) , "CANCELADO", "CANCELADO", "CANCELADO" ).subscribe(data=>{})
              })

              /* DESPUES DE ACTUALIZARLO LO REENVIA A OTRA VISTA */
              this.router.navigate(['layout/controlDeMantenimiento'])
            })  
          })

        }
        /* SI NO AGREGA UNA AZON DE CANCELACIÓN RETORNA UN MENSAJE DE ERROR */
        else
        {
          Swal.fire({
            title:'No ingresaste la causa de cancelación',
            icon:'error',
            showCloseButton:false,
            allowEnterKey:false,
            allowEscapeKey:false,
            allowOutsideClick:false,
            timer:1700
          })
        }
        
      }

    })
  }

  /* 
    HABILITA LOS CAMPOS NECESARIOS DEL FORMULARIO PARA INICIAR CON LA EDICION DEL MANTENIMIENTO DE EQUIPO Y CARGA EN LA VISTA
    LOS DATOS DEL EQUIPO A REGISTRAR SU MANTENIMIENTO
  */
  clickEditar(mantenimientoEquipo:MantenimientoEquipo)
  {

    this.formulario.controls['tipo_Mantenim'].setValue(null)
    this.formulario.controls['Observaciones'].setValue(null)
    this.formulario.controls['comentarios_U'].setValue(null)
    this.formulario.controls['actividad_Man'].setValue(null)
    this.formulario.controls['problema_dete'].setValue(null)
    this.formulario.controls['actividad_Man'].disable()
    this.verCampoProblema=true

    this.mantenimiento___equipo = mantenimientoEquipo
    this.nombre_equipo = mantenimientoEquipo.equipo.name
    this.formulario.controls['tipo_Mantenim'].enable()
    this.formulario.controls['Observaciones'].enable()
    this.formulario.controls['comentarios_U'].enable()
    this.tipoManService.allTiposMantenimientoActivos().subscribe(lista_tipos=>{
      this.lista_Tipo_de_Manten = lista_tipos
    })
  }

  /* 
      CADA QUE DE CLICK EN GUARDAR LUEGO DE LLENAR LO REALIZADO A CADA EQUIPO, SE LANZARÁ UNA VENTANA EMERGENTE 
      EN DONDE SE NOS PIDE UNA CONFIRMACIÓN
  */
  clickGuardar(event:any)
  {
    Swal.fire({
      icon:'question',
      title:'¿Desea guardar este detalle de mantenimiento de equipo?',
      allowEnterKey:false,
      allowEscapeKey: false,
      allowOutsideClick:false,
      showCancelButton:true,
      showConfirmButton:true,
      cancelButtonText:'NO',
      confirmButtonText:'SÍ',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
    }).then( async (result)=>{
      if(result.isConfirmed)
      {

        /* EN CASO DE CONFIRMARMAMOS QUE SEA PARTE DE LOS EQUIPOS AGENDADOS*/
        if(this.mantenimiento___equipo.id_mantenimiento_equipo==null)
        {

          /* CAPTURAMOS LOS INPUTS PARA VALIDAR SI ESTAN VACÍOS O NO */
          let comentarios:string = this.formulario.value.comentarios_U
          let observacion:string = this.formulario.value.comentarios_U
          let problema_lo:string = this.formulario.value.problema_dete
          
          /* EN CASO DE ESTAR VACÍO SE RELLENAN CON VALORES PREDETERMINADOS */
          if(comentarios == null || comentarios == undefined ){ comentarios = "SIN COMENTARIOS" }
          if(observacion == null || observacion == undefined ){ observacion = "SIN COMENTARIOS" }
          if(problema_lo == null || problema_lo == undefined ){ problema_lo = "SIN DESCRIPCION" }
          
          /* AÑADIMOS LOS INPUTS CAPTURADOS EN LOS CAMPOS DE MANTENIMEIENTO EQUIPO */
          this.mantenimiento___equipo.comentarios_usuario = comentarios
          this.mantenimiento___equipo.observ_equipo = observacion
          this.mantenimiento___equipo.problema_localizado = problema_lo
          this.mantenimiento___equipo.nombre_equipo = this.mantenimiento___equipo.equipo.name

          /* AGREGAMOS LAS ACTIVIDADES REALIZADAS EN EL MANTENIMIENTO DEL EQUIPO Y SALVAMOS LOS CAMBIOS REALIZADOS */
          let lista_act: ActividadMan [] = this.formulario.value.actividad_Man
          this.mantenimeintoEquipoService.saveRegistrosDeMantenimiento( this.mantenimiento___equipo, this.id_registro_mantenimiento ).subscribe(mantenimiento=>{
            lista_act.forEach(actividad=>{
              this.elementMantenimientoService.saveElementoMantenimiento(Number(mantenimiento.id_mantenimiento_equipo),Number(actividad.id_actividad_mant)).subscribe(data=>{})
            })
          })
        }
        else
        {

          /* EN CASO DE NO SER UN EQUIPO AGENDADO, SE GENERA SU MANTENIMIENTO_EQUIPO CORRESPONDIENTE Y BÀSICAMENTE REALIZA LO MISMOD E ARRIBA*/
          let comentarios:string = this.formulario.value.comentarios_U
          let observacion:string = this.formulario.value.comentarios_U
          let problema_lo:string = this.formulario.value.problema_dete
          if(comentarios == null || comentarios == undefined ){ comentarios = "SIN COMENTARIOS" }
          if(observacion == null || observacion == undefined ){ observacion = "SIN COMENTARIOS" }
          if(problema_lo == null || problema_lo == undefined ){ problema_lo = "SIN DESCRIPCION" }

          let lista_act: ActividadMan [] = this.formulario.value.actividad_Man
          this.mantenimeintoEquipoService.updateMantenimientoEquipo( Number(this.mantenimiento___equipo.id_mantenimiento_equipo), comentarios, observacion,problema_lo ).subscribe(data=>{
            lista_act.forEach(actividad=>{
              this.elementMantenimientoService.saveElementoMantenimiento(Number(this.mantenimiento___equipo.id_mantenimiento_equipo),Number(actividad.id_actividad_mant)).subscribe(data=>{})
            })

          })
        }
        
        /* UNA VEZ REALIZADO TODO LO ANTERIOR LANZAMOS UNA VENTANA DE CONFIRMACIÒN QUE MUESTRA QUE EN EFECTO SE REALIZARON LOS CAMBIOS */
        Swal.fire({
          icon:'success',
          title:'Mantenimiento de equipo guardado',
          allowEnterKey:false,
          allowEscapeKey:false,
          allowOutsideClick:false,
          showConfirmButton:false,
          timer:2000
        }).then( async (r)     =>{

          /* FINALMENTE DEJAMOS VACÍOS LOS CAMPOS DEL FORMULARIO Y LO DEJAMOS LISTO PARA EL SIGUIENTE MANTENIMEINTO DE EQUIPO A EDITAR */
          let lista:MantenimientoEquipo [] = this.dataSource.data
          lista.splice(lista.indexOf(this.mantenimiento___equipo),1)
          this.dataSource.data = lista
          this.formulario.controls['tipo_Mantenim'].setValue(null)
          this.formulario.controls['Observaciones'].setValue(null)
          this.formulario.controls['comentarios_U'].setValue(null)
          this.formulario.controls['actividad_Man'].setValue(null)
          this.formulario.controls['problema_dete'].setValue(null)
          this.nombre_equipo = ""
          
          this.formulario.controls['tipo_Mantenim'].disable()
          this.formulario.controls['Observaciones'].disable()
          this.formulario.controls['comentarios_U'].disable()
          this.formulario.controls['actividad_Man'].disable()
          this.verCampoProblema=true
  
        })
      }
    })
  }

  /* EN CASO DE DAR CLICK HACIA ATRAS, NOS REDIRIGE A LA VISTA DE CONTROL DE MANTENIMIENTOS */
  clickAtras(event:any)
  {
    this.router.navigate(['layout/controlDeMantenimiento'])
  }
  
  /* 
      EN CASO DE TENER QUE AGREGAR OTRO EQUIPO QUE NO FUE AGENDADO, SE PUEDE SELECCIONAR DEL INPUT SUERIOR DE 
      EQUIPOS Y ESTE LO ENVÌA A LA DATA DE LOS EQUIPOS A EDITAR SU MANTENIMEINTO  
  */
  clickAgregarEquipo(equipo:Equipo)
  { 
    /* DECLARAMOS UNA LISTA AUXILIAR PARA CONTENER EL DATA DE LOS EQUIPOS A EDITAR SU MANTENIMEINTO */
    let lista_mantenimiento_aux = this.dataSource.data

    /* 
      AQUÍ AÑADE AL EQUIPO A UN DETALLE SIN ID DE REGISTRO PERO QUE SIRVE PARA CNTENER AL EQUIPO QUE SE QUIERE 
      REGISTRAR SU MANTENIMEINTO 
    */
    let mantenimientoEquipo: MantenimientoEquipo = new MantenimientoEquipo();
    mantenimientoEquipo.equipo_id           = equipo.id
    mantenimientoEquipo.equipo              = equipo
    mantenimientoEquipo.nombre_equipo       = equipo.name + ""
    mantenimientoEquipo.tipo_activo         = equipo.tipo
    
    /* AGREGAMOS AL EQUIPO A LA LISTA AUXILIAR Y ESA LLISTA LA MANDAMOS AL DATA SOURCE PARA ACTUALIZAR SU CONTENIDO */
    lista_mantenimiento_aux.push(mantenimientoEquipo)
    this.dataSource.data = lista_mantenimiento_aux
    
    /* DEJAMOS EN BLANCO LOS INPUTS PARA AÑADIR OTRO EQUIPO */
    this.input___Equipo.setValue(null)
    this.lista____equipos.splice( this.lista____equipos.indexOf(equipo) , 1 )
  }

  /* 
    CANCELAR EL REGISTRO CANCELA EL MANTENIMEINTO PARA UN EQUIPO, IGUAL REVISA SI FUE AGENDADO O NO  
  */
  clickCancelarRegistro( mantenimiento: MantenimientoEquipo)
  {
    let lista_mantenimiento_aux : MantenimientoEquipo [] = this.dataSource.data

    /*PRIMERO VALIDAMOS SI EL MANTENIMIENTO ESTÁ EN LA BASE DE DATOS O NO */
    if(mantenimiento.id_mantenimiento_equipo!=null)
    {
      /** DE APARECER EN LA BASE DE DATOS SE PREGUNTA SI REALMENTE SE QUIERE REMOVER */
      Swal.fire({
        icon:'question',
        title:'¿Quiere cancelar el mantenimeinto para este equipo?',
        allowEnterKey:false,
        allowEscapeKey:false,
        allowOutsideClick:false,
        showCancelButton:true,
        showConfirmButton:true,
        confirmButtonText:'SÍ',
        cancelButtonText:'NO',
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
      }).then( (result)=>{
        if(result.isConfirmed)
        {
          
          this.mantenimeintoEquipoService.updateMantenimientoEquipo( Number(mantenimiento.id_mantenimiento_equipo) , "CANCELADO" , "CANCELADO","CANCELADO").subscribe(data=>{
            //REMOVEMOS EL MANTENIMIENTO DEL DATA SOURCE 
            lista_mantenimiento_aux.splice(lista_mantenimiento_aux.indexOf(mantenimiento),1)
            this.dataSource.data = lista_mantenimiento_aux      
          })
          /* Borramos los campos del formulario en caso de haberse seleccionado el equipo  */
          this.formulario.controls['tipo_Mantenim'].setValue(null)
          this.formulario.controls['Observaciones'].setValue(null)
          this.formulario.controls['comentarios_U'].setValue(null)
          this.formulario.controls['actividad_Man'].setValue(null)
          this.formulario.controls['problema_dete'].setValue(null)
          this.nombre_equipo = ""
          
          this.formulario.controls['tipo_Mantenim'].disable()
          this.formulario.controls['Observaciones'].disable()
          this.formulario.controls['comentarios_U'].disable()
          this.formulario.controls['actividad_Man'].disable()
          this.verCampoProblema = true
        }
      })
    }
    else
    {
      /**REMOVEMOS EL MANTENIMIENTO DEL DATA SOURCE */
      lista_mantenimiento_aux.splice(lista_mantenimiento_aux.indexOf(mantenimiento),1)
      this.dataSource.data = lista_mantenimiento_aux
      this.lista____equipos.push(mantenimiento.equipo)
    }
  }

  /*
     CUANDO YA NO EXISTAN MÀS EQUIOS A LOS CUALES DAR MANTENIMIENTO, PROCEDEMOS A DAR POR TERMINADA O CONCLUIDA
     ESTE MANTENIMIENTO (AGENDADO O NO)
  */
  clickGuardarRegistro(event:any)
  {
    /* PRMERO SE SOLICITA UNA CONFIRMACIÓN */
    Swal.fire({
      icon:'question',
      title:'¿Quieres dar por terminado este mantenimiento?',
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      showCancelButton:true,
      showConfirmButton:true,
      confirmButtonText:'SÍ',
      cancelButtonText:'NO',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
    }).then(  (result)=>{

      if(result.isConfirmed)
      {
        let id_empleado_realiza    :number   = JSON.parse(localStorage.getItem('empleado') || '{}').id;
        let nombre_empleado_realiza:string   = JSON.parse(localStorage.getItem("empleado") || '{}').nombreEmpleado + " " + JSON.parse(localStorage.getItem("empleado") || '{}').apellidoPat + " " + JSON.parse(localStorage.getItem("empleado") || '{}').apellidoMat

        let observaciones_generales:string = this.input_Observ_G.value
        if(observaciones_generales == null || observaciones_generales==""){observaciones_generales="SIN OBSERVACIONES"}
        this.registro_mantenimiento.observ_generales = observaciones_generales
        this.registro_mantenimiento.status_registro = "REALIZADO"
        this.registro_mantenimiento.id_empleado_realiza = id_empleado_realiza
        this.registro_mantenimiento.nombre_empleado_realiza=nombre_empleado_realiza

        /* EN CASO DE CONFIRMAR SOLO SE ACTUALIZA EL ESTADO DEL REGISTRO DE MANTENIMEINTO GENERAL */

        this.registro_mantenimeintoService.updateRegistrosMantenimiento( this.registro_mantenimiento ).subscribe(data=>{
          /* CONFIRMAMOS LA OPERACION MEDIANTE UN CUADRO DE CONFIRMACIÓN */
          Swal.fire({
            icon:'success',
            title:'Mantenimiento realizado',
            allowEnterKey:false,
            allowEscapeKey:false,
            allowOutsideClick:false,
            showCancelButton:false,
            showConfirmButton:false,
            timer:2000
          }).then((result)=>{

            /* REDIRECCIONAMOS A OTRA VISTA */
            this.router.navigate(['layout/controlDeMantenimiento'])
          })  
        })
      }
    })
  }
  
  /* H A B I L I T A R    B T N    G U A R D A R */
  habilitar_Btn_Guardar()
  {
    if(this.formulario.value.tipo_Mantenim!=null && this.formulario.value.actividad_Man?.length > 0)
    { 
      return true
    }
    return false
  }
  
  /* V A L I D A C I Ó N */
  validacion_data()
  {
    if(this.dataSource.data.length ==0 )
      {
        return true
      }
      return false
  }
  
  /** B A R R A   D E   B Ú S Q U E D A*/
  llenarListaEquiposDispo()
  {
    
    let listaTablas:string[] = ["Computer","NetworkEquipment","Printer","Monitor","Peripheral"]
    let sucursal: string =""

    /** CERIFICAMOS Y CAMBIAMOS LA SUCURSAL SI ES CORPORIATIVO POR CAS */
    if( this.registro_mantenimiento.sucursal_Tabla == "CORPORATIVO" ){ sucursal = "CAS" }
    else{ sucursal = this.registro_mantenimiento.sucursal_Tabla + "" }
    
    /** CARGAMOS TODOS LOS EQUIPOS DE LA SUCURSAL */
    for(let i = 0; i< listaTablas.length; i++)
    {
      this.equipoService.todosLosEquipos( listaTablas[i] ).subscribe(lista_equipos_tabla=>{
        lista_equipos_tabla.forEach( equipo=>{

          if(equipo.locations_id == sucursal && equipo.states_id=="Asignado") 
          {
                if( listaTablas[i]=="Computer"         ){ equipo.tipo = "COMPUTADORA"        }
            else if( listaTablas[i]=="NetworkEquipment" ){ equipo.tipo = "DISPOSITIVO DE RED" }
            else if( listaTablas[i]=="Printer"          ){ equipo.tipo = "IMPRESORA"          }
            else if( listaTablas[i]=="Monitor"          ){ equipo.tipo = "MONITOR"           }
            else if( listaTablas[i]=="Peripheral"       ){ equipo.tipo = "DISPOSITIVO"        }
            
            this.lista____equipos.push(equipo)
          }
        })

        /** FILTRAMOS LOS EQUIPOS QUE YA ESTAN AGENDADOS DE LOS QUE SON OPCCIONA A GREGAR*/
        this.filtrar()
      }) 
    }
  }

  filtrar()
  {
    /**  CARGAMOS LOS EQUIPOS AGENDADOS */
    this.mantenimeintoEquipoService.mantenimientosEquipoPorIdRegistro( this.id_registro_mantenimiento ).subscribe( lista_manteniminetos =>{
      lista_manteniminetos.forEach(mantenimiento=>{
        for(let i = 0; i < this.lista____equipos.length; i++)
        {
          /** COMPARAMOS */
          if
          (
            mantenimiento.equipo_id   ==  this.lista____equipos[i].id   &&
            mantenimiento.tipo_activo ==  this.lista____equipos[i].tipo
          )
          {
            this.lista____equipos.splice(this.lista____equipos.indexOf(this.lista____equipos[i]),1)
          }    
        }
      })
    })
  }

  accionFiltrado()
  {
    this.lista_equipos_filtrada = this.input___Equipo.valueChanges.pipe(
      startWith(''),
      map(value => this.filtrado(value)),
    );
  }

  private filtrado(value: string): Equipo[] 
  {
    const filterValue = (value+"").toLowerCase();
    return this.lista____equipos.filter(option => option.name.toLowerCase().includes(filterValue));  
  }
 
  mensajeDeError()
  {
    Swal.fire({
      icon:'error',
      title:'Sin conexión ',
      text:'Vuelva a intentarlo más tarde.',
      showConfirmButton:false,
      timer:3000,
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false
    })
  }
}