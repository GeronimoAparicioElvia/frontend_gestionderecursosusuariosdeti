import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleMantenimientoEditComponent } from './detalle-mantenimiento-edit.component';

describe('DetalleMantenimientoEditComponent', () => {
  let component: DetalleMantenimientoEditComponent;
  let fixture: ComponentFixture<DetalleMantenimientoEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetalleMantenimientoEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleMantenimientoEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
