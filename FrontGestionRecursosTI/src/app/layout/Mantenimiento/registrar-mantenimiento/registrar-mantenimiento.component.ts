import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Equipo } from 'src/app/administracion/modelos/Inventario/Equipo/equipo';
import { ActividadMan } from 'src/app/administracion/modelos/Mantenimiento/actividad-man';
import { MantenimientoEquipo } from 'src/app/administracion/modelos/Mantenimiento/mantenimiento-equipo';
import { RegistroMantenimiento } from 'src/app/administracion/modelos/Mantenimiento/registro-mantenimiento';
import { TipoMan } from 'src/app/administracion/modelos/Mantenimiento/tipo-man';
import { Sucursal } from 'src/app/administracion/modelos/sucursal';
import { SucursalService } from 'src/app/administracion/servicios';
import { EquipoService } from 'src/app/administracion/servicios/Inventario/Equipo/equipo.service';
import { ActividadManService } from 'src/app/administracion/servicios/Mantenimiento/actividad-man.service';
import { ElementMantenimientoService } from 'src/app/administracion/servicios/Mantenimiento/element-mantenimiento.service';
import { MantenimientoEquipoService } from 'src/app/administracion/servicios/Mantenimiento/mantenimiento-equipo.service';
import { RegistroMantenimientoService } from 'src/app/administracion/servicios/Mantenimiento/registro-mantenimiento.service';
import { TipoManService } from 'src/app/administracion/servicios/Mantenimiento/tipo-man.service';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registrar-mantenimiento',
  templateUrl: './registrar-mantenimiento.component.html',
  styleUrls: ['./registrar-mantenimiento.component.scss']
})
export class RegistrarMantenimientoComponent implements OnInit {
  /** O B J E C T S */
  equipo__select !: Equipo 

  /** F O R M   C O N T R O L S */
  input_sucursal: FormControl = new FormControl()   /*CONTROLADOR DEL CAMPO NOMBRE SUCURSAL */
  input___Equipo: FormControl = new FormControl()   /*CONTROLADOR DEL CAMPO NOMBRE EQUIPO */
  input_Observ_G: FormControl = new FormControl()   /*CONTROLADOR DEL CAMPO NOMBRE OBSERVACIONES O COMENTARIOS */

  /** L I S T A S */
  lista_sucursales        : Sucursal      []  =  []
  listaFiltradaSucursales : ReplaySubject<Sucursal[]> = new ReplaySubject<Sucursal[]>(1);
  protected _onDestroy = new Subject<void>();

  lista____equipos: Equipo [] = []              /* LISTA DE EQUIPOS PERTENECIENTES A UNA SUCURSAL */
  lista_equipos_filtrada!: Observable<Equipo[]>; /* LISTA FILTRADA DE LOS EQUIPOS BUSCANDO COICIDENCIAS DE NOMBRE */

  lista_Tipos_Mantenimie: TipoMan     [] = [] /* LISTA DE TIPOS DE MANTENIMEINTO DEL SELECT*/
  lista_actividades_Mant: ActividadMan[] = [] /* LISTA DE ACTIVIDADES DE MANTENIMEINTO DEL SELECT*/

  /** T A B L A */
  encabezados:string[] = ["OPCIONES","EQUIPO","TIPO DE MANTENIMIENTO"]     /* ENCABEZADO DE LA TABLA */
  dataSourceME          = new MatTableDataSource<MantenimientoEquipo>([]); /* DATA SOURCE EQUIPOS DE LA TABLA*/
  @ViewChild('paginatorME', { static: true }) public paginatoME !: MatPaginator;  /* PAGINATOR DE LA TABLA */


  verCampoProblema:boolean = true

  constructor(
    private actividadManService         : ActividadManService         , /* CONEXIÓN CON LA TABLA ACTIVIDAD DE MANTENIMIENTO DE LA BASE DE DATOS */
    private equipoService               : EquipoService               , /* CONEXIÓN CON LA TABLA EQUIPO DE  GLPI*/
    private router                      : Router                      ,
    private sucursalService             : SucursalService             , /* CONEXIÓN CON EL ENDPOINT SUCURSALES */
    private tipoManService              : TipoManService              , /* CONEXIÓN CON LA TABLA TIPO DE MANTENIMIENTO DE LA BASE DE DATOS */
    private mantenimientoEquipoService  : MantenimientoEquipoService  , /* CONEXIÓN CON LA TABLA MANTENIMIENTO EQUIPO DE LA BASE DE DATOS */
    private registroMantenimientoService: RegistroMantenimientoService, /* CONEXIÓN CON LA TABLA REGISTRO DE MANTENIMIENTO DE LA BASE DE DATOS */
    private elementMantenimientoService : ElementMantenimientoService , /* CONEXIÓN CON LA TABLA ELEMENTO DE MANTENIMIENTO DE LA BASE DE DATOS */
    private formBuilder                 : FormBuilder                 ,
    ){}

    /** F O R M U L A R I O */
    formulario = this.formBuilder.group({
      tipo_Mantenim:             [{value: '', disabled: true},[]] , /* CONTROL DEL CAMPO TIPO DE MANTENIMIENTO */
      actividad_Man:             [{value: '', disabled: true},[]] , /* CONTROL DEL CAMPO ACTIVIDAD DE MANTENIMIENTO */
      Observaciones:             [{value: '', disabled: true},[]] , /* CONTROL DEL CAMPO OBSERVACIONES DE MANTENIMIENTO */
      comentarios_U:             [{value: '', disabled: true},[]] , /* CONTROL DEL CAMPO COMENTARIOS DE USUARIO */
      preblema_dete:             [{value: '', },[]] ,               /* CONTROL DEL CAMPO PROBLEMA O FALLO */
    });

    ngOnInit()
    { 
      this.llenarListas()
      this.accionFiltrado()
      this.listaFiltradaSucursales.next(this.lista_sucursales.slice());
      this.input_sucursal_nombre.valueChanges
        .pipe(takeUntil(this._onDestroy))
        .subscribe(() => {
          this.filterSucursales();
        });
    } 

    /** D A T O S   D E   I N I C I O */
    llenarListas()
    {

      /* CARGAMOS LA LISTA DE SUCURSALES */
      this.sucursalService.getSucursales().subscribe(lista_sucursales=>{
        this.lista_sucursales = lista_sucursales
      })
    }

    /* EVENTO   DE   CLICK QUE CARGA EL EQUIPO EN EL FORMULARIO  PARA EDIITAR SU MANTENIMIENTO EQUIPO, 
       BORRANDO EL COTENIDO DEL FORMULARIO Y LO HABILITA PARA EDITARLO 
    */
    click_Select_Equipo( equipo : Equipo )
    {
      this.equipo__select = equipo
      this.input___Equipo.setValue(equipo.name)

      this.formulario.controls['tipo_Mantenim'].setValue(null)
      this.formulario.controls['Observaciones'].setValue(null)
      this.formulario.controls['comentarios_U'].setValue(null)
      this.formulario.controls['actividad_Man'].setValue(null)

      this.formulario.controls['tipo_Mantenim'].enable()
      this.formulario.controls['Observaciones'].enable()
      this.formulario.controls['comentarios_U'].enable()

      this.tipoManService.allTiposMantenimientoActivos().subscribe(lista_tipos=>{
        this.lista_Tipos_Mantenimie = lista_tipos
      })
    }

    /* PRECARGA LOS MANTENIMEINTOS EN UNA TABLA ANTES DE GUARDARLOS EN LA BASE DE DATOS */
    click_pre_guardar(event:any)
    {
      /* LOS CAMPOS DEL FORMULARIO SON EVALUADOS SI ESTAN O NO VACÍOS */
      let comentarios  : string  = this.formulario.value.comentarios_U
      let observaciones: string  = this.formulario.value.Observaciones
      let tipo_man     : TipoMan = this.formulario.value.tipo_Mantenim
      let problema_dete: string  = this.formulario.value.preblema_dete

      /* EN CASO DE ENCONTRARSE VACÍOS SE LE DEFINE UNA CADENA POR DEFECTO*/
      if(comentarios  ==null || comentarios==undefined ){ comentarios="SIN COMENTARIOS" }
      if(observaciones==null || comentarios==undefined ){ observaciones="SIN OBSERVACIONES" }
      if(problema_dete==null || comentarios==undefined ){ observaciones="SIN DESCRIPCIÓN" }
      

      /*  AHOSA COLOCAMOS LOS CAMPOS NECESARIOS PARA GENERAR UN MANTENIMEINTO EQUIPO */
      let mantenimientoEquipo: MantenimientoEquipo = new MantenimientoEquipo();
      mantenimientoEquipo.equipo_id           = this.equipo__select.id
      mantenimientoEquipo.equipo              = this.equipo__select
      mantenimientoEquipo.nombre_equipo       = this.equipo__select.name
      mantenimientoEquipo.tipo_activo         = this.equipo__select.tipo
      mantenimientoEquipo.tMantenimineto      = tipo_man
      mantenimientoEquipo.nombre_tipo_man     = mantenimientoEquipo.tMantenimineto.nombre
      mantenimientoEquipo.comentarios_usuario = comentarios
      mantenimientoEquipo.observ_equipo       = observaciones
      mantenimientoEquipo.problema_localizado = problema_dete
      mantenimientoEquipo.lista_actividades   = this.formulario.value.actividad_Man
      
      let lista_mantenimiento: MantenimientoEquipo [] = this.dataSourceME.data
      lista_mantenimiento.push(mantenimientoEquipo)
      this.dataSourceME.data = lista_mantenimiento
      this.dataSourceME.paginator = this.paginatoME

      /*  VACIAMOS Y BLOQUEMOS EL FORMULARIO PARA EL PRÓXIMO EQUIPO A EDITAR SU MANTENIMEINTO */
      this.lista____equipos.splice(this.lista____equipos.indexOf(this.equipo__select),1)
      this.input___Equipo.setValue(null)
      
      /*  DESABILITAMOS */
      this.formulario.controls['tipo_Mantenim'].disable()
      this.formulario.controls['Observaciones'].disable()
      this.formulario.controls['comentarios_U'].disable()
      this.formulario.controls['actividad_Man'].disable()
      this.verCampoProblema = true

      /*  VACIAMOS */
      this.formulario.controls['tipo_Mantenim'].setValue(null)
      this.formulario.controls['Observaciones'].setValue(null)
      this.formulario.controls['comentarios_U'].setValue(null)
      this.formulario.controls['actividad_Man'].setValue(null)
      this.formulario.controls['preblema_dete'].setValue(null)
    }

    /* ELIMINA UN REGISTRO DE MANTENIMIENTO DE LA TABLA */
    click_remover(mantenimiento:MantenimientoEquipo)
    {
      let lista_aux:MantenimientoEquipo [] = this.dataSourceME.data
      lista_aux.splice(lista_aux.indexOf(mantenimiento),1)
      this.dataSourceME.data = lista_aux
      this.lista____equipos.push( mantenimiento.equipo )
    }

    /* CANCELA TODO EL REGISTRO DE LA TABLA HASTA ESE MOMENTO */
    clickCancelar(event:any)
    {
      Swal.fire({
        title            : '¿Está seguro de salir antes de guardar?',
        icon             : 'question',
        text             : 'Nota: Toda la información se perderá al salir sin guardar',
        allowEnterKey    : false,
        allowEscapeKey   : false,
        allowOutsideClick: false,
        showCancelButton : true,
        showConfirmButton: true,
        confirmButtonText: 'SÍ',
        cancelButtonText : 'NO'
      }).then((result)=>{
        if(result.isConfirmed)
        {
          this.router.navigate(['layout/controlDeMantenimiento'])
        }
      })
    }

    /* GUARDA TODO LO INGRESADO (LA DATA DE LA TABLA, OBSERVACIONES ETC) */
    clickGuardar(event:any)
    {
      /* PEDIMOS CONFIRMACIÓN */
      Swal.fire({
        icon:'question',
        title:'¿Quieres guardar este registro de mantenimiento?',
        allowEnterKey:false,
        allowEscapeKey:false,
        allowOutsideClick:false,
        showCancelButton:true,
        showConfirmButton:true,
        confirmButtonText:'SÍ',
        cancelButtonText:'NO',
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
      }).then( async (result)=>{
        
        /* AL CONFIRMAR SE INGRES LA INFORMACON NECESARIA PARA GENERAR UN REGISTRO GENERAL DE MANTENIMEINTO */
        if(result.isConfirmed)
        {
          let registroMantenimiento:RegistroMantenimiento = new RegistroMantenimiento()
          let observaciones_generales:string = this.input_Observ_G.value
          let nombre:string = JSON.parse(localStorage.getItem("empleado") || '{}').nombreEmpleado + " " + 
                              JSON.parse(localStorage.getItem("empleado") || '{}').apellidoPat    + " " + 
                              JSON.parse(localStorage.getItem("empleado") || '{}').apellidoMat

          
          /* VALIDAMOS QUE LOS CAMPOS NO ESTEN VACÌOS */
          if(observaciones_generales == null || observaciones_generales=="")
          {
            observaciones_generales="SIN OBSERVACIONES"
          }


          /* CREAMOS EL REGISTRO GENERAÑ */
          registroMantenimiento.sucursal_id           = this.input_sucursal.value.idSucursal
          registroMantenimiento.nombre_sucursal       = this.input_sucursal.value.nombreSucursal
          registroMantenimiento.fecha_creacion        = new Date()
          registroMantenimiento.fecha_mod             = new Date()
          registroMantenimiento.fecha_agendada        = new Date()
          registroMantenimiento.observ_generales      = observaciones_generales
          registroMantenimiento.status_registro       = "REALIZADO"
          registroMantenimiento.descripcion           = "EVENTUAL"
          registroMantenimiento.id_empleado_registra  = JSON.parse(localStorage.getItem('empleado') || '{}').id;
          registroMantenimiento.id_empleado_realiza   = JSON.parse(localStorage.getItem('empleado') || '{}').id;
          registroMantenimiento.nombre_empleado_realiza = nombre
          registroMantenimiento.nombre_empleado_registra = nombre

          /* EN BASE A LO GUARDADO GENERAMOS LOS DETALLES DE MANTENIMIENTO */
          this.registroMantenimientoService.saveRegistrosDeMantenimiento(registroMantenimiento).subscribe(registro_guardado=>{
            this.guardarMantenimientos(Number(registro_guardado.id_registro_mantenimiento),this.dataSourceME.data)
          })

          /* AL TERMINAR MOSTRAMOS UN MENSAJE DE QUE SE TERMINÓ CON ÉXITO ESTA OPERACIÓN */
          Swal.fire({
            icon:'success',
            title:'Mantenimiento registrado',
            allowEnterKey:false,
            allowEscapeKey:false,
            allowOutsideClick:false,
            showCancelButton:false,
            showConfirmButton:false,
            timer:2000
          }).then((redireccion)=>{
            
            /* REDIRECCIONAMOS AL CONTROL DE MANTENIMIENTOS */
            this.router.navigate(['layout/controlDeMantenimiento'])
          })
        }
      })
    }

    /* CREMOS LOS DETALLES DE MANTENIMIENTO */
    guardarMantenimientos(id_registro:number, mantenimientos:MantenimientoEquipo[])
    {
      /* VALIDAMOS QUE AUN SE TENGAN REGISTROS POR GUARDAR */
      if(mantenimientos.length==0)
      {
        return 
      }
      /* EN CASO DE TENERLOS PRIMERO GENRAMOS SU REGISTRO EN LA BASE DE DATOS */
      else
      {
        this.mantenimientoEquipoService.saveRegistrosDeMantenimiento(mantenimientos[0],id_registro).subscribe(data=>{
          
          /* AHORA RECORREMOS TODAS LAS ACTIVIDADES QUE SE LE REALIZARON E IGUALMENTE LAS GUARDAMOS EN LA BASE DE DATOS */
          mantenimientos[0].lista_actividades.forEach(actividad=>{

            this.elementMantenimientoService.saveElementoMantenimiento(Number(data.id_mantenimiento_equipo),Number(actividad.id_actividad_mant)).subscribe(data=>{})
          })

          /* ENVIAMOS NUEVAMENTE LOS DETALLES SIN REGISTRAR A ESTE MÉTODO NUEVAMENTE */
          let aux: MantenimientoEquipo[] = mantenimientos
          aux.splice(0,1)
          this.guardarMantenimientos(id_registro,aux)
          
        })
      }
    }

    /* 
      SELECCIONAMOS EL TIPO DE MANTENIMEINTO  Y CARGA LAS ACTIVIDADES ACTIVAS QUE 
      PERTENECEN AL TIPO DE MANTENIMEINTO
    */
    select_tipo_mantenimiento()
    {
      /* HABILITAMOS EL CAMPO PARA SELECCIONAR LAS ACTIVIDADES DE MANTENIMEINTO */
      this.formulario.controls['actividad_Man'].enable()
      let tipo_man : TipoMan = this.formulario.value.tipo_Mantenim

      /* CARGAMOS LAS ACTIVIDADES CORRESPONDIENTES A ESTE TIPO DE MANTENIMIENTO */
      this.actividadManService.allActividadesDeMantenimientoActivasPorTipoMantenimiento( Number(tipo_man.id_tipo_mant) ).subscribe(lista_act=>{
        this.lista_actividades_Mant = lista_act
      })

      /* VALIDA SI ES CORRECTIVO O PREVENTIVO PARA HABILITAR O NO UN CAPO EXTRA */
      if(tipo_man.nombre=="CORRECTIVO")
      {
        this.formulario.controls['preblema_dete'].setValue(null)
        this.verCampoProblema = false
      }
      else
      {
        this.verCampoProblema = true
        this.formulario.controls['preblema_dete'].setValue(null)
      }
    }

    /** F I L T R O   D E   B Ú S Q U E D A   D E   E Q U I P O */
    accionFiltrado()
    {
      this.lista_equipos_filtrada = this.input___Equipo.valueChanges.pipe(
        startWith(''),
        map(value => this.filtrado(value)),
      );
    }

    private filtrado(value: string): Equipo[] 
    {
      const filterValue = (value+"").toLowerCase();
      return this.lista____equipos.filter(option => option.name.toLowerCase().includes(filterValue));  
    }

    /** CARGA UNA LISTA CON TODOS LOS EQUIPOS QUE HAY EN UNA SUCURSAL */
    async llenar_Lista_Equipos_Sucursal(event:any)
    {

      /* AL SELECCIONAR LA SUCURSAL, SE CARGAN LOS EQUIPOS QUE EXISTEN EN ESA SUCURSAL. 
         BORRAMOS Y DESHABILITAMOS EL FORMULARIO PARA QUE ESTÉ LISTO PARA INICIAR CON 
         SU LLENADO CORRESPONDIENTE DEL MANTENIMINTO A UN EQUIPO 
      */
      this.input___Equipo.setValue(null)
      
      this.formulario.controls['tipo_Mantenim'].setValue(null)
      this.formulario.controls['actividad_Man'].setValue(null)
      this.formulario.controls['Observaciones'].setValue(null)
      this.formulario.controls['comentarios_U'].setValue(null)

      this.formulario.controls['tipo_Mantenim'].disable()
      this.formulario.controls['actividad_Man'].disable()
      this.formulario.controls['Observaciones'].disable()
      this.formulario.controls['comentarios_U'].disable()

      let lista_aux: MantenimientoEquipo [] = this.dataSourceME.data
      lista_aux.splice(0, lista_aux.length)
      this.dataSourceME.data = lista_aux

      let sucursal: string    = ""
      let tablas  : string [] = ["Computer","Networkequipment","Monitor","Printer","Peripheral","Rack","Phone"]
      this.lista____equipos.splice(0,this.lista____equipos.length)

      if(this.input_sucursal.value.nombreSucursal == "CORPORATIVO"){ sucursal = "CAS" }
      else { sucursal = this.input_sucursal.value.nombreSucursal}

      /* CARGAMOS LOS EQUIPOS DE LA SUCURSAL */
      for(let i = 0; i < tablas.length; i ++)
      {
        this.equipoService.todosLosEquipos(tablas[i]).subscribe(lista=>{
          lista.forEach(equipo=>{
            
            if(equipo.locations_id == sucursal && equipo.states_id=="Asignado")
            {
                   if(tablas[i]=="Computer"         ){ equipo.tipo="COMPUTADORA" }
              else if(tablas[i]=="Networkequipment" ){ equipo.tipo="DISPOSITIVO DE RED" }
              else if(tablas[i]=="Monitor"          ){ equipo.tipo="IMPRESORA" }
              else if(tablas[i]=="Printer"          ){ equipo.tipo="MONITOR" }
              else if(tablas[i]=="Peripheral"       ){ equipo.tipo="DISPOSITIVO" }
              else if(tablas[i]=="Rack"             ){ equipo.tipo="RACK" }
              else if(tablas[i]=="Phone"            ){ equipo.tipo="TELEFONO" }

              this.lista____equipos.push(equipo)
            }
          })
        })
      }
    }
    
    /* HABILITA EL BOTÓN PARA GUARDAR TODO EL MANTENIMIENTO */
    habilitar_boton()
    {
      if( this.formulario.value.tipo_Mantenim != null && this.formulario.value.actividad_Man?.length > 0 )
      {
        return true
      }
      return false
    }

    /* HABILITA EL BOTÓN PARA PRE CARGAR EN LA TABLA UN DETALLE DE MANTENIMIENTO */
    habilitar_boton_guardar_Todo()
    {
      if(this.dataSourceME.data.length > 0)
      {
        return true;
      }
      return false;
    }

    /*  ABRE UNA VENTANA CON INFORMACIÓN SOBRE LA VISTA  */
    clickMasInformacion(event:any)  
    {
      Swal.fire({
        title:'Registro eventual de mantenimeinto',
        html:
        '<p align="justify">Por lo regular siempre se busca que los mantenimientos sean programados, en caso de que exista algún'+
        'imprevisto en alguna sucursal, esta pantalla sirve para documentar el mantenimiento que se esta realizando en curso. Pa'+
        'ra manejar adecuadamente esta opcion se debe hacer lo siguiente:</p>'                                                   + 
        
        '<p align="justify"> <b>- SUCURSAL: </b>  El primer input es para que se seleccione la sucursal en la que se esta reali'+
        'zando el mantenimiento</p>'                                                                                             +
        
        '<p align="justify"> <b>- EQUIPO: </b>  El segundo input es para buscar y seleccionar el equipo actual al que se le est'+
        'a dando manteimiento</p>'                                                                                               +
        
        '<p align="justify"> <b>- FORMULARIO: </b>  Al seleccionar un equipo este pasará al formulario en donde se tendra que e'+
        'legir el tipo de mantenimiento que se realizó, las actividades, observaciones o comentarios que se tengan y enc aso de '+
        'ser una reparación colocar la razón de esta. Finalizando al dar click en salvar.</p>'                                   +

        '<p align="justify"> <b>- TABLA: </b>  Se visualizan todos los equipos que fueron considerados o que participaron duran'+
        'te el mantenimeinto.</p>'                                                                                               +

        '<p align="justify"> <b>- BOTONES: </b> Guardar y Cancelar mantenimiento.</p>'
        ,
        allowEnterKey:false,
        allowEscapeKey:false,
        allowOutsideClick:false,
        confirmButtonColor: '#d33',
        confirmButtonText:'Cerrar'
      })
    }

    /* CARGA EL OBJETO EMPLEADO QUE S EUTILIA PARA LA ASIGNACION */
    async clickCargarSucursal(event:any)
    {       
      let event2:any
      await this.llenar_Lista_Equipos_Sucursal(event2)
    }

    input_sucursal_nombre:FormControl =  new FormControl()

    /*Filtro que busca coincidencias dentro del select search */
    protected filterSucursales() {
      if (!this.lista_sucursales) {
        return;
      }
      /* Recuperamos el valor del input */
      let search = this.input_sucursal_nombre.value;
      if (!search) {
        this.listaFiltradaSucursales.next(this.lista_sucursales.slice());
        return;
      } else {
        search = search.toLowerCase();
      }
      /* Retornamos sla lista de eempleados que cumplen con las coincidencias */
      this.listaFiltradaSucursales.next(
        this.lista_sucursales.filter(sucursal => sucursal.nombreSucursal.toLowerCase().indexOf(search) > -1)
      );
    }
}
