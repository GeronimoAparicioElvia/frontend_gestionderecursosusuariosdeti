import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgendarMantenimientoComponent } from './agendar-mantenimiento.component';

describe('AgendarMantenimientoComponent', () => {
  let component: AgendarMantenimientoComponent;
  let fixture: ComponentFixture<AgendarMantenimientoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgendarMantenimientoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgendarMantenimientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
