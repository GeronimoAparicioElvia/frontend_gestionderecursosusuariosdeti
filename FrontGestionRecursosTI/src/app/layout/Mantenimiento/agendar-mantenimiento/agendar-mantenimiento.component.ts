import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Equipo } from 'src/app/administracion/modelos/Inventario/Equipo/equipo';
import { Sucursal } from 'src/app/administracion/modelos/sucursal';
import   Swal from 'sweetalert2';
import { ActividadManService } from 'src/app/administracion/servicios/Mantenimiento/actividad-man.service';
import { ActividadMan } from 'src/app/administracion/modelos/Mantenimiento/actividad-man';
import { RegistroMantenimiento } from 'src/app/administracion/modelos/Mantenimiento/registro-mantenimiento';
import { RegistroMantenimientoService } from 'src/app/administracion/servicios/Mantenimiento/registro-mantenimiento.service';
import { MantenimientoEquipo } from 'src/app/administracion/modelos/Mantenimiento/mantenimiento-equipo';
import { MantenimientoEquipoService } from 'src/app/administracion/servicios/Mantenimiento/mantenimiento-equipo.service';
import { Router } from '@angular/router';
import { SucursalService } from 'src/app/administracion/servicios';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { EquipoService } from 'src/app/administracion/servicios/Inventario/Equipo/equipo.service';




@Component({
  selector: 'app-agendar-mantenimiento',
  templateUrl: './agendar-mantenimiento.component.html',
  styleUrls: ['./agendar-mantenimiento.component.scss']
})
export class AgendarMantenimientoComponent implements OnInit {

  /** E L E M E N T O S   E X T R A */
  fecha:Date = new Date();

  /** T A B L A */
  encabezados:string [] = ["OPCIONES","NOMBRE DEL EQUIPO","ESTADO","USUARIO RESPONSABLE","TIPO","MODELO","COMENTARIOS"]
  dataSource           = new MatTableDataSource<Equipo>([]);
  @ViewChild('paginator', { static: true }) public paginator !: MatPaginator;
  
  /** F O R M S   C O N T R O L S */
  sucursal              = new FormControl()
  tipo_De_Equipo        = new FormControl()
  actividades           = new FormControl()
  actividad_mante       = new FormControl()
  fecha_agend           = new FormControl()
  observ_generales      = new FormControl()
  
  /** L I S T A S */
  lista_tipo_Equipo       : string        []  =  []
  lista_activid_a_realizar: ActividadMan  []  =  []
  listaEquipos            : Equipo        []  =  []

  lista_sucursales        : Sucursal      []  =  []
  listaFiltradaSucursales : ReplaySubject<Sucursal[]> = new ReplaySubject<Sucursal[]>(1);
  protected _onDestroy = new Subject<void>();

  listaTablas:string[] = ["Computer","NetworkEquipment","Printer","Monitor","Peripheral","Rack","Phone"]
  input_sucursal_nombre:FormControl =  new FormControl()
  input_sucursal:FormControl =  new FormControl()

  /** B A N D E R A S */
  habilitar_formulario:boolean = false
  constructor( public dialog:MatDialog, 
               private actividadManService: ActividadManService,
               private router:Router,
               private sucursalService: SucursalService,
               private registroMantenimientoService: RegistroMantenimientoService,
               private mantenimientoEquipoService: MantenimientoEquipoService,
               private equipoService :EquipoService
             ){}

  ngOnInit()
  { 
    this.llenarListas() 
    this.llenarListaSucursales()
    this.lista_tipo_Equipo = this.equipoService.listaActivosAsignacion(2)
    this.listaFiltradaSucursales.next(this.lista_sucursales.slice());
    this.input_sucursal_nombre.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSucursales();
      });
  }

  /*Llenamos la lista de actividades */
  llenarListas()
  {
    this.actividadManService.allActividadesDeMantenimientoPorTipoMantenimiento(1).subscribe(result=>{
      this.lista_activid_a_realizar = result
    },
    /* De no tener conexión con el servidor retornamos un mensaje de error */
    err=>
    {
      Swal.fire({
        icon:'error',
        title:'Error',  
        text:'Sin conexión con el servidor, intentelo más tarde',
        allowEnterKey:false,
        allowEscapeKey:false,
        allowOutsideClick:false,
        showConfirmButton:false,
        timer:3000
      })
    }
    )
  }
  /*Llenamos la lista de sucursales */
  llenarListaSucursales()
  {
    this.sucursalService.getSucursales().subscribe(data=>{
      this.lista_sucursales = data
    },
    /* De no tener conexión con el servidor retornamos un mensaje de error */
    err=>
    {
      Swal.fire({
        icon:'error',
        title:'Error',  
        text:'Sin conexión con el servidor, intentelo más tarde',
        allowEnterKey:false,
        allowEscapeKey:false,
        allowOutsideClick:false,
        showConfirmButton:false,
        timer:3000
      })
    }
    )
  }

  /* HABILITA EL BOTÓN PARA SELECCIOANR LOS EQUIPOS CON LOS QUE CUENTA UNA SUCURSAL */
  habilitarBotonBuscar()
  {
    if(this.sucursal.value!=null)
    {
      return true
    }
    return false
  }

  /* HABILITA EL BOTÓN PARA GUARDAR ESTE MANTENIMIENTO PROGRAMADO */
  habilitarBotonAgendar()
  {
    if(this.dataSource.data.length>0 && this.fecha_agend.value!=null && this.equiposSeleccionados())
    {
      return true
    }
    return false
  }
  
  /* GUARDA ESTE REGISTRO DE MANTENIMEINTO COMO PROGRAMADO */
  clickAgendar(event:any)
  {
    /* PEDIMOS CONFIRMACIÓN */
    Swal.fire({
      icon:'question',
      title:'¿Quiere agendar estos equipos para mantenimiento?',
      showCancelButton:true,
      showConfirmButton:true,
      confirmButtonText:'Sí',
      cancelButtonText:'No',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false
    }).then( (result)=> {

      /* AL CONFIRMAR  GENERAMOS UN REGISTRO GENERAL Y LO GUARDAMOS EN LA ABSE DE DATOS */
      if(result.isConfirmed)
      {
        if(this.observ_generales.value==null){this.observ_generales.setValue("SIN COMENTARIOS")}
        let registroMantenimiento:RegistroMantenimiento = new RegistroMantenimiento()
        registroMantenimiento.sucursal_id               = this.sucursal.value.idSucursal
        if(this.sucursal_obj.nombreSucursal=="CAS"){this.sucursal_obj.nombreSucursal="CORPORATIVO"}
        registroMantenimiento.nombre_sucursal           = this.sucursal_obj.nombreSucursal
        registroMantenimiento.fecha_creacion            = new Date()
        registroMantenimiento.fecha_mod                 = new Date()
        registroMantenimiento.fecha_agendada            = this.fecha_agend.value
        registroMantenimiento.descripcion               = "PROGRAMADO"
        registroMantenimiento.observ_generales          = this.observ_generales.value
        registroMantenimiento.status_registro           = "ESPERA"
        registroMantenimiento.id_empleado_registra      = JSON.parse(localStorage.getItem('empleado') || '{}').id;

        /* AÑADIMOS EL NOMBRE DE QUIEN AGENDA EL MANTENIMEINTO */
        let nombre:string = JSON.parse(localStorage.getItem("empleado") || '{}').nombreEmpleado + " " + JSON.parse(localStorage.getItem("empleado") || '{}').apellidoPat + " " + JSON.parse(localStorage.getItem("empleado") || '{}').apellidoMat
        registroMantenimiento.nombre_empleado_registra   = nombre
        
        /* GUARDAMOS EN LA ABASE DE DATOS */
        this.registroMantenimientoService.saveRegistrosDeMantenimiento(registroMantenimiento).subscribe(registro=>{
          
          /* CREMOS LOS DETALLES DE TODOS LOS EQUIPOS SELECCIONADOS */
          let mantenimientoEquipo: MantenimientoEquipo = new MantenimientoEquipo();

          this.dataSource.data.forEach(element=>{
            if(element.bandera == true)
            {
              mantenimientoEquipo.nombre_equipo = element.name+""
              mantenimientoEquipo.equipo_id   = element.id
              mantenimientoEquipo.tipo_activo = element.tipo
              mantenimientoEquipo.comentarios_usuario = element.comentariosExtra

              /* GUARDAMOS CADA DETALLE */
              this.mantenimientoEquipoService.saveRegistrosDeMantenimiento(mantenimientoEquipo,Number(registro.id_registro_mantenimiento)).subscribe(
                data=>{},
                err=>{
                  Swal.fire({
                    icon:'error', 
                    title:'Sin conexión, intentelo más tarde',
                    text:'intentelo más tarde',
                    showConfirmButton:false,
                    allowEnterKey:false,
                    allowEscapeKey:false,
                    allowOutsideClick:false,
                    timer:2000
                  })
                }
              )

            }
          })

          /* AL TERMINAR LANZAMOS UN MENSAJE DE REALIZACIÓN*/
          Swal.fire({
            icon:'success',
            title:'Mantenimiento agendado',
            showCancelButton:false,
            showConfirmButton:false,
            allowEnterKey:false,
            allowEscapeKey:false,
            allowOutsideClick:false,
            timer:2000
          }).then((result)=>{
            /* REDIRIGIMOS AL CONTROL DE MANTENIMEINTOS */
            this.router.navigate(['layout/controlDeMantenimiento'])
          })
        })
      }
    })
  }

  /* CANCELA LO QUE SE ESTÀ REALIZANDO */
  clickCancelar(event:any)
  {
    /* PEDIMOS CONFIRMACIÓN */
    Swal.fire({
      icon:'question',
      title:'¿Quiere salir sin guardar?',
      showCancelButton:true,
      showConfirmButton:true,
      confirmButtonText:'SÍ',
      cancelButtonText:'NO',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false
    }).then( (result)=> {
      if(result.isConfirmed)
      {
        /* REDIRIGIMOS AL HOME */
        this.router.navigate(['layout/home'])
      }
    })
  }

  /* ABRE UNA VENTANA QUE EXPLICA ¿CÓMO Y QUÉ? REALIZAR EN LA VISTA */
  clickMasInformacion(event:any)
  {
    Swal.fire({
      title:'AGENDAR MANTENIMIENTO',
      html:
      
      '<p align="justify">Comenzamos eligiendo la sucursal en la que se quiere agendar un mantenimiento'+
      ' para seguidamente se de click en el botón azul con icono de lupa que se encuentra a la derecha.'+
      '</p>'   +
      
      '<p align="justify"> Automaticamente aparece una ventana en donde se deben elegir los equipos'+
      'que se van agendar o en caso de agendar todos se da click en el botón inferior izquierdo col'+
      'or rosa. Una vez seleccionado los equipos se da click en el botón azul del centro. o enc cas'+
      'o de equibocarse de sucursal se puede dar click en e boton rojo cancelar y seleccionar otra '+
      'sucursal</p>' +

      '<p align="justify">Seleccionado los equipos solo se elige una fecha para realizar el mantenimiento.</p>'+

      '<p align="justify">Los comentarios y observaciones son opcionales.</p>',

      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      confirmButtonColor: '#d33',
      confirmButtonText:'Cerrar'
    })
  }
   
  /* CARGA EL OBJETO EMPLEADO QUE S EUTILIA PARA LA ASIGNACION */
   clickCargarSucursal(event:any)
   { 
     this.sucursal_obj = this.input_sucursal.value
     this.sucursal.setValue(this.input_sucursal.value)
     this.cargarDataTabla()   
   }

  sucursal_obj:Sucursal = new Sucursal()

  /* CARGA LOS EQUIPOS DE LA SUCURSAL*/
  cargarDataTabla()
  {
    /* Valida si la sucursal elegida fue el corporativo */
    if(this.sucursal_obj.nombreSucursal=="CORPORATIVO")
    {
      this.sucursal_obj.nombreSucursal="CAS" /* En caso de ser el corporativo se cambia a buscar los equipos del CAS 
                                                    que es como esta registrado en glpi*/
    }

    let listaAux: Equipo [] = []  
    this.listaTablas.forEach(tabla=>{ /* hacemos un for a la lista de tablas de glpi */
      this.equipoService.todosLosEquipos(tabla).subscribe(listaTabla=>{/* Consultamos los equipos de la tabla en turno */
        listaTabla.forEach(equipo=>{
          if(equipo.locations_id == this.sucursal_obj.nombreSucursal && equipo.states_id=="Asignado") /*validamos que solo sean equipos asignados en la sucursal */
          {
                 if( tabla=="Computer"         ){ equipo.tipo = "COMPUTADORA"        ; equipo.modelo = equipo.computermodels_id}
            else if( tabla=="NetworkEquipment" ){ equipo.tipo = "DISPOSITIVO DE RED" ; equipo.modelo = equipo.networkequipmentmodels_id}
            else if( tabla=="Printer"          ){ equipo.tipo = "IMPRESORA"          ; equipo.modelo = equipo.printermodels_id}
            else if( tabla=="Monitor"          ){ equipo.tipo = "MONITOR"            ; equipo.modelo = equipo.monitormodels_id}
            else if( tabla=="Peripheral"       ){ equipo.tipo = "DISPOSITIVO"        ; equipo.modelo = equipo.peripheralmodels_id}
            else if( tabla=="Rack"             ){ equipo.tipo = "RACK"               ; equipo.modelo = equipo.rackmodels_id}
            else if( tabla=="Phone"            ){ equipo.tipo = "TELEFONO"           ; equipo.modelo = equipo.phonemodels_id}
            
            listaAux.push(equipo)
          }
        })
        this.dataSource.data = listaAux
        this.dataSource.paginator = this.paginator
      },
      err=>
      {
        Swal.fire({
          title:'Error',
          icon:'error',
          text:'',
          allowEnterKey:false,
          allowEscapeKey:false,
          allowOutsideClick:false,
          confirmButtonColor: '#d33',
          confirmButtonText:'Cerrar'
        })
      }
      )
    })
  }

  /*SELECCIONA TODOS LOS EQUIPOS DE LA TABLA */
  checkAll(valor:boolean)
  {
    /* HABILITA LA CASILLA EN CADA FILA EN CASO QUE HABILIT EL CHECK DE TODOS */
    if(valor== true)
    {
      this.dataSource.data.forEach(equipo=>{
      
        equipo.bandera = true
        
      })
    }
    /* DESHABILITA LA CASILLA EN CADA FILA EN CASO QUE DESHABILITE EL CHECK DE TODOS */
    else
    {
      this.dataSource.data.forEach(equipo=>{
      
        equipo.bandera = false
        
      })
    }
  }  

  /*MÉTODO QUE VALIDA SI SE SELECCINÓ UN EQUIPO EN LA TABLA */
  equiposSeleccionados()
  {
    let total:number = 0

    this.dataSource.data.forEach(equipo=>{
      
      if(equipo.bandera == true)
      {
        total = total + 1
      }
      
    })

    if (total>0)
    {
      return true
    }
    else 
    { 
      return false
    }
  }

  /*Filtro que busca coincidencias dentro del select search */
  protected filterSucursales() {
    if (!this.lista_sucursales) {
      return;
    }
    /* Recuperamos el valor del input */
    let search = this.input_sucursal_nombre.value;
    if (!search) {
      this.listaFiltradaSucursales.next(this.lista_sucursales.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    /* Retornamos sla lista de eempleados que cumplen con las coincidencias */
    this.listaFiltradaSucursales.next(
      this.lista_sucursales.filter(sucursal => sucursal.nombreSucursal.toLowerCase().indexOf(search) > -1)
    );
  }
}
