import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { RegistroMantenimiento } from 'src/app/administracion/modelos/Mantenimiento/registro-mantenimiento';
import { SucursalService, UsuarioService } from 'src/app/administracion/servicios';
import { ElementMantenimientoService } from 'src/app/administracion/servicios/Mantenimiento/element-mantenimiento.service';
import { RegistroMantenimientoService } from 'src/app/administracion/servicios/Mantenimiento/registro-mantenimiento.service';
import { TipoManService } from 'src/app/administracion/servicios/Mantenimiento/tipo-man.service';
import Swal from 'sweetalert2';
import * as pdfMake from "pdfmake/build/pdfmake";
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import { iif } from 'rxjs';
import { TipoMan } from 'src/app/administracion/modelos/Mantenimiento/tipo-man';
(<any>pdfMake).vfs = pdfFonts.pdfMake.vfs;
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-control-de-mantenimiento',
  templateUrl: './control-de-mantenimiento.component.html',
  styleUrls: ['./control-de-mantenimiento.component.scss']
})
export class ControlDeMantenimientoComponent implements OnInit {

  /** T A B L A   M A N T E N I M I E N T O S   S I N   R E A L I Z A R */
  listaEncabezadoSR:string[] = ["OPCIONES","SUCURSAL","AGENDADO POR","OBSERVACIONES GENERALES","FECHA AGENDADA","FECHA DE CREACIÓN"]
  dataSourceSR           = new MatTableDataSource<RegistroMantenimiento>([]);
  @ViewChild('paginatorSR', { static: true }) public paginatorSR !: MatPaginator;

  /** T A B L A   M A N T E N I M I E N T O S   R E A L I Z A D O S */
  listaEncabezadoR:string[] = ["OPCIONES","SUCURSAL","TIPO DE REGISTRO","AGENDADO POR","REALIZADO POR","OBSERVACIONES GENERALES","FECHA DE REALIZACIÓN"]
  dataSourceR           = new MatTableDataSource<RegistroMantenimiento>([]);
  @ViewChild('paginatorR', { static: true }) public paginatorR !: MatPaginator;

  /** T A B L A   M A N T E N I M I E N T O S   C A N C E L A D O S */
  listaEncabezadosC:string[] = ["OPCIONES","SUCURSAL","OBSERVACIONES GENERALES","AGENDADO POR","CANCELADO POR","FECHA AGENDADA","FECHA DE CANCELACION"]
  dataSourceC           = new MatTableDataSource<RegistroMantenimiento>([]);
  @ViewChild('paginatorC', { static: true }) public paginatorC !: MatPaginator;

  constructor(private registroMantenimientoService  : RegistroMantenimientoService,
              private sucursalService               : SucursalService             ,
              private usuarioService                : UsuarioService              ,
              private router                        : Router                      ,
              private tipoManService                : TipoManService
              ) { }

  ngOnInit()
  {
    this.llenarDataSR()
    this.llenarDataR()
    this.llenarDataC()
  }

  /*   LLENA LA TABLA DE MANTENIMIENTOS SIN REALIZAR */
  llenarDataSR()
  {
    /*  REALIZAMOS LA CONSULTA PARA LLAMAR A TODAS LAS SOLICITUDES ATENDIDAS */
    this.registroMantenimientoService.registrosEspera().subscribe(consulta=>{

      /*  LA LISTA DE MANTENIMIENTOS DE RESULTADO SE RECORRE REGISTRO POR REGISTRO */
      consulta.forEach(registro=>{

        /*  LA LISTA DE MANTENIMIENTOS DE RESULTADO SE RECORRE REGISTRO POR REGISTRO */
        registro.fecha_agendad_tabla = (registro.fecha_agendada + "").substring(0,10)
        registro.fecha_modific_tabla = (registro.fecha_mod + "").substring(0,10)

      })

      /* UNA VEZ TERMINADO DE RECORRER TODA LA CONSULTA SE ENVÍA A LA DATA DE LA PRIMER TABLA DE LA VISTA */
      this.dataSourceSR.data = consulta;
      this.dataSourceSR.paginator = this.paginatorSR;
    })
  }

  /*   LLENA LA TABLA DE MANTENIMIENTOS REALIZADOS */
  llenarDataR()
  {
    /*  REALIZAMOS LA CONSULTA PARA LLAMAR A TODAS LAS SOLICITUDES ATENDIDAS */
    this.registroMantenimientoService.registrosRealizados().subscribe(consulta=>{

      /*  LA LISTA DE MANTENIMIENTOS DE RESULTADO SE RECORRE REGISTRO POR REGISTRO */
      consulta.forEach(registro=>{

        /*  SE LE DA FORMATO A LAS FECHAS DEL MANTENIMIENTO */
        registro.fecha_modific_tabla = (registro.fecha_mod + "").substring(0,10)

        /*  SE ANEXA EL NOMBRE DE LA SUCURSAL */
        this.sucursalService.getSucursal(Number(registro.sucursal_id)).subscribe(data=>{registro.sucursal_Tabla = data.nombreSucursal})


        /* VÁLISA SI FUE UN MANTENIMIENTO PROGRAMADO O EVENTUAL */
        if(registro.descripcion=="PROGRAMADO")
        {
          this.usuarioService.getAnEmpleado(Number(registro.id_empleado_registra)).subscribe(data=>{ registro.nombre_empleado__regist = data.nombre + " " + data.apellidoPat + " " + data.apellidoMat })
        }
        else
        {
          registro.nombre_empleado__regist = "FUE MANTENIMIENTO EVENTUAL"
        }

      })



      /** UNA VEZ TERMINADO DE RECORRER TODA LA CONSULTA SE ENVÍA A LA DATA DE LA SEGUNDA TABLA DE LA VISTA */
      this.dataSourceR.data = consulta;
      this.dataSourceR.paginator = this.paginatorR;
    })
  }

  /*   LLENA LA TABLA DE MANTENIMIENTOS CANCELADOS */
  llenarDataC()
  {
    /*  REALIZAMOS LA CONSULTA PARA LLAMAR A TODAS LOS MANTENIMIENTOS CANCELADOS */
    this.registroMantenimientoService.registrosCancelados().subscribe(consulta=>{

      /*  LA LISTA DE MANTENIMIENTOS DE RESULTADO SE RECORRE REGISTRO POR REGISTRO */
      consulta.forEach(registro=>{

        /**SE LE DA FORMATO A LAS FECHAS DEL MANTENIMIENTO */
        registro.fecha_agendad_tabla = (registro.fecha_agendada + "").substring(0,10)
        registro.fecha_modific_tabla = (registro.fecha_mod + "").substring(0,10)

      })

      /*   UNA VEZ TERMINADO DE RECORRER TODA LA CONSULTA SE ENVÍA A LA DATA DE LA TERCER TABLA DE LA VISTA */
      this.dataSourceC.data = consulta;
      this.dataSourceC.paginator = this.paginatorC;
    })
  }

  /*  ENVIA CUALQUIER REGISTRO DE MANTENIMIENTO SIN REALIZAR A EDICIÓN */
  clickEditar(id_registro_mantenimiento:number)
  {
    /*   REDIRECCIONA A OTRA VISTA EN LA CUAL SE PUEDE EDITAR LOS MANTENIMIENTOS PROGRAMADOS QUE AUN NO SE REALIZAN AÚN */
    this.router.navigate(['layout/detalleMantenimientoEdit/'+id_registro_mantenimiento])
  }

  /*  ENVIA CUALQUIER REGISTRO DE MANTENIMIENTO REALIZADO O CANCELADO A VER A DETALLE */
  clickVisualizar(id_registro_mantenimiento:number, status_registro:string)
  {
    /*  REDIRECCIONA A OTRA VISTA EN LA CUAL PODREMOS VER A DETALLE EL MANTENIMIENTO INDEPENDIENTEMENTE DE SU ESTATUS ACTUAL */
    this.router.navigate(['layout/detalleMantenimiento/'+id_registro_mantenimiento+'/'+status_registro])
  }

  /*  MUESTRA UNA VVENTANA CON INFORMACIÓN DE LA PANTALLA EN CURSO */
  clickMasInformacion(event:any)
  {
    /*   ABRE UNA VENTANA EMERGENTE QUE SE DEDICA A MOSTRAR INFORMACION SOBRE LO QUE SE PUEDE REALIZAR EN LA VISTA */
    Swal.fire({
      title:'Control de mantenimientos',
      html:
      '<p align="justify">En la parte superior de la pantalla se observan 3 pestañas, cada una filtra los mantenimientos deacu'+
      'erdo al estado actual de estos:</p>'                                                                                    +

      '<p align="justify"> <b>PENDIENTES: </b>  Son todos los mantenimientos agendados que aún no se realizan</p>'             +

      '<p align="justify"> <b>REALIZADOS: </b>  Son todos los mantenimientos (sean agendados o no) que ya fueron aten'+
      'didos</p>'                                                                                                 +

      '<p align="justify"> <b>CANCELADOS: </b>  Muestra todos los mantenimientos suspeendidos y la razón por la que se decidió suspender su '+
      'realización.</p>'                                                                                                       +

      '<p align="justify"> En todas las opcciones se podrà acceder a una opcion en la cual se pueda mostrar a detalle cada reg'+
      'istro de mantenimiento independiente a su status actual.</p>'                                                           ,
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      confirmButtonColor: '#d33',
      confirmButtonText:'Cerrar'
    })
  }

  clickMasInformacionReportes(event:any)
  {
    /*   ABRE UNA VENTANA EMERGENTE QUE SE DEDICA A MOSTRAR INFORMACION SOBRE LO QUE SE PUEDE REALIZAR EN LA VISTA */
    Swal.fire({
      title:'Reporte de mantenimiento',
      html:
      '<p align="justify">Los reportes de mantenimiento se generan de forma general o a detale, para ambos casos es '+
      'necesario ingresar el tipo de mantenimiento (PROGRAMADO O EVENTUAL), el estado actual (REALIZADO, PENDIENTE o '+
      'CANCELADO) y un rango de fechas.</p>'                                                           ,
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      confirmButtonColor: '#d33',
      confirmButtonText:'Cerrar'
    })
  }

  /*  FILTRO DE LA BARRA DE BUSQUEDA EN LA TABLA DE MANTENIMIENTOS SIN REALIZAR */
  filtroDataDourceSR(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceSR.filter = filterValue.trim().toLowerCase();
  }

  /*  FILTRO DE LA BARRA DE BUSQUEDA EN LA TABLA DE MANTENIMIENTOS REALIZADOS */
  filtroDataDourceR(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceR.filter = filterValue.trim().toLowerCase();
  }

  /*  FILTRO DE LA BARRA DE BUSQUEDA EN LA TABLA DE MANTENIMIENTOS CANCELADOS */
  filtroDataDourceC(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceC.filter = filterValue.trim().toLowerCase();
  }


  /*Formulario para el reporte general */
  /**/
  /**/    /*Listas de opciones */
  /**/    opciones__tipo__de__mantenimiento:string[]=["PROGRAMADO","EVENTUAL"]
  /**/    opciones_estatus_mantenimie:string[]=["PENDIENTE","REALIZADO","CANCELADO"]
  /**/
  /**/    range = new FormGroup({
  /**/      start: new FormControl(),
  /**/      end: new FormControl(),
  /**/    });
  /**/
  /**/    tipo_mant_nombre:string=""
  /**/    status_mantenimi:string=""
  /*********************************************************************************/

  /*Habilita los botones para generar el reporte general */
  habilitarBotonReporteGenerl()
  {
    /*Valida si tipo de mantenimiento, el status y el rango de fechas no se encuentren vacíos */
    if(this.range.value.start!= null && this.range.value.end!=null)
    {
      return true
    }
    return false
  }

  /* Descarga un archivo excel para generar el reporte general*/
  clickCrearReporteGeneral(event:any)
  {

    let status:string = ""
    if(this.status_mantenimi =="PENDIENTE"){status ="ESPERA"}
    else{status=this.status_mantenimi}

    /*Manda a realizar la consulta en spring */
    this.registroMantenimientoService.reporteGeneral(this.range.value.start,this.range.value.end ).subscribe(data=>{

      if(data.length>0)
      {
        data.forEach(registro=>{
          if(registro.mantenimiento=="EVENTUAL")
          {
            registro.fecha_programada = "FUE EVENTUAL"
            registro.agendado_por     = "FUE EVENTUAL"
          }
          if(registro.estatus=="ESPERA")
          {
            registro.fecha_realizacion="SIN REALIZAR AÚN"
            registro.realizado_por="SIN REALIZAR AÚN"
          }
        })
        /*Creamos el archivo excel y se descarga */
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
        const workbook: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(workbook, worksheet, 'Sheet1');
        XLSX.writeFile(workbook, "Reporte general de mantenimientos.xlsx");
      }
      else/* En caso que no tenga registros lanza el mensaje */
      {
        this.mensajeDeError()
      }
    })
  }

  /*Formulario para el reporte a detalle */
  /**/
  /**/    /*Listas de opciones */
  /**/    opciones__tipo__de__mantenimientoD:string[]=["PROGRAMADO","EVENTUAL"]
  /**/    opciones_estatus_mantenimieD:string[]=["PENDIENTE","REALIZADO","CANCELADO"]
  /**/
  /**/    rangeD = new FormGroup({
  /**/      startD: new FormControl(),
  /**/      endD: new FormControl(),
  /**/    });
  /**/
  /**/    tipo_mant_nombreD:string=""
  /**/    status_mantenimiD:string=""
  /*********************************************************************************/


  /*Habilita los botones para generar el reporte a detalle */
  habilitarBotonReporteDetalle()
  {
    /*Valida si tipo de mantenimiento, el status y el rango de fechas no se encuentren vacíos */
    if(this.tipo_mant_nombreD !="" && this.status_mantenimiD !="" && this.rangeD.value.startD!= null && this.rangeD.value.endD!=null)
    {
      return true
    }
    return false
  }

  bandera_status_mantenimiD:boolean = true
  seleccionTipoDeMantenimiento(event:any)
  {
    if(this.tipo_mant_nombreD=="EVENTUAL")
    {
      this.status_mantenimiD ="REALIZADO"
      this.bandera_status_mantenimiD=false
    }
    else
    {
      this.status_mantenimiD=""
      this.bandera_status_mantenimiD = true
    }
  }

  /* Descarga un archivo excel para generar el reporte a detalle */
  clickCrearReporteDetalle(event:any)
  {
    if(this.status_mantenimiD =="PENDIENTE")
    {
      /*Manda a realizar la consulta en spring */
      this.registroMantenimientoService.reporteEspera(this.tipo_mant_nombreD,this.rangeD.value.startD,this.rangeD.value.endD ).subscribe(data=>{
        
        if(data.length>0)
        {
          data.forEach(detalle=>{
              detalle.fecha_realizacion="SIN REALIZAR AÚN"
              detalle.realizado_por="SIN REALIZAR AÚN"
              detalle.comentarios_del_usuario="SIN REALIZAR AÚN"
              detalle.observaciones_del_equipo="SIN REALIZAR AÚN"
          })
          this.descargarReporteDetalle(data,"Programados pedientes")
        }
        else/* En caso que no tenga registros lanza el mensaje */
        {
          this.mensajeDeError()
        }
      })
    }
    if(this.status_mantenimiD == "REALIZADO")
    {
      let cadena:string ="Programados Realizados"
      /*Manda a realizar la consulta en spring */
      this.registroMantenimientoService.reporteRealizados(this.tipo_mant_nombreD,this.rangeD.value.startD,this.rangeD.value.endD ).subscribe(data2=>{
        
        if(data2.length>0)
        {
          data2.forEach(detalle=>{
            if(detalle.mantenimiento=="EVENTUAL")
            {
              detalle.fecha_programada = "FUE EVENTUAL"
              detalle.agendado_por     = "FUE EVENTUAL"
              cadena ="Eventuales"
            }
          })
          this.descargarReporteDetalle(data2,cadena)
        }
        else/* En caso que no tenga registros lanza el mensaje */
        {
          this.mensajeDeError()
        }
      })
    }
    if(this.status_mantenimiD == "CANCELADO")
    {
      /*Manda a realizar la consulta en spring */
      this.registroMantenimientoService.reporteCancelados(this.tipo_mant_nombreD,this.rangeD.value.startD,this.rangeD.value.endD ).subscribe(data3=>{
        if(data3.length>0)
        {
          data3.forEach(detalle=>{
            detalle.fecha_realizacion="FUE CANCELADO"
            detalle.realizado_por="FUE CANCELADO"
            detalle.comentarios_del_usuario="FUE CANCELADO"
            detalle.observaciones_del_equipo="FUE CANCELADO"
          })
          this.descargarReporteDetalle(data3,"Cancelados")
        }
        else/* En caso que no tenga registros lanza el mensaje */
        {
          this.mensajeDeError()
        }
      })
    }



  }

  descargarReporteDetalle(dataRegistro:RegistroMantenimiento[], tipo:string)
  {
    /*Creamos el archivo excel y se descarga */
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(dataRegistro);
    const workbook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workbook, worksheet, 'Sheet1');
    XLSX.writeFile(workbook, "Reporte de Mantenimiento a Detalle: "+tipo+".xlsx");
  }

  /* Mensaje que aparece cuando no encuentra registros d emantenimeinto en determminadas fechas */
  mensajeDeError()
  {
    Swal.fire({
      title:'Sin registros durante este período',
      icon:'warning',
      text:'Ingrese un período más extenso',
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      confirmButtonColor: '#d33',
      confirmButtonText:'Cerrar'
    }) 
  }
}

