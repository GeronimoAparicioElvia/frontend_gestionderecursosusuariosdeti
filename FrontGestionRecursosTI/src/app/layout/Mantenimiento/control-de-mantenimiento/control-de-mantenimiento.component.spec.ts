import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlDeMantenimientoComponent } from './control-de-mantenimiento.component';

describe('ControlDeMantenimientoComponent', () => {
  let component: ControlDeMantenimientoComponent;
  let fixture: ComponentFixture<ControlDeMantenimientoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ControlDeMantenimientoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlDeMantenimientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
