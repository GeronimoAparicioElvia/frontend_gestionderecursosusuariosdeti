import { Component, OnInit , QueryList, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import Swal from 'sweetalert2';
/**Modelo */
import { Equipo } from 'src/app/administracion/modelos/Inventario/Equipo/equipo';
/** Servicios */
import {ActivatedRoute} from "@angular/router"
import { EquipoService } from 'src/app/administracion/servicios/Inventario/Equipo/equipo.service';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/administracion/servicios';
import { Usuario } from 'src/app/administracion/modelos';
import { Empleado } from 'src/app/administracion/modelos/empleado';
/** */

@Component({
  selector: 'app-gestion-equipo',
  templateUrl: './gestion-equipo.component.html',
  styleUrls: ['./gestion-equipo.component.scss']
})
export class GestionEquipoComponent  implements OnInit {

  tabla       :string =""
  tablaTipo   :string =""
  tablaModelo :string =""
  empleado    :Empleado= new Empleado();

  /** Select */
  tipoDispositivo = new FormControl()
  listaactivos: string      [] = ["COMPUTADORAS","DISPOSITIVOS DE RED","IMPRESORAS","MONITORES","DISPOSITIVOS","RACKS","TELEFONOS"]


  /** Tablas */
  encabezadostabla   : String [] = ["opciones","nombre","numSerie","tipo","modelo","fabricante","sucursalStatus","fecha_mod"]

  dataSourceT          = new MatTableDataSource<Equipo>([])
  dataSourceA          = new MatTableDataSource<Equipo>([])
  dataSourceS          = new MatTableDataSource<Equipo>([])
  dataSourceB          = new MatTableDataSource<Equipo>([])

  @ViewChild('paginatorT', { static: true }) public paginatorT !: MatPaginator;
  @ViewChild('paginatorA', { static: true }) public paginatorA !: MatPaginator;
  @ViewChild('paginatorS', { static: true }) public paginatorS !: MatPaginator;
  @ViewChild('paginatorB', { static: true }) public paginatorB !: MatPaginator;

  constructor(
    private equipoService : EquipoService,
    private router        : Router,
    private activeRouter  : ActivatedRoute,
    private usuarioService: UsuarioService
    ) {}

  ngOnInit(){
    /*Swal.fire({

      icon: 'info',
      title: 'Elija un tipo de activo para poder visualizar los equipos existentes en la base de datos de GLPI.',
      showConfirmButton: true,
    });*/
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000,
    })
    Toast.fire({
      icon: 'info',
      title: 'Elija un tipo de activo para poder visualizar los equipos existentes en la base de datos de GLPI.',
    })
    this.activeRouter.params.subscribe(params=>{
      this.tipoDispositivo.setValue(params['nomActivo']);
    });
    let event:any;
    this.seleccionarTipoDispositivo(event)
  }

  /** Llena el data de todos los dispositivos segun sea el activo seleccionado */
  llenarDataT(){
    let listaAux: Equipo [] =[]
    this.equipoService.todosLosEquipos(this.tabla).subscribe(lista=>{lista.forEach(element=>{

      if(this.tipoDispositivo.value=="COMPUTADORAS"){element.tipo=element.computertypes_id;element.modelo=element.computermodels_id}
      else if(this.tipoDispositivo.value=="DISPOSITIVOS DE RED"){element.tipo=element.networkequipmenttypes_id;element.modelo=element.networkequipmentmodels_id}
      else if(this.tipoDispositivo.value=="IMPRESORAS"){element.tipo=element.printertypes_id;element.modelo=element.printermodels_id}
      else if(this.tipoDispositivo.value=="MONITORES"){element.tipo=element.monitortypes_id;element.modelo=element.monitormodels_id}
      else if(this.tipoDispositivo.value=="DISPOSITIVOS"){element.tipo=element.peripheraltypes_id;element.modelo=element.printermodels_id}
      else if(this.tipoDispositivo.value=="RACKS"){element.tipo=element.racktypes_id;element.modelo=element.rackmodels_id}
      else if(this.tipoDispositivo.value=="TELEFONOS"){element.tipo=element.phonetypes_id;element.modelo=element.phonemodels_id}
      listaAux.push(element)
      })
      this.dataSourceT.data = listaAux;
      this.dataSourceT.paginator = this.paginatorT;
    }, err=>{
      Swal.fire({
        position: 'center',
        icon: 'error',
        text: 'Los equipos no se pueden visualizar debido a que no hay conexión con el servidor, inténtelo más tarde o contacte al área de T.I. ',
        title: 'Alta de equipos',
      })
    })
  }

  /** Llena el data de todos los dispositivos Asignados según sea el activo seleccionado*/
  llenarDataA(){
    let listaAux: Equipo [] =[]
    this.equipoService.todosLosEquipos(this.tabla).subscribe(lista=>{lista.forEach(element=>{
      if(element.states_id=="Asignado")
      {
        this.usuarioService.getUsernamePrueba(element.users_id+"").subscribe(usuarioApi=>{
          let usuario = usuarioApi as Usuario;
          this.empleado = usuario.empleado;
          element.users_id=this.empleado.nombre+" "+this.empleado.apellidoPat+" "+ this.empleado.apellidoMat;
          console.log(element.users_id+"nom");
        });
        if(this.tipoDispositivo.value=="COMPUTADORAS"){element.tipo=element.computertypes_id;element.modelo=element.computermodels_id}
        else if(this.tipoDispositivo.value=="DISPOSITIVOS DE RED"){element.tipo=element.networkequipmenttypes_id;element.modelo=element.networkequipmentmodels_id}
        else if(this.tipoDispositivo.value=="IMPRESORAS"){element.tipo=element.printertypes_id;element.modelo=element.printermodels_id}
        else if(this.tipoDispositivo.value=="MONITORES"){element.tipo=element.monitortypes_id;element.modelo=element.monitormodels_id}
        else if(this.tipoDispositivo.value=="DISPOSITIVOS"){element.tipo=element.peripheraltypes_id;element.modelo=element.printermodels_id}
        else if(this.tipoDispositivo.value=="RACKS"){element.tipo=element.racktypes_id;element.modelo=element.rackmodels_id}
        else if(this.tipoDispositivo.value=="TELEFONOS"){element.tipo=element.phonetypes_id;element.modelo=element.phonemodels_id}
        listaAux.push(element)
      }
      })
      this.dataSourceA.data = listaAux
      this.dataSourceA.paginator = this.paginatorA;
    })
  }

  /** Llena el data de todos los dispositivos en Almacén según sea el activo seleccionado*/
  llenarDataS(){
    let listaAux: Equipo [] =[]
    this.equipoService.todosLosEquipos(this.tabla).subscribe(lista=>{lista.forEach(element=>{
      if(element.states_id == "Stock" || element.states_id == "No Asignado" || element.states_id == "Principal" || element.states_id == "Backup" || element.states_id == "" )
      {
        if(this.tipoDispositivo.value=="COMPUTADORAS"){element.tipo=element.computertypes_id;element.modelo=element.computermodels_id}
        else if(this.tipoDispositivo.value=="DISPOSITIVOS DE RED"){element.tipo=element.networkequipmenttypes_id;element.modelo=element.networkequipmentmodels_id}
        else if(this.tipoDispositivo.value=="IMPRESORAS"){element.tipo=element.printertypes_id;element.modelo=element.printermodels_id}
        else if(this.tipoDispositivo.value=="MONITORES"){element.tipo=element.monitortypes_id;element.modelo=element.monitormodels_id}
        else if(this.tipoDispositivo.value=="DISPOSITIVOS"){element.tipo=element.peripheraltypes_id;element.modelo=element.printermodels_id}
        else if(this.tipoDispositivo.value=="RACKS"){element.tipo=element.racktypes_id;element.modelo=element.rackmodels_id}
        else if(this.tipoDispositivo.value=="TELEFONOS"){element.tipo=element.phonetypes_id;element.modelo=element.phonemodels_id}
        listaAux.push(element)
      }
      })
      this.dataSourceS.data = listaAux;
      this.dataSourceS.paginator = this.paginatorS;
    })
  }

  /** Llena el data de todos los dispositivos en estado de Baja según sea el activo seleccionado*/
  llenarDataB(){
    let listaAux: Equipo [] =[]
    this.equipoService.todosLosEquipos(this.tabla).subscribe(lista=>{lista.forEach(element=>{
      if(element.states_id == "Baja" || element.states_id == "Vendido" || element.states_id == "Robo" )
      {
        if(this.tipoDispositivo.value=="COMPUTADORAS"){element.tipo=element.computertypes_id;element.modelo=element.computermodels_id}
        else if(this.tipoDispositivo.value=="DISPOSITIVOS DE RED"){element.tipo=element.networkequipmenttypes_id;element.modelo=element.networkequipmentmodels_id}
        else if(this.tipoDispositivo.value=="IMPRESORAS"){element.tipo=element.printertypes_id;element.modelo=element.printermodels_id}
        else if(this.tipoDispositivo.value=="MONITORES"){element.tipo=element.monitortypes_id;element.modelo=element.monitormodels_id}
        else if(this.tipoDispositivo.value=="DISPOSITIVOS"){element.tipo=element.peripheraltypes_id;element.modelo=element.printermodels_id}
        else if(this.tipoDispositivo.value=="RACKS"){element.tipo=element.racktypes_id;element.modelo=element.rackmodels_id}
        else if(this.tipoDispositivo.value=="TELEFONOS"){element.tipo=element.phonetypes_id;element.modelo=element.phonemodels_id}
        listaAux.push(element)
      }
      })
      this.dataSourceB.data = listaAux;
      this.dataSourceB.paginator = this.paginatorB;
    })
  }

  /**Métodos para buscar por medio de la barra de búsqueda */
  /**Filtro del data source todos */
  filtroDataDourceT(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceT.filter = filterValue.trim().toLowerCase();
  }

  /**Filtro del data source asignados */
  filtroDataDourceA(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceA.filter = filterValue.trim().toLowerCase();
  }

  /**Filtro del data source stock */
  filtroDataDourceS(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceS.filter = filterValue.trim().toLowerCase();
  }

  /**Filtro del data source baja */
  filtroDataDourceB(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceB.filter = filterValue.trim().toLowerCase();
  }

  /** Evento del tab */
  myTabSelectedIndexChange(index: number){
    console.log("indice: " + index)
    if(index==1 && this.dataSourceA.data.length==0){ this.llenarDataA() }
    else if(index==2 && this.dataSourceS.data.length==0){ this.llenarDataS() }
    else if(index==3 && this.dataSourceB.data.length==0){ this.llenarDataB() }
  }

  /** Botones*/
  /** Envia a la vista de detalle equipo  */
  botonVerADetalle(id_equipo:number){
    this.router.navigate(['layout/detalleEquipo/' + id_equipo + '/' + this.tipoDispositivo.value ]);
  }

  /** Select tipo de activo y se le asignan los nombres de las tablas segun corresponda */
  seleccionarTipoDispositivo(event:any){
    if(this.tipoDispositivo.value == "COMPUTADORAS"){ this.tabla="Computer"; this.tablaTipo="ComputerType"; this.tablaModelo="ComputerModel"; this.llenarDataT();this.llenarDataA();this.llenarDataS();this.llenarDataB() }
    else if(this.tipoDispositivo.value == "DISPOSITIVOS DE RED"){ this.tabla="Networkequipment"; this.tablaTipo="NetworkequipmentType"; this.tablaModelo="NetworkequipmentModel";  this.llenarDataT();this.llenarDataA();this.llenarDataS();this.llenarDataB() }
    else if(this.tipoDispositivo.value == "IMPRESORAS"){ this.tabla="Printer"; this.tablaTipo="PrinterType"; this.tablaModelo="PrinterModel";  this.llenarDataT();this.llenarDataA();this.llenarDataS();this.llenarDataB() }
    else if(this.tipoDispositivo.value == "MONITORES"){ this.tabla="Monitor"; this.tablaTipo="MonitorType"; this.tablaModelo="MonitorModel";  this.llenarDataT();this.llenarDataA();this.llenarDataS();this.llenarDataB() }
    else if(this.tipoDispositivo.value == "DISPOSITIVOS"){ this.tabla="Peripheral"; this.tablaTipo="PeripheralType"; this.tablaModelo="PeripheralModel";  this.llenarDataT();this.llenarDataA();this.llenarDataS();this.llenarDataB() }
    else if(this.tipoDispositivo.value == "RACKS"){ this.tabla="Rack"; this.tablaTipo="RackType"; this.tablaModelo="RackModel";  this.llenarDataT();this.llenarDataA();this.llenarDataS();this.llenarDataB() }
    else if(this.tipoDispositivo.value == "TELEFONOS"){ this.tabla="Phone"; this.tablaTipo="PhoneType"; this.tablaModelo="PhoneModel";  this.llenarDataT();this.llenarDataA();this.llenarDataS();this.llenarDataB() }
  }

  /**Redireccionara al detalle del equipo pasandole su id */
  detalleEquipo(id_equipo: number){
    if(this.tipoDispositivo.value == "COMPUTADORAS"){this.router.navigate(['layout/detalleEquipo/' + id_equipo + '/COMPUTADORA']) }
    else if(this.tipoDispositivo.value == "DISPOSITIVOS DE RED"){this.router.navigate(['layout/detalleEquipo/' + id_equipo + '/DISP. RED']) }
    else if(this.tipoDispositivo.value == "IMPRESORAS"){this.router.navigate(['layout/detalleEquipo/' + id_equipo + '/IMPRESORA']) }
    else if(this.tipoDispositivo.value == "MONITORES"){this.router.navigate(['layout/detalleEquipo/' + id_equipo + '/MONITOR']) }
    else if(this.tipoDispositivo.value == "DISPOSITIVOS"){this.router.navigate(['layout/detalleEquipo/' + id_equipo + '/PERIFERICO']) }
    else if(this.tipoDispositivo.value == "RACKS"){this.router.navigate(['layout/detalleEquipo/' + id_equipo + '/RACK']) }
    else if(this.tipoDispositivo.value == "TELEFONOS"){this.router.navigate(['layout/detalleEquipo/' + id_equipo + '/TELEFONO']) }
  }

  /**Metódo que muestra la información de ayuda */
  botonInformacion(){
    Swal.fire({
      position: 'top-end',
      icon: 'info',
      title: 'GESTIÓN DE EQUIPOS',
      showConfirmButton: false,
      html: '<p align="justify">Para poder visualizar los equipos existentes, deberá elegir un tipo de activo. \n</p>'
           +'<p align="justify">Posteriormente podrá visualizar todos los equipos de esa categoría y además visualizarlos por el estatus en el que se encuentre. \n</p>',
           confirmButtonColor: '#3085d6',
           cancelButtonColor: '#d33',
           confirmButtonText: 'Sí'
    })
  }
}
