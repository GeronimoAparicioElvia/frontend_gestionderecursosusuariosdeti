import { ActivatedRoute,Params} from "@angular/router"
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import   html2canvas from "html2canvas";
import { MatTableDataSource} from '@angular/material/table';
import { Router } from '@angular/router';
import   Swal from 'sweetalert2';
import { timer } from 'rxjs';
/**Modelo 1234*/
import { Componente } from 'src/app/administracion/modelos/Inventario/Componente/componente';
import { Equipo } from 'src/app/administracion/modelos/Inventario/Equipo/equipo';
import { Estado } from 'src/app/administracion/modelos/Inventario/Estado/estado';
import { Fabricante } from 'src/app/administracion/modelos/Inventario/Fabricante/fabricante';
import { Modelo } from 'src/app/administracion/modelos/Inventario/Modelo/modelo';
import { SucursalGlpi } from 'src/app/administracion/modelos/Inventario/Sucursal/sucursal-glpi';
import { Tipo } from 'src/app/administracion/modelos/Inventario/Tipo/tipo';
import { UsuarioGlpi } from 'src/app/administracion/modelos/Inventario/Usuario/usuario-glpi';
/** Serivicios */
import { ComponenteService } from 'src/app/administracion/servicios/Inventario/Componente/componente.service';
import { EquipoService } from 'src/app/administracion/servicios/Inventario/Equipo/equipo.service';
import { EstadoService } from 'src/app/administracion/servicios/Inventario/Estado/estado.service';
import { FabricanteService } from 'src/app/administracion/servicios/Inventario/Fabricante/fabricante.service';
import { ItemComponenteService } from 'src/app/administracion/servicios/Inventario/Item_Componente/item-componente.service';
import { ModeloService } from 'src/app/administracion/servicios/Inventario/Modelo/modelo.service';
import { SucursalGlpiService } from 'src/app/administracion/servicios/Inventario/Sucursal/sucursal-glpi.service';
import { TipoService } from 'src/app/administracion/servicios/Inventario/Tipo/tipo.service';
import { UsuarioGlpiService } from 'src/app/administracion/servicios/Inventario/Usuario/usuario-glpi.service';
import { Empleado } from "src/app/administracion/modelos/empleado";
import { UsuarioService } from "src/app/administracion/servicios";



@Component({
  selector: 'app-detalle-equipo',
  templateUrl: './detalle-equipo.component.html',
  styleUrls: ['./detalle-equipo.component.scss']
})
export class DetalleEquipoComponent implements OnInit {
    //Atrapa el token de sesion cuando inicia
  token: string = JSON.parse(localStorage.getItem('glpiToken') || '{}').token


  nomActivo  : string ="";
  tipoActivo : string ="";
  tipo   : string ="";
  modelo : string="";
  tabla  : string="";
  id     : number=0;
  idSO: string="";         idProcesor: string="";    idFirmware="";
  crearSO: boolean=true;   crearDProcesor=true;      crearDFirmware=true;
  body   : string="";
  imgcreada: boolean=true;
  banderaForm:boolean=false;
  vacio: string="";
  colaborador : Empleado= new Empleado();
  equipoInicial: Equipo = new Equipo();
  //Listas para los select
  listaactivos    : string        [] = [];
  listaTipos      : Tipo          [] = [];
  listaEstados    : Estado        [] = [];
  listaModelo     : Modelo        [] = [];
  listaFabricante : Fabricante    [] = [];
  listaSucursal   : SucursalGlpi  [] = [];
  listaUsuarios   : UsuarioGlpi   [] = [];
  listaResTecnico : UsuarioGlpi   [] = [];
  listaEmpleados  : Empleado      [] = [];

  //Listas para los select del sistema operativo
  listaSO! : Componente[];            listaVersionSO! : Componente[];    listaArquitectura! : Componente[];   listaServPack! : Componente[];
  //Listas para los select de los componentes de la computadora
  listaProcesador! : Componente[];    listaFirmware!   : Componente[];
  listaMemoria!    : Componente[];    listaTipoMemoria!: Componente[];
  listaHardDrive!  : Componente[];    listaNetCard!    : Componente[];      listaDrive!  : Componente[];
  listaGraCard!    : Componente[];    listaSoundCard!  : Componente[];
  listaController! : Componente[];    listaDisco!      : Componente[];
  listaEquipos    : Equipo      [] = [];
  listaNomImventario: string    [] = [];

   /* Encabezados de la tabla de Componente */
   encabezadostabla   : String [] = ["c1","c2","c3","c4"]
   encabezadostabla3   : String [] = ["c1","c2","c3"]
   encabezadostabla2   : String [] = ["c1","c2"]

  /* El dataSource de la tabla TODOS */
  dataSourceMemoria        = new MatTableDataSource<string[]>([]);
  dataSourceHardDrive      = new MatTableDataSource<string[]>([]);
  dataSourceNetCard        = new MatTableDataSource<string[]>([]);
  dataSourceDrive          = new MatTableDataSource<string[]>([]);
  dataSourceGraCard        = new MatTableDataSource<string[]>([]);
  dataSourceSoundCard      = new MatTableDataSource<string[]>([]);
  dataSourceController     = new MatTableDataSource<string[]>([]);

  //Variables del método agregarTipoModelo
  inputNombre:any=""
  inputComentarios:any=""

  constructor(
              private formBuilder         : FormBuilder         ,
              private tipoService         : TipoService         ,
              private modeloService       : ModeloService       ,
              private fabricanteService   : FabricanteService   ,
              private equipoService       : EquipoService       ,
              private sucursalGlpiService : SucursalGlpiService ,
              private activeRouter        : ActivatedRoute      ,
              private router              : Router              ,
              private usuarioGlpiService  : UsuarioGlpiService  ,
              private estadoService       : EstadoService       ,
              private componenteService   : ComponenteService,
              private itemComponenteService : ItemComponenteService,
              private usuarioService      : UsuarioService
              ){
                 this.llenarListas();
                 this.llenarListasComponentes();
                 this.parametros();
                 this.selectActivo();
                 this.buscarEquipo();
                 this.buscarColaborador();
                 this.formActualizar.disable();
                 this.formSistemaOperativo.disable();
                 this.buscarSistemaOperativo();
                 this.llenarTablas();
                 this.llenarListaNomImventario();
                }

  /**Form con los datos del formulario para actualizar */
  formActualizar = this.formBuilder.group({
    activo: [{value: '', disabled: true}, Validators.required],
    tipo:               ['', [Validators.required]],
    nombreInventario:   ['', [Validators.required]],
    modelo:             ['', [Validators.required]],
    numeroSerie:        ['', [Validators.required]],
    fabricante:         ['', [Validators.required]],
    responsableTecnico: [''],
    sucursal:           ['', [Validators.required]],
    uuid:               [''],
    status:             ['', [Validators.required]],
    usuario:            [''],
    puesto:             [''],
    comentarios:        ['']
  });
  /** Form con los datos del siste operativo*/
  formSistemaOperativo=this.formBuilder.group({
    nombreSO      : [''],
    version       : [''],
    arquitectura  : [''],
    servicePack   : [''],
    idProducto    : [''],
    numeroSerieSO : ['']
  });
  /* Form de los componentes*/
  formComponente=this.formBuilder.group({
    procesador  : [{value: '', disabled: true}],
    firmware    : [{value: '', disabled: true}]
  });
  /**Form de los componentes de las tablas */
  formTablaComponente=this.formBuilder.group({
    memoria   : [''],
    hardDrive : [''],
    netCard   : [''],
    graCard   : [''],
    soundCard : [''],
    drive     : [''],
    controller: ['']
  });

  ngOnInit() {}

  /** Captura los parámtros de entrada del URL */
  parametros()
  {
    this.activeRouter.params.subscribe(params=>{
      this.id = params['id']
      this.nomActivo = params['nomActivo']

    });
  }

  /**Llena la lista ´listaNomInventario´ de todos los tipos de solicitud*/
  llenarListaNomImventario(){
    let listaTablas:string []=["Computer", "Peripheral", "Printer","Networkequipment", "Phone", "Rack", "Monitor" ];
    listaTablas.forEach(activo=>{
      this.equipoService.todosLosEquipos(activo).subscribe(equipos=>{
        equipos.forEach(equipo=>{this.listaNomImventario.push(equipo.name?.trim()); });
      });
    });
    /**Llena la lista de activos */
    this.listaactivos= this.equipoService.listaActivos();
  }

  /**Método que asigna el puesto del colaborador que se elije */
  selectEmpleado(){
    this.formActualizar.controls['puesto'].setValue("");
    this.usuarioService.getAnEmpleado(this.formActualizar.value.usuario).subscribe(empleado =>{
      this.colaborador=empleado;
      this.formActualizar.controls['puesto'].setValue(this.colaborador?.puesto?.nombrePuesto);

    });
  }
  /**Busca al colaborador de glpi en el endpoint /Empleados, para mostrarlo en pantalla */
  buscarColaborador(){
    this.equipoService.unEquipo(this.tabla, this.id).subscribe(val=>{
      this.usuarioGlpiService.anUsuario(Number(val.users_id), this.token).subscribe(usuarioGL=>{
        this.usuarioService.getAllEmpleados().subscribe(empleados=>{
          empleados.forEach(empleadoSof=>{
            let apellidos=empleadoSof.apellidoPat+" "+empleadoSof.apellidoMat;
            apellidos=apellidos.toLowerCase();
            if(empleadoSof?.nombre?.toLocaleLowerCase()==usuarioGL?.firstname?.toLocaleLowerCase()){
              if(apellidos==usuarioGL.realname?.toLocaleLowerCase()){
                this.formActualizar.controls['usuario'].setValue(empleadoSof.idEmpleado);
                console.log(empleadoSof.idEmpleado+"id"+empleadoSof.nombre+" nombre");
                this.colaborador= empleadoSof;
              }
            }
          });
        });
      });
    });
  }
  /** Búsqueda de un equipo en especifico */
  /** Hace una consulta de los datos del equipo */
  buscarEquipo(){
    this.equipoService.unEquipo(this.tabla,this.id).subscribe(val =>{
          this.equipoInicial= val;
          let nInventario = val.name?.trim();
          let uuidsinEspacios= val.uuid?.trim();

          let comentarios : string = val.comment+"";
          //Elimina saltos de linea y mas para que se pueda agregar correctamente
          comentarios = comentarios.replace(/(\r\n|\n|\r)/gm, " ");
          this.formActualizar.patchValue({
            activo             : this.nomActivo,
            nombreInventario   : nInventario,
            numeroSerie        : val.serial,
            puesto             : val.contact,
            uuid               : uuidsinEspacios,
            comentarios        : comentarios,
            fabricante         : val.manufacturers_id,
            responsableTecnico : val.users_id_tech,
            status             : val.states_id,
            sucursal           : val.locations_id,
          });
          /**Dependiendo del activo al que corresponda el equipo, se le asignan los valores a las variables*/
          if(this.nomActivo =="COMPUTADORA"){
            this.formActualizar.patchValue({
              modelo             : val.computermodels_id,
              tipo               : val.computertypes_id
            });
          }
          if(this.nomActivo =="DISP. RED"){
            this.formActualizar.patchValue({
              modelo : val.networkequipmentmodels_id,
              tipo   : val.networkequipmenttypes_id
            });
          }
          if(this.nomActivo =="IMPRESORA"){
            this.formActualizar.patchValue({
              modelo : val.printermodels_id,
              tipo   : val.printertypes_id
            });
          }
          if(this.nomActivo =="MONITOR"){
            this.formActualizar.patchValue({
              modelo : val.monitormodels_id,
              tipo   : val.monitortypes_id
            });
          }
          if(this.nomActivo =="RACK"){
            this.formActualizar.patchValue({
              modelo : val.rackmodels_id,
              tipo   : val.racktypes_id
            });
          }
          if(this.nomActivo =="PERIFERICO"){
            this.formActualizar.patchValue({
              modelo : val.peripheralmodels_id,
              tipo   : val.peripheraltypes_id
            });
          }
          if(this.nomActivo =="TELEFONO"){
            this.formActualizar.patchValue({
              modelo : val.phonemodels_id,
              tipo   : val.peripheraltypes_id
            });
          }
    }, err=>{
      Swal.fire({
        icon: 'error',
        //title: 'Actualizar Sistema Operativo',
        text: 'Error al cargar datos del equipo, inténtelo más tarde.',
        showConfirmButton: false,
        timer: 2000
      });
    });
  }

  /** Búsca el Sistema operativo y componentes */
  buscarSistemaOperativo(){
    this.itemComponenteService.allSubItems(this.id,"Item_OperatingSystem").subscribe(val=>{
        this.formSistemaOperativo.patchValue({
          nombreSO     : val[val.length-1].operatingsystems_id,
          version      : val[val.length-1].operatingsystemversions_id,
          arquitectura : val[val.length-1].operatingsystemarchitectures_id,
          servicePack  : val[val.length-1].operatingsystemservicepacks_id,
          idProducto   : val[val.length-1].license_id,
          numeroSerieSO: val[val.length-1].license_number
        });
        this.idSO=val[val.length-1].id+"";
        this.crearSO=false;
    });
    this.itemComponenteService.allSubItems(this.id,"Item_DeviceProcessor").subscribe(val=>{
        this.formComponente.patchValue({
          procesador   : val[val.length-1].deviceprocessors_id
        });
        this.idProcesor=val[val.length-1].id+"";
        this.crearDProcesor=false;
    });
    this.itemComponenteService.allSubItems(this.id,"Item_DeviceFirmware").subscribe(val=>{
        this.formComponente.patchValue({
          firmware     : val[val.length-1].devicefirmwares_id
        });
        this.crearDFirmware=false;
        this.idFirmware=val[val.length-1].id+"";
        this.crearDFirmware=false;
    });
  }
  /** Llena las tablas con información de los componentes de la computadora */
  llenarTablas(){
    let dataMemory     : string[][]=[];
    let dataHardDrive  : string[][]=[];
    let dataNetCard    : string[][]=[];
    let dataDrive      : string[][]=[];
    let dataGraCard    : string[][]=[];
    let dataSoundCard  : string[][]=[];
    let dataController : string[][]=[];

    /**Se realiza la consulta para traer todas las memorias de la computadora */
    this.itemComponenteService.allSubItems(this.id,"Item_DeviceMemory").subscribe(response=>{
      response.forEach(element=>{
        let nomMemoria="";
        let frecuencia="";
        let sucursal="";

        this.listaMemoria.forEach(val=>{
          if(val.id==element.devicememories_id){
            nomMemoria=val.designation+"";
            frecuencia=val.frequence+"";
          }
        });
        this.listaSucursal.forEach(val=>{
          if(val.id==element.locations_id){sucursal=val.name+"";}
            });
       /**Llenar el arreglo para la tabla de memorias  de la computadora */
        dataMemory.push([nomMemoria, frecuencia,element.size+"", sucursal]);
        this.dataSourceMemoria.data=dataMemory;
      })});

      /**Se realiza la consulta para traer hard drive de la computadora */
    this.itemComponenteService.allSubItems(this.id,"Item_DeviceHardDrive").subscribe(response=>{
      response.forEach(element=>{
        let nombreH="";
        let sucursal="";

        this.listaHardDrive.forEach(val=>{
          if(val.id==element.deviceharddrives_id){
            nombreH=val.designation+"";
          };
        });
        this.listaSucursal.forEach(val=>{
          if(val.id==element.locations_id){sucursal=val.name+"";}
            });
       /**Llenar el arreglo para la tabla de hard drive  de la computadora */
        dataHardDrive.push([nombreH, element.capacity+"",element.serial+"", sucursal]);
        this.dataSourceHardDrive.data=dataHardDrive;
      })});

        /**Se realiza la consulta para traer network card de la computadora */
    this.itemComponenteService.allSubItems(this.id,"Item_DeviceNetworkCard").subscribe(response=>{
      response.forEach(element=>{
        let nombreN="";
        let velocidad="";
        let sucursal="";

        this.listaNetCard.forEach(val=>{
          if(val.id==element.devicenetworkcards_id){
            nombreN=val.designation+"";
            velocidad=val.bandwidth+"";
          };
        });
        this.listaSucursal.forEach(val=>{
          if(val.id==element.locations_id){sucursal=val.name+"";}
            })
       /**Llenar el arreglo para la tabla de hard drive  de la computadora */
        dataNetCard.push([nombreN, velocidad+"",element.mac+"", sucursal]);
        this.dataSourceNetCard.data=dataNetCard;
      })});

       /**Se realiza la consulta para traer drive de la computadora */
    this.itemComponenteService.allSubItems(this.id,"Item_DeviceDrive").subscribe(response=>{
      response.forEach(element=>{
        let nombreD="";
        let sucursal="";

        this.listaDrive.forEach(val=>{
          if(val.id==element.devicedrives_id){
            nombreD=val.designation+"";
          };
        });
        this.listaSucursal.forEach(val=>{
          if(val.id==element.locations_id){sucursal=val.name+"";}
            });
       /**Llenar el arreglo para la tabla de hard drive  de la computadora */
        dataDrive.push([nombreD, sucursal]);
        this.dataSourceDrive.data=dataDrive;
      })});

      /**Se realiza la consulta para traer TARJETA GRAFICA de la computadora */
    this.itemComponenteService.allSubItems(this.id,"Item_DeviceGraphicCard").subscribe(response=>{
      response.forEach(element=>{
        let nombreG="";
        let sucursal="";

        this.listaGraCard.forEach(val=>{
          if(val.id==element.devicegraphiccards_id){
            nombreG=val.designation+"";
          }
        });
        this.listaSucursal.forEach(val=>{
          if(val.id==element.locations_id){sucursal=val.name+"";}
            });
       /**Llenar el arreglo para la tabla de hard drive  de la computadora */
        dataGraCard.push([nombreG, element.memory+"", sucursal]);
        this.dataSourceGraCard.data=dataGraCard;
      })});


       /**Se realiza la consulta para traer TARJETA DE SONIDO de la computadora */
    this.itemComponenteService.allSubItems(this.id,"Item_DeviceSoundCard").subscribe(response=>{
      response.forEach(element=>{
        let nombre="";
        let sucursal="";

        this.listaSoundCard.forEach(val=>{
          if(val.id==element.devicesoundcards_id){
            nombre=val.designation+"";
          }
        });
        this.listaSucursal.forEach(val=>{
          if(val.id==element.locations_id){sucursal=val.name+"";}
            });
       /**Llenar el arreglo para la tabla de hard drive  de la computadora */
        dataSoundCard.push([nombre, sucursal]);
        this.dataSourceSoundCard.data=dataSoundCard;
      })});

       /**Se realiza la consulta para traer TARJETA DE SONIDO de la computadora */
    this.itemComponenteService.allSubItems(this.id,"Item_DeviceSoundCard").subscribe(response=>{
      response.forEach(element=>{
        let nombre="";
        let sucursal="";

        this.listaSoundCard.forEach(val=>{
          if(val.id==element.devicesoundcards_id){
            nombre=val.designation+"";
          }
        });
        this.listaSucursal.forEach(val=>{
          if(val.id==element.locations_id){sucursal=val.name+"";}
            })
       /**Llenar el arreglo para la tabla de hard drive  de la computadora */
        dataSoundCard.push([nombre, sucursal]);
        this.dataSourceSoundCard.data=dataSoundCard;
      })});

       /**Se realiza la consulta para traer los controladores de la computadora */
    this.itemComponenteService.allSubItems(this.id,"Item_DeviceControl").subscribe(response=>{
      response.forEach(element=>{
        let nombre="";
        let sucursal="";

        this.listaController.forEach(val=>{
          if(val.id==element.devicecontrols_id){
            nombre=val.designation+"";
          }
        });
        this.listaSucursal.forEach(val=>{
          if(val.id==element.locations_id){sucursal=val.name+"";}
            });
       /**Llenar el arreglo para la tabla de hard drive  de la computadora */
        dataController.push([nombre, sucursal]);
        this.dataSourceController.data=dataController;
      })});

  }
  /** Agrega un componente al registro de computadora */
  agregarComponente(componente : string){
    let bodyComp=""
    let idComponente=0;
    let crearRegistro=true;

    if(componente== "SO"){
      crearRegistro=this.crearSO;
      this.tabla="Item_OperatingSystem";
      idComponente=Number(this.idSO);
      bodyComp=   "                    \"operatingsystems_id\": \""+this.formSistemaOperativo.value.nombreSO+"\",\n"
                + "                    \"operatingsystemversions_id\": \""+this.formSistemaOperativo.value.version+"\",\n"
                + "                    \"operatingsystemservicepacks_id\": \""+this.formSistemaOperativo.value.servicePack+"\",\n"
                + "                    \"operatingsystemarchitectures_id\": \""+this.formSistemaOperativo.value.arquitectura+"\",\n"
                + "                    \"license_number\": \""+this.formSistemaOperativo.value.numeroSerieSO+"\",\n"
                + "                    \"license_id\": \""+this.formSistemaOperativo.value.idProducto+"\"\n"
    }
    if(componente== "Procesor"){
      crearRegistro=this.crearDProcesor;
      this.tabla="Item_DeviceProcessor";
      idComponente=Number(this.idProcesor);
      bodyComp=   "                    \"operatingsystems_id\": \""+this.formSistemaOperativo.value.nombreSO+"\",\n"
                + "                    \"operatingsystemversions_id\": \""+this.formSistemaOperativo.value.version+"\",\n"
                + "                    \"operatingsystemservicepacks_id\": \""+this.formSistemaOperativo.value.servicePack+"\",\n"
                + "                    \"operatingsystemarchitectures_id\": \""+this.formSistemaOperativo.value.arquitectura+"\",\n"
                + "                    \"license_number\": \""+this.formSistemaOperativo.value.numeroSerieSO+"\",\n"
                + "                    \"license_id\": \""+this.formSistemaOperativo.value.idProducto+"\"\n"
        }

    this.body=  "{\n"
                + "    \"input\": {    \n"
                + "                    \"items_id\": "+this.id+",\n"
                + "                    \"itemtype\": \"Computer\",\n"
                +                      bodyComp
                + "    }\n"
                + "}";
    //Crear sistema operativo en caso de que no este creado
    if(crearRegistro ==true){
      this.equipoService.insertarItem(this.tabla,this.body);
    }else if(crearRegistro==false){
      //actualizar el sistema operativo
      this.equipoService.actualizarItem(this.tabla, idComponente,this.body).subscribe(data=>{
        Swal.fire({
          icon: 'success',
          title: 'Actualizar Sistema Operativo',
          text: 'El sistema operativo se actualizó exitosamente',
          showConfirmButton: false,
          timer: 2000
        });
      }, err=>{
        Swal.fire({
          icon: 'info',
          title: 'Actualizar Sistema Operativo',
          text: 'Ocurrio un error de conexión, intentelo más tarde o contacte al área de T.I.',
          showConfirmButton: false,
          timer: 2000
        });
      });
    }
  }
  /**     LLENAR LISTAS de fabricantes, sucursales,estados del los items, usuarios y empleados    */
  llenarListas(){
   /** Lista Sucursales */
    this.sucursalGlpiService.allSucursales(this.token).subscribe(             response => { this.listaSucursal   = response.sort(this.SortArraySucursales); });
    /** Lista usuarios */
    this.usuarioGlpiService.allUsuarios(this.token).subscribe(                response => { this.listaUsuarios   = response;} );
    /** Lista estado */
    this.estadoService.allEstadosItems(this.token).subscribe(                 response => { this.listaEstados    = response;} );
    /** Lista responsable tecnico */
    this.usuarioGlpiService.allUsuarios(this.token).subscribe(val=>{
      this.listaUsuarios=val;
      for(let i=0;i<this.listaUsuarios.length ;i++){
        if (this.listaUsuarios[i].usertitles_id==1){
          this.listaResTecnico.push(this.listaUsuarios[i]);
        }
      }
    });
    //this.usuarioService.getAllEmpleados().subscribe(lista=>{this.listaEmpleados=lista});
    this.usuarioService.getAllEmpleados().subscribe(empleados=>{ this.listaEmpleados=empleados.sort(this.SortArray);  });
    this.llenarListaFabricantes();
  }
  /**Llena lista de fabricantes */
  llenarListaFabricantes(){
    /** Lista Fabricantes */
    this.fabricanteService.allFabricantes(this.token).subscribe(              response => { this.listaFabricante = response.sort(this.SortArrayFabric); });
  }
  /**Metódo para ordenar las sucursales por nombre e ignorar signos de puntuación */
  SortArraySucursales(x: SucursalGlpi, y: SucursalGlpi){
    return x.name.localeCompare(y.name, 'fr', {ignorePunctuation: true});
  }
  /**Metódo para ordenar el fabricante por nombre e ignorar signos de puntuación */
  SortArrayFabric(x: Fabricante, y: Fabricante){
    return x.name.localeCompare(y.name, 'fr', {ignorePunctuation: true});
  }
  /**Metódo para ordenar las sucursales por nombre e ignorar signos de puntuación */
  SortArray(x: Empleado, y: Empleado){
    return x.nombre.localeCompare(y.nombre, 'fr', {ignorePunctuation: true});
  }
  /** Lista de componentes */
  llenarListasComponentes(){
    this.llenarListasSO();
   /*Llenar listas para los componentes*/
    this.componenteService.allComponentes("DeviceProcessor", this.token).subscribe(val=>{this.listaProcesador = val;});
    this.componenteService.allComponentes("DeviceFirmware", this.token).subscribe(val=>{this.listaFirmware = val;});
    this.componenteService.allComponentes("DeviceMemory",this.token).subscribe(val=>{this.listaMemoria=val;});
    this.componenteService.allComponentes("DeviceMemoryType",this.token).subscribe(val=>{this.listaTipoMemoria=val;});
    this.componenteService.allComponentes("DeviceHardDrive",this.token).subscribe(val=>{this.listaHardDrive=val;});
    this.componenteService.allComponentes("DeviceNetworkCard",this.token).subscribe(val=>{this.listaNetCard=val;});
    this.componenteService.allComponentes("DeviceDrive",this.token).subscribe(val=>{this.listaDrive=val;});
    this.componenteService.allComponentes("DeviceGraphicCard",this.token).subscribe(val=>{this.listaGraCard=val;});
    this.componenteService.allComponentes("DeviceSoundCard",this.token).subscribe(val=>{this.listaSoundCard=val;});
    this.componenteService.allComponentes("DeviceControl",this.token).subscribe(val=>{this.listaController=val;});
  }
  /**Llena las listas para el sistema operativo */
  llenarListasSO(){
    /** Listas para el Sistema Operativo */
    this.componenteService.allComponentes("OperatingSystem", this.token).subscribe(val=>{this.listaSO = val.sort(this.SortArrayComponente);});
    this.componenteService.allComponentes("OperatingSystemVersion", this.token).subscribe(val=>{this.listaVersionSO = val.sort(this.SortArrayComponente);});
    this.componenteService.allComponentes("OperatingSystemArchitecture", this.token).subscribe(val=>{this.listaArquitectura = val.sort(this.SortArrayModelos1);});
    this.componenteService.allComponentes("OperatingSystemServicePack", this.token).subscribe(val=>{this.listaServPack = val.sort(this.SortArrayComponente);});

  }
  /**Metódo para ordenar los componentes por nombre e ignorar signos de puntuación */
  SortArrayComponente(x: Componente, y: Componente){
    return x.name.localeCompare(y.name, 'fr', {ignorePunctuation: true});
  }
  /**Retorna true o false segun el activo que se le pase como parámetro */
  activoSeleccionado(activo : string): boolean{
    if(activo == this.nomActivo)
      return true;
    return false;
  }
   /**Identifica el activo seleccionado y le inserta las variables segun corresponda */
  selectActivo(){
    if(this.nomActivo =="COMPUTADORA"){
      this.tipo="ComputerType"
      this.modelo="ComputerModel"
      this.tabla="Computer"
    }
    if(this.nomActivo =="DISP. RED"){
      this.tipo="NetworkequipmentType"
      this.modelo="NetworkequipmentModel"
      this.tabla="Networkequipment"
    }
    if(this.nomActivo =="IMPRESORA"){
      this.tipo="PrinterType"
      this.modelo="PrinterModel"
      this.tabla="Printer"
    }
    if(this.nomActivo =="MONITOR"){
      this.tipo="MonitorType"
      this.modelo="MonitorModel"
      this.tabla="Monitor"
    }
    if(this.nomActivo =="RACK"){
      this.tipo="RackType"
      this.modelo="RackModel"
      this.tabla="Rack"
    }
    if(this.nomActivo =="PERIFERICO"){
      this.tipo="PeripheralType"
      this.modelo="PeripheralModel"
      this.tabla="Peripheral"
    }
    if(this.nomActivo =="TELEFONO"){
      this.tipo="PhoneType"
      this.modelo="PhoneModel"
      this.tabla="Phone"
    }
    this.listaModelo=[];
    this.listaTipos=[];
    this.selectTipoModelo();
  }
  /**Llena las lista de tipo y modelo de manera ordenada */
  selectTipoModelo(){
    if(this.tipo=="ComputerType"){    this.tipoService.allTiposItems(this.tipo).subscribe(tipos=>{this.listaTipos=tipos.sort(this.SortArrayModelos1);});
    }else{    this.tipoService.allTiposItems(this.tipo).subscribe(tipos=>{this.listaTipos=tipos.sort(this.SortArrayItems);});
     }
    this.tipoService.allTiposItems(this.tipo).subscribe(tipos=>{this.listaTipos=tipos.sort(this.SortArrayItems);});
    /**En caso de ser un disp. de red o periférico, se utiliza otro metódo de ordanamiento, ya que por sus datos no se permite hcaerlo de la misma manera*/
    if(this.modelo=="NetworkequipmentModel" || this.modelo=="PeripheralModel"){
      this.modeloService.allModelosItems(this.modelo).subscribe(modelos=>{ this.listaModelo=modelos.sort(this.SortArrayModelos1);});
    }else{
      this.modeloService.allModelosItems(this.modelo).subscribe(modelos=>{ this.listaModelo=modelos.sort(this.SortArrayModelos2);});
    }
  }
  /**Método para ordenar los modelos que no son cadenas */
  SortArrayModelos1(x: Modelo, y: Modelo){
    if (x.name < y.name) {return -1;}
     if (x.name > y.name) {return 1;}
     return 0;
  }
   /**Método para ordenar los modelos que son cadenas */
  SortArrayModelos2(x: Modelo, y: Modelo){
    return x.name.localeCompare(y.name, 'fr', {ignorePunctuation: true});
  }
  /**Método para ordenar objetos que son cadenas */
  SortArrayItems(x: Tipo, y: Tipo){
    return x.name.localeCompare(y.name, 'fr', {ignorePunctuation: true});
  }
  /**Metódo que actualiza al equipo */
  actualizarEquipo(){
      //Inserta los datos para el tipo de activo
    let tipoComputadora = " \", \"computermodels_id\": \" "+this.formActualizar.value.modelo
                          +" \", \"computertypes_id\": \" "+this.formActualizar.value.tipo

    let tipoPeriferico = " \", \"peripheralmodels_id\": \" "+this.formActualizar.value.modelo
                         +" \", \"peripheraltypes_id\": \" "+this.formActualizar.value.tipo

    let tipoImpresora = " \", \"printermodels_id\": \" "+this.formActualizar.value.modelo
                        +" \", \"printertypes_id\": \" "+this.formActualizar.value.tipo

    let tipoEquipoRed = " \", \"networkequipmentmodels_id\": \" "+this.formActualizar.value.modelo
                       +" \", \"networkequipmenttypes_id\": \" "+this.formActualizar.value.tipo

    let tipoTelefono = " \", \"phonemodels_id\": \" "+this.formActualizar.value.modelo
                       +" \", \"phonetypes_id\": \" "+this.formActualizar.value.tipo

    let tipoRack = " \", \"rackmodels_id\": \" "+this.formActualizar.value.modelo
                  +" \", \"racktypes_id\": \" "+this.formActualizar.value.tipo
                  +" \", \"number_units\": \" "+this.formActualizar.value.numUnidades

    let tipoMonitor = " \", \"monitormodels_id\": \" "+this.formActualizar.value.modelo
                     +" \", \"monitortypes_id\": \" "+this.formActualizar.value.tipo

    /**Se asignan los valores, segun el activo al que corresponda */
    if(this.nomActivo =="COMPUTADORA"){
    this.tipoActivo=tipoComputadora
    }
    if(this.nomActivo =="DISP. RED"){
    this.tipoActivo=tipoEquipoRed
    }
    if(this.nomActivo =="IMPRESORA"){
    this.tipoActivo=tipoImpresora
    }
    if(this.nomActivo =="MONITOR"){
    this.tipoActivo=tipoMonitor
    }
    if(this.nomActivo =="RACK"){
    this.tipoActivo=tipoRack
    }
    if(this.nomActivo =="PERIFERICO"){
    this.tipoActivo=tipoPeriferico
    }
    if(this.nomActivo =="TELEFONO"){
    this.tipoActivo=tipoTelefono
    }
    //Valida que el nombre de inventario no se repita
    if(this.validarNomInventario()==false){
        //Valida que los campos esten bien requisitados
        if(this.formActualizar.valid){
          //Comprueba que sea un colaborador registrado en GLPI
          this.usuarioGlpiService.allUsuariosSearch(this.colaborador.nombre+"", (this.colaborador.apellidoPat+" "+this.colaborador.apellidoMat)).subscribe(usuarios=>{
            /**Entra en caso de que encuentre al colaborador seleccionado en Glpi o no se haya elegido colaborador */
            if(usuarios.length>=1 ||this.colaborador.nombre==""){
              let idUsuario=0;
              if(usuarios.length>=1){
                idUsuario=Number(usuarios[0].id)
              }
              let comentarios : string = this.formActualizar.value.comentarios;
              this.body = "{\"input\": {\"name\": \" "+this.formActualizar.value.nombreInventario
                              +" \", \"serial\": \" "+this.formActualizar.value.numeroSerie
                              +" \", \"contact\": \" "+this.formActualizar.value.puesto
                              +" \", \"users_id_tech\":\" "+this.formActualizar.value.responsableTecnico
                              +" \", \"comment\": \" "+comentarios
                              +" \", \"locations_id\":\" "+this.formActualizar.value.sucursal
                              +this.tipoActivo
                              +" \", \"manufacturers_id\": \" "+this.formActualizar.value.fabricante
                              +" \", \"users_id\": \" "+idUsuario//+usuarios[0].id+0 //ID perteneciente al usuario de GLPI
                              +" \", \"states_id\": \" "+this.formActualizar.value.status
                              +" \", \"uuid\": \" "+this.formActualizar.value.uuid
                              +"\"}}";
              //Inicio ----
                  /* Se realiza el registro */
                  this.equipoService.actualizarItem(this.tabla,this.id,this.body).subscribe(data=>{
                    Swal.fire({
                      icon: 'success',
                      title: 'Actualizar equipo',
                      text: 'El equipo se actualizó exitosamente',
                      showConfirmButton: false,
                      timer: 2000
                    });
                  }, err=>{
                    Swal.fire({
                      icon: 'info',
                      title: 'Actualizar equipo',
                      text: 'Ocurrio un error de conexión, intentelo más tarde o contacte al área de T.I.',
                      showConfirmButton: false,
                      timer: 2000
                    });
                  });
            }else{
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: 'Actualizar equipo',
                text: ' El colaborador que eligió no se encuentra registrado en el sistema GLPI. '
                    +'Favor de agregar al colaborador en GLPI y después intente realizar la actualización.',
              });
            }
          });
        }else{
          Swal.fire({
            icon: 'info',
            title: 'Actualizar equipo',
            text: 'Es necesario que los campos esten debidamente requisitados',
            showConfirmButton: false,
            timer: 2000
          });
        }
      }else {
        Swal.fire({
          icon: 'info',
          title: 'Actualizar equipo',
          text: 'Es nombre de inventario ya existe en la base de datos, porfavor pruebe con otro',
          showConfirmButton: false,
          timer: 2000
        });
      }
  }
  /**Metodo que compra que el nombre de inventario nuevo, no sea igual a uno ya existente */
  validarNomInventario(){
    if(this.formActualizar.value.nombreInventario != this.equipoInicial.name?.trim()){
      return this.listaNomImventario.includes(this.formActualizar.value.nombreInventario);
    }
    return false;
  }
/** Métodos de los botones */
/** Descarga el código de barras */
  descargarCodigoBarras() {
    html2canvas(document.querySelector("#codigoBarras")!).then(canvas => {

      let imagenCreada = canvas.toDataURL();
      this.downloadImage(imagenCreada)
    });
    this.imgcreada = true;
  }
/* Descargar la imagen */
  async downloadImage(imageSrc="") {

    const image = await fetch(imageSrc)
    const imageBlog = await image.blob()
    const imageURL = URL.createObjectURL(imageBlog)

    const link = document.createElement('a')
    link.href = imageURL
    link.download = "código_"+this.formActualizar.value.nombreInventario//'código de barras'
    document.body.appendChild(link)
    link.click()
    document.body.removeChild(link)
  }
  /* Verifica que se pueda editar solo si no esta en un estado de vendido, robado o dado de baja */
  isEditable(){
    if(this.formActualizar.value.status!=3 && this.formActualizar.value.status!=7 && this.formActualizar.value.status!=8){
      return true;
    } else {
      return false;
    }
  }

  /* Habilita y dehabilita la edicion */
  habilitarEdicion(){
    if(this.banderaForm==false){
      this.formActualizar.enable();
      this.formActualizar.controls['activo'].disable();
      this.formSistemaOperativo.enable();
      this.banderaForm=true;
    }
      else if(this.banderaForm==true){
        this.formActualizar.disable();
        this.formSistemaOperativo.disable();
        this.banderaForm=false;
      }
  }

  /* Aregar un tipo o  modelo de un determinado tipo de activo, o un componente de una computadora */
  async agregarTipoModeloComponente(tipTabla:string, nomTabla: string){
    if(tipTabla=="TIPO")
        nomTabla=this.tipo
        else if(tipTabla=="MODELO")
                nomTabla=this.modelo

    const { value: formValues } = await Swal.fire({
      title: 'ARTÍCULO NUEVO - '+tipTabla+' DE '+this.nomActivo,
      html:'<mat-label class="label">Nombre</mat-label>'+
      '<input id="nombre" required class="swal2-input">' +
      '<mat-label class="label">Comentarios</mat-label>'+
      '<input id="comentarios" class="swal2-input">'
     ,
      focusConfirm: false,
      preConfirm: () => {
        this.inputNombre = (<HTMLInputElement>document.getElementById('nombre')).value;
        this.inputComentarios = (<HTMLInputElement>document.getElementById('comentarios')).value;
        this.body = "{\"input\": {\"name\": \" "+this.inputNombre
                      +" \", \"comment\": \" "+this.inputComentarios
                      +"\"}}";

      let mensaje=""
      this.inputNombre= this.inputNombre?.trim();
      this.inputComentarios=this.inputComentarios?.trim();
      if(this.inputNombre !="" && this.inputNombre!=null ){
        this.equipoService.insertarItem(nomTabla, this.body);mensaje="Registrando..."
      }else{mensaje="Error, no es posible registrar vacíos"}
      return  mensaje
      }
    })

    if (formValues) {
      Swal.fire(JSON.stringify(formValues))
    }
    timer(10000);

    this.listaModelo.splice(0,this.listaModelo.length)
    this.listaTipos.splice(0,this.listaTipos.length)
    this.modeloService.allModelosItems(this.modelo).subscribe(val=>{this.listaModelo=val;});
    this.tipoService.allTiposItems(this.tipo).subscribe(val=>{this.listaTipos=val;});
  }

  /**Metódo que muestra la información de ayuda */
  botonInformacion(){
    Swal.fire({
      position: 'top-end',
      icon: 'info',
      title: 'DETALLE DE EQUIPO',
      showConfirmButton: false,
      html: '<p align="justify">A continuación podrá visualizar todos los los datos pertenecientes al equipo seleccionado.\n</p>'
           +'<p align="justify">Además al precionar el botón azul para editar, todos los campos se habilitarán para su edición \n</p>'
           +'<p align="justify">* Para la edición deben estar completados todos los campos que son requeridos, los cuales se encuentran con un asterisco.\n</p>'
           +'<p align="justify">Cada equipo que edite debe contener un tipo y modelo, en  caso de que no se encuentre el  requerido, podrá realizar el alta presionando el botón de (+) que se encuentra a un lado del campo.\n</p>'
           +'<p align="justify">Todos los cambios se verán reflejados en la base de datos de GLPI.</p>',
           confirmButtonColor: '#3085d6',
           cancelButtonColor: '#d33',
           confirmButtonText: 'Sí'
    })
  }
  /** Regresar a la vista de gestion de equipos */
  goBack(){
         if(this.nomActivo=="COMPUTADORA"){this.router.navigate(['layout/gestionequipos2/COMPUTADORAS']) }
    else if(this.nomActivo=="DISP. RED")  {this.router.navigate(['layout/gestionequipos2/DISPOSITIVOS DE RED']) }
    else if(this.nomActivo=="IMPRESORA")  {this.router.navigate(['layout/gestionequipos2/IMPRESORAS']) }
    else if(this.nomActivo=="MONITOR")    {this.router.navigate(['layout/gestionequipos2/MONITORES']) }
    else if(this.nomActivo=="PERIFERICO") {this.router.navigate(['layout/gestionequipos2/DISPOSITIVOS']) }
    else if(this.nomActivo=="RACK")       {this.router.navigate(['layout/gestionequipos2/RACKS']) }
    else if(this.nomActivo=="TELEFONO")   {this.router.navigate(['layout/gestionequipos2/TELEFONOS']) }
  }
}
