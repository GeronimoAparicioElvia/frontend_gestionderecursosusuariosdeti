import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators} from '@angular/forms';
import html2canvas from "html2canvas";
/**Modelos */
import { Estado } from 'src/app/administracion/modelos/Inventario/Estado/estado';
import { Fabricante } from 'src/app/administracion/modelos/Inventario/Fabricante/fabricante';
import { Modelo } from 'src/app/administracion/modelos/Inventario/Modelo/modelo';
import { SucursalGlpi } from 'src/app/administracion/modelos/Inventario/Sucursal/sucursal-glpi';
import { Tipo } from 'src/app/administracion/modelos/Inventario/Tipo/tipo';
import { UsuarioGlpi } from 'src/app/administracion/modelos/Inventario/Usuario/usuario-glpi';
/** Servicios */
import { EquipoService } from 'src/app/administracion/servicios/Inventario/Equipo/equipo.service';
import { EstadoService } from 'src/app/administracion/servicios/Inventario/Estado/estado.service';
import { FabricanteService } from 'src/app/administracion/servicios/Inventario/Fabricante/fabricante.service';
import { ModeloService } from 'src/app/administracion/servicios/Inventario/Modelo/modelo.service';
import { SucursalGlpiService } from 'src/app/administracion/servicios/Inventario/Sucursal/sucursal-glpi.service';
import { TipoService } from 'src/app/administracion/servicios/Inventario/Tipo/tipo.service';
import   Swal from 'sweetalert2';
import { UsuarioGlpiService } from 'src/app/administracion/servicios/Inventario/Usuario/usuario-glpi.service';
import { UsuarioService } from 'src/app/administracion/servicios';
import { Empleado } from 'src/app/administracion/modelos/empleado';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { map, startWith, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-alta-equipo',
  templateUrl: './alta-equipo.component.html',
  styleUrls: ['./alta-equipo.component.scss']
})
export class AltaEquipoComponent implements OnInit {
  //Atrapa el token de sesion cuando inicia
  token:string=JSON.parse(localStorage.getItem('glpiToken') || '{}').token

  activo:string="";
  tabla: string =""
  body: any ={};
  tipoActivo: any;
  tipo : any="ComputerType"
  modelo : any="ComputerModel"
  nomActivo : string="";
  returnUrl!: string;
  colaborador: Empleado= new Empleado();
  /**Campos para la busqueda del colaborador en la bd de GLPI 1234 */
  nombre   =""
  apellidos=""
  /**Form Control del que se le asigna el equipo */
  colaboradorFilter = new FormControl();
  inputColaboradorF = new FormControl();
  protected onDestroy  = new Subject<void>();

  //Listas para los select
  listaactivos    : string      [] = [];
  listaTipos      : Tipo        [] = [];
  listaModelo     : Modelo      [] = [];
  listaFabricante : Fabricante  [] = []
  listaSucursal!  : SucursalGlpi[];
  listaUsuarios   : UsuarioGlpi [] = [];
  listaEmpleados  : Empleado    [] = [];
  listaResTecnico : UsuarioGlpi [] = [];
  listaEstado     : Estado      [] = [];
  listaNomImventario: string    [] = [];
  /**Lista para el select del que suscribe */
  listaFiltradaEmp      :   ReplaySubject<Empleado[]> = new ReplaySubject<Empleado[]>(1);



  constructor(
              private router: Router,
              private route: ActivatedRoute,
              private formBuilder: FormBuilder,
              private equipoService : EquipoService,
              private fabricanteService:FabricanteService,
              private modeloService:ModeloService,
              private tipoService:TipoService,
              private sucursalGlpiService: SucursalGlpiService,
              private usuarioGlpiService : UsuarioGlpiService,
              private estadoService:EstadoService,
              private usuarioService:UsuarioService
              ) {}

  formInsertar = this.formBuilder.group({
    activo:             ['', [Validators.required]],
    tipo:               ['', [Validators.required]],
    nombreInventario:   ['', [Validators.required]],
    modelo:             ['', [Validators.required]],
    numeroSerie:        ['', [Validators.required]],
    fabricante:         ['', [Validators.required]],
    responsableTecnico: [''],
    sucursal:           ['', [Validators.required]],
    uuid:               [''],
    numUnidades:        ['', [Validators.minLength(3)]],
    puesto:             [{value: '', disabled: true},[]] ,
    comentarios:        ['']
  });

  /**En este metodo se llenan las listas */
  ngOnInit(): void {

    Swal.close();
    /**Llena las listas */
    this.sucursalGlpiService.allSucursales(this.token).subscribe(sucGLPI=>{this.listaSucursal=sucGLPI.sort(this.SortArraySucursales);});
    this.estadoService.allEstadosItems(this.token).subscribe(val=>{this.listaEstado = val;});
    this.usuarioGlpiService.allUsuarios(this.token).subscribe(val=>{
      this.listaUsuarios=val;
      for(let i=0;i<this.listaUsuarios.length ;i++){
        if (this.listaUsuarios[i].usertitles_id==1){
          this.listaResTecnico.push(this.listaUsuarios[i]);
        }
      }
    });
    this.listaactivos=this.equipoService.listaActivos();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    this.llenarListaNomImventario();
    this.llenarListaColab();
    /**Llena la lista de fabricantes */
    this.llenarListaFabricantes();

    /**Escucha los cambios del valor de búsqueda del colaborador*/
    this.listaFiltradaEmp.next(this.listaEmpleados.slice());
    this.inputColaboradorF.valueChanges.pipe(takeUntil(this.onDestroy)).subscribe(() => {
        this.filterColaborador();
    });
  }

  /**Llena la lista de fabricantes */
  llenarListaFabricantes(){
    this.fabricanteService.allFabricantes(this.token).subscribe(val=>{this.listaFabricante=val.sort(this.SortArrayFabric);});
  }

  /**Metódo para ordenar las sucursales por nombre e ignorar signos de puntuación */
  SortArraySucursales(x: SucursalGlpi, y: SucursalGlpi){
    return x.name.localeCompare(y.name, 'fr', {ignorePunctuation: true});
  }
  /**Metódo para ordenar el fabricante por nombre e ignorar signos de puntuación */
  SortArrayFabric(x: Fabricante, y: Fabricante){
    return x.name.localeCompare(y.name, 'fr', {ignorePunctuation: true});
  }

  /**Llena una lista  */
  llenarListaColab(){
    //this.listaEmpleados.splice(0,this.listaEmpleados.length);
    this.usuarioService .getAllEmpleados().subscribe( val => {
      val.forEach(emp=>{
        emp.nomb_compl= emp.nombre+" "+ emp.apellidoPat+" "+emp.apellidoMat
      });
      this.listaEmpleados = val;
    } );
  }

  /**Realiza la búsqueda para el colaborador a quien se le solicitan los servicios */
  protected filterColaborador(){
    if(!this.listaEmpleados){
      return;
    }
    /**Se optiene el valor del input para el colaborador*/
    let search = this.inputColaboradorF.value;
    if (!search) {
      this.listaFiltradaEmp.next(this.listaEmpleados.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    /**Retorna la lista de los colaboradores que cumplen con la busqueda */
    this.listaFiltradaEmp.next( this.listaEmpleados.filter(empleado => empleado.nomb_compl.toLowerCase().indexOf(search) > -1) );
  }

  /**Llena la lista ´listaNomInventario´ de todos los tipos de solicitud*/
  llenarListaNomImventario(){
    let listaTablas:string []=["Computer", "Peripheral", "Printer","Networkequipment", "Phone", "Rack", "Monitor" ];
    listaTablas.forEach(activo=>{
      this.equipoService.todosLosEquipos(activo).subscribe(equipos=>{
        equipos.forEach(equipo=>{this.listaNomImventario.push(equipo.name?.trim()); });
      });
    });
  }

   //Metodo para agregar el equipo
   agregarEquipo(){
    //Dependiendo del tipo de equipo hace referencia a sus atributos de tipo y modelo
     let tipoComputadora = " \", \"computermodels_id\": \" "+this.formInsertar.value.modelo
                           +" \", \"computertypes_id\": \" "+this.formInsertar.value.tipo

     let tipoPeriferico = " \", \"peripheralmodels_id\": \" "+this.formInsertar.value.modelo
                          +" \", \"peripheraltypes_id\": \" "+this.formInsertar.value.tipo

     let tipoImpresora = " \", \"printermodels_id\": \" "+this.formInsertar.value.modelo
                          +" \", \"printertypes_id\": \" "+this.formInsertar.value.tipo

     let tipoEquipoRed = " \", \"networkequipmentmodels_id\": \" "+this.formInsertar.value.modelo
                          +" \", \"networkequipmenttypes_id\": \" "+this.formInsertar.value.tipo

     let tipoTelefono = " \", \"phonemodels_id\": \" "+this.formInsertar.value.modelo
                          +" \", \"phonetypes_id\": \" "+this.formInsertar.value.tipo

     let tipoRack = " \", \"rackmodels_id\": \" "+this.formInsertar.value.modelo
                    +" \", \"racktypes_id\": \" "+this.formInsertar.value.tipo
                    +" \", \"number_units\": \" "+this.formInsertar.value.numUnidades

     let tipoMonitor = " \", \"monitormodels_id\": \" "+this.formInsertar.value.modelo
                       +" \", \"monitortypes_id\": \" "+this.formInsertar.value.tipo

    //Se asignan las valores a las variables, segun sea el activo seleccionado
    if(this.formInsertar.value.activo =="COMPUTADORA"){
      this.tipoActivo=tipoComputadora
      this.tabla="Computer"
    }
    if(this.formInsertar.value.activo =="DISP. RED"){
      this.tipoActivo=tipoEquipoRed
      this.tabla="Networkequipment"
    }
    if(this.formInsertar.value.activo =="IMPRESORA"){
      this.tipoActivo=tipoImpresora
      this.tabla="Printer"
    }
    if(this.formInsertar.value.activo =="MONITOR"){
      this.tipoActivo=tipoMonitor
      this.tabla="Monitor"
    }
    if(this.formInsertar.value.activo =="RACK"){
      this.tipoActivo=tipoRack
      this.tabla="Rack"
    }
    if(this.formInsertar.value.activo =="PERIFERICO"){
      this.tipoActivo=tipoPeriferico
      this.tabla="Peripheral"
    }
    if(this.formInsertar.value.activo =="TELEFONO"){
      this.tipoActivo=tipoTelefono
      this.tabla="Phone"
    }
    /**Asigna un nombre de puesto vacío en caso de que este sea indefinido */
    if(this.colaborador?.puesto?.nombrePuesto==undefined){
      this.colaborador.puesto.nombrePuesto="";
    }
    //Valida que el nombre de inventario no se repita
    let validarInventario : boolean = this.listaNomImventario.includes(this.formInsertar.value.nombreInventario);
    if(validarInventario==false){
      //Valida que los campos esten bien requisitados
      if(this.formInsertar.valid){
        //Comprueba que sea un colaborador registrado en GLPI
        this.usuarioGlpiService.allUsuariosSearch(this.nombre,this.apellidos).subscribe(usuarios=>
          { if(usuarios.length>=1){
            this.body = "{\"input\": {\"name\": \" "+this.formInsertar.value.nombreInventario
                        +" \", \"serial\": \" "+this.formInsertar.value.numeroSerie
                        +" \", \"contact\": \" "+this.colaborador?.puesto?.nombrePuesto//this.formInsertar.value.puesto
                        +" \", \"users_id_tech\":\" "+this.formInsertar.value.responsableTecnico
                        +" \", \"comment\": \" "+this.formInsertar.value.comentarios
                        +" \", \"locations_id\":\" "+this.formInsertar.value.sucursal
                        +this.tipoActivo
                        +" \", \"manufacturers_id\": \" "+this.formInsertar.value.fabricante
                        +" \", \"users_id\": \" "+usuarios[0].id//Toma el id encontrado en GLPI
                        +" \", \"states_id\": \" "+2//this.formInsertar.value.status//+2 El 2 significa que esta en stock
                        +" \", \"uuid\": \" "+this.formInsertar.value.uuid
                        +"\"}}";

            //Inicio ----s
            let timer :any
            Swal.fire({
              title: 'Espere por favor!',
              html: 'Registrando...',
              timerProgressBar: true,
              didOpen: () => {
                Swal.showLoading()

                /* Se realiza el registro */
                this.equipoService.insertarItem(this.tabla, this.body);
                /**Redirecciona segun el tipo de dispositivo */
                      if(this.nomActivo=="COMPUTADORA"){this.router.navigate(['layout/gestionequipos2/COMPUTADORAS']) }
                else if(this.nomActivo=="DISP. RED")  {this.router.navigate(['layout/gestionequipos2/DISPOSITIVOS DE RED']) }
                else if(this.nomActivo=="IMPRESORA")  {this.router.navigate(['layout/gestionequipos2/IMPRESORAS']) }
                else if(this.nomActivo=="MONITOR")    {this.router.navigate(['layout/gestionequipos2/MONITORES']) }
                else if(this.nomActivo=="PERIFERICO") {this.router.navigate(['layout/gestionequipos2/DISPOSITIVOS']) }
                else if(this.nomActivo=="RACK")       {this.router.navigate(['layout/gestionequipos2/RACKS']) }
                else if(this.nomActivo=="TELEFONO")   {this.router.navigate(['layout/gestionequipos2/TELEFONOS']) }

                    Swal.fire({

                      position: 'center',
                      icon: 'success',
                      title: 'Alta de equipos',
                      text: ' El equipo se registró exitosamente.',

                    }).then(
                      data => {
                        /**Redirecciona segun el tipo de dispositivo */
                        if(this.nomActivo=="COMPUTADORA"){this.router.navigate(['layout/gestionequipos2/COMPUTADORAS']) }
                        else if(this.nomActivo=="DISP. RED"){this.router.navigate(['layout/gestionequipos2/DISPOSITIVOS DE RED']) }
                        else if(this.nomActivo=="IMPRESORA"){this.router.navigate(['layout/gestionequipos2/IMPRESORAS']) }
                        else if(this.nomActivo=="MONITOR"){this.router.navigate(['layout/gestionequipos2/MONITORES']) }
                        else if(this.nomActivo=="PERIFERICO"){this.router.navigate(['layout/gestionequipos2/DISPOSITIVOS']) }
                        else if(this.nomActivo=="RACK"){this.router.navigate(['layout/gestionequipos2/RACKS']) }
                        else if(this.nomActivo=="TELEFONO"){this.router.navigate(['layout/gestionequipos2/TELEFONOS']) }
                      }
                    )
              },
              willClose: () => {
                clearInterval(timer)
              }
            }).then((result) => {
              /* Read more about handling dismissals below */
              if (result.dismiss === Swal.DismissReason.timer) {

              }
            })
            /**En caso de que el colborador no se encuentre registrado en GLPI */
          }else{
            Swal.fire({
              position: 'center',
              icon: 'error',
              title: 'Alta de equipos',
              text: ' El colaborador que eligió no se encuentra registrado en el sistema GLPI. '
              +'Favor de agregar al colaborador en GLPI y después intente generar el alta.',
            })
          }
          }, err=>{
            console.log(err);
            Swal.fire({
              position: 'center',
              icon: 'error',
              title: 'Error en la conexión',
              text: 'Lo sentimos, el registro no se realizó correctamente debido a que no hay conexión con el servidor, inténtelo más tarde',

            })
          })

      }else{
        Swal.fire({
          icon: 'info',
          title: 'Alta de equipos',
          text: 'Es necesario que los campos esten debidamente requisitados',
          showConfirmButton: false,
          timer: 2000
        });
      }
    }else {
      Swal.fire({
        icon: 'info',
        title: 'Alta de equipos',
        text: 'Es nombre de inventario ya existe en la base de datos, porfavor pruebe con otro',
        showConfirmButton: false,
        timer: 2000
      });
    }
  }
  /**Retorna true o false segun el activo que se le pase como parámetro */
   activoSeleccionado(activo : string): boolean{
      if(activo == this.formInsertar.value.activo)
        return true;
      return false;
   }
   /**Identifica el activo seleccionado y le inserta las variables segun corresponda */
   selectActivo(){
    if(this.formInsertar.value.activo =="COMPUTADORA"){
      this.tipo="ComputerType"
      this.modelo="ComputerModel"
    }
    if(this.formInsertar.value.activo =="DISP. RED"){
      this.tipo="NetworkequipmentType";
      this.modelo="NetworkequipmentModel";
    }
    if(this.formInsertar.value.activo =="IMPRESORA"){
      this.tipo="PrinterType";
      this.modelo="PrinterModel";
    }
    if(this.formInsertar.value.activo =="MONITOR"){
      this.tipo="MonitorType";
      this.modelo="MonitorModel";
    }
    if(this.formInsertar.value.activo =="RACK"){
      this.tipo="RackType";
      this.modelo="RackModel";
    }
    if(this.formInsertar.value.activo =="PERIFERICO"){
      this.tipo="PeripheralType";
      this.modelo="PeripheralModel";
    }
    if(this.formInsertar.value.activo =="TELEFONO"){
      this.tipo="PhoneType";
      this.modelo="PhoneModel";
    }
    /**Limpia las listas de tipo y modelo */
    this.listaModelo=[];
    this.listaTipos=[];
    this.selectTipoModelo();
    this.nomActivo=this.formInsertar.value.activo;
  }
  /**Llena las lista de tipo y modelo de manera ordenada */
  selectTipoModelo(){
    if(this.tipo=="ComputerType"){    this.tipoService.allTiposItems(this.tipo).subscribe(tipos=>{this.listaTipos=tipos.sort(this.SortArrayModelos1);});
    }else{    this.tipoService.allTiposItems(this.tipo).subscribe(tipos=>{this.listaTipos=tipos.sort(this.SortArrayTipos);});
     }
    //this.tipoService.allTiposItems(this.tipo).subscribe(tipos=>{this.listaTipos=tipos.sort(this.SortArrayTipos);});
    /**En caso de ser un disp. de red o periférico, se utiliza otro metódo de ordanamiento, ya que por sus datos no se permite hcaerlo de la misma manera*/
    if(this.modelo=="NetworkequipmentModel" || this.modelo=="PeripheralModel"){
      this.modeloService.allModelosItems(this.modelo).subscribe(modelos=>{ this.listaModelo=modelos.sort(this.SortArrayModelos1);});
    }else{
      this.modeloService.allModelosItems(this.modelo).subscribe(modelos=>{ this.listaModelo=modelos.sort(this.SortArrayModelos2);});
    }
  }
  /**Método para ordenar los modelos que no son cadenas */
  SortArrayModelos1(x: Modelo, y: Modelo){
     if (x.name < y.name) {return -1;}
      if (x.name > y.name) {return 1;}
      return 0;
  }
  /**Método para ordenar los modelos que son cadenas */
  SortArrayModelos2(x: Modelo, y: Modelo){
    return x.name.localeCompare(y.name, 'fr', {ignorePunctuation: true});
  }
  /**Método para ordenar los tipos de computadora*/
  SortArrayTipos(a: Tipo, b: Tipo){
    return a.name.localeCompare(b.name, 'fs', {ignorePunctuation: true});
  }
  //Metodo que crea y descarga el código de barras
  descargarCodigoBarras() {
    html2canvas(document.querySelector("#codigoBarras")!).then(canvas => {
      let imagenCreada = canvas.toDataURL();
      this.downloadImage(imagenCreada)
    });
  }
  //Metódo para descargar la imagen
  async downloadImage(imageSrc="") {

    const image = await fetch(imageSrc)
    const imageBlog = await image.blob()
    const imageURL = URL.createObjectURL(imageBlog)

    const link = document.createElement('a')
    link.href = imageURL
    link.download = "código_"+this.formInsertar.value.nombreInventario
    document.body.appendChild(link)
    link.click()
    document.body.removeChild(link)
  }

  //Variables del método agregarTipoModelo
  inputNombre:any=""
  inputComentarios:any=""
  //Método para agregar un tipo, modelo o fabricante de un determinado tipo de activo
  async agregarTipoModelo(tipTabla:string){
    let tabla:string
    if(tipTabla=="TIPO")
        tabla=this.tipo
        else if(tipTabla=="MODELO")
                tabla=this.modelo
                else if(tipTabla=="FABRICANTE")
                  tabla="Manufacturer"

    const { value: formValues } = await Swal.fire({
      title: 'ARTÍCULO NUEVO - '+tipTabla+' DE '+this.nomActivo,
      html:'<mat-label class="label">Nombre</mat-label>'+
      '<input id="nombre" class="swal2-input">' +
      '<mat-label class="label">Comentarios</mat-label>'+
      '<input id="comentarios" class="swal2-input">'
      ,
      focusConfirm: false,
      preConfirm: () => {
        this.inputNombre = (<HTMLInputElement>document.getElementById('nombre')).value;
        this.inputComentarios = (<HTMLInputElement>document.getElementById('comentarios')).value;
        this.body = "{\"input\": {\"name\": \" "+this.inputNombre
                      +" \", \"comment\": \" "+this.inputComentarios
                      +"\"}}";

        let mensaje="";
        this.inputNombre= this.inputNombre?.trim();
        this.inputComentarios=this.inputComentarios?.trim();
        if(this.inputNombre !="" && this.inputNombre!=null ){
          this.equipoService.insertarItem(tabla, this.body);
          mensaje="Registrando..."
        }else{mensaje="Error, no es posible registrar vacíos"}
        return [
          mensaje
          ]
      }
    })

    if (formValues) {
      Swal.fire(JSON.stringify(formValues))
    }
  }
  /**Método  que redirige al inicio*/
  goBack(){
    this.router.navigate(['layout/home/']);
  }
  /**Permite mostrar los botones para agregar un tipo y modelo solo si ya se ha elegido el activo */
  mostrarBotonesTipoModelo(){
    if(this.formInsertar.value.activo!=""){return true}
    else{return false}
  }

  /**carga el puesto del empleado */
  cargarDatosEmpleado(event : any){
    this.colaborador = this.colaboradorFilter.value;


    /**Borramos el contenido que tengan los campos de cargo y sucursal */
    this.formInsertar.controls['puesto'].setValue("");
    this.formInsertar.controls['puesto'].setValue(this.colaborador?.puesto?.nombrePuesto);
    this.nombre   =this.colaborador.nombre+"";
    this.apellidos=this.colaborador.apellidoPat+" "+this.colaborador.apellidoMat;
  }

  /**Permite habilitar el boton para registrar el equipo *
  habilitarBoton(){
    if(this.colaboradorFilter.value != null ){
       return true}
    return false
  }*/

  /**Metódo que muestra la información de ayuda */
  botonInformacion(){
    Swal.fire({
      position: 'top-end',
      icon: 'info',
      title: 'ALTA DE EQUIPO',
      showConfirmButton: false,
      html: '<p align="justify">Como primer paso deberá elegir el tipo de dispositivo que registrará, como puede ser una COMPUTADORA,  DISP. RED, IMPRESORA, MONITOR,  RACK, PERIFERICO o TELEFONO, de la misma manera debe completar todos los campos que son requeridos.\n</p>'
           +'<p align="justify">Para cada equipo que registre debe elegir tipo y modelo, en  caso de que no se encuentre el  requerido, podrá realizar el alta presionando el botón de (+) que se encuentra a un lado del campo.\n</p>'
           +'<p align="justify">El registro se  realizará en la base de datos de GLPI.</p>',
           confirmButtonColor: '#3085d6',
           cancelButtonColor: '#d33',
           confirmButtonText: 'Sí'
    })
  }
}
