import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { SolicitudAsigEquipoService } from 'src/app/administracion/servicios/Asignacion_Equipo/solicitud-asig-equipo.service';
import { SolicitudService } from 'src/app/administracion/servicios/Control_Usuario/solicitud.service';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  permisosU :any;/*Lista de permisos del usuario */
  /**Contador para cada tipo de solicitudes  */
  numSoliAlta : number = 0;
  numSoliAct  : number = 0;
  numSoliSusp : number = 0;
  numSoliEqui : number = 0;
  /* Propiedades del funcionamiento del componente */
  isActive: boolean | undefined;
  collapsed!: boolean;
  showMenu!: string;
  pushRightClass!: string;
  @Output() collapsedEvent = new EventEmitter<boolean>();

  constructor(
    public router       : Router,
    private solicitudAsigEquipoService: SolicitudAsigEquipoService,
    private solicitudService : SolicitudService
  ){

      this.router.events.subscribe(val => {
          if (
              val instanceof NavigationEnd &&
              window.innerWidth <= 992 &&
              this.isToggled()
          ) {
              this.toggleSidebar();
          }
      });
  }

  ngOnInit() {
      this.isActive = false;
      this.collapsed = false;
      this.showMenu = '';
      this.pushRightClass = 'push-right';
      this.permisosU = JSON.parse(localStorage.getItem('permisos')!);
      this.mostrarNumSolicitudes();

  }

    /* Con este método buscamos si  el usuario cuenta con un permiso */
  tienePermiso(permiso: string){
    if(this.permisosU.length>0) /*Validamos que la lista de permisos no esté vacía */
    {
      for(let i=0; i<this.permisosU.length;i++)/* Recorremos la lista de permisos */
      {
        if( this.permisosU[i].nombre == permiso ) /*validamos si el permiso actual es el que buscamos */
        {
          return true /* retorna verdadero en caso de encontrar el permiso */
        }
      }
    }
    return false  /* retorna falso si no encuentra el permiso */
  }

  addExpandClass(element: any) /*Expande los submenus dentro del sidebar */
  {
    if (element === this.showMenu) {
        this.showMenu = '0';
    } else {
        this.showMenu = element;
    }
  }

  toggleCollapsed() /*Oculta la barra del sidebar */
  {
      this.collapsed = !this.collapsed;
      this.collapsedEvent.emit(this.collapsed);
  }

  isToggled(): boolean
  {
    const dom: any = document.querySelector('body');
    return dom.classList.contains(this.pushRightClass);
  }

  toggleSidebar() {
      const dom: any = document.querySelector('body');
      dom.classList.toggle(this.pushRightClass);
  }

  /**Metódo para realizar el conteo de las solicitudes de servicios en espera */
  mostrarNumSolicitudes(){
    this.numSoliAlta = 0;
    this.numSoliAct  = 0;
    this.numSoliSusp = 0;
    this.solicitudService.todasLasSolicitudes().subscribe(responseSoli=>{
      responseSoli.forEach(solicitud=>{
        /**Realiza la operación para identificar cuantas solicitudes de alta de servicios hay en espera */
        if(solicitud.tipo_solicitud_usuario.tipo_solicitud=="ALTA DE SERVICIOS" && solicitud.status_solicitud=="Espera"){
          this.numSoliAlta++
        }
        /**Realiza la operación para identificar cuantas solicitudes de activación de privilegios hay en espera */
        if(solicitud.tipo_solicitud_usuario.tipo_solicitud=="ACTIVACIÓN DE PRIVILEGIOS" && solicitud.status_solicitud=="Espera"){
          this.numSoliAct++
        }
        /**Realiza la operación para identificar cuantas solicitudes de suspensión de servicios hay en espera */
        if(solicitud.tipo_solicitud_usuario.tipo_solicitud=="SUSPENSIÓN DE SERVICIOS" && solicitud.status_solicitud=="Espera"){
          this.numSoliSusp++
        }
      });
    });
  }

  /**Metódo para realizar el conteo de las solicitudes de equipo en espera */
  mostrarNumSolicitudesEquipo()
  { /* Hace peticion para traer todas las solicitudes de equipo en espera */
    this.solicitudAsigEquipoService.todasLasSolicitudesPorStatus(1).subscribe(
      data=>
      { /* Recuperamos el total de solicitudes */
        this.numSoliEqui = data.length
      }
    )
  }
}
