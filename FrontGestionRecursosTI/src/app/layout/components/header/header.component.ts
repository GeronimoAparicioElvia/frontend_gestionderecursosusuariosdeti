import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService} from 'src/app/administracion/servicios';

@Component({
  selector: 'app-header',
  templateUrl:'./header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public pushRightClass: string='';
    usernameLocal: any= localStorage.getItem('currentUser');
    username: String = JSON.parse(this.usernameLocal).username;

    constructor( public router: Router,
                  public auhenticationService : AuthenticationService){
    }

    ngOnInit() {}

     /**Método para cerrar sesión y redirigir al login */
     logout() {
      this.auhenticationService.logout();
    }

}
