import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelosEquipoComponent } from './modelos-equipo.component';

describe('ModelosEquipoComponent', () => {
  let component: ModelosEquipoComponent;
  let fixture: ComponentFixture<ModelosEquipoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModelosEquipoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelosEquipoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
