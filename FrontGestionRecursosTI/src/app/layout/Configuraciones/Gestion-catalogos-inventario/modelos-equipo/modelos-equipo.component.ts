import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Modelo } from 'src/app/administracion/modelos/Inventario/Modelo/modelo';
import { Tipo } from 'src/app/administracion/modelos/Inventario/Tipo/tipo';
import { TipoService } from 'src/app/administracion/servicios/Inventario/Tipo/tipo.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-modelos-equipo',
  templateUrl: './modelos-equipo.component.html',
  styleUrls: ['./modelos-equipo.component.scss']
})
export class ModelosEquipoComponent implements OnInit {
  /**Tabla de los modelos de equipo */
  dataSource          = new MatTableDataSource<Modelo>([]);
  modelo="ComputerModel";
  categoria="COMPUTADORA";
  banderaFormulario=false;
  banderaEditar=false;
  idModeloEquipo=0;
   /* Paginators */
   @ViewChild(MatPaginator) paginator  !: MatPaginator

  encabezadostabla     : String [] = ["editar","categoria","modelo","comentarios","fecha_creacion","fecha_mod"]

  constructor(
    private formBuilder           : FormBuilder,
    private tipoService           : TipoService,
  ) {}

  /**Formulario con los campos del modelo de equipo */
  formModelo = this.formBuilder.group({
    categoria: [{value: '', disabled: true}],
    modeloEquipo: ['', [Validators.required]],
    comentarios: ['']
  });

  ngOnInit(): void {
    this.llenarTabla();
  }
  /**Captura los datos dependiendo del activo que seleccione */
  capturarDatos(index: number){
    this.limpiarFormulario();
    if(index ==0) {
      this.modelo="ComputerModel"
      this.categoria="COMPUTADORA";
    }
    if(index ==1) {
      this.modelo="NetworkequipmentModel"
      this.categoria="DISP. DE RED";
    }
    if(index ==2) {
      this.modelo="PrinterModel"
      this.categoria="IMPRESORA";
    }
    if(index ==3) {
      this.modelo="MonitorModel"
      this.categoria="MONITOR";
    }
    if(index ==4) {
      this.modelo="RackModel"
      this.categoria="RACK";
    }
    if(index ==5) {
      this.modelo="PeripheralModel"
      this.categoria="PERIFERICO";
    }
    if(index ==6) {
      this.modelo="PhoneModel"
      this.categoria="TELEFONO";
    }
    this.llenarTabla();
  }

  /**Metodo para llenar la tabla de datos */
  llenarTabla(){
    let listaModelosEquipos: Tipo[]=[];
    /** Vaciamos la tabla, para despues ingresar los datos*/
    this.dataSource.data = listaModelosEquipos;
    this.tipoService.allTiposItems(this.modelo).subscribe(modelosEquipos=>{
      this.dataSource.data=modelosEquipos;
      this.dataSource.paginator=this.paginator;
    }, err=>{
      Swal.fire({
        position: 'center',
        icon: 'error',
        title: 'Error de conexión',
        text: 'Lo sentimos los modelos de equipo no se cargaron correctamente debido a que no hay conexión con el servidor, por favor  inténtelo más tarde.',
      });
    });
  }
  /**Realiza el filtro de la tabla por lo que ingresa el usuario en la barra de búsqueda */
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  /**Limpia el formulario de los datos */
  limpiarFormulario(){
    this.banderaFormulario=false;
    this.banderaEditar=false;
    this.formModelo.controls['modeloEquipo'].setValue('');
    this.formModelo.controls['comentarios'].setValue('');
  }
  /**Toma los datos ingresados en el formulario y con ellos agrega un modelo de equipo */
  agregarModeloEquipo(){
    /**Valida que los servicios esten bien requisitados */
    if(this.formModelo.valid){
      let body = "{\"input\": {\"name\": \" "+this.formModelo.value.modeloEquipo
                +" \", \"comment\": \" "+this.formModelo.value.comentarios
                +"\"}}";
      //El token de sesion se genera en el service
      /**Se realiza el insert a la bd */
      this.tipoService.insertarTipo(this.modelo,body).subscribe(dato=>{
        this.llenarTabla();
        this.limpiarFormulario();
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Alta de modelos de equipo',
          text: ' El modelo de equipo se agrego exitosamente',
        })
      /**Se identifica cuando la consulta no se realizó exitosamente */
      },err => {
        console.log(err);
        Swal.fire({
          position: 'center',
          icon: 'error',
          text: 'El registro no realizó correctamente debido a que no hay conexión con el servidor, inténtelo más tarde o contacte al área de T.I. ',
          title: 'Alta de modelos de equipo',
        })
      } );

    } else{
      Swal.fire({
        icon: 'info',
        title: 'Alta de modelos de equipo',
        text: 'Es necesario que los campos esten debidamente requisitados',
        showConfirmButton: false,
        timer: 2000
      });
    }
  }
  /**Muestra el modelo de equipo seleccionado en le formulario, listo para poder ser editado */
  mostrarModelo(idModelo: number){
    this.tipoService.anTipo(this.modelo,idModelo).subscribe(modeloEquipo=>{
      this.formModelo.controls['modeloEquipo'].setValue(modeloEquipo.name);
      this.formModelo.controls['comentarios'].setValue(modeloEquipo.comment);
      /**Cambia las variable par poder mostrar los botones correspondientes */
      this.banderaFormulario=true;
      this.banderaEditar=true;
    });
    this.idModeloEquipo=idModelo;
  }
  /**Edita el modelo de equipo con los valores ingresados en el formulario */
  editarModelo(){
    /**Valida los campos para después obtenerlos */
    if(this.formModelo.valid){
      let body = "{\"input\": {\"name\": \" "+this.formModelo.value.modeloEquipo
                +" \", \"comment\": \" "+this.formModelo.value.comentarios
                +"\"}}";
      this.tipoService.updateTipo(this.modelo,this.idModeloEquipo ,body).subscribe(data=>{
        this.llenarTabla();
        this.limpiarFormulario();
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Actualización de modelos de equipo',
          text: ' El modelo de equipo se actualizó exitosamente',
        })
       /**Se identifica cuando la consulta no se realizó exitosamente */
      },err => {
        console.log(err);
        Swal.fire({
          position: 'center',
          icon: 'error',
          text: 'La actualización no  se realizó correctamente debido a que no hay conexión con el servidor, inténtelo más tarde o contacte al área de T.I. ',
          title: 'Actualización de modelos de equipo',
        })
      });

    } else{
      Swal.fire({
        icon: 'info',
        title: 'Actualización de modelos de equipo',
        text: 'Es necesario que los campos esten debidamente requisitados',
        showConfirmButton: false,
        timer: 2000
      });
    }
  }
}
