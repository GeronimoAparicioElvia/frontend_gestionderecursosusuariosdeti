import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Tipo } from 'src/app/administracion/modelos/Inventario/Tipo/tipo';
import { TipoService } from 'src/app/administracion/servicios/Inventario/Tipo/tipo.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-tipos-equipo',
  templateUrl: './tipos-equipo.component.html',
  styleUrls: ['./tipos-equipo.component.scss']
})
export class TiposEquipoComponent implements OnInit {
  /**Tabla de los tipos de equipo */
  dataSource          = new MatTableDataSource<Tipo>([]);
  tipo="ComputerType";
  categoria="COMPUTADORA";
  banderaFormulario=false;
  banderaEditar=false;
  idTipoEquipo=0;

   /* Paginators */
   @ViewChild(MatPaginator) paginator  !: MatPaginator

  encabezadostabla     : String [] = ["editar","categoria","tipoEquipo","comentarios","fecha_creacion","fecha_mod"]

  constructor(
    private formBuilder           : FormBuilder,
    private tipoService           : TipoService,
  ) { }
  /**Formulario con los campos del tipo de equipo */
  formTipo = this.formBuilder.group({
    categoria: [{value: '', disabled: true}],
    tipoEquipo: ['', [Validators.required]],
    comentarios: ['']
  });

  ngOnInit(): void {
    this.llenarTabla();
  }
  /**Captura los datos dependiendo del activo que seleccione */
  capturarDatos(index: number){
    this.limpiarFormulario();
    if(index ==0) {
      this.tipo="ComputerType";
      this.categoria="COMPUTADORA";
    }
    if(index ==1) {
      this.tipo="NetworkequipmentType";
      this.categoria="DISP. DE RED";
    }
    if(index ==2) {
      this.tipo="PrinterType";
      this.categoria="IMPRESORA";
    }
    if(index ==3) {
      this.tipo="MonitorType";
      this.categoria="MONITOR";
    }
    if(index ==4) {
      this.tipo="RackType";
      this.categoria="RACK";
    }
    if(index ==5) {
      this.tipo="PeripheralType";
      this.categoria="PERIFERICO";
    }
    if(index ==6) {
      this.tipo="PhoneType"
      this.categoria="TELEFONO";
    }
    this.llenarTabla();
  }

  /**Metodo para llenar la tabla de datos */
  llenarTabla(){
    let listaTiposEquipos: Tipo[]=[];
    /** Vaciamos la tabla, para después ingresar los datos*/
    this.dataSource.data = listaTiposEquipos;
    this.tipoService.allTiposItems(this.tipo).subscribe(tiposEquipos=>{
      this.dataSource.data=tiposEquipos;
      this.dataSource.paginator=this.paginator;
    }, err=>{
      Swal.fire({
        position: 'center',
        icon: 'error',
        title: 'Error de conexión',
        text: 'Lo sentimos los tipos de equipo no se cargaron correctamente debido a que no hay conexión con el servidor, por favor  inténtelo más tarde.',
      });
    });
  }
  /**Realiza el filtro de la tabla por lo que ingresa el usuario en la barra de búsqueda */
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  /**Limpia el formulario de los datos */
  limpiarFormulario(){
    this.banderaFormulario=false;
    this.banderaEditar=false;
    this.formTipo.controls['tipoEquipo'].setValue('');
    this.formTipo.controls['comentarios'].setValue('');
  }
  /**Toma los datos ingresados en el formulario y con ellos agrega un tipo de equipo */
  agregarTipoEquipo(){
    /**Valida que los servicios esten bien requisitados */
    if(this.formTipo.valid){
      let body = "{\"input\": {\"name\": \" "+this.formTipo.value.tipoEquipo
                +" \", \"comment\": \" "+this.formTipo.value.comentarios
                +"\"}}";
      //El token de sesion se genera en el service
      /**Se realiza el insert a la bd */
      this.tipoService.insertarTipo(this.tipo,body).subscribe(dato=>{
        this.llenarTabla();
        this.limpiarFormulario();
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Alta de tipos de equipo',
          text: ' El tipo de equipo se agrego exitosamente',
        })
      /**Se identifica cuando la consulta no se realizó exitosamente */
      },err => {
        console.log(err);
        Swal.fire({
          position: 'center',
          icon: 'error',
          text: 'El registro no realizó correctamente debido a que no hay conexión con el servidor, inténtelo más tarde o contacte al área de T.I. ',
          title: 'Alta de tipos de equipo',
        })
      } );

    } else{
      Swal.fire({
        icon: 'info',
        title: 'Alta de tipos de equipo',
        text: 'Es necesario que los campos esten debidamente requisitados',
        showConfirmButton: false,
        timer: 2000
      });
    }
  }
  /**Muestra el tipo de equipo seleccionado en le formulario, listo para poder ser editado */
  mostrarTipo(idTipo: number){
    this.tipoService.anTipo(this.tipo,idTipo).subscribe(tipoEquipo=>{
      this.formTipo.controls['tipoEquipo'].setValue(tipoEquipo.name);
      this.formTipo.controls['comentarios'].setValue(tipoEquipo.comment);
      /**Cambia las variable par poder mostrar los botones correspondientes */
      this.banderaFormulario=true;
      this.banderaEditar=true;
    });
    this.idTipoEquipo=idTipo;
  }
  /**Edita el tipo de equipo con los valores ingresados en el formulario */
  editarTipo(){
    /**Valida los campos para después obtenerlos */
    if(this.formTipo.valid){
      let body = "{\"input\": {\"name\": \" "+this.formTipo.value.tipoEquipo
                +" \", \"comment\": \" "+this.formTipo.value.comentarios
                +"\"}}";
      this.tipoService.updateTipo(this.tipo,this.idTipoEquipo ,body).subscribe(data=>{
        this.llenarTabla();
        this.limpiarFormulario();
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Actualización de tipos de equipo',
          text: ' El tipo de equipo se actualizó exitosamente',
        })
       /**Se identifica cuando la consulta no se realizó exitosamente */
      },err => {
        console.log(err);
        Swal.fire({
          position: 'center',
          icon: 'error',
          text: 'La actualización no  se realizó correctamente debido a que no hay conexión con el servidor, inténtelo más tarde o contacte al área de T.I. ',
          title: 'Actualización de tipos de equipo',
        })
      });

    } else{
      Swal.fire({
        icon: 'info',
        title: 'Actualización de tipo de equipos',
        text: 'Es necesario que los campos esten debidamente requisitados',
        showConfirmButton: false,
        timer: 2000
      });
    }
  }

}
