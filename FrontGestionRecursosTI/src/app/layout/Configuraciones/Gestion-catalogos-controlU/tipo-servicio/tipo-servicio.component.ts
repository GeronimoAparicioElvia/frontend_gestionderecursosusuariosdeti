import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { TipoServicio } from 'src/app/administracion/modelos/Control_Usuario/tipo-servicio';
import { TipoServicioService } from 'src/app/administracion/servicios/Control_Usuario/tipo-servicio.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-tipo-servicio',
  templateUrl: './tipo-servicio.component.html',
  styleUrls: ['./tipo-servicio.component.scss']
})
export class TipoServicioComponent implements OnInit {
  /**Tabla de los tipos de servicios */
  dataSource          = new MatTableDataSource<TipoServicio>([]);
  banderaFormulario=false;
  banderaEditar=false;
  idServicio=0;

   /* Paginators */
   @ViewChild(MatPaginator) paginator  !: MatPaginator

  encabezadostabla     : String [] = ["editar","nombre_servicio","privilegio","status_servicio","descripcion_serv","fechaCreacionServ","fecha_mod_serv"];
  listaStatus          : String [] = ["ACTIVO", "INACTIVO"];
  listaPrivilegio      : String [] = ["SI", "NO"];

  constructor(
    private formBuilder           : FormBuilder,
    private tipoServicioService   : TipoServicioService,
  ) { }
  /**Formulario con los campos del tipo de servicio */
  formServicio = this.formBuilder.group({
    nomServicio: ['', [Validators.required]],
    privilegio:  ['', [Validators.required]],
    status: ['', [Validators.required]],
    descripcion: ['']
  });

  ngOnInit(): void {
    this.llenarTabla();
  }
  /**Metodo para llenar la tabla de datos */
  llenarTabla(){
    let listaServicios: TipoServicio[]=[];
    /** Vaciamos la tabla, para despues ingresar los datos*/
    this.dataSource.data = listaServicios;
    this.tipoServicioService.todosLosServicios().subscribe(servicios=>{
        servicios.forEach(servicio=>{
          /**Se traducen los valores a si y no */
          if(servicio.tiene_privilegio==true){
            servicio.privilegio="SI";
          }else{
            servicio.privilegio="NO";          }
        });
      this.dataSource.data=servicios;
      this.dataSource.paginator=this.paginator;
    },err=>{
      Swal.fire({
        position: 'center',
        icon: 'error',
        title: 'Error de conexión',
        text: 'Lo sentimos los tipos de servicios no se cargaron correctamente debido a que no hay conexión con el servidor, por favor  inténtelo más tarde',
      });
    });
  }
  /**Realiza el filtro de la tabla por lo que ingresa el usuario en la barra de búsqueda */
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  /**Limpia el formulario de los datos */
  limpiarFormulario(){
    this.banderaFormulario=false;
    this.banderaEditar=false;
    this.formServicio.controls['nomServicio'].setValue('');
    this.formServicio.controls['privilegio'].setValue('');
    this.formServicio.controls['status'].setValue('');
    this.formServicio.controls['descripcion'].setValue('');
  }
  /**Toma los datos ingresados en el formulario y con ellos agrega un tipo de servicio */
  agregarServicio(){
    /**Valida que los servicios esten bien requisitados */
    if(this.formServicio.valid){
      let servicio : TipoServicio = new TipoServicio();
      servicio.nombre_servicio= this.formServicio.value.nomServicio;
      servicio.descripcion_serv=this.formServicio.value.descripcion;
      servicio.status_servicio=this.formServicio.value.status;
      /**Convierte los valores del privilegio a true o false */
      if(this.formServicio.value.privilegio=='SI'){
        servicio.tiene_privilegio=true;
      }else{
        servicio.tiene_privilegio=false;
      }
      /**Se realiza el insert a la bd */
      this.tipoServicioService.agregarServicio(servicio).subscribe(dato=>{
        this.llenarTabla();
        this.limpiarFormulario();
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Alta de servicios',
          text: ' El servicio se agrego exitosamente',

        })
        /**En caso de error manda un mensaje */
      },err => {
        console.log(err);
        Swal.fire({
          position: 'center',
          icon: 'error',
          text: 'El registro no realizó correctamente debido a que no hay conexión con el servidor, inténtelo más tarde o contacte al área de T.I. ',
          title: 'Alta de servicios',
        })
      } );

    } else{
      Swal.fire({
        icon: 'info',
        title: 'Alta de servicios',
        text: 'Es necesario que los campos esten debidamente requisitados',
        showConfirmButton: false,
        timer: 2000
      });
    }
  }
  /**Muestra el servicio seleccionado en el formulario, listo para poder ser editado */
  mostrarServicio(idTipoServicio: number){
    this.tipoServicioService.unTipoServicio(idTipoServicio).subscribe(servicio=>{
      this.formServicio.controls['nomServicio'].setValue(servicio.nombre_servicio);
      this.formServicio.controls['status'].setValue(servicio.status_servicio);
      this.formServicio.controls['descripcion'].setValue(servicio.descripcion_serv);
      /**Convierte los valores del privilegio a SI o NO */
      if(servicio.tiene_privilegio==true){
        this.formServicio.controls['privilegio'].setValue("SI");
      }else{
        this.formServicio.controls['privilegio'].setValue("NO");
      }
      /**Cambia las variable par poder mostrar los botones correspindientes */
      this.banderaFormulario=true;
      this.banderaEditar=true;
    });
    this.idServicio=idTipoServicio;
  }

  /**Edita el tipo de servicio con los valores ingresados en el formulario */
  editarServicio(){
    /**Valida los campos para después obtenerlos */
    if(this.formServicio.valid){
      let tipoServicio : TipoServicio = new TipoServicio();
      tipoServicio.id_tipo_servicio=this.idServicio;
      tipoServicio.nombre_servicio= this.formServicio.value.nomServicio;
      tipoServicio.status_servicio=this.formServicio.value.status;
      tipoServicio.descripcion_serv=this.formServicio.value.descripcion;
      tipoServicio.tiene_privilegio=this.formServicio.value.privilegio;
      if(this.formServicio.value.privilegio=='SI'){
        tipoServicio.tiene_privilegio=true;
      }else{
        tipoServicio.tiene_privilegio=false;
      }
      /**Se realiza el update en la bd */
      this.tipoServicioService.actualizarServicio(tipoServicio).subscribe(data=>{
        this.llenarTabla();
        this.limpiarFormulario();
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Actualización de servicios de T.I.',
          text: ' El servicio se actualizó exitosamente',
        })
      },err => {
        console.log(err);
        Swal.fire({
          position: 'center',
          icon: 'error',
          text: 'La actualización no  se realizó correctamente debido a que no hay conexión con el servidor, inténtelo más tarde o contacte al área de T.I. ',
          title: 'Actualización de servicios de T.I.',
        })
      });

    } else{
      Swal.fire({
        icon: 'info',
        title: 'Actualización de servicios de T.I.',
        text: 'Es necesario que los campos esten debidamente requisitados',
        showConfirmButton: false,
        timer: 2000
      });
    }

  }
}
