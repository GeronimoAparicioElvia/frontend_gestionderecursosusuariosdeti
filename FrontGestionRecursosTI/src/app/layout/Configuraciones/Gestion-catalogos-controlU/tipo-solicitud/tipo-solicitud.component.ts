import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { TipoSolicitud } from 'src/app/administracion/modelos/Control_Usuario/tipo-solicitud';
import { TipoSolicitudService } from 'src/app/administracion/servicios/Control_Usuario/tipo-solicitud.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-tipo-solicitud',
  templateUrl: './tipo-solicitud.component.html',
  styleUrls: ['./tipo-solicitud.component.scss']
})
export class TipoSolicitudComponent implements OnInit {
  /**Tabla de los tipos de solicitud */
  dataSource          = new MatTableDataSource<TipoSolicitud>([]);
  banderaFormulario=false;
  banderaEditar=false;
  idTipoSolicitud=0;
  /**Toma el token de sesión para acceder a GLPI */
  token:string=JSON.parse(localStorage.getItem('glpiToken') || '{}').token

   /* Paginators */
   @ViewChild(MatPaginator) paginator  !: MatPaginator

  encabezadostabla     : String [] = ["editar","tipo_solicitud","status_solicitud","descripcion","fecha_creacion","fecha_mod"];
  listaTiposSoli       : String [] = ["ALTA DE SERVICIOS", "ACTIVACIÓN DE PRIVILEGIOS","SUSPENSIÓN DE SERVICIOS"];
  listaStatus          : String [] = ["ACTIVO", "INACTIVO"]


  constructor(
    private formBuilder           : FormBuilder,
    private tipoSolicitudService  : TipoSolicitudService,
  ) { }
  /**Formulario con los campos del tipo de solicitud */
  formTipoSolicitud = this.formBuilder.group({
    tipoSolicitud: ['', [Validators.required]],
    status: ['', [Validators.required]],
    descripcion: ['']
  });

  ngOnInit(): void {
    this.llenarTabla();
  }

  /**Metodo para llenar la tabla de datos */
  llenarTabla(){
    let listaTiposSolicitud: TipoSolicitud[]=[];
    /** Vaciamos la tabla, para despues ingresar los datos*/
    this.dataSource.data = listaTiposSolicitud;
    this.tipoSolicitudService.todosLosTiposSolicitud().subscribe(tiposSolicitud=>{
      this.dataSource.data=tiposSolicitud;
      this.dataSource.paginator=this.paginator;
    }, err=>{
      Swal.fire({
        position: 'center',
        icon: 'error',
        title: 'Error de conexión',
        text: 'Lo sentimos los tipos de solicitudes de servicios no se cargaron correctamente debido a que no hay conexión con el servidor, por favor  inténtelo más tarde.',
      });
    });
  }
  /**Realiza el filtro de la tabla por lo que ingresa el usuario en la barra de búsqueda */
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  /**Limpia el formulario de los datos */
  limpiarFormulario(){
    this.banderaFormulario=false;
    this.banderaEditar=false;
    this.formTipoSolicitud.controls['tipoSolicitud'].setValue('');
    this.formTipoSolicitud.controls['status'].setValue('');
    this.formTipoSolicitud.controls['descripcion'].setValue('');
  }
  /**Toma los datos ingresados en el formulario y con ellos agrega un tipo de solicitud */
  agregarTipoSolicitud(){
    /**Valida que los servicios esten bien requisitados */
    if(this.formTipoSolicitud.valid){
      let tipoSolicitud : TipoSolicitud = new TipoSolicitud();
      tipoSolicitud.tipo_solicitud= this.formTipoSolicitud.value.tipoSolicitud;
      tipoSolicitud.status_solicitud=this.formTipoSolicitud.value.status;
      tipoSolicitud.descripcion=this.formTipoSolicitud.value.descripcion;
      /**Se realiza el insert a la bd */
      this.tipoSolicitudService.agregarTipoSolicitud(tipoSolicitud).subscribe(dato=>{
        this.llenarTabla();
        this.limpiarFormulario();
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Alta de tipos de solicitud',
          text: ' El tipo de solicitud se agrego exitosamente',
        })
        /**Se identifica cuando la consulta no se realizó exitosamente */
      },err => {
        console.log(err);
        Swal.fire({
          position: 'center',
          icon: 'error',
          text: 'El registro no realizó correctamente debido a que no hay conexión con el servidor, inténtelo más tarde o contacte al área de T.I. ',
          title: 'Alta de tipos de solicitud',
        })
      } );

    } else{
      Swal.fire({
        icon: 'info',
        title: 'Alta de tipos de solicitud',
        text: 'Es necesario que los campos esten debidamente requisitados',
        showConfirmButton: false,
        timer: 2000
      });
    }
  }
  /**Muestra el tipo de solicitud seleccionado en le formulario, listo para poder ser editado */
  mostrarTipoSolicitud(idTipoSoli: number){
    this.tipoSolicitudService.unTipoSolicitud(idTipoSoli).subscribe(tipoSoli=>{
      this.formTipoSolicitud.controls['tipoSolicitud'].setValue(tipoSoli.tipo_solicitud);
      this.formTipoSolicitud.controls['status'].setValue(tipoSoli.status_solicitud);
      this.formTipoSolicitud.controls['descripcion'].setValue(tipoSoli.descripcion);
      /**Cambia las variable par poder mostrar los botones correspondientes */
      this.banderaFormulario=true;
      this.banderaEditar=true;
    });
    this.idTipoSolicitud=idTipoSoli;
  }
  /**Edita el tipo de solicitud con los valores ingresados en el formulario */
  editarTipoSolicitud(){
    /**Valida los campos para después obtenerlos */
    if(this.formTipoSolicitud.valid){
      let tipoSolicitud : TipoSolicitud = new TipoSolicitud();
      tipoSolicitud.id_tipo_solicitud_usuario=this.idTipoSolicitud;
      tipoSolicitud.tipo_solicitud= this.formTipoSolicitud.value.tipoSolicitud;
      tipoSolicitud.status_solicitud=this.formTipoSolicitud.value.status;
      tipoSolicitud.descripcion=this.formTipoSolicitud.value.descripcion;
      /**Se realiza el update en la bd */
      this.tipoSolicitudService.actualizarTipoSolicitud(tipoSolicitud).subscribe(data=>{
        this.llenarTabla();
        this.limpiarFormulario();
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Actualización de tipos de solicitud',
          text: ' El tipo de solicitud se actualizó exitosamente',
        })
      /**Se identifica cuando la consulta no se realizó exitosamente */
      },err => {
        console.log(err);
        Swal.fire({
          position: 'center',
          icon: 'error',
          text: 'La actualización no  se realizó correctamente debido a que no hay conexión con el servidor, inténtelo más tarde o contacte al área de T.I. ',
          title: 'Actualización de tipos de solicitud',
        })
      });

    } else{
      Swal.fire({
        icon: 'info',
        title: 'Actualización de tipos de solicitud',
        text: 'Es necesario que los campos esten debidamente requisitados',
        showConfirmButton: false,
        timer: 2000
      });
    }

  }

}
