import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { TipoMan } from 'src/app/administracion/modelos/Mantenimiento/tipo-man';
import { TipoManService } from 'src/app/administracion/servicios/Mantenimiento/tipo-man.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-tipo-mantenimiento',
  templateUrl: './tipo-mantenimiento.component.html',
  styleUrls: ['./tipo-mantenimiento.component.scss']
})
export class TipoMantenimientoComponent implements OnInit {
  /**Tabla de los tipos de mantenimiento */
  dataSource          = new MatTableDataSource<TipoMan>([]);
  banderaFormulario=false;
  banderaEditar=false;
  idTipoMan=0;

   /* Paginators */
   @ViewChild(MatPaginator) paginator  !: MatPaginator

  encabezadostabla     : String [] = ["editar","tipo_mant","status_mant","descripcion","fecha_creacion"];
  listaStatus          : String [] = ["ACTIVO", "INACTIVO"]

  constructor(
    private formBuilder           : FormBuilder,
    private tipoManService        : TipoManService,
  ) { }
  /**Formulario con los campos del tipo de mantenimiento */
  formTipoMan = this.formBuilder.group({
    nomMant: ['', [Validators.required]],
    status: ['', [Validators.required]],
    descripcion: ['']
  });

  ngOnInit(): void {
    this.llenarTabla();
  }

  /**Metodo para llenar la tabla de datos */
  llenarTabla(){
    let listaTiposMan: TipoMan[]=[];
    /** Vaciamos la tabla, para después ingresar los datos*/
    this.dataSource.data = listaTiposMan;
    this.tipoManService.allTiposMantenimiento().subscribe(tiposMan=>{
      this.dataSource.data=tiposMan;
      this.dataSource.paginator=this.paginator;
    }, err=>{
      Swal.fire({
        position: 'center',
        icon: 'error',
        title: 'Error de conexión',
        text: 'Lo sentimos los tipos de mantenimiento no se cargaron correctamente debido a que no hay conexión con el servidor, por favor  inténtelo más tarde.',
      });
    });
  }
  /**Realiza el filtro de la tabla por lo que ingresa el usuario en la barra de búsqueda */
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  /**Limpia el formulario de los datos */
  limpiarFormulario(){
    this.banderaFormulario=false;
    this.banderaEditar=false;
    this.formTipoMan.controls['nomMant'].setValue('');
    this.formTipoMan.controls['status'].setValue('');
    this.formTipoMan.controls['descripcion'].setValue('');
  }

  /**Toma los datos ingresados en el formulario y con ellos agrega un tipo de mantenimiento */
  agregarTipoMant(){
    /**Valida que los servicios esten bien requisitados */
    if(this.formTipoMan.valid){
      let tipoMan : TipoMan = new TipoMan();
      tipoMan.nombre = this.formTipoMan.value.nomMant;
      tipoMan.status_mant=this.formTipoMan.value.status;
      tipoMan.descripcion=this.formTipoMan.value.descripcion;
      /**Se realiza el insert a la bd */
      this.tipoManService.saveTipoMantenimiento(tipoMan).subscribe(dato=>{
        this.llenarTabla();
        this.limpiarFormulario();
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Alta de tipos de mantenimiento',
          text: ' El tipo de mantenimiento se agrego exitosamente',
        })
      /**Se identifica cuando la consulta no se realizó exitosamente */
      },err => {
        console.log(err);
        Swal.fire({
          position: 'center',
          icon: 'error',
          text: 'El registro no realizó correctamente debido a que no hay conexión con el servidor, inténtelo más tarde o contacte al área de T.I. ',
          title: 'Alta de tipos de mantenimiento',
        })
      } );

    } else{
      Swal.fire({
        icon: 'info',
        title: 'Alta de tipos de mantenimiento',
        text: 'Es necesario que los campos esten debidamente requisitados',
        showConfirmButton: false,
        timer: 2000
      });
    }
  }

  /**Muestra el tipo de mantenimiento en el formulario, listo para poder ser editado */
  mostrarTipoMant(idTipoMan: number){
    this.tipoManService.anTipoMantenimiento(idTipoMan).subscribe(tipoMan=>{
      this.formTipoMan.controls['nomMant'].setValue(tipoMan.nombre);
      this.formTipoMan.controls['status'].setValue(tipoMan.status_mant);
      this.formTipoMan.controls['descripcion'].setValue(tipoMan.descripcion);
      /**Cambia las variable par poder mostrar los botones correspondientes */
      this.banderaFormulario=true;
      this.banderaEditar=true;
    });
    this.idTipoMan=idTipoMan;
  }

  /**Edita el tipo de mantenimiento con los valores ingresados en el formulario */
  editarTipoMant(){
    /**Valida los campos para después obtenerlos */
    if(this.formTipoMan.valid){
      let tipoMan : TipoMan = new TipoMan();
      tipoMan.id_tipo_mant=this.idTipoMan;
      tipoMan.nombre= this.formTipoMan.value.nomMant;
      tipoMan.status_mant=this.formTipoMan.value.status;
      tipoMan.descripcion=this.formTipoMan.value.descripcion;

      this.tipoManService.updateTipoMantenimiento(tipoMan).subscribe(data=>{
        this.llenarTabla();
        this.limpiarFormulario();
        Swal.fire({

          position: 'center',
          icon: 'success',
          title: 'Actualización de tipos de mantenimiento',
          text: ' El tipo de mantenimiento se actualizó exitosamente',

        })
       /**Se identifica cuando la consulta no se realizó exitosamente */
      },err => {
        console.log(err);
        Swal.fire({
          position: 'center',
          icon: 'error',
          text: 'La actualización no  se realizó correctamente debido a que no hay conexión con el servidor, inténtelo más tarde o contacte al área de T.I. ',
          title: 'Actualización de tipos de mantenimiento',
        })
      });

    } else{
      Swal.fire({
        icon: 'info',
        title: 'Actualización de tipos de mantenimiento',
        text: 'Es necesario que los campos esten debidamente requisitados',
        showConfirmButton: false,
        timer: 2000
      });
    }

  }
}
