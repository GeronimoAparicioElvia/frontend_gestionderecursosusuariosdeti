import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActividadMantenimientoComponent } from './actividad-mantenimiento.component';

describe('ActividadMantenimientoComponent', () => {
  let component: ActividadMantenimientoComponent;
  let fixture: ComponentFixture<ActividadMantenimientoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActividadMantenimientoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActividadMantenimientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
