import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActividadMan } from 'src/app/administracion/modelos/Mantenimiento/actividad-man';
import { TipoMan } from 'src/app/administracion/modelos/Mantenimiento/tipo-man';
import { ActividadManService } from 'src/app/administracion/servicios/Mantenimiento/actividad-man.service';
import { TipoManService } from 'src/app/administracion/servicios/Mantenimiento/tipo-man.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-actividad-mantenimiento',
  templateUrl: './actividad-mantenimiento.component.html',
  styleUrls: ['./actividad-mantenimiento.component.scss']
})
export class ActividadMantenimientoComponent implements OnInit {
    /**Tabla de los actividades de mantenimiento */
  dataSource          = new MatTableDataSource<ActividadMan>([]);
  banderaFormulario=false;
  banderaEditar=false;
  idActividadMan=0;

   /* Paginators */
   @ViewChild(MatPaginator) paginator  !: MatPaginator

  encabezadostabla     : String [] = ["editar","nombre_act","status_actividad","status_actividad","descripcion","fecha_creacion"];
  listaStatus          : String [] = ["ACTIVO", "INACTIVO"];
  listaTipoMant        : TipoMan [] = [];

  constructor(
    private formBuilder           : FormBuilder,
    private tipoManService        : TipoManService,
    private actividadManService   : ActividadManService,
  ) {
    /**Se llena la lista de tipos de mantenimiento de solo los que se encuentran activos */
    this.tipoManService.allTiposMantenimiento().subscribe(tiposMan=>{
      tiposMan.forEach(tipoMan=>{
        if(tipoMan.status_mant=="ACTIVO"){
          this.listaTipoMant.push(tipoMan);
        }
      });
    });
  }
  /**Formulario con los campos de la actividad de mantenimiento */
  formActividad = this.formBuilder.group({
    actividad: ['', [Validators.required]],
    tipoMant:  ['', [Validators.required]],
    status: ['', [Validators.required]],
    descripcion: ['']
  });

  ngOnInit(): void {
    this.llenarTabla();
  }

  /**Metodo para llenar la tabla de datos */
  llenarTabla(){
    let listaActMan: ActividadMan[]=[];
    /** Vaciamos la tabla, para después ingresar los datos*/
    this.dataSource.data = listaActMan;
    this.actividadManService.allActividadesDeMantenimiento().subscribe(actividades=>{
      actividades.forEach(actividad=>{
        this.tipoManService.anTipoMantenimiento(Number(actividad.tipo_mant_id)).subscribe(tipoM=>{
          actividad.tipo_mant=tipoM;
        });
      });
      this.dataSource.data=actividades;
      this.dataSource.paginator=this.paginator;
    }, err=>{
      Swal.fire({
        position: 'center',
        icon: 'error',
        title: 'Error de conexión',
        text: 'Lo sentimos las actividades de mantenimiento no se cargaron correctamente debido a que no hay conexión con el servidor, por favor  inténtelo más tarde.',
      });
    });
  }

  /**Realiza el filtro de la tabla por lo que ingresa el usuario en la barra de búsqueda */
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  /**Limpia el formulario de los datos */
  limpiarFormulario(){
    this.banderaFormulario=false;
    this.banderaEditar=false;
    this.formActividad.controls['actividad'].setValue('');
    this.formActividad.controls['tipoMant'].setValue('');
    this.formActividad.controls['status'].setValue('');
    this.formActividad.controls['descripcion'].setValue('');
  }
  /**Toma los datos ingresados en el formulario y con ellos agrega la actividad de mantenimiento */
  agregarActividadMant(){
    /**Valida que los servicios esten bien requisitados */
    if(this.formActividad.valid){
      let actividad : ActividadMan = new ActividadMan();
      actividad.nombre_act= this.formActividad.value.actividad;
      actividad.descripcion=this.formActividad.value.descripcion;
      actividad.status_actividad=this.formActividad.value.status;
      /**Se realiza el insert a la bd */
      this.actividadManService.saveActividadMantenimiento(actividad, Number(this.formActividad.value.tipoMant)).subscribe(dato=>{
        this.llenarTabla();
        this.limpiarFormulario();
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Alta de actividades de mantenimiento',
          text: ' La actividad se agrego exitosamente',
        })
      /**Se identifica cuando la consulta no se realizó exitosamente y se emite un mensaje*/
      },err => {
        console.log(err);
        Swal.fire({
          position: 'center',
          icon: 'error',
          text: 'El registro no realizó correctamente debido a que no hay conexión con el servidor, inténtelo más tarde o contacte al área de T.I. ',
          title: 'Alta de actividades de mantenimiento',
        })
      } );

    } else{
      Swal.fire({
        icon: 'info',
        title: 'Alta de actividades de mantenimiento',
        text: 'Es necesario que los campos esten debidamente requisitados',
        showConfirmButton: false,
        timer: 2000
      });
    }
  }
  /**Muestra la actividad de mantenimiento en el formulario, listo para poder ser editado */
  mostrarActividadMant(idActividadMan: number){
    this.actividadManService.anTipoMantenimiento(idActividadMan).subscribe(actividad=>{
      this.formActividad.controls['actividad'].setValue(actividad.nombre_act);
      this.formActividad.controls['tipoMant'].setValue(actividad.tipo_mant_id);
      this.formActividad.controls['status'].setValue(actividad.status_actividad);
      this.formActividad.controls['descripcion'].setValue(actividad.descripcion);
      /**Cambia las variable par poder mostrar los botones correspondientes */
      this.banderaFormulario=true;
      this.banderaEditar=true;
    });
    this.idActividadMan=idActividadMan
  }
  /**Edita el actividad de mantenimiento con los valores ingresados en el formulario */
  editarActividadMant(){
    /**Valida los campos para después obtenerlos */
    if(this.formActividad.valid){
      let actividadMan : ActividadMan = new ActividadMan();
      actividadMan.id_actividad_mant=this.idActividadMan;
      actividadMan.nombre_act= this.formActividad.value.actividad;
      actividadMan.status_actividad=this.formActividad.value.status;
      actividadMan.descripcion=this.formActividad.value.descripcion;
      this.actividadManService.updateActividadMantenimiento(actividadMan, Number(this.formActividad.value.tipoMant)).subscribe(data=>{
        this.llenarTabla();
        this.limpiarFormulario();
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Actualización de actividades de mantenimiento ',
          text: ' La actividad se actualizó exitosamente',
        })
      /**Se identifica cuando la consulta no se realizó exitosamente y se emite un mensaje*/
      },err => {
        console.log(err);
        Swal.fire({
          position: 'center',
          icon: 'error',
          text: 'La actualización no  se realizó correctamente debido a que no hay conexión con el servidor, inténtelo más tarde o contacte al área de T.I. ',
          title: 'Actualización de actividades de mantenimiento ',
        })
      });

    } else{
      Swal.fire({
        icon: 'info',
        title: 'Actualización de actividades de mantenimiento ',
        text: 'Es necesario que los campos esten debidamente requisitados',
        showConfirmButton: false,
        timer: 2000
      });
    }

  }
}
