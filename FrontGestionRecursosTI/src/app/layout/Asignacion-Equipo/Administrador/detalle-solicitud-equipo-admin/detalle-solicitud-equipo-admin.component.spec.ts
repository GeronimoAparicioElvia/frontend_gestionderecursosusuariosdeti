import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleSolicitudEquipoAdminComponent } from './detalle-solicitud-equipo-admin.component';

describe('DetalleSolicitudEquipoAdminComponent', () => {
  let component: DetalleSolicitudEquipoAdminComponent;
  let fixture: ComponentFixture<DetalleSolicitudEquipoAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetalleSolicitudEquipoAdminComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleSolicitudEquipoAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
