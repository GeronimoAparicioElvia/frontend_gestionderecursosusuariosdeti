import { Component, OnInit } from '@angular/core';
import { FormBuilder} from '@angular/forms';
import {ActivatedRoute} from "@angular/router"
import {UsuarioService } from 'src/app/administracion/servicios';
import { SolicitudAsigEquipoService } from 'src/app/administracion/servicios/Asignacion_Equipo/solicitud-asig-equipo.service';
import { SolicitudAsigEquipo } from 'src/app/administracion/modelos/Asignacion_Equipo/solicitud-asig-equipo';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-detalle-solicitud-equipo-admin',
  templateUrl: './detalle-solicitud-equipo-admin.component.html',
  styleUrls: ['./detalle-solicitud-equipo-admin.component.scss']
})
export class DetalleSolicitudEquipoAdminComponent implements OnInit {

  /* Parámetros a capturar de la URL */
  id: number      = 0
  idcolab: number = 0
  status!:string        /* Bandera para validar */

  /*  Constructor */
  constructor( private usuarioService             : UsuarioService              , /*  Conexión con el endpoint uempleados                        */
               private formBuilder                : FormBuilder                 ,
               private solicitudAsigEquipoService : SolicitudAsigEquipoService  , /*  Conexión con la tabla solicitud equipo de la base de datos */
               private router                     : Router                      ,
               private activeRouter               : ActivatedRoute
             ) {}
  
  /*Form Control group */
  formsolicitudAsigEq = this.formBuilder.group
  ({
    colaborador               : [{value: '', disabled: true},[]], /* Nombre del colaborador                     */
    colacargo                 : [{value: '', disabled: true},[]], /* Cargo del colaborador                      */
    colasucursal              : [{value: '', disabled: true},[]], /* Sucursal del colaborador                   */
    solicitante               : [{value: '', disabled: true},[]], /* Nombre del solicitante                     */
    solicargo                 : [{value: '', disabled: true},[]], /* Cargo del solicitante                      */
    solisucursal              : [{value: '', disabled: true},[]], /* Sucursal del solicitante                   */
    fecha_de_solicitud        : [{value: '', disabled: true},[]], /* Fecha en que se realizó la solicitud       */
    fecha_planeada_asignacion : [{value: '', disabled: true},[]], /* Fecha límite para realizar la asignación   */
    fecha_ultima_modificacion : [{value: '', disabled: true},[]], /* Fecha de la ultima modificación            */
    equipo_solicitado         : [{value: '', disabled: true},[]], /* Tipo de activo solicitado                  */
    descripcion               : [{value: '', disabled: true},[]], /* Uso o descripción del equipo solicitado    */
    observaciones             : [{value: '', disabled: true},[]]  /* Observaciones o comentarios                */
  });

  ngOnInit() 
  { 
    this.cargarDatosSolicitud();
  }

  /* Llena la data de la vista con los datos de la solicitud */
  cargarDatosSolicitud()
  {
    /* Captura los datos enviados a travez de la Url*/
    this.activeRouter.params.subscribe(params =>{
      this.id = params['id_solicitud']                /*  Id de la solicitud      */
      this.status = params['status_solicitud']        /*  Status de la solicitud  */
      this.solicitudAsigEquipoService.solicitudPorIdSolicitud(this.id).subscribe(val=>
      {
        this.idcolab = Number(val.colaborador_id)
        /*Creamos un modelo de solicitud auxiliar */ 
        let solicitud:SolicitudAsigEquipo = new SolicitudAsigEquipo();
        
        /* Cargamos los datos del empleado mencionado en la solicitud */
        this.usuarioService.getAnEmpleado(Number(val.colaborador_id)).subscribe
        (
          val=> 
          { 
            /*Cargamos el nombre del colaborador */
            this.formsolicitudAsigEq.controls['colaborador'].setValue ( val.nombre + " " + val.apellidoPat + " " + val.apellidoMat)
            /*validamos si el cargo no se encuentra vacío o nulo*/
            if(val.puesto!=null)
            { this.formsolicitudAsigEq.controls['colacargo'].setValue   ( val.puesto.nombrePuesto)  }
            else /*De ser nulo agregamos una leyenda por defecto a este campo */
            { this.formsolicitudAsigEq.controls['colacargo'].setValue   ( "Sin información")  }
            /*validamos si la oficina no se encuentra vacío o nulo*/
            if(val.sucursal!=null)
            { this.formsolicitudAsigEq.controls['colasucursal'].setValue   ( val.sucursal.nombreSucursal)  }
            else /*De ser nulo agregamos una leyenda por defecto a este campo */
            { this.formsolicitudAsigEq.controls['colasucursal'].setValue   ( "Sin información")  }
          },
          err=>
          {
            Swal.fire({
              icon:'error',
              title:'Error al cargar los datos del empleado, intentelo más tarde',
              timer:2000,
              allowEnterKey:false,
              allowEscapeKey:false,
              allowOutsideClick:false,
              showConfirmButton:false
            })
          }
        )
        
        /* Cargamos los datos del solicitante */
        this.usuarioService.getAnEmpleado(Number(val.solicitante_id)).subscribe
        (
          val=> 
          { 
            /*Cargamos el nombre del colaborador */
            this.formsolicitudAsigEq.controls['solicitante'].setValue ( val.nombre + " " + val.apellidoPat + " " + val.apellidoMat)
            /*validamos si el cargo no se encuentra vacío o nulo*/
            if(val.puesto!=null)
            { this.formsolicitudAsigEq.controls['solicargo'].setValue   ( val.puesto.nombrePuesto)  }
            else /*De ser nulo agregamos una leyenda por defecto a este campo */
            { this.formsolicitudAsigEq.controls['solicargo'].setValue   ( "Sin información")  }
            /*validamos si la oficina no se encuentra vacío o nulo*/
            if(val.sucursal!=null)
            { this.formsolicitudAsigEq.controls['solisucursal'].setValue   ( val.sucursal.nombreSucursal)  }
            else /*De ser nulo agregamos una leyenda por defecto a este campo */
            { this.formsolicitudAsigEq.controls['solisucursal'].setValue   ( "Sin información")  }
          },
          err=>
          {
            Swal.fire({
              icon:'error',
              title:'Error al cargar los datos del Solicitante, intentelo más tarde',
              timer:2000,
              allowEnterKey:false,
              allowEscapeKey:false,
              allowOutsideClick:false,
              showConfirmButton:false
            })
          }
        )
        
        /* Cargamos los datos de la solicitud */
        /* Damos formato a la fecha límite de asignación */
        this.formsolicitudAsigEq.controls['fecha_planeada_asignacion'].setValue
        (
          (val.fecha_planeada_asig+"").substring(8,10)  + /*  Día */
          "-"                                           +
          (val.fecha_planeada_asig+"").substring(5,7)   + /*  Mes */
          "-"                                           +
          (val.fecha_planeada_asig+"").substring(0,4)     /*  Año */
        )
        this.formsolicitudAsigEq.controls['equipo_solicitado'].setValue(val.tipo_equipo)  /* Tipo de equipo solicitado */
        this.formsolicitudAsigEq.controls['descripcion'].setValue(val.descrip_equipo)     /* Descripción del uso o del equipo solicitado */
        this.formsolicitudAsigEq.controls['observaciones'].setValue(val.observaciones)    /* Comentarios y observaciones generales */
        /* Damos formato a la fecha en que se creó la solicitud de asignación */
        this.formsolicitudAsigEq.controls['fecha_de_solicitud'].setValue
        ( 
          (val.fecha_solicitud)?.substring(8,10)  + /*  Día */ 
          "-"                                     + 
          (val.fecha_solicitud)?.substring(5,7)   + /*  Mes */  
          "-"                                     + 
          (val.fecha_solicitud)?.substring(0,4)     /*  Año */ 
        )

        /* Damos formato a la última fecha en que se modificó la solicitud de asignación */
        this.formsolicitudAsigEq.controls['fecha_ultima_modificacion'].setValue
        (
          (val.fecha_mod+"").substring(8,10)  + /*  Día */ 
          "-"                                 + 
          (val.fecha_mod+"").substring(5,7)   + /*  Mes */ 
          "-"                                 + 
          (val.fecha_mod+"").substring(0,4))    /*  Año */ 
      },
      err=>
          {
            Swal.fire({
              icon:'error',
              title:'Sin conexión con el servidor, intentelo más tarde',
              timer:2000,
              allowEnterKey:false,
              allowEscapeKey:false,
              allowOutsideClick:false,
              showConfirmButton:false
            })
          }
      );
    })
  }

  /*Dependiendo del estado de la solicitud va a mostrar si es una solicitud atendida/en espera/cancelada */
  mostrarTitulo()
  {
    if(this.status == "ESPERA")
    {
      return true
    }
    return false
  }

  /* Habilita los botones para contestar la solicitud solo si se encuentra sin atender */
  habilitarBotones()
  {
    if(this.status=="ESPERA"){return true}
    return false
  }

  /* Evento que inicia la aprobación de la solicitud y asignación de equipos */
  clickAprovarSolicitud()
  {
    /* PREGUNTAMOS SI QUEREMOS APROBAR LA SOLICITUD */
    Swal.fire({
      title: '¿Está seguro de aprobar esta solicitud?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'SÍ',
      cancelButtonText:'NO',
      allowOutsideClick:false,
      allowEscapeKey:false,
      allowEnterKey:false
    }).then((result) => {

      /* EN CASO DE CONFIRMAR NOS REDIRIGE A OTRA VISTA EN DONDE PODREMOS REALIAR LA ASIGNACIÓN */
      if (result.isConfirmed) {
        this.router.navigate(["layout/asignacionIndirecta/" +  this.idcolab + "/" + this.formsolicitudAsigEq.value.equipo_solicitado+"/"+this.id+"/"+this.formsolicitudAsigEq.value.descripcion])
      }
    })
  }

  /* Evento que inicia el rechazo de una solicitud */
  async clickRechazarSolicitud()
  {
    Swal.fire({
      icon:'question',
      title: '¿Quiere rechazar esta solicitud?',
      showCancelButton: true,
      showConfirmButton: true,
      cancelButtonText:"NO",
      confirmButtonText: 'SÍ',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false
    }).then(async result=>{
      if(result.isConfirmed)
      {
        const { value: text } = await Swal.fire({
          title: 'Agrega la razón para rechazar la solicitud',
          input: 'text',
          inputLabel: 'Ejem. Está persona debe un equipo',
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'Aceptar',
          allowEnterKey:false,
          allowEscapeKey:false,
          allowOutsideClick:false
        })
        
        if(text)/* Valida que se agregó una razon para rechazar la soliicitud */
        {
          /* 
              EN CASO DE CONFIRMAR:
                - PRIMERO CAMBIAMOS EL STATUS DE LA SOLICITUD
          */
          this.solicitudAsigEquipoService.solicitudPorIdSolicitud(this.id).subscribe(data=>{
            data.fecha_mod = new Date().toISOString()
            data.status_solicitud = "RECHAZADO"
            data.observaciones = text
            this.solicitudAsigEquipoService.update(data).subscribe(val=>{
              Swal.fire({
                icon: 'success',
                title:'Solicitud rechazada',
                timer: 1500,
                allowOutsideClick:false

              })
              /* NOS REDIRIGIMOS A LA GESTION DE SOLICITUDES */
              this.router.navigate(["layout/solicitudesDeEquipoAdministrador"])
            })
          })        
        }
        else/* Si no hay nada en la razón retona un mensaje de advrtencia */
        {
          Swal.fire({
            title: 'No se agregó una razón de rechazo',
            icon:'warning',
            timer:3000,
            showConfirmButton:false,
            allowEnterKey:false,
            allowEscapeKey:false,
            allowOutsideClick:false
          })
        }
      }
    })
  }

  /* Redirecciona a la pantalla de todas las solcitudes*/
  clickGoback()
  {
    this.router.navigate(['layout/solicitudesDeEquipoAdministrador']);
  }

  /* Evento que inicia la cancelación de la solicitud */
  clickCancelararSolicitud(event:any)
  {

    /* PREGUNTA SI ESTAMOS SEGUROS DE CANCELAR LA SOLICITUD */
    Swal.fire({
      title: '¿Está seguro de cancelar esta solicitud?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'SÍ',
      cancelButtonText:'NO'
    }).then((result) => {

      /* EN CASO DE CONFIRMAR CANCELAMOS LA SOLICTUD*/
      if (result.isConfirmed) {
        
        this.solicitudAsigEquipoService.solicitudPorIdSolicitud(this.id).subscribe(data=>{
          data.fecha_mod = new Date().toISOString()
          data.status_solicitud = "CANCELADO"
          this.solicitudAsigEquipoService.update(data).subscribe(val=>{
            Swal.fire({
              icon:'success',
              title:'Solicitud cancelada',
              showConfirmButton: false,
              timer: 2000
            })

            /* NOS REDIRIGIMOS A LA VISTA DE GESTION DE SOLICITUDES */
            this.router.navigate(['layout/solicitudesDeEquipoAdministrador'])
          })
        })
      }
    })
  }
}
