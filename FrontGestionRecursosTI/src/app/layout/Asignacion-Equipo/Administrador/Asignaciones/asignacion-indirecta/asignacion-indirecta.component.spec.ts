import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignacionIndirectaComponent } from './asignacion-indirecta.component';

describe('AsignacionIndirectaComponent', () => {
  let component: AsignacionIndirectaComponent;
  let fixture: ComponentFixture<AsignacionIndirectaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AsignacionIndirectaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignacionIndirectaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
