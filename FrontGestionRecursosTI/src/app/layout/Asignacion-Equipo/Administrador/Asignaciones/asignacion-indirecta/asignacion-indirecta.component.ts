import { ActivatedRoute} from "@angular/router"
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators} from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router }  from '@angular/router';
import   Swal from 'sweetalert2';
/**Modelos */
import { Empleado } from 'src/app/administracion/modelos/empleado';
import { Equipo } from 'src/app/administracion/modelos/Inventario/Equipo/equipo';
import { Perfil } from 'src/app/administracion/modelos/perfil';
import { Sucursal } from 'src/app/administracion/modelos/sucursal';
/**Servicios */
import { EquipoService } from 'src/app/administracion/servicios/Inventario/Equipo/equipo.service';
import { UsuarioService } from 'src/app/administracion/servicios';
import { RegistroAsignacionEquipoService } from 'src/app/administracion/servicios/Asignacion_Equipo/registro-asignacion-equipo.service';
import { DetalleRegistroService } from 'src/app/administracion/servicios/Asignacion_Equipo/detalle-registro.service';
import { UsuarioGlpiService } from 'src/app/administracion/servicios/Inventario/Usuario/usuario-glpi.service';
import { SolicitudAsigEquipoService } from 'src/app/administracion/servicios/Asignacion_Equipo/solicitud-asig-equipo.service';
import { RegistroAsignacionEquipo } from "src/app/administracion/modelos/Asignacion_Equipo/registro-asignacion-equipo";
import { DetalleRegistro } from "src/app/administracion/modelos/Asignacion_Equipo/detalle-registro";


@Component({
  selector: 'app-asignacion-indirecta',
  templateUrl: './asignacion-indirecta.component.html',
  styleUrls: ['./asignacion-indirecta.component.scss']
})
export class AsignacionIndirectaComponent implements OnInit {

  /* Data de la tabla de equipos */
  encabezadostabla   : String [] = ["OPCIONES","NOMBRE","NÚMERO DE SERIE","TIPO","MODELO","FABRICANTE"]  /* Encabezados de la tabla  */
  dataSource          = new MatTableDataSource<Equipo>([]);         /* Data de la tabla         */
  @ViewChild(MatPaginator) paginator  !: MatPaginator               /* Paginatr de la tabla     */

  /*  Parámetros */
  id_solicitud      !: number /*  Id de la solictud */
  descripcion       !: string /*  Descripcion del uso o del equipo solicitado */
  colaborador_id    !: number /*  id del empleado */

  /* Elementos de la vista */
  empleado          !: Empleado
  nombre   : string  =""
  apellidos: string  =""
  tabla    : string  =""
  oficina!:string
  cargo!:string

  /*  Constructor */
  constructor( private activatedRoute                 : ActivatedRoute                  , 
               private detalleRegistroService         : DetalleRegistroService          , /* Conexión con la tabla detalle_registro de la base de datos   */
               private equipoService                  : EquipoService                   , /* Conexión con la tabla equipo de Glpi*/
               private formBuilder                    : FormBuilder                     , 
               private registroAsignacionEquipoService: RegistroAsignacionEquipoService , /* Conexión con la tabla registro_asignacion de la base de datos*/
               private router                         : Router                          , 
               private solicitudAsigEquipoService     : SolicitudAsigEquipoService      , /* Conexión con la tabla solicitud_equipo de la base de datos   */
               private usuarioService                 : UsuarioService                  , /* Conexión con el end point usuarios                           */
               private usuarioGlpiService             : UsuarioGlpiService              , /* Conexión con la tabla usuarios de Glpi                       */
             ){}       

  /*Form control group */
  formAsignacionDiecta = this.formBuilder.group
  ({
    activo           : [{value: '', disabled: true}, Validators.required], /* Activo              */
    colaborador_name : [{value: '', disabled: true}, Validators.required], /* Nombre del empleado */ 
    cargo_name       : [{value: '', disabled: true}, Validators.required], /* Cargo               */
    sucursal_name    : [{value: '', disabled: true}, Validators.required]  /* Oficina             */
  });


  ngOnInit() 
  {
    /** RECIBIMOS LA INFORMCIÓN DE LA SOLICITUD CUANDO ES APROBADA */
    this.activatedRoute.params.subscribe(params =>{
      /** Recibimos en que tabla se va a buscar los disponible */
      this.formAsignacionDiecta.controls['activo'].setValue(params['tipo_activo'] + "")
      /** Recibimos el id del colaborador */
      this.colaborador_id = params['id_colaborador']
      /** Recibimos el id de la solicitud*/
      this.id_solicitud   = params['id_solicitud']
      /** Recibimos la descripcion del equipo en caso que sea un periférico */
      this.descripcion = params['descripcion']
      /** Cargamos los datos del colaborador y llenamos la tabla con base a los datos de la solictud */
      this.cargarDatosColaborador()
      this.llenarData()
    })
  }
  
  /* Método que carga los equipos */
  llenarData()
  {

    /* Cargamos los equipos disponibles del tipo solicitado */
         if(this.formAsignacionDiecta.value.activo == "COMPUTADORA"         ){ this.tabla = "Computer"          ;}
    else if(this.formAsignacionDiecta.value.activo == "DISPOSITIVO DE RED"  ){ this.tabla = "Networkequipment"  ;}
    else if(this.formAsignacionDiecta.value.activo == "IMPRESORA"           ){ this.tabla = "Printer"           ;}
    else if(this.formAsignacionDiecta.value.activo == "MONITOR"             ){ this.tabla = "Monitor"           ;}
    else if(this.formAsignacionDiecta.value.activo == "PERIFÉRICO"          ){ this.tabla = "Peripheral"        ;}
    else if(this.formAsignacionDiecta.value.activo == "RACK"                ){ this.tabla = "Rack"              ;}
    else if(this.formAsignacionDiecta.value.activo == "TELEFONO"            ){ this.tabla = "Phone"             ;}

    /* Declaramos una lista auxiliar para manipular los datos de la consulta */
    let listaEquipo: Equipo[] = [];
     
    /* Mandamos a llamar todos los equipos del tipo solicitado */
    this.equipoService.todosLosEquipos(this.tabla).subscribe(val=>{val.forEach(element=>{
      

        /* Filtamos solo los equipos disponibles */
        if(element.states_id == "Stock" || element.states_id == "No Asignado" || element.states_id == "Principal" || element.states_id == "Backup" || element.states_id == "" )
        { 
          /* Añadimos el tipo y modelo de la tabla en cuestión */
               if(this.formAsignacionDiecta.value.activo == "COMPUTADORA" ){ element.tipo=element.computertypes_id        ; element.modelo=element.computermodels_id         }
          else if(this.formAsignacionDiecta.value.activo == "DISPOSITIVO DE RED"){ element.tipo=element.networkequipmenttypes_id; element.modelo=element.networkequipmentmodels_id }
          else if(this.formAsignacionDiecta.value.activo == "IMPRESORA"   ){ element.tipo=element.printertypes_id         ; element.modelo=element.printermodels_id          }
          else if(this.formAsignacionDiecta.value.activo == "MONITOR"     ){ element.tipo=element.monitortypes_id         ; element.modelo=element.monitormodels_id          }
          else if(this.formAsignacionDiecta.value.activo == "PERIFÉRICO"  ){ element.tipo=element.peripheraltypes_id      ; element.modelo=element.peripheralmodels_id       }
          else if(this.formAsignacionDiecta.value.activo == "RACK"        ){ element.tipo=element.racktypes_id            ; element.modelo=element.rackmodels_id             }
          else if(this.formAsignacionDiecta.value.activo == "TELEFONO"    ){ element.tipo=element.phonetypes_id           ; element.modelo=element.phonemodels_id            }
          

          /*Filtramos los equipos disponibles */
          if(this.formAsignacionDiecta.value.activo == "DISPOSITIVO DE RED" )
          {
            if(element.tipo==this.descripcion )
            {
              /* Agregamos ele quipo a la lista auxiliar */
              listaEquipo.push(element)  
            }
          }
          else if(this.formAsignacionDiecta.value.activo == "PERIFÉRICO")
          {
            if(element.tipo==this.descripcion )
            {
              /* Agregamos ele quipo a la lista auxiliar */
              listaEquipo.push(element)  
            }
          }
          else
          {
            /* Agregamos ele quipo a la lista auxiliar */
            listaEquipo.push(element)  
          }
                       

          this.dataSource.data=listaEquipo
          this.dataSource.paginator=this.paginator
        }

      })
      if(this.dataSource.data.length==0) /* En caso que no se tenga equipos disponibles retorna un mensaje */
      {
        this.mensajeDeAdvertencia("No hay equipos disponibles, intentelo más tarde") /* Mensaje */
      } 

      
    }, err=>{ /* En caso que se tenga un error retorna un mensaje de error */
      this.mensajeDeError("Sin conexción, vuelva a intentarlo más tarde") /* Llama al método que despliega el mensaje */
    })  
  }

  /*Cargamos los datos del empleado */
  cargarDatosColaborador()
  {
    /* Creamos un modelo de empleado */
    let empleadoAux: Empleado = new Empleado()
    /* Realizamos la consulta en el endpoint*/
    this.usuarioService.getAnEmpleado(this.colaborador_id).subscribe
    (
      val=>
      {
        this.empleado=val;empleadoAux = val;

      /*Cargamos el nombre del empleado */
      this.formAsignacionDiecta.controls['colaborador_name'].setValue(this.empleado.nombre + " " + this.empleado.apellidoPat + " " + this.empleado.apellidoMat)
      
      /*declaramos dos variables temporales para validar si estan vacías o no */
      let sucursal  : Sucursal = empleadoAux.sucursal;
      let puesto    : Perfil     = empleadoAux.puesto;
      
      this.nombre = empleadoAux.nombre + ""
      this.apellidos = empleadoAux.apellidoPat + " " + empleadoAux.apellidoMat

      /* Primer validamos si el cargo se encuentra vacío o nulo*/
      if(puesto != null)
      {
         this.formAsignacionDiecta.controls['cargo_name'].setValue(puesto.nombrePuesto); 
         this.cargo = puesto.nombrePuesto+""
      }
      else  /*De encotrarse vacío o nulo agrega una cadena por defecto */
      {
        this.cargo = "Sin infromación";
      }
      
      /* Ahora validamos si la oficina o sucursal se encuentra vacío o nulo*/
      if(sucursal != null){ 
        this.formAsignacionDiecta.controls['sucursal_name'].setValue(sucursal.nombreSucursal); 
        this.oficina = sucursal.nombreSucursal+""
      }
      else  /*De encotrarse vacío o nulo agrega una cadena por defecto */
      {
        this.oficina = "Sin infromación";
      }
    }, err=>{ /* En caso que se tenga un error retorna un mensaje de error */
      this.mensajeDeError("Error al cargar los datos del empleado, vuelva a intentarlo más tarde") /* Llama al método que despliega el mensaje */
    })    
  }

  /* Evento que se desata cuando se selecciona el equipo */
  asignarEquipo(id_equipo:number, tipo:string, nombre_equipo:string, comentarios:string)
  {
    /* Validamos si es una cpu, para redirigirlo a otra pantalla */
    if( nombre_equipo.includes("CPU") )
    {
      this.router.navigate(['layout/asignacion_computadora_escitorio/'+this.nombre+'/'+this.apellidos+'/'+id_equipo+'/'+nombre_equipo+'/'+(this.id_solicitud+"")+"/"+this.colaborador_id+'/'+comentarios]) 
    }

    /* De ser otro tipo de equipo comenzamos el proseso de asignación de equipo*/
    else 
    {
      /* Mostramos una ventana de confirmación */
      Swal.fire({
        title: '¿Está usted seguro de asignar este equipo?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'SÍ',
        cancelButtonText:'NO',
        allowOutsideClick:false,
        allowEscapeKey:false,
        allowEnterKey:false
      }).then(async (result) => {

        /* Mostramos una ventana de confirmación */
        if (result.isConfirmed) 
        {

          /* Mostramos una ventana para agregar los comentarios y obersrvaciones */
          const { value: text } = await Swal.fire({
            input: 'textarea',
            inputLabel: 'OBSERVACIONES O COMENTARIOS',
            inputPlaceholder: '...',
            inputAttributes: 
            {
              'aria-label': 'Type your message here'
            },
            allowEnterKey:false,
            allowEscapeKey:false,
            allowOutsideClick:false
          })

          /* Validamos que no se encuentre vacío este campo para agregarlo a las observaciones del detalle  */
          /*En caso de estar vacío agregamos una cadena por defecto y los comentarios originales del equipo  */
          let observaciones = text
          if (observaciones == null || observaciones == "") {
            observaciones = comentarios
          }
          /* En caso de no tener observaciones agregamos los los comentarios originales del equipo y las observaciones recientes*/
          else
          {
            observaciones = observaciones + ", " + comentarios
          }

          /* Creamos un objeto de tipo Registro de asignación */
          let registroAsignacionEquipo: RegistroAsignacionEquipo = new RegistroAsignacionEquipo(); 
          registroAsignacionEquipo.fecha_asignacion   = new Date()
          registroAsignacionEquipo.fecha_modificacion = new Date()
          registroAsignacionEquipo.observaciones                 = "ASIGNACIÓN CON SOLICITUD"

          /* Validamos que no se tenga que subir una responsiva obligatoriamente */
          if(this.tabla == "Computer" || tipo == "PROYECTOR")
          {
            registroAsignacionEquipo.status_registro               = "REGISTRADO"  
          }
          /* en caso de no ser una computadora o proyector no es necesario pedir la responsiva de equipo */
          else
          {
            registroAsignacionEquipo.status_registro               = "ACTIVO"  
          }
          
          /* Validamos que no se tenga que subir una responsiva obligatoriamente */
          registroAsignacionEquipo.colaborador_id = this.colaborador_id
          registroAsignacionEquipo.nombre_empleado_asignado = this.formAsignacionDiecta.value.colaborador_name
          

          this.asignacionDeEquipo(id_equipo,observaciones,id_equipo,nombre_equipo,registroAsignacionEquipo)
        }
        
      }) 
    }
  }

  /* Evento para cancelar el proceso de asignación */
  cancelar(event:any)
  {
    /* 
        Mandamos una ventana de confirmación
    */
    Swal.fire({
      title: '¿Está seguro de cancelar esta asignación?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'SÍ',
      cancelButtonText:'NO',
      allowOutsideClick:false,
      allowEscapeKey:false,
      allowEnterKey:false
    }).then((result) => {
      if (result.isConfirmed) 
      {
        /* De confirmarse retornamos a la pantalla de todas las solcitudes sin repsonder*/
        this.router.navigate(['layout/solicitudesDeEquipoAdministrador'])
      }})
  }

  /* Mostramos una ventana con información sobre la pantalla */
  clickMasInformacion(event:any)  
  {
    /* Abre una ventana de confirmación*/
    Swal.fire({
      title:'Asignación de equipo con solicitud',
      html:
      '<p align="justify">En la parte superior se observan los datos relevantes para la asignacion.</p>'  +

      '<p align="justify">Solamente elija el equipo seleccinando el botón azul que se encuentra a la '    +
      'izquierda del nombre.</p>'                                                                         +

      '<p align="justify">Confirme e ingrese comentarios u observaciones (los ultimos dos pcionales).</p>'+

      '<p align="justify"><b>NOTA:</b> En caso de elegir un equipo de escritorio se redirigirá a'         +
      ' otra ventana.</p>'                                                                                ,

      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      confirmButtonColor: '#d33',
      confirmButtonText:'Cerrar'
    })
  }

  /* Cambia el status y usuario del equipo en GLpi */
  asignacionDeEquipo(id:number, observaciones:string, id_equipo:number, nombre_equipo:string,registroAsignacionEquipo:RegistroAsignacionEquipo)
  {
    let sucursal_obj:Sucursal = new Sucursal() /* creamos un objeto para cuando la sucursal sea nula */
    sucursal_obj.nombreSucursal = "Sin sucursal a buscar"

            
    this.usuarioService.getAnEmpleado(this.colaborador_id).subscribe(empleado_endpoit=>{
    
      if(empleado_endpoit.sucursal==null)/* valida si el empleado tiene el campo sucursal como nulo */
      {
        empleado_endpoit.sucursal = sucursal_obj /* De tenerlo solo agrega el valor por defecto */
      }

      /*Buscamos la sucursal en Glpi */
      this.equipoService.buscarSucursal(empleado_endpoit.sucursal.nombreSucursal).subscribe(sucursales=>{
        /** Buscamos al usuario en Glpi */   
        this.usuarioGlpiService.allUsuariosSearch(this.nombre,this.apellidos).subscribe(val=>{


          /* Validamos que las consultas no estén vacías */
          if(sucursales.length>0) /* Primero validamos la Sucursal */
          {
            if(val.length>0)  /* Ahora validamos el Empleado */
            {
              /* Actualizamos al equipo en Glpi */
              let body = "{\"input\": {\"states_id\": \" "  + 1                                               + 
                            " \", \"contact\": \" "         + (empleado_endpoit.puesto.nombrePuesto+"")       +
                            " \", \"users_id\": \" "        + val[0].id                                       + 
                            " \", \"locations_id\": \" "    + sucursales[0].id                                +
                            "\"}}";
              /*Realizamos la peticion al back conectado a Glpi */
              this.equipoService.actualizarItem(this.tabla,id,body).subscribe(data=>{}, err=>{this.mensajeDeError("Sin conexión con el servidor, Intentelo más tarde")})



              /* Ahora creamos el registro de asignación y detalle de asignación*/
              /*Guardamos el registro especificando que se tiene una solicitud */
              this.registroAsignacionEquipoService.asignacionI(registroAsignacionEquipo,this.id_solicitud).subscribe(data=>{
                          
                /*Guardamos el detalle de registro corresondiente */
                this.generarDetalleRegistro( Number(data.id_registro_asignacion), id_equipo,nombre_equipo, observaciones)
              })


              /*Cambiamos el estado de la solicitu */
              this.atenderSolicitud()
            }
            else /* Si no encuentra al empleado retorna un mensaje de error */
            {
              this.mensajeDeError("El empleado seleccionado no se encuentra registrado en Glpi")              
            }
          }
          else /* Si no encuentra la sucursal retorna un mensaje de error */
          {
            this.mensajeDeError("La sucursal a la que pertenece el empleado no se encuentra registrada en Glpi")
          }
        })
      })
    })
  }

  /* Cambai el estado actual de la solictud de en espera a APROBADO*/
  atenderSolicitud()
  {
    this.solicitudAsigEquipoService.solicitudPorIdSolicitud(this.id_solicitud).subscribe(data=>{

      /* Agregamos la fecha actual en que se atendió la solictud */
      data.fecha_mod = new Date().toISOString()
      data.status_solicitud = "APROBADO"

      /*Agregamos el responsable de atende rla solicitud */
      data.administrador_TI = JSON.parse(localStorage.getItem('empleado') || '{}').id;
      this.solicitudAsigEquipoService.update(data).subscribe(val=>{
        Swal.fire({
          icon:'success',
          title:'Asignación realizada',
          showConfirmButton:false,
          timer: 1700
        }).then(()=>{
          /*Redirigimos a la pantalla de pendientes de responsivas */
          this.router.navigate(['layout/seguimientoAsignacionEquipo'])
        })
      },
      /*En caso de no poder realizarse esta operacion retornamos un mensaje de error */
      err=>{
        /* En caso que se tenga un error retorna un mensaje de error */
        this.mensajeDeError("Sin conexción, vuelva a intentarlo más tarde") /* Llama al método que despliega el mensaje */
      })
    })
  }

  /* Genera y guarda un detalle de registro en la base de datos */
  generarDetalleRegistro(id_registro_asignacion:number, id_equipo:number, nombre_equipo:string, observaciones:string)
  {

    /* Creamos un objeto de tipo detalle de registro */
    let detalleRegistro:DetalleRegistro     = new DetalleRegistro()
    detalleRegistro.equipo_id               = id_equipo
    detalleRegistro.observ_equipo           = observaciones
    detalleRegistro.status_detalle          = "ACTIVO"
    detalleRegistro.nombre_equipo           = nombre_equipo

    /* Agregamos el tipo de equipo*/
          if( this.tabla=="Computer"         ){ detalleRegistro.tipo_activo="COMPUTADORA"        }
    else if( this.tabla=="Networkequipment"  ){ detalleRegistro.tipo_activo="DISPOSITIVO DE RED" }
    else if( this.tabla=="Printer"           ){ detalleRegistro.tipo_activo="IMPRESORA"          }
    else if( this.tabla=="Monitor"           ){ detalleRegistro.tipo_activo="MONITOR"            }
    else if( this.tabla=="Peripheral"        ){ detalleRegistro.tipo_activo="DISPOSITIVO"        }
    else if( this.tabla=="Rack"              ){ detalleRegistro.tipo_activo="RACK"               }
    else if( this.tabla=="Phone"             ){ detalleRegistro.tipo_activo="TELEFONO"           }

    /* Guardamos el detalle de registro */
    this.detalleRegistroService.save(detalleRegistro,id_registro_asignacion); 
  }

  /*  Filtro de la barra de busqueda  de la tabla*/
  filtroDataDource(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  /* Método que retorna un mensaje de advertencia*/
  mensajeDeAdvertencia(texto:string)
  {
    Swal.fire({
      icon:'warning',
      title:texto,
      timer:2000,
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      showConfirmButton:false
    })
  }

  /* Método que retorna un mensaje de error*/
  mensajeDeError(texto:string)
  {
    Swal.fire({
      title:'Error',
      text:texto,
      icon:'error',
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      showConfirmButton:false,
      timer:3000
    })
  }
}
