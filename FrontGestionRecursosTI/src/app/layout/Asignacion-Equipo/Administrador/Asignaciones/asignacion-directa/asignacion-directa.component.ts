import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
/**Modelos */
import { Empleado } from 'src/app/administracion/modelos/empleado';
import { Equipo } from 'src/app/administracion/modelos/Inventario/Equipo/equipo';
/**Servicios */
import { DetalleRegistroService } from 'src/app/administracion/servicios/Asignacion_Equipo/detalle-registro.service';
import { EquipoService } from 'src/app/administracion/servicios/Inventario/Equipo/equipo.service';
import { RegistroAsignacionEquipoService } from 'src/app/administracion/servicios/Asignacion_Equipo/registro-asignacion-equipo.service';
import { UsuarioService } from 'src/app/administracion/servicios';
import { UsuarioGlpiService } from 'src/app/administracion/servicios/Inventario/Usuario/usuario-glpi.service';
import { RegistroAsignacionEquipo } from 'src/app/administracion/modelos/Asignacion_Equipo/registro-asignacion-equipo';
import { DetalleRegistro } from 'src/app/administracion/modelos/Asignacion_Equipo/detalle-registro';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Sucursal } from 'src/app/administracion/modelos/sucursal';

@Component({
  selector: 'app-asignacion-directa',
  templateUrl: './asignacion-directa.component.html',
  styleUrls: ['./asignacion-directa.component.scss']
})

export class AsignacionDirectaComponent implements OnInit {

  /*  Listas a utilizar */
  tablas    :string   [] = []
  empleados :Empleado [] = []
  listaFiltradaColaboradores    :   ReplaySubject<Empleado[]> = new ReplaySubject<Empleado[]>(1);

  /*Form Control */
  input_colaborador: FormControl = new FormControl()
  empleado_seleccionado: FormControl = new FormControl()
  activo               : FormControl = new FormControl()

  /* Modelos */
  empleado_asignacion:Empleado = new Empleado()
  tabla_glpi:string = ""

  /* Data de la tabla de equipos  */
  encabezadostabla   : String [] = ["OPCIONES","NOMBRE","NÚMERO DE SERIE","TIPO","MODELO","FABRICANTE"]  /*  Encabezados */
  dataSource          = new MatTableDataSource<Equipo>([]);         /*  Data        */
  @ViewChild(MatPaginator) paginator  !: MatPaginator               /*  Paginator   */

  protected _onDestroy = new Subject<void>();

  /*  Constructor */
  constructor( private usuarioService                 : UsuarioService                  , /*Conexión con el end point empleados */
               private usuarioGlpiService             : UsuarioGlpiService              , /*Conexión con el las tablas usuario de Glpi */
               private equipoService                  : EquipoService                   , /*Conexión con el las tablas equipos de Glpi */
               private registroAsignacionEquipoService: RegistroAsignacionEquipoService , /*Conexión con la tabla registro_asignacion de la base de datos */
               private detalleRegistroService         : DetalleRegistroService          , /*Conexión con la tabla detalleregistro de la base de datos */
               private router                         : Router
             ){}       

  ngOnInit() 
  {
    this.llenarListas()
    this.listaFiltradaColaboradores.next(this.empleados.slice());
    this.input_colaborador.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterEmpleado();
      });
  }

  /* Método que carga la lista de empleados */
  llenarListas()
  {
    /*Realizamos la consulta al end point para retornar todos los empleados */
    this.usuarioService.getAllEmpleados().subscribe(consulta=>{
      
      /*Recorremos la consulta para construir los nombres completos */
      consulta.forEach(empleado=>{
        empleado.nomb_compl = empleado.nombre + " " + empleado.apellidoPat + " " + empleado.apellidoMat
      })
      
      /* Asignamos la data a la lista */
      this.empleados = consulta
    },
    /*De existir un error de conexión retornará un mensaje de error */
    err=>{
      Swal.fire({
        showConfirmButton: false,
        allowEnterKey:false,
        allowEscapeKey:false,
        allowOutsideClick:false,
        timer: 3000,
        icon: 'error',
        title: 'Error al cargar la lista de empleados, intentelo más tarde'
      })
    })

    /*Cargamos las opciones de equipos a asignar */
    this.tablas = this.equipoService.listaActivosAsignacion(2)
  }

  /* Crea un detalle de registro a un equipo para el registro general */  
  async generarDetalleRegistro(id_registro_asignacion:number, id_equipo:number, nombre_equipo:string, observaciones:string)
  {

    /* Crea un objeto de tipo DetalleAsignacion */
    let detalleRegistro:DetalleRegistro     = new DetalleRegistro()
    detalleRegistro.equipo_id               = id_equipo
    detalleRegistro.observ_equipo           = observaciones
    detalleRegistro.status_detalle          = "ACTIVO"
    detalleRegistro.nombre_equipo          =  nombre_equipo

    /* Agrega el tipo de equipo al que pertenece y guarda el detalle*/
         if( this.tabla_glpi=="Computer"         ){ detalleRegistro.tipo_activo="COMPUTADORA"       ; this.detalleRegistroService.save(detalleRegistro,id_registro_asignacion);}
    else if( this.tabla_glpi=="Networkequipment" ){ detalleRegistro.tipo_activo="DISPOSITIVO DE RED"; this.detalleRegistroService.save(detalleRegistro,id_registro_asignacion);}
    else if( this.tabla_glpi=="Printer"          ){ detalleRegistro.tipo_activo="IMPRESORA"         ; this.detalleRegistroService.save(detalleRegistro,id_registro_asignacion);}
    else if( this.tabla_glpi=="Monitor"          ){ detalleRegistro.tipo_activo="MONITOR"           ; this.detalleRegistroService.save(detalleRegistro,id_registro_asignacion);}
    else if( this.tabla_glpi=="Peripheral"       ){ detalleRegistro.tipo_activo="DISPOSITIVO"       ; this.detalleRegistroService.save(detalleRegistro,id_registro_asignacion);}
    else if( this.tabla_glpi=="Rack"             ){ detalleRegistro.tipo_activo="RACK"              ; this.detalleRegistroService.save(detalleRegistro,id_registro_asignacion);}
    else if( this.tabla_glpi=="Phone"            ){ detalleRegistro.tipo_activo="TELEFONO"          ; this.detalleRegistroService.save(detalleRegistro,id_registro_asignacion);}
  }

  /* Evento del botón más información del cual emerge una ventana con información sobre como llenar el formulario */
  clickMasInformacion(event:any)  
  {
    Swal.fire({
      title:'Instrucciones',
      html:
      '<p align="justify"><b>-</b>  Ingrese y seleccione el nombre del empleado a quien va asignar un equipo.</p>'             +
      '<p align="justify"><b>-</b>  Seleccione el tipo de dispositivo, al hacerlo se cargarán los equipos disponibles</p>'     +
      '<p align="justify"><b>-</b>  Para asignar el equipo, seleccione el botón azul a la derecha del nombre del equipo.</p>'  +
      '<p align="justify"><b>-</b>  Al elegir un equipo emerge una ventana de confirmación en donde puede o no continuar con ' +
      'la asignación.</p>'                                                                                                     +
      '<p align="justify"><b>NOTA: </b>En caso de ser un equipo de escritorio(Cualquier equipo con la palabra Desktop en su '  +
      'tipo) será redirigido a otra pantalla para elegir los demas componentes.</p>',
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      confirmButtonColor: '#d33',
      confirmButtonText:'Cerrar'
    })
  }

  /* Evento con el cual comienza el proceso de la asignación del equipo seleccionado */
  clickAsignar(id_equipo:number, tipo: string, nombre_equipo:string, comentarios:string)
  {

    /* Valida si el colaborador ya fue elegido */
    if(this.empleado_asignacion.apellidoPat!=undefined)
    {

      /* 
        De tratarse de una cpu nos redirigimos a otra ventana en donde poder elegir los demas componentes
      */
      if( nombre_equipo.includes("CPU") )
      {
        this.router.navigate(['layout/asignacion_computadora_escitorio/'+ this.empleado_asignacion.nombre+'/'+( this.empleado_asignacion.apellidoPat + " " + this.empleado_asignacion.apellidoMat ) + '/' + id_equipo + '/'+nombre_equipo+'/directa/' + this.empleado_asignacion.idEmpleado+'/'+comentarios]) 
      }
      else /* De ser otro tipo de equipo comenzaos por preguntar si se està seguro de realizar la asignación */
      {
        /* Mandamos una ventana de confirmación */
        Swal.fire({
          icon:'question',
          text:'¿Esta seguro de asignar este equipo ?',
          showCancelButton:true,
          showConfirmButton:true,
          cancelButtonText:'NO',
          confirmButtonText:'SÍ',
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          allowEnterKey:false,
          allowEscapeKey: false,
          allowOutsideClick:false,
        }).then( async (result)=>{
        
          /* En caso de confirmarse */
          if(result.isConfirmed)
          {
            let sucursal_obj:Sucursal = new Sucursal() /* creamos un objeto para cuando la sucursal sea nula */
            sucursal_obj.nombreSucursal = "Sin sucursal a buscar"

            if(this.empleado_asignacion.sucursal==null)/* valida si el empleado tiene el campo sucursal como nulo */
            {
              this.empleado_asignacion.sucursal = sucursal_obj /* De tenerlo solo agrega el valor por defecto */
            }

            /* Emerge otra ventana que solicita comentarios u observaciones (Opcional) */
            const { value: text } = await Swal.fire({
              input: 'textarea',
              inputLabel: 'OBSERVACIONES O COMENTARIOS',
              inputPlaceholder: '...',
              inputAttributes: 
              {
                'aria-label': 'Type your message here'
              },
              allowEnterKey:false,
              allowEscapeKey:false,
              allowOutsideClick:false
            })

            /* Validamos que los comentarios u observaciones no se encuentren vaciós */
            let observaciones = text
            if (observaciones == null || observaciones == "") 
            {

              /*De encontrarse vaciós solo agregamos los comentarios que trae por defecto el equipo más una cadena por default*/
              observaciones = comentarios + ". Sin nuevas observaciones."
            }
            else
            {
              /*De tener conmentarios agregamos estos comentarios nuevos y los que posea con anterioridad */
              observaciones = observaciones + ", " + comentarios
            }

            /* Creamos un objeto de tipo registro de asignación*/
            let registroAsignacionEquipo: RegistroAsignacionEquipo = new RegistroAsignacionEquipo();       
            registroAsignacionEquipo.id_registro_asignacion        = 0

            /* Validamos que no se un equipo que requiera una responsiva de forma obligatoria (COMPUTADORA o PROYECTOR) */
            if(this.tabla_glpi == "Computer" || tipo == "PROYECTOR")
            {
              registroAsignacionEquipo.status_registro               = "REGISTRADO"  
            }

            /* En caso de ser otro tipo de equio solo no solicitamos la responsiva de forma obligatoria */
            if(this.tabla_glpi != "Computer" && tipo != "PROYECTOR")
            {
              registroAsignacionEquipo.status_registro               = "ACTIVO"
            }
            registroAsignacionEquipo.colaborador_id                = this.empleado_asignacion.idEmpleado
            registroAsignacionEquipo.fecha_asignacion              = new Date()
            registroAsignacionEquipo.fecha_modificacion            = new Date()
            registroAsignacionEquipo.observaciones                 = "ASIGNACIÓN DIRECTA"

            registroAsignacionEquipo.nombre_empleado_asignado      = this.empleado_asignacion.nombre      + " " +
                                                                     this.empleado_asignacion.apellidoPat + " " +
                                                                     this.empleado_asignacion.apellidoMat
            
            /* Cambia el status y responsable del equipo en Glpi */
            this.equipoService.buscarSucursal(this.empleado_asignacion.sucursal.nombreSucursal).subscribe(sucursales=>{

              /* Buscamos el empleado en Glpi */
              this.usuarioGlpiService.allUsuariosSearch( (this.empleado_asignacion.nombre+"") , (this.empleado_asignacion.apellidoPat + " " + this.empleado_asignacion.apellidoMat) ).subscribe(val=>{

                /*Validamos que ambas consultas se encuentren en glpi */
                if(sucursales.length>0) /* Primero revisamos si la sucursal se encuentra en Glpi*/
                {
                  if( val.length>0)  /* Ahora revisamos si el empleado se encuentra en Glpi*/
                  {

                    /*Actualizamos el registro del equipo en glpi */
                    let body = "{\"input\": {\"states_id\": \" "      + 1                                                                                   +
                                  " \", \"contact\": \" "         + (this.empleado_asignacion.puesto.nombrePuesto+"")                                   +
                                  " \", \"users_id\": \" "        + val[0].id                                                                           +
                                  " \", \"locations_id\": \" "    + sucursales[0].id +
                                  "\"}}";
                    /*Realiza la peticion de cambio al back conectado a Spring */
                    this.equipoService.actualizarItem(this.tabla_glpi,id_equipo,body).subscribe(data=>{console.log(data)})

                    /* Se realiza la asignación de forma directa sin solicitud */
                    this.registroAsignacionEquipoService.asignacionD(registroAsignacionEquipo).subscribe(registro=>
                    {
                      /* Creamos un detalle de registro para el equipo */
                      this.generarDetalleRegistro(Number(registro.id_registro_asignacion),id_equipo,nombre_equipo, observaciones)
    
                      /* Mostramos un mensaje de confirmación */
                      Swal.fire({
                        title:'Equipo asignado',
                        icon:'success',
                        timer:2000,
                        allowEnterKey:false,
                        allowEscapeKey: false,
                        allowOutsideClick:false,
                        showConfirmButton:false
                      }).then((result)=>{
                        /* Enviamos a visitar los pendientes */
                        this.router.navigate(['layout/seguimientoAsignacionEquipo'])
                      })
                    },
                    /*En caso de no realizarse la operación retorna un mensjae de error */
                    err=>{
                      Swal.fire({
                        allowEnterKey:false,
                        allowEscapeKey:false,
                        allowOutsideClick:false,
                        showConfirmButton: false,
                        timer: 3000,
                        timerProgressBar: true,
                        icon: 'error',
                        title: 'Sin conexión, intentelo más tarde'
                      })
                    }) 

                  }
                  else /* Si no encuentra al empleado retorna un mensaje de error */
                  {
                    this.mensajeDeError("El empleado seleccionado no se encuentra registrado en Glpi")
                  }
                }
                else /* Si no encuentra la sucursal retorna un mensaje de error */
                {
                  this.mensajeDeError("La sucursal a la que pertenece el empleado no se encuentra registrada en Glpi")
                }
              })
            })
          }
        })
      }
    }
    /* En caso de no elegirse, emerge una ventana en donde se solicita ingresarlo */
    else
    {
      Swal.fire({
        title:'Error',
        text: 'Aún no selecciona al colaborador',
        icon:'error',
        showConfirmButton:false,
        timer:1700
      })
    }
  }

  /* Evento del select que carga todos los equipos disponibles de la clase seleccionada*/
  clickSeleccionarTipoDispositivo(event:any)
  {

    /* Declaramos uan lista auxiliar para manejar la data de la tabla */
    let listaAux: Equipo [] =[]

    /* Revisamos que tipo de equipo se seleccionó */
         if(  this.activo.value=="COMPUTADORA"        ){ this.tabla_glpi= "Computer" }
    else if(  this.activo.value=="DISPOSITIVO DE RED" ){ this.tabla_glpi= "Networkequipment" }
    else if(  this.activo.value=="IMPRESORA"          ){ this.tabla_glpi= "Printer" }
    else if(  this.activo.value=="MONITOR"            ){ this.tabla_glpi= "Monitor" }
    else if(  this.activo.value=="DISPOSITIVO"        ){ this.tabla_glpi= "Peripheral" }
    else if(  this.activo.value=="RACK"               ){ this.tabla_glpi= "Rack" }
    else if(  this.activo.value=="TELEFONO"           ){ this.tabla_glpi= "Phone" }

    /* Consultamos todos los equipos disponibles que pertenescan al tipo de equipo seleccionado */
    this.equipoService.todosLosEquipos(this.tabla_glpi).subscribe(lista=>{lista.forEach(element=>{
        if(element.states_id == "Stock" || element.states_id == "No Asignado" || element.states_id == "Principal" || element.states_id == "Backup" || element.states_id == "" )
        {
          /*Agregamos modelo y tipo segun corresponda */
               if(  this.activo.value=="COMPUTADORA"        ){  element.tipo = element.computertypes_id         ; element.modelo=element.computermodels_id        }  
          else if(  this.activo.value=="DISPOSITIVO DE RED" ){  element.tipo = element.networkequipmenttypes_id ; element.modelo=element.networkequipmentmodels_id}
          else if(  this.activo.value=="IMPRESORA"          ){  element.tipo = element.printertypes_id          ; element.modelo=element.printermodels_id         }
          else if(  this.activo.value=="MONITOR"            ){  element.tipo = element.monitortypes_id          ; element.modelo=element.monitormodels_id         }
          else if(  this.activo.value=="DISPOSITIVO"        ){  element.tipo = element.peripheraltypes_id       ; element.modelo=element.printermodels_id         }
          else if(  this.activo.value=="RACK"               ){  element.tipo = element.racktypes_id             ; element.modelo=element.rackmodels_id            }
          else if(  this.activo.value=="TELEFONO"           ){  element.tipo = element.phonetypes_id            ; element.modelo=element.phonemodels_id           }
          
          /* Cada equipo que cumpla con las condiciones se agrega a la lista auxiliar */
          listaAux.push(element)
        }  
      })

      /* De haber equipos disponibles agregamos los equipos disponibles a la data de la tabla */
      if(listaAux.length>0)
      {
        this.dataSource.data = listaAux;this.dataSource.paginator = this.paginator;
      }
      /* De no haber equipos disponibles retornamos un mensaje de error */
      else
      {
        if(this.dataSource.data.length==0)
      {
        
        Swal.fire({
          icon:'warning',
          title:'No hay equipos disponibles, intentelo más tarde',
          timer:2000,
          allowEnterKey:false,
          allowEscapeKey:false,
          allowOutsideClick:false,
          showConfirmButton:false
        }) 
      }
      }  
    },
    /*De existir un error de conexión retornará un mensaje de error */
    err=>{
      Swal.fire({
        showConfirmButton: false,
        timer: 3000,
        icon: 'error',
        title: 'Sin conexión con el servidor, intentelo más tarde',
        allowEnterKey:false,
        allowEscapeKey:false,
        allowOutsideClick:false
        
      })
    }
    )
  }

  /* Cargamos el empleado seleccionado al modelo declarado */
  clickLlenarDatosDelColaborador(event:any)
  { 
    this.empleado_asignacion = this.empleado_seleccionado.value
  }

  /*  Filtro de la barra de busqueda de la tabla de los equipos */
  filtroDataDource(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  /*Filtro de los empleados en el select search */
  protected filterEmpleado() {
    if (!this.empleados) {
      return;
    }
    /* Recuperamos el valor del input */
    let search = this.input_colaborador.value;
    if (!search) {
      this.listaFiltradaColaboradores.next(this.empleados.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    /* Retornamos sla lista de eempleados que cumplen con las coincidencias */
    this.listaFiltradaColaboradores.next(
      this.empleados.filter(empleado => empleado.nomb_compl.toLowerCase().indexOf(search) > -1)
    );
  }


  mensajeDeError(texto:string)
  {
    Swal.fire({
      title:'Error',
      text:texto,
      icon:'error',
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      showConfirmButton:false,
      timer:3000
    })
  }
}
