import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignacionDirectaComponent } from './asignacion-directa.component';

describe('AsignacionDirectaComponent', () => {
  let component: AsignacionDirectaComponent;
  let fixture: ComponentFixture<AsignacionDirectaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AsignacionDirectaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignacionDirectaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
