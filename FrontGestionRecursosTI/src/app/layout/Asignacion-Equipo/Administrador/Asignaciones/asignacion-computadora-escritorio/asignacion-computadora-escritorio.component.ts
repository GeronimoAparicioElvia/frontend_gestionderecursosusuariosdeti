import { Component, OnInit , ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Equipo } from 'src/app/administracion/modelos/Inventario/Equipo/equipo';
import { EquipoService } from 'src/app/administracion/servicios/Inventario/Equipo/equipo.service';
import Swal from 'sweetalert2';
import { FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { UsuarioGlpiService } from 'src/app/administracion/servicios/Inventario/Usuario/usuario-glpi.service';
import { RegistroAsignacionEquipoService } from 'src/app/administracion/servicios/Asignacion_Equipo/registro-asignacion-equipo.service';
import { DetalleRegistroService } from 'src/app/administracion/servicios/Asignacion_Equipo/detalle-registro.service';
import { SolicitudAsigEquipoService } from 'src/app/administracion/servicios/Asignacion_Equipo/solicitud-asig-equipo.service';
import { RegistroAsignacionEquipo } from 'src/app/administracion/modelos/Asignacion_Equipo/registro-asignacion-equipo';
import { DetalleRegistro } from 'src/app/administracion/modelos/Asignacion_Equipo/detalle-registro';
import { UsuarioService } from 'src/app/administracion/servicios';
import { Empleado } from 'src/app/administracion/modelos/empleado';
import { Sucursal } from 'src/app/administracion/modelos/sucursal';

@Component({
  selector: 'app-asignacion-computadora-escritorio',
  templateUrl: './asignacion-computadora-escritorio.component.html',
  styleUrls: ['./asignacion-computadora-escritorio.component.scss']
})
export class AsignacionComputadoraEscritorioComponent implements OnInit {

  /*  Elemetnos de la vista */
  nombreTabla    : string = "MONITORES"
  tabla          : string = "Monitor"

  /** Elementos de asignación */
  indice:number = 2               /* Índice para saber en que tabla buscar                          */
  filtro:string = ""              /* Filtro para indicar que tipo de equipo buscar tabla de equipos */
  equipos:number [] = [0,0,0,0,0] /* Areglo con los Id's de los equipos a asignar*/
  
  /** Form Controls */
  monitor  = new FormControl({value: '', disabled: true}) /* Control con el que se valida que se seleccionó un moitor     */
  teclado  = new FormControl({value: '', disabled: true}) /* Control con el que se valida que se seleccionó un teclado    */
  mouse    = new FormControl({value: '', disabled: true}) /* Control con el que se valida que se seleccionó un mouse      */
  reulador = new FormControl({value: '', disabled: true}) /* Control con el que se valida que se seleccionó un regulador  */

  observaciones_cpu       = new FormControl()  /* Input con las observaciones o comentarios de la cpu     seleccionada */
  observaciones_monitor   = new FormControl()  /* Input con las observaciones o comentarios del monitor   seleccionado */
  observaciones_teclado   = new FormControl()  /* Input con las observaciones o comentarios del teclado   seleccionado */
  observaciones_mouse     = new FormControl()  /* Input con las observaciones o comentarios del mouse     seleccionado */
  observaciones_regulador = new FormControl()  /* Input con las observaciones o comentarios del regulador seleccionado */

  label_cpu      :string = "" /* Label con el nombre de la cpur  seleccionada   */
  label_monitor  :string = "" /* Label con el nombre del monitor seleccionado   */
  label_teclado  :string = "" /* Label con el nombre del teclado seleccionado   */
  label_mouse    :string = "" /* Label con el nombre del mouse seleccionado     */
  label_reulador :string = "" /* Label con el nombre del regulador seleccionado */

  bton_cpu      :string = "primary" /* Propiedad para cargar el color del botón Cpu       al elegirse un equipo de esta categoría  */
  bton_monitor  :string = "primary" /* Propiedad para cargar el color del botón Monitor   al elegirse un equipo de esta categoría  */
  bton_teclado  :string = "primary" /* Propiedad para cargar el color del botón Teclado   al elegirse un equipo de esta categoría  */
  bton_mouse    :string = "primary" /* Propiedad para cargar el color del botón Mouse     al elegirse un equipo de esta categoría  */
  bton_regulador:string = "primary" /* Propiedad para cargar el color del botón regulador al elegirse un equipo de esta categoría  */

  /*Elementos de la data de la tabla de equipos*/
  dataSource          = new MatTableDataSource<Equipo>([]); /* Data de la tabla      */
  @ViewChild(MatPaginator) paginator  !: MatPaginator       /* Paginator de la tabla */
  encabezados: string [] = ["OPCIONES","NOMBRE","NÚMERO DE SERIE","TIPO","MODELO","FABRICANTE"]  /* Encabezados genéricos */

  empleado_asignado!: Empleado; /* Modelo u objeto empleado al que se le va asignar los equipos*/

  /** Parámetros a capturar */
  id_cpu         !: number  
  comen_cpu      !: string
  nombre_cpu     !: string
  id_colaborador !: number
  nombre         !: string
  apellidos      !: string
  tipoAsignacion !: string

  constructor(private equipoService                   : EquipoService                   , /* Conexión con la tabla equipos de Glpi                            */
              private activatedRoute                  : ActivatedRoute                  , 
              private usuarioGlpiService              : UsuarioGlpiService              , /* Conexión con la tabla usuarios de Glpi                           */
              private router                          : Router                          ,
              private registroAsignacionEquipoService : RegistroAsignacionEquipoService , /* Conexión con la tabla registro asignacin de la base de datos     */
              private detalleResgistroService         : DetalleRegistroService          , /* Conexión con la tabla detalle de registro  de la base de datos   */
              private solicitudAsigEquipoService      : SolicitudAsigEquipoService      , /* Conexión con la tabla solicitudes de equipos de la base de datos */
              private usuarioService                  : UsuarioService                    /* Conexión con el endpoint empleados                               */
             ){}

  ngOnInit()
  {
    /* Capturamos los paràmetros enviados a travez de la URL */
    this.activatedRoute.params.subscribe(params =>{
      this.nombre         = params['nombre_empleado'    ];
      this.apellidos      = params['apellidos_empleado' ];
      this.id_cpu         = params['id_computadora'     ];
      this.nombre_cpu     = params['nombre_computadora' ];
      this.tipoAsignacion = params['asignacion'         ];
      this.id_colaborador = params['id_empleado'        ];
      this.comen_cpu      = params['comentariosCPU'     ];
    })

    /* Agregamos el label y el id de la Cpu enviado por la URL */
    this.label_cpu  = this.nombre_cpu
    this.equipos[0] =this.id_cpu

    /* Vaciamos los labels e inputs de los demas componentes  */
    this.observaciones_cpu      .setValue("" + this.comen_cpu)      
    this.observaciones_monitor  .setValue("")
    this.observaciones_teclado  .setValue("")
    this.observaciones_mouse    .setValue("")
    this.observaciones_regulador.setValue("")

    this.llenarData();
  }
 
  /* 
      ESTÉ MÉTODO CARGA LOS DATOS DEL EQUIPO SELECIONADO DEACUERDO A A TABLA EN LA QUE SE ENCUENTRE 
      EL CÓDIGO REPRESENTA LA TABLA Y LA POSICION EN EL ARRAY QUE RESUARDA EL TIO´P DE EQUIPO
      
      NÚM CÓDIGO        POS. ARRAY        TIPO DE EQUIPO
          1                 0                 CPU (COMPUTADORA)
          2                 1                 MONITOR
          3                 2                 TECLADO
          4                 3                 MOUSE
          5                 4                 REGULADOR
  */
  cargarTabla(codigo:number)
  {
    /*Compara el valor del recibido en los parámetros */
    switch (codigo) {
        /* De elegir las comutadoras, carga la tabla de equipos con las Cpu disponibles en ese momento */
        case 1:
            this.nombreTabla  = "COMPUTADORAS (CPU's)";   /*Cambiamos el nombre de la tabla indicando que equipos está mostrando  */
            this.filtro       = "CPU"                 /*Filtro para caragra solo las Cpu                                      */
            this.indice       = 1                         /* Aquí le decimos que el equipo seleccionado se irá a la posicion 1    */
            this.tabla        = "Computer"                /* Aquí le decimos que debe buscar en la tabla Computer                 */
            this.llenarData()
            
            break;
        case 2:
            this.nombreTabla  = "MONITORES";   /*Cambiamos el nombre de la tabla indicando que equipos está mostrando  */
            this.filtro       = "MON"          /*Filtro para caragra solo los Monitores                                */
            this.indice       = 2              /* Aquí le decimos que el equipo seleccionado se irá a la posicion 2    */
            this.tabla        = "Monitor"      /* Aquí le decimos que debe buscar en la tabla Monitor                  */
            this.llenarData()
            
            break;
        case 3:
            this.nombreTabla = "TECLADOS";   /*Cambiamos el nombre de la tabla indicando que equipos está mostrando  */
            this.filtro      = "TECLADO"     /*Filtro para caragra solo los Teclados                                 */
            this.indice=3                    /* Aquí le decimos que el equipo seleccionado se irá a la posicion 3    */
            this.tabla="Peripheral"          /* Aquí le decimos que debe buscar en la tabla Peripheral               */
            this.llenarData()
            
            break;
        case 4:
            this.nombreTabla = "MOUSES";   /*Cambiamos el nombre de la tabla indicando que equipos está mostrando  */
            this.filtro      = "MOUSE"     /*Filtro para caragra solo los Mouse's                                  */
            this.indice=4                  /* Aquí le decimos que el equipo seleccionado se irá a la posicion 4    */
            this.tabla="Peripheral"        /* Aquí le decimos que debe buscar en la tabla Peripheral               */
            this.llenarData()
            
            break;
        case 5:
            this.nombreTabla = "REGULADORES";   /*Cambiamos el nombre de la tabla indicando que equipos está mostrando  */
            this.filtro      = "REGULADOR"      /*Filtro para caragra solo los Reguladores                              */
            this.indice=5                       /* Aquí le decimos que el equipo seleccionado se irá a la posicion 5    */
            this.tabla="Peripheral"             /* Aquí le decimos que debe buscar en la tabla Peripheral               */
            this.llenarData()
            
            break;
    }
  }
  
  /*
     CUANDO SE DA CLICK EN EL EQUPO A ASIGNAR, SE CARGA EN EL ARRAY EL ID DEL EQUIPO Y SE AGREGA EL NOMBRE DEL 
     EQUIPO PARA SABER QUE EQUIPO A SE SELECCIONÓ 
  */
  seleccionarEquipo(nombre:string,id_equipo: number)
  {
    if(this.indice==1){ this.equipos[0]=id_equipo;  this.label_cpu      = nombre;}
    if(this.indice==2){ this.equipos[1]=id_equipo;  this.label_monitor  = nombre; this.bton_monitor   = "accent"}
    if(this.indice==3){ this.equipos[2]=id_equipo;  this.label_teclado  = nombre; this.bton_teclado   = "accent"}
    if(this.indice==4){ this.equipos[3]=id_equipo;  this.label_mouse    = nombre; this.bton_mouse     = "accent"}
    if(this.indice==5){ this.equipos[4]=id_equipo;  this.label_reulador = nombre; this.bton_regulador = "accent"}
  }

  /* Carga la tabla con los equipos disponibles, por defecto carga los monitores ya que en las pantallas 
    anteriores se seleccionó al Cpu*/
  llenarData()
  {
    /*Removemos el contenido de la tabla */
    let data_tabla:Equipo[]= this.dataSource.data
    data_tabla.splice(0,this.dataSource.data.length)
    this.dataSource.data=data_tabla

    let listaAux:Equipo[]=[]

    /* Realizamos la consulta con la tabla actual*/
    this.equipoService.todosLosEquipos(this.tabla).subscribe(
      lista=>
      { 
        /*Recorremos la consulta obtenida */
        lista.forEach(element=>{
          
          /* Filtramos los equipos disponibles */
          if(element.states_id == "Stock" || element.states_id == "No Asignado" || element.states_id == "Principal" || element.states_id == "Backup" || element.states_id == "" )
          {
            /*Computadoras */
            if(this.indice==1)
            {
              /* Cargamos en tipo y modelo del equipo los campos de tipo y modelo de una computadora */
              element.tipo=element.computertypes_id;element.modelo=element.computermodels_id
              /* Filtramos que sean solo cpu's */
              if((element.name).includes(this.filtro))
              {
                listaAux.push(element);
              }
            }
            /*Monitor */
            else if(this.indice==2)
            {
              element.tipo=element.monitortypes_id;element.modelo=element.monitormodels_id
              /* Filtramos que sean solo cpu's */
              if((element.name).includes(""))
              {
                listaAux.push(element);
              }
            }
            /*Regulador, teclado y mouse */
            else
            {
              /* Cargamos en tipo y modelo del equipo los campos de tipo y modelo de un periférico */
              element.tipo=element.peripheraltypes_id;element.modelo=element.peripheralmodels_id
              /*Filtra los tipos de equipo para el caso de regulador, teclado y mouse */  
              if((element.tipo+"").includes(this.filtro))
              {
                listaAux.push(element);
              }
            }
          }
        });
        /* En caso que no se tengan equipos disponibles se retorna un mensaje de error */
        if(listaAux.length==0)
        {
          Swal.fire({
            title:'Error',
            text:'No hay equipos disponibles, intentelo más tarde',
            icon:'error',
            timer:2000
          })
        }
        
        /* ENVIA LOS EQUIPOS DISPONIBLES AL DATA SOURCE */
        this.dataSource.data = listaAux; 
        this.dataSource.paginator=this.paginator
      }, 
      err=>
      {
        /* En caso de tenenr problemas para retornar los equipos, emerge un mensaje de error */
        Swal.fire({
          showConfirmButton: false,
          allowEnterKey:false,
          allowEscapeKey:false,
          allowOutsideClick:false,
          timer: 3000,
          icon: 'error',
          title: 'Sin conexión con el servidor, intentelo más tarde'
        })
      }
  )
    /*cargamos los datos del empleado */
    this.usuarioService.getAnEmpleado(this.id_colaborador).subscribe(
      data=>{this.empleado_asignado = data;},
      err=>/* En caso que se tenga problemas de conexión se retorna un mensaje de error */
      {
        Swal.fire({
          showConfirmButton: false,
          allowEnterKey:false,
          allowEscapeKey:false,
          allowOutsideClick:false,
          timer: 3000,
          icon: 'error',
          title: 'Error al cargar los datos del empleado, intentelo más tarde'
        })
      }
    )
  }

  /* Asigna los equipo seleccioandos al empleado */
  asignar(event:any)
  {
    /*Primero revisamos que todos los componentes fueran seleccionados */
    if(this.monitor.value!=null&&this.teclado.value!=null&&this.mouse.value!=null&&this.reulador.value!=null)
        {
          /*Comenzamos preguntando si se está seguro de realizar esta asignación */
          Swal.fire({
            title: '¿Está seguro de realizar esta asignación?',
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'SÍ',
            cancelButtonText:'NO'
          }).then((result) => {
            if (result.isConfirmed) {
      
              let sucursal_obj:Sucursal = new Sucursal() /* creamos un objeto para cuando la sucursal sea nula */
              sucursal_obj.nombreSucursal = "Sin sucursal a buscar"

              /* Creamos el registro general */
              let registroAsignacionEquipo: RegistroAsignacionEquipo = new RegistroAsignacionEquipo()
              registroAsignacionEquipo.id_registro_asignacion        = 0
              registroAsignacionEquipo.status_registro               = "REGISTRADO"
              registroAsignacionEquipo.colaborador_id                = this.id_colaborador
              registroAsignacionEquipo.fecha_asignacion              = new Date()
              registroAsignacionEquipo.fecha_modificacion            = new Date()
              registroAsignacionEquipo.nombre_empleado_asignado      = this.empleado_asignado.nombre      +
                                                                       " "                                +
                                                                       this.empleado_asignado.apellidoPat +
                                                                       " "                                +
                                                                       this.empleado_asignado.apellidoMat
              
              

              this.usuarioService.getAnEmpleado(this.id_colaborador).subscribe(empleado=>
                {
                  if(empleado.sucursal==null)/* valida si el empleado tiene el campo sucursal como nulo */
                  {
                    empleado.sucursal = sucursal_obj /* De tenerlo solo agrega el valor por defecto */
                  }
                  let body:string=""                
                  /* Consultamos la sucursal en Glpi */
                  this.equipoService.buscarSucursal(empleado.sucursal.nombreSucursal).subscribe(sucursales=>{ 
                    /* Consultamos al empleado en Glpi */
                    this.usuarioGlpiService.allUsuariosSearch(this.nombre,this.apellidos).subscribe(val=>{
                        /* validamos que las consultas no estèn vacías */
                        if(sucursales.length>0)/* Primero validamos las sucursales */
                        {
                          if(val.length>0)/* Ahora validamos al Empleado */
                          {
                            /*Actualizamos los equipos en Glpi */
                            body = "{\"input\": {\"states_id\": \" " + 1 + +" \", \"contact\": \" " + (empleado.puesto.nombrePuesto+"") +" \", \"users_id\": \" "+ val[0].id + " \", \"locations_id\": \" " + sucursales[0].id +"\"}}";
                            this.equipoService.actualizarItem("Computer"  ,this.equipos[0],body).subscribe(data=>{}, err=>{ this.mensajeDeerror()}) /* Cpu       */
                            this.equipoService.actualizarItem("Monitor"   ,this.equipos[1],body).subscribe(data=>{}, err=>{ this.mensajeDeerror()}) /* Monitor   */
                            this.equipoService.actualizarItem("Peripheral",this.equipos[2],body).subscribe(data=>{}, err=>{ this.mensajeDeerror()}) /* Teclado   */
                            this.equipoService.actualizarItem("Peripheral",this.equipos[3],body).subscribe(data=>{}, err=>{ this.mensajeDeerror()}) /* Mouse     */
                            this.equipoService.actualizarItem("Peripheral",this.equipos[4],body).subscribe(data=>{}, err=>{ this.mensajeDeerror()}) /* Regulador */ 


                            /*Validamos si se tiene una solicitud o si es una asignación directa */
                            if(this.tipoAsignacion=="directa")
                            {
                              /* De ser directa se realiza el post para guardar este registro general y el resultado es usado 
                                para asignarlo a los detalles de registro 
                              */
                              registroAsignacionEquipo.observaciones = "ASIGNACIÓN DIRECTA"
                              this.registroAsignacionEquipoService.asignacionD(registroAsignacionEquipo).subscribe(data=>{
                                this.generarDetalleRegistro(Number(data.id_registro_asignacion))

                                /*Desplegamos en pantalla un mensaje de confirmación */
                                this.mensajeDeAsignacionRealizada()
                              },
                              /*En caso de existir un error al realizarse la consulta retorna un mensaje de error */
                              err=>{this.mensajeDeerror()})
                            }
                            /** Para una asignacion que viene de solicitud enviamos el registro y el id de la solicitud */
                            else
                            {
                              /* De ser con solicitud se agrega el id de la solictud al realizar el post para guardar este registro general y el 
                                resultado es usado para asignarlo a los detalles de registro 
                              */
                              registroAsignacionEquipo.observaciones = "ASIGNACIÓN CON SOLICITUD"
                              let id_solicitud:number = Number(this.tipoAsignacion)
                              this.registroAsignacionEquipoService.asignacionI(registroAsignacionEquipo,id_solicitud).subscribe(data=>{
                                this.generarDetalleRegistro(Number(data.id_registro_asignacion))

                                /** Cambiamos el status de la solicitud por "APROBADO" */
                                this.aprobarSolicitud()

                                /*Desplegamos en pantalla un mensaje de confirmación */
                                this.mensajeDeAsignacionRealizada()
                              },
                              /*En caso de existir un error al realizarse la consulta retorna un mensaje de error */
                              err=>{this.mensajeDeerror()})
                            }

                          }
                          /* En caso de no recibir nada retornamos un mensaje de error */
                          else
                          {
                            this.mensajeDeError2("El empleado seleccionado no se encuentra registrado en Glpi")
                          }
                        }
                        /* En caso de no recibir nada retornamos un mensaje de error */
                        else
                        {
                          this.mensajeDeError2("La sucursal a la que pertenece el empleado no se encuentra registrada en Glpi")
                        }
                    })
                  },
                  /* En caso de no recibir nada retornamos un mensaje de error */
                  err=>{
                    this.mensajeDeerror()
                  })
                },
                /* En caso de no recibir nada retornamos un mensaje de error */
                err=>{
                  this.mensajeDeerror()
                }
              )
            }
          })    
    }
    /*En el improbable caso que no se selecione alguno de estos equipos retorna un mensajde error */
    else
    {
      Swal.fire({
        icon:'error',
        title:'Error',
        text:'No has seleccionado todos los componentes de la computadora',
        timer:1700
      })
    }
  }

  /*Cancela el proceso de asignacion de la computadora de escritorio */
  cancelar(event:any)
  {
    /* Pedimos confirmación para cancelar este proceso de asignación */
    Swal.fire({
      title: '¿Está seguro de cancelar está asignación?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'SÍ',
      cancelButtonText:'NO',
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false
    }).then((result) => {

      /* DE CONFIRMAR ESTA PREGUNTA NOS REDIRIGIMOS A LA VISTA QUE CORRESPONDA */
      if (result.isConfirmed) {
        
        /* SI VENIMOS DE LA ASIGNACIÓN DIRECTA NOS REDIRIGGE A LA VISTA DE ASIGNACIONES SDIRECTAS */
        if(this.tipoAsignacion=="directa"){this.router.navigate(['layout/asignacionDirecta'])}

        /**DE CONTAR CON SOLICITUD NOS REDIRIGE A VER LA GESTION DE SOLICITUDES */
        else{this.router.navigate(['layout/solicitudesDeEquipoAdministrador'])}
        
      }
    })
  }

  /*Método que cambia el status de la solicitud de equipo por APROBADO */
  aprobarSolicitud()
  {
    let id_solicitud: number = Number(this.tipoAsignacion) /*Cargamos el Id de la solicitud */
    this.solicitudAsigEquipoService.solicitudPorIdSolicitud(id_solicitud).subscribe(data=>{ /*Buscamos la solictud por su Id */
      data.fecha_mod        = new Date().toISOString()    /*  Agregamos la fecha de modificación */
      data.status_solicitud = "APROBADO"                  /*  Agregamos el nuevo estado */
      this.solicitudAsigEquipoService.update(data).subscribe(val=>{}, err=>{this.mensajeDeerror()}) /*Realizamos el cambio */
    })
  }

  /* GENERA LOS 5 DETALLES DE ASIGNACION DE UN EQUIPO DE ESCRITORIO */
  async generarDetalleRegistro(id_registro_asignacion:number)
  {
    /* CAPTURAMOS LAS OBSERVACIONES EN VARIABLES LOCALES */
    let ob_______cpu: string = this.observaciones_cpu.value + this.comen_cpu
    let ob___monitor: string = this.observaciones_monitor.value
    let ob___teclado: string = this.observaciones_teclado.value
    let ob_____mouse: string = this.observaciones_mouse.value
    let ob_regulador: string = this.observaciones_regulador.value


    /* VALIDAMOS SI TIENEN ALGO INGRESADO, DE NO TENERLO SE ASIGNA UNA LEYENDA POR DEFECTO */
    if(ob_______cpu == "" || ob_______cpu == undefined || ob_______cpu == null){ ob_______cpu = "SIN COMENTARIOS" }
    if(ob___monitor == "" || ob___monitor == undefined || ob___monitor == null){ ob___monitor = "SIN COMENTARIOS" }
    if(ob___teclado == "" || ob___teclado == undefined || ob___teclado == null){ ob___teclado = "SIN COMENTARIOS" }
    if(ob_____mouse == "" || ob_____mouse == undefined || ob_____mouse == null){ ob_____mouse = "SIN COMENTARIOS" }
    if(ob_regulador == "" || ob_regulador == undefined || ob_regulador == null){ ob_regulador = "SIN COMENTARIOS" }


    /* CREAMOS UN DETALLE QUE SERVIRA DE BASE PARA AGREGARLOS AL REGISTRO DEL EQUIPO DE ESCRITORIO */
    let detalleRegistro:DetalleRegistro     = new DetalleRegistro()
    detalleRegistro.status_detalle          = "ACTIVO"
    
    /* CPU*/
    detalleRegistro.equipo_id               = this.equipos[0]
    detalleRegistro.nombre_equipo           = this.nombre_cpu
    detalleRegistro.tipo_activo             = "COMPUTADORA"
    detalleRegistro.observ_equipo           = ob_______cpu
    this.detalleResgistroService.save(detalleRegistro,id_registro_asignacion)
    
    /* MONITOR   */
    detalleRegistro.nombre_equipo           = this.label_monitor
    detalleRegistro.equipo_id               = this.equipos[1]
    detalleRegistro.tipo_activo             = "MONITOR"
    detalleRegistro.observ_equipo           = ob___monitor
    this.detalleResgistroService.save(detalleRegistro,id_registro_asignacion)
    
    /* TECLADO   */
    detalleRegistro.nombre_equipo           = this.label_teclado
    detalleRegistro.equipo_id               = this.equipos[2]
    detalleRegistro.tipo_activo             = "DISPOSITIVO"
    detalleRegistro.observ_equipo           = ob___teclado
    this.detalleResgistroService.save(detalleRegistro,id_registro_asignacion)
    
    /* MOUSE     */
    detalleRegistro.nombre_equipo           = this.label_mouse
    detalleRegistro.equipo_id               = this.equipos[3]
    detalleRegistro.tipo_activo             = "DISPOSITIVO"
    detalleRegistro.observ_equipo           = ob_____mouse
    this.detalleResgistroService.save(detalleRegistro,id_registro_asignacion)
    
    /* REGULADOR */
    detalleRegistro.nombre_equipo           = this.label_reulador
    detalleRegistro.equipo_id               = this.equipos[4]
    detalleRegistro.tipo_activo             = "DISPOSITIVO"
    detalleRegistro.observ_equipo           = ob_regulador
    this.detalleResgistroService.save(detalleRegistro,id_registro_asignacion)
  }

  /* Despliega una ventana con información de l ventana */
  clickMasInformacion(event:any)
  {
    Swal.fire({
      title:'Asignación de computadora de escritorio',
      html:
      
      '<p align="justify"> Inicialmente se carga la CPU del equipo. Ahora solo hace falata elegir '     +
      'el resto de componentes: <b>MONITOR</b>, <b>TECLADO</b>, <b>MOUSE</b> Y <b>REGULADOR</b>.</p>'   +

      '<p align="justify">Los botones que se encuentran arriba de la tabla de selección '+
      ' cargan los equipos disponibles.</p>'                                             +

      '<p align="justify">Para elegir los componentes solo hay que dar click en los botones que se '      +
      'encuentran al inicio de cada fila de la tabla. Igualmente se pueden agregar observaciones o coment'+
      'arios a cada componente.</p>'+

      '<p align="justify">Para terminar la asignación seleccione el botón azul con la etiqueta <b>Asignar</b>.</p>',
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      confirmButtonColor:'#d33',
      confirmButtonText:'Cerrar'
    })
  }

  /* Habilita el botón Asignar */
  habilitarBotonGuardar()
  {
    /*Valida que todos los equipos fueran seleccionados */
    if(
        this.label_cpu      != "" &&
        this.label_monitor  != "" &&
        this.label_teclado  != "" &&
        this.label_mouse    != "" &&
        this.label_reulador != "" 
      )
    {
      return true;
    }
    return false;
  }

  /*Método que despiega un mensaje de error de conexión */
  mensajeDeerror()
  {
    Swal.fire({
      icon: 'error',
      title: 'Sin conexión, intentelo más tarde',
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      showConfirmButton: false,
      timer: 3000,
    })
  }
  /*Método que despiega un mensaje de error de conexión */
  mensajeDeError2(texto:string)
  {
    Swal.fire({
      icon: 'error',
      title: 'Error',
      text:texto,
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      showConfirmButton: false,
      timer: 3000,
    })
  }

  mensajeDeAsignacionRealizada()
  {
    /* Mostramos un mensaje de confirmación */
    Swal.fire({
      icon:'success',
      title:'Asignación realizada con éxito',
      showConfirmButton:false,
      timer:2000
    }).then((r)=>{
      /** Nos redirigimos a otra vista donde mostramos los registros sin subir responsiva */
      this.router.navigate(['layout/seguimientoAsignacionEquipo'])
    })
  }
}
