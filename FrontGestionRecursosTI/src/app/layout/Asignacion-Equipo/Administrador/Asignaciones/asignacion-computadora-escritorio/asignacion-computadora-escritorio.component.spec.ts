import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignacionComputadoraEscritorioComponent } from './asignacion-computadora-escritorio.component';

describe('AsignacionComputadoraEscritorioComponent', () => {
  let component: AsignacionComputadoraEscritorioComponent;
  let fixture: ComponentFixture<AsignacionComputadoraEscritorioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AsignacionComputadoraEscritorioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignacionComputadoraEscritorioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
