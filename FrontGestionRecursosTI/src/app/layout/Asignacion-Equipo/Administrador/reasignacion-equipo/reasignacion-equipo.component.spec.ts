import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReasignacionEquipoComponent } from './reasignacion-equipo.component';

describe('ReasignacionEquipoComponent', () => {
  let component: ReasignacionEquipoComponent;
  let fixture: ComponentFixture<ReasignacionEquipoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReasignacionEquipoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReasignacionEquipoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
