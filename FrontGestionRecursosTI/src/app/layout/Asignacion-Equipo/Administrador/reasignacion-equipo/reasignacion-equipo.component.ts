import { Component, OnInit , QueryList, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import Swal from 'sweetalert2';
/**Modelo */
import { Empleado } from 'src/app/administracion/modelos/empleado';
import { Equipo } from 'src/app/administracion/modelos/Inventario/Equipo/equipo';
/** Servicios */
import { DetalleRegistroService } from 'src/app/administracion/servicios/Asignacion_Equipo/detalle-registro.service';
import { EquipoService } from 'src/app/administracion/servicios/Inventario/Equipo/equipo.service';
import { UsuarioGlpiService } from 'src/app/administracion/servicios/Inventario/Usuario/usuario-glpi.service';
import { UsuarioService } from 'src/app/administracion/servicios';
import { RegistroAsignacionEquipo } from 'src/app/administracion/modelos/Asignacion_Equipo/registro-asignacion-equipo';
import { takeUntil } from 'rxjs/operators';
import { ReplaySubject, Subject } from 'rxjs';
import { Sucursal } from 'src/app/administracion/modelos/sucursal';


@Component({
  selector: 'app-reasignacion-equipo',
  templateUrl: './reasignacion-equipo.component.html',
  styleUrls: ['./reasignacion-equipo.component.scss']
})
export class ReasignacionEquipoComponent implements OnInit {

  /* Data de la tabla*/
  encabezados:string[] = ["OPCIONES","NOMBRE","TIPO","MODELO","SERIE","FABRICANTE"] /* Encabezados genéricos  */
  dataSource           = new MatTableDataSource<Equipo>([]);                    /* Data de la tabla       */
  @ViewChild('paginatorS', { static: true }) public paginatorS !: MatPaginator; /* Paginator de la tabla  */
 
  /* inputs de los nombres */
  input_Empleado_libera: FormControl = new FormControl(); /* Nombre del empleado al que se le remueven los equipos */
  input_Empleado_reasig: FormControl = new FormControl(); /* Nombre del empleado que recibe los equipos            */

  /*  Modelos y objetos empleado  */
  empleado_libera:Empleado = new Empleado();  /* Empleado al que se le remueven los equipos     */
  empleado_reasig:Empleado = new Empleado();  /* Empleado al que se le van asignar los equipos  */

  /* Listas de empleados para los inputs */
  listaColaboradores_Salida            :   Empleado [] = [];
  listaFiltradaColaboradores_Salida    :   ReplaySubject<Empleado[]> = new ReplaySubject<Empleado[]>(1);

  listaColaboradores_Reasig            :   Empleado [] = [];
  listaFiltradaColaboradores_Reasig    :   ReplaySubject<Empleado[]> = new ReplaySubject<Empleado[]>(1);

  protected _onDestroy  = new Subject<void>();
  protected _onDestroy2 = new Subject<void>();

  input_colaboradorSalida:FormControl=new FormControl()
  input_colaboradorRecibe:FormControl=new FormControl()

  /*Lista de las tablas de equipo */
  listaTablas       : string    [] = ["Computer","Monitor","Networkequipment","Peripheral","Printer","Rack","Phone"]

  constructor(  private detalleRegistroService          : DetalleRegistroService          , /* Conexión con la tabla detalle registro en la base de datos     */
                private equipoService                   : EquipoService                   , /* Conexión con la tabla Equipo de Glpi                           */
                private usuarioGlpiService              : UsuarioGlpiService              , /* Conexión con la tabla Usuario de Glpi                          */
                private usuarioService                  : UsuarioService                    /* Conexión con el end point empleados                            */
             )
                {}

  ngOnInit()
  {
    this.llenarListasUsuario()  

    this.listaFiltradaColaboradores_Salida.next(this.listaColaboradores_Salida.slice());
    this.input_colaboradorSalida.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterEmpleadoLibera();
      });

    this.listaFiltradaColaboradores_Reasig.next(this.listaColaboradores_Reasig.slice());
    this.input_colaboradorRecibe.valueChanges
      .pipe(takeUntil(this._onDestroy2))
      .subscribe(() => {
        this.filterEmpleadoRecibe();
      });
  }

  /*Valida si ya se seleccionó un equipo de la tabla  */
  dataSeleccionada()
  {
    let data : number = 0                   /* Número de equipos seleccionados                    */
    this.dataSource.data.forEach(equipo=>{  /* Recorre la lista de equipos en la tabla            */
      if(equipo.bandera==true)              /* Valida que el equipo fue seleccionado              */
      {
        data = data + 1                     /*De estar seleccioando aumenta el número de la data  */
      }
    })

    /* Validamos si el nùmero de equipos seleccionados es mayor a 0 para retornar un verdadero */
    if (data>0){return true}
    /*Si no cumple la condición simplemente retorna un falso*/
    else{return false}
  }

  /* Craga las listas de empleados */
  llenarListasUsuario()
  {

    /*  Realziamos la consulta para llamara todos los empleados */
    this.usuarioService.getAllEmpleados().subscribe(lista=>{ 
      
      /*  Recorremos la consulta y construimos los nombres completos */
      lista.forEach(empleado=>{
        empleado.nomb_compl = empleado.nombre + " " + empleado.apellidoPat + " " + empleado.apellidoMat
      })

      /*
        colocamos la consulta en las listas de empleados para los inputs
      */
      this.listaColaboradores_Salida  = lista; 
      this.listaColaboradores_Reasig  = lista;
    },
    /* Si existe un error en la consulta retorna un mensaje de error */
    err=>{
      Swal.fire({
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        icon: 'error',
        title: 'Error al cargar la lista de empleados, intentelo más tarde',
        allowEnterKey:false,
        allowEscapeKey:false,
        allowOutsideClick:false
      })
      
    }) 
  }

  /* Carga la data del empleado seleccionado y los equipos que tiene asignados */
  llenarData(evet:any)
  {
    /*
      CUANDO SE SELECCIONE AL EMPLEADO QUE VA A LIBERAR EQUIPOS SE VA A REALIAR LO SIGUIENETE:

        1. UNO DE LOS 2 OBJETOS EMPLEADO DECLARADOS AL INICIO VA A CONTENER EL OBJETO EMPLEADO QUE SE SELECCIONÓ.
        2. AL INPUT SE LE VA A ASIGNAR EL VALOR DEL NOMBRE COMPLETO DE ESTE EMPLEADO
        3. EN BASE AL NOMBRE YY APELLIDOS SE VA A BUSCAR EL ID DE ESTE EMPLEADO EN LA BD DE GLPI
        4. SE VA A REALIAR UNA CONSULTA
     */

    /*INGRESAMOS EL VALOR DEL OBJETO EMPLEEADO EN EL OBJ EMPLEADO DECLARAO AL INICIO */
    this.empleado_libera = this.input_Empleado_libera.value

    /*AÑADIMOS EL NOMBRE COMPLETO AL VALOR DEL FORM CONTROL DEL INPUT */
    //this.input_Empleado_libera.setValue(this.empleado_libera.nomb_compl);
    let lista_equipos: Equipo [] = []
    
    /**BUSCAMOS EL ID DEL EMPLEADO EN LA BD DE GLPI BASANDONOS EN EL NOMBRE EY APELLIDOS */
    this.usuarioGlpiService.allUsuariosSearch( (this.empleado_libera.nombre + "") , (this.empleado_libera.apellidoPat + " " + this.empleado_libera.apellidoMat ) ).subscribe(usuario=>{
      
      /*REALIZAMOS UN FOR EACH A UN ARREGLO QUE CONTIENE EL NOMNRE DE LA TABLA QUE VAMOS A CONSULTAR*/
      this.listaTablas.forEach(tabla=>{

        /*
          AL RESULTADO DE LA CONSULTA DE IGUAL FORMA LE REALIAMOS UN FOR EACH PARA COMPARAR SI EL EQUIPO ESTA 
          ASIGNADO AL COLABORADOR INGRESADO.
        */
        this.equipoService.todosLosEquipos(tabla).subscribe(lista_Equipos=>{
          lista_Equipos.forEach(equipo=>{

            /**EN CASO EN QUE SI TENGA ASIGNADO ESTE EQUIPO SE PROCEDE A ASIGNARLE LA TABLA A LA QUE PRTENECE Y EL TIPO DE EQUIPO PARA AÑADIRLO EN UN */
            if(equipo.users_id == usuario[0].name)
            {
                   if(tabla=="Computer"         ){ equipo.tipo = "COMPUTADORA"         ; equipo.tabla ="Computer"        ; equipo.modelo = equipo.computermodels_id        }
              else if(tabla=="Networkequipment" ){ equipo.tipo = "DISPOSITIVO DE RED"  ; equipo.tabla ="Networkequipment"; equipo.modelo = equipo.networkequipmentmodels_id}
              else if(tabla=="Printer"          ){ equipo.tipo = "IMPRESORA"           ; equipo.tabla ="Printer"         ; equipo.modelo = equipo.printermodels_id         }
              else if(tabla=="Monitor"          ){ equipo.tipo = "MONITOR"             ; equipo.tabla ="Monitor"         ; equipo.modelo = equipo.monitormodels_id         }
              else if(tabla=="Peripheral"       ){ 
                
                
                if(equipo.peripheraltypes_id == "PROYECTOR")
                {
                  equipo.tipo = equipo.peripheraltypes_id; 
                  equipo.tabla ="Peripheral"; 
                  equipo.modelo = equipo.peripheralmodels_id      
                }
                else
                {
                  equipo.tipo = "DISPOSITIVO"; 
                  equipo.tabla ="Peripheral"; 
                  equipo.modelo = equipo.peripheralmodels_id      
                }
                
              }
              else if(tabla=="Rack"             ){ equipo.tipo = "RACK"                ; equipo.tabla ="Rack"            ; equipo.modelo = equipo.rackmodels_id            }
              else if(tabla=="Phone"            ){ equipo.tipo = "TELEFONO"            ; equipo.tabla ="Phone"           ; equipo.modelo = equipo.phonemodels_id           }
              lista_equipos.push(equipo)
            }

            /*  FINALEMNETE ESOS EQUIPOS SON ENVIADOS AL DATA SOURCE DE LA PRIMER TABLA PERTENECIENTE AL PRIMER EMPLEADO */
            this.dataSource.data = lista_equipos
            this.dataSource.paginator = this.paginatorS
          })

        })
      })
    },
    err=>{  
      Swal.fire({
        icon:'error',
        title:'Error al cargar los datos del empleado, intentelo más tarde',
        timer:2000,
        allowEnterKey:false,
        allowEscapeKey:false,
        allowOutsideClick:false,
        showConfirmButton:false
      })  
    })
  }

  /*  Carga la data del empleado que va a recibir los equipos */
  clickSeleccionEmpleadoR(event:any)
  {
    this.empleado_reasig = this.input_Empleado_reasig.value
  }

  /*Evento que inicia la reasignación de los equipos*/
  clickReasignar(event:any)
  {
    /*  Solicitamos una confirmación de esta opción */
   Swal.fire({
     icon: 'warning',
     titleText:'¿Está seguro de reasignar estos equipos?',
     showCancelButton: true,
     confirmButtonColor: '#3085d6',
     cancelButtonColor: '#d33',
     confirmButtonText: 'SÍ',
     cancelButtonText:'NO'
   }).then((result) => { 
     if(result.isConfirmed)
     {


      let sucursal_obj:Sucursal = new Sucursal() /* creamos un objeto para cuando la sucursal sea nula */
      sucursal_obj.nombreSucursal = "Sin sucursal a buscar"

      /*  Confirmado la operación comenzamos con lo siguiente */
      let lista_nombre__equipos :string[] =[] /* declaramos una lista para los nombres              */
      let lista_tipos_deactivos :string[] =[] /* declaramos una lista para los tipos de los equipos */
      let lista___ids___equipos :number[] =[] /* declaramos una lista de los id`s de los equipos    */
      let lista_____comentarios :string[] =[] /* declaramos una lista con los comentarios           */
      let lista__tabla___equipo :string[] =[] /* declaramos una lista con la tabla de cada equipo   */

      /* Recorremos la data para buscar los equipos seleccionados */
      this.dataSource.data.forEach(equipo=>{

        if(equipo.bandera==true)  /* De cumplir la condicion agregamos el equipo a reasignar */
        {
          lista_nombre__equipos.push(equipo.name      ) /* Nombres de los equipos     */
          lista_tipos_deactivos.push(equipo.tipo+""   ) /* Tipo de los equipos        */
          lista___ids___equipos.push(Number(equipo.id)) /* Id de los equipos          */
          lista_____comentarios.push(equipo.comment+"") /* Comentarios de los equipos */
          lista__tabla___equipo.push(equipo.tabla+""  ) /*  tabla de los equipos      */
        }
      })

      let id_administrador:number = JSON.parse(localStorage.getItem("empleado") || '{}').id
      let nombre_____admin:string = JSON.parse(localStorage.getItem("empleado") || '{}').nombreEmpleado + " " + 
                                    JSON.parse(localStorage.getItem("empleado") || '{}').apellidoPat + " " +
                                    JSON.parse(localStorage.getItem("empleado") || '{}').apellidoMat 
      
                                    /* creamos un nuevo registro de asignacion */
      /* Creamos el registro general */
      let registroAsignacionEquipo: RegistroAsignacionEquipo = new RegistroAsignacionEquipo()
      registroAsignacionEquipo.id_registro_asignacion        = 0
      registroAsignacionEquipo.status_registro               = "REGISTRADO"
      registroAsignacionEquipo.colaborador_id                = this.empleado_reasig.idEmpleado
      registroAsignacionEquipo.fecha_asignacion              = new Date()
      registroAsignacionEquipo.fecha_modificacion            = new Date()
      registroAsignacionEquipo.observaciones                 = "REASIGNACION DE EQUIPO"
      registroAsignacionEquipo.nombre_empleado_asignado      = this.empleado_reasig.nombre        +
                                                               " "                                +
                                                               this.empleado_reasig.apellidoPat   +
                                                               " "                                +
                                                               this.empleado_reasig.apellidoMat
      registroAsignacionEquipo.administrador_TI              = id_administrador
      registroAsignacionEquipo.nombre_empleado_registra      = nombre_____admin

      if(this.empleado_reasig.sucursal==null)/* valida si el empleado tiene el campo sucursal como nulo */
      {
        this.empleado_reasig.sucursal = sucursal_obj /* De tenerlo solo agrega el valor por defecto */
      }
      
      /* Consultamos el Id del empleado en Glpi*/
      this.usuarioGlpiService.allUsuariosSearch
        (
          this.empleado_reasig.nombre, 
          (this.empleado_reasig.apellidoPat + " " +this.empleado_reasig.apellidoMat)
        ).subscribe(usuarios=>{

          /* Enviamos toda la data al metodo de reasignación del back */
          this.equipoService.buscarSucursal(this.empleado_reasig.sucursal.nombreSucursal).subscribe(sucursal=>{

            if(usuarios.length>0) /* validamos que el empleado se encuentre en glpi */
            {
              if(sucursal.length>0) /* validamos que la sucursal se encuentre en glpi */
              {
                this.detalleRegistroService.reasignarEquipos
                (
                  lista_nombre__equipos,
                  lista_tipos_deactivos,
                  lista___ids___equipos,
                  lista_____comentarios,
                  lista__tabla___equipo,
                  Number(sucursal[0].id),
                  usuarios[0].id+"",
                  this.empleado_reasig.puesto.nombrePuesto+"",
                  registroAsignacionEquipo
                )
              }
              else /* Si no encuentra la sucursal retorna un mensaje de error */
              {
                this.mensajeDeError("La sucursal a la que pertenece el empleado que recibiá los equipos no se encuentra registrada en Glpi")
              }
            }
            else /* Si no encuentra al empleado retorna un mensaje de error */
            {
              this.mensajeDeError("El empleado  que recibiá los equipos no se encuentra registrado en Glpi")
            }   
          },err=>
          {
            Swal.fire({
              allowEnterKey:false,
              allowEscapeKey:false,
              allowOutsideClick:false,
              showConfirmButton: false,
              timer: 3000,
              icon: 'error',
              title: 'Sin conexión, intentelo más tarde'
            })
          }
        )
      })

     }
   })
  }

  /* Selecciona todos los equipos de la tabla */
  clickCheckAll(valor:boolean)
  {
    /* HABILITA LA CASILLA EN CADA FILA EN CASO QUE HABILIT EL CHECK DE TODOS */
    if(valor== true)
    {
      this.dataSource.data.forEach(equipo=>{
      
        equipo.bandera = true
        
      })
    }
    /* DESHABILITA LA CASILLA EN CADA FILA EN CASO QUE DESHABILITE EL CHECK DE TODOS */
    else
    {
      this.dataSource.data.forEach(equipo=>{
      
        equipo.bandera = false
        
      })
    }
  }

  /* Método que despliega una ventana con información sobre la pantalla actual*/
  clickMasInformacion(event:any)  
  {
    
    Swal.fire({
      title:'Reasignación de equipos',
      html:
      '<p align="justify"><b>-</b> Comenzamos ingresando el nombre del empleado al que vamos a removerle los equipos.</p>' +
      '<p align="justify"><b>-</b> Ahora ingresamos el nombre del empleado que va a recibir los equipos.</p>' +
      '<p align="justify"><b>-</b> Elegimos los equipos que vamos a transferir dando click en el botón azul  de inicio de cada fila.</p>'+
      '<p align="justify"><b>-</b> Si queremos remover un equipo de la lista de equipos a reasignar, damos click en el botón rojo de inicio de cada fila.</p>'+
      '<p align="justify"><b>-</b> Una vez seleccionados todos los equipos a transferir damos click en el botón reasignar,'+
      ' donde solo se tiene que confirmar la transferencia.</p>'+
      '<p align="justify"><b>NOTA: </b>Las computadoras de escritorio son transferidas completas con tan solo seleccionar un componente.</p>',
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      confirmButtonColor: '#d33',
      confirmButtonText:'Cerrar'
    })
  }

  /*Habilita el check que selecciona todos los equipos */
  habilitarCheckAll()
  {
    if(this.dataSource.data.length>0)
    {
      return true
    }
    return false
  }

  /* Habilita el botòn pare reasignar equipos */
  habilitarBotonesReasignacion()
  {
    if( 
        this.empleado_reasig.apellidoPat!=undefined && 
        this.dataSeleccionada()                     
      )
    {
      return false;
    }
    return true;
  }

  protected filterEmpleadoLibera() {
    if (!this.listaColaboradores_Salida) {
      return;
    }
    /* Recuperamos el valor del input */
    let search = this.input_colaboradorSalida.value;
    if (!search) {
      this.listaFiltradaColaboradores_Salida.next(this.listaColaboradores_Salida.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    /* Retornamos sla lista de eempleados que cumplen con las coincidencias */
    this.listaFiltradaColaboradores_Salida.next(
      this.listaColaboradores_Salida.filter(empleado => empleado.nomb_compl.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterEmpleadoRecibe() {
    if (!this.listaColaboradores_Reasig) {
      return;
    }
    /* Recuperamos el valor del input */
    let search = this.input_colaboradorRecibe.value;
    if (!search) {
      this.listaFiltradaColaboradores_Reasig.next(this.listaColaboradores_Reasig.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    /* Retornamos sla lista de eempleados que cumplen con las coincidencias */
    this.listaFiltradaColaboradores_Reasig.next(
      this.listaColaboradores_Reasig.filter(empleado => empleado.nomb_compl.toLowerCase().indexOf(search) > -1)
    );
  }

  /* Filtro de la barra de búsqueda de la tabla de equipos */
  filtroDataDourceEquipos(event: Event) 
  {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  mensajeDeError(texto:string)
  {
    Swal.fire({
      title:'Error',
      text:texto,
      icon:'error',
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      showConfirmButton:false,
      timer:3000
    })
  }
}