import { Component, OnInit , QueryList, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import Swal from 'sweetalert2';
/**Modelo */
/** Servicios */
import { Router } from '@angular/router';
import { SolicitudAsigEquipoService } from 'src/app/administracion/servicios/Asignacion_Equipo/solicitud-asig-equipo.service';
import { SolicitudAsigEquipo } from 'src/app/administracion/modelos/Asignacion_Equipo/solicitud-asig-equipo';
import { UsuarioService } from 'src/app/administracion/servicios';
import { Sucursal } from 'src/app/administracion/modelos/sucursal';
import { Perfil } from 'src/app/administracion/modelos/perfil';

@Component({
  selector: 'app-gestion-solicitudes-equipo-sdmin',
  templateUrl: './gestion-solicitudes-equipo-sdmin.component.html',
  styleUrls: ['./gestion-solicitudes-equipo-sdmin.component.scss']
})
export class GestionSolicitudesEquipoSdminComponent implements OnInit {

  /*  Encabezados genéricos */
  encabezadostabla   : String [] = ["OPCIONES","EQUIPO SOLICITADO","COLABORADOR","CARGO DEL COLABORADOR","OFICINA/SUCURSAL","SOLICITANTE","FECHA DE SOLICITUD"]
  encabezadostablaA  : String [] = ["OPCIONES","STATUS","EQUIPO SOLICITADO","COLABORADOR","CARGO DEL COLABORADOR","OFICINA/SUCURSAL","SOLICITANTE","FECHA DE ATENCIÓN"]
  encabezadostablaC  : String [] = ["OPCIONES","EQUIPO SOLICITADO","COLABORADOR","CARGO DEL COLABORADOR","OFICINA/SUCURSAL","SOLICITANTE","FECHA DE CANCELACIÓN"]
  
  /** Data source de las tablas */  
  dataSourceP          = new MatTableDataSource<SolicitudAsigEquipo>([])  /* Solcitudes pendientes */
  dataSourceA          = new MatTableDataSource<SolicitudAsigEquipo>([])  /* Solcitudes Atendidas  */
  dataSourceC          = new MatTableDataSource<SolicitudAsigEquipo>([])  /* Solcitudes Canceladas */

  /*  Paginator de cada tabla */
  @ViewChild('paginatorP', { static: true }) public paginatorP !: MatPaginator; /* Paginator Pendientes */
  @ViewChild('paginatorA', { static: true }) public paginatorA !: MatPaginator; /* Paginator Atendidas  */
  @ViewChild('paginatorC', { static: true }) public paginatorC !: MatPaginator; /* Paginator Cancelados */

  /* Constructor */
  constructor( 
                private solicitudAsigEquipoService  : SolicitudAsigEquipoService, /*Conexión con la tabla solicitudes de la base de datos */
                private empleadoService             : UsuarioService            , /*Conexión con el end point  Usuarios                   */
                private router                      : Router 
             ) {}

  ngOnInit()
  {
    this.llenarDataP()
  }

  /* Método para llenar las solicitudes pendientes */
  llenarDataP()
  {
    let sucursal:Sucursal = new Sucursal()    /* Obj sucursal auxiliar */
    sucursal.nombreSucursal="Sin información"
    let puesto:Perfil= new Perfil()           /* Obj puesto auxiliar */
    puesto.nombrePuesto="Sin información"

    /**MANDA A LLAMAR TODAS LAS SOLICITUDES REALIZADAS QUE ESTAN EN ESPERA  */
    this.solicitudAsigEquipoService.todasLasSolicitudesPorStatus(1).subscribe(lista=>{
      lista.forEach(element=>{

        /**SE REALIA UN CICLO FOR PARA AGREGAR LOS DATOS DELL SOLICITANTE Y EL EMPLEADO MENCIONADO EN LA SOLICITUD */
        element.fecha_solicitud = element.fecha_solicitud?.substring(8,10)+"-"+element.fecha_solicitud?.substring(5,7)+"-"+element.fecha_solicitud?.substring(0,4);  
        this.empleadoService.getAnEmpleado(Number(element.solicitante_id)).subscribe(val=>{element.name_solicitante_id = val.nombre+" "+val.apellidoPat+" "+val.apellidoMat})
        this.empleadoService.getAnEmpleado(Number(element.colaborador_id)).subscribe(val=>
          {
            if(val.puesto==null){ val.puesto=puesto }
            if(val.sucursal==null){ val.sucursal=sucursal }
            element.name_colaborador_id = val.nombre+" "+val.apellidoPat+" "+val.apellidoMat
            element.name_cargo_colaborador=val.puesto.nombrePuesto
            element.colaborador=val
          }
        )
      })
      
      /*SE CARGA LA DATA EN LA TABLA CORRESPONDIENTE */
      this.dataSourceP.data=lista
      this.dataSourceP.paginator=this.paginatorP
    }, 
    err=>
    {
      Swal.fire({
        icon:'error',
        title:'Sin conexión con el servidor, intentelo más tarde',
        timer:2000,
        allowEnterKey:false,
        allowEscapeKey:false,
        allowOutsideClick:false,
        showConfirmButton:false
      })
    })
  }

  /* Método para llenar las solicitudes Atendidas */
  llenarDataA()
  {
    let sucursal:Sucursal = new Sucursal()    /* Obj sucursal auxiliar */
    sucursal.nombreSucursal="Sin información"
    let puesto:Perfil= new Perfil()           /* Obj puesto auxiliar */
    puesto.nombrePuesto="Sin información"

    /**MANDA A LLAMAR TODAS LAS SOLICITUDES REALIZADAS QUE YA FUERON ATENDIDAS  */
    this.solicitudAsigEquipoService.todasLasSolicitudesPorStatus(3).subscribe(
      lista=>
      {
        lista.forEach(element=>{ 
          
          /**SE REALIA UN CICLO FOR PARA AGREGAR LOS DATOS DELL SOLICITANTE Y EL EMPLEADO MENCIONADO EN LA SOLICITUD */ 
          this.empleadoService.getAnEmpleado(Number(element.solicitante_id)).subscribe(val=>{element.name_solicitante_id = val.nombre+" "+val.apellidoPat+" "+val.apellidoMat})
          this.empleadoService.getAnEmpleado(Number(element.colaborador_id)).subscribe(val=>
          {
            if(val.puesto==null){ val.puesto=puesto }
            if(val.sucursal==null){ val.sucursal=sucursal }
            element.name_colaborador_id = val.nombre+" "+val.apellidoPat+" "+val.apellidoMat
            element.name_cargo_colaborador=val.puesto.nombrePuesto
            element.colaborador=val
          })
          element.fecha_mod = element.fecha_mod?.substring(8,10)+"-"+element.fecha_mod?.substring(5,7)+"-"+element.fecha_mod?.substring(0,4);  
        })
        
        /*SE CARGA LA DATA EN LA TABLA CORRESPONDIENTE */
        this.dataSourceA.data=lista
        this.dataSourceA.paginator=this.paginatorA
      }, err=>
      {
        Swal.fire({
          icon:'error',
          title:'Sin conexión con el servidor, intentelo más tarde',
          timer:2000,
          allowEnterKey:false,
          allowEscapeKey:false,
          allowOutsideClick:false,
          showConfirmButton:false
        })
      }
    )
  }
  
  /* Método para llenar las solicitudes Canceladas */
  llenarDataC()
  {
    let sucursal:Sucursal = new Sucursal()    /* Obj sucursal auxiliar */
    sucursal.nombreSucursal="Sin información"
    let puesto:Perfil= new Perfil()           /* Obj puesto auxiliar */
    puesto.nombrePuesto="Sin información"

    /**MANDA A LLAMAR TODAS LAS SOLICITUDES REALIZADAS QUE FUERON CANCELADAS  */
    this.solicitudAsigEquipoService.todasLasSolicitudesPorStatus(2).subscribe(lista=>{
      lista.forEach(element=>{  
        
        /**SE REALIA UN CICLO FOR PARA AGREGAR LOS DATOS DELL SOLICITANTE Y EL EMPLEADO MENCIONADO EN LA SOLICITUD */
        this.empleadoService.getAnEmpleado(Number(element.solicitante_id)).subscribe(val=>{element.name_solicitante_id = val.nombre+" "+val.apellidoPat+" "+val.apellidoMat})
        this.empleadoService.getAnEmpleado(Number(element.colaborador_id)).subscribe(val=>
        {
          if(val.puesto==null){ val.puesto=puesto }
          if(val.sucursal==null){ val.sucursal=sucursal }
          element.name_colaborador_id = val.nombre+" "+val.apellidoPat+" "+val.apellidoMat
          element.name_cargo_colaborador=val.puesto.nombrePuesto
          element.colaborador=val
        })
        element.fecha_mod = element.fecha_mod?.substring(8,10)+"-"+element.fecha_mod?.substring(5,7)+"-"+element.fecha_mod?.substring(0,4);  
      })

      /*SE CARGA LA DATA EN LA TABLA CORRESPONDIENTE */
      this.dataSourceC.data=lista
      this.dataSourceC.paginator=this.paginatorC
    }, 
    err=>
    {
      Swal.fire({
        icon:'error',
        title:'Sin conexión con el servidor, intentelo más tarde',
        timer:2000,
        allowEnterKey:false,
        allowEscapeKey:false,
        allowOutsideClick:false,
        showConfirmButton:false
      })
    }
    )
  }

  /* Redirige a otra pantalla para ver a detalle la solicitud */
  clickVerDetalle(id_solicitud:number,status_solicitud:string)
  {
    this.router.navigate(['layout/detalleSolicitudEquipoAdministrador/'+id_solicitud+'/'+status_solicitud]);
  }
  
  /*carga las solicitudes segun sea la pestaña seleccionada */
  myTabSelectedIndexChange(index: number) 
  { 
    /* Carga la tabla de solicitudes atendidas */
    if(index==1 ){ if(this.dataSourceA.data.length==0){ this.llenarDataA() } }
    /* Carga la tabla de solicitudes canceladas */
    else if(index==2 ){ if(this.dataSourceC.data.length==0){ this.llenarDataC() } }
  }

  /*Abrre una ventana con información sobre la pantalla */
  clickMasInformacion(event:any)  
  {

    Swal.fire({
      title:'Administración de solicitudes de equipo',
      html:
      '<p align="justify"><b>Pendientes: </b> Se muestran todas las solicitudes que aún no son ' +
      'ni aprobadas o rechazadas, brindando un vistazo general de la solicitud.</p>'             +

      '<p align="justify"><b>Atendidas: </b> Se muestran todas las solicitudes que ya fueron rev'+
      'izadas, datos generales de la solicitud y si fue aprobada o rechazada.</p>'               +

      '<p align="justify"><b>Canceladas: </b> Se muestran todas las solicitudes canceladas.</p>' +
      
      '<p align="justify"> <b>Nota: </b> Al dar click en el botón rosa se puede acceder a visual'+
      'izar la solicitud a detalle.</p>',
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      confirmButtonColor: '#d33',
      confirmButtonText:'Cerrar'
    })
  }

  /* Filtro de la barra de búsqueda  de la tabla  de solicitudes pendientes */
  filtroDataP(event: Event) 
  {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceP.filter = filterValue.trim().toLowerCase();
  }

  /* Filtro de la barra de búsqueda  de la tabla  de solicitudes atendidas */
  filtroDataA(event: Event) 
  {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceA.filter = filterValue.trim().toLowerCase();
  }

  /* Filtro de la barra de bùsqueda  de la tabla  de solicitudes canceladas */
  filtroDataC(event: Event) 
  {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceC.filter = filterValue.trim().toLowerCase();
  }

}