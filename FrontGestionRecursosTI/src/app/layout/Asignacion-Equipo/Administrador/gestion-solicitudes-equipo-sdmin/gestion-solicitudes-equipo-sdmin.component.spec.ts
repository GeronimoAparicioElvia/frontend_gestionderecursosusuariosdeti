import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionSolicitudesEquipoSdminComponent } from './gestion-solicitudes-equipo-sdmin.component';

describe('GestionSolicitudesEquipoSdminComponent', () => {
  let component: GestionSolicitudesEquipoSdminComponent;
  let fixture: ComponentFixture<GestionSolicitudesEquipoSdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionSolicitudesEquipoSdminComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionSolicitudesEquipoSdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
