import { Component, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { UsuarioService } from 'src/app/administracion/servicios';
import { SolicitudAsigEquipoService } from 'src/app/administracion/servicios/Asignacion_Equipo/solicitud-asig-equipo.service';
import { Empleado } from 'src/app/administracion/modelos/empleado';
import { TipoService } from 'src/app/administracion/servicios/Inventario/Tipo/tipo.service';
import { Tipo } from 'src/app/administracion/modelos/Inventario/Tipo/tipo';
import { SolicitudAsigEquipo } from 'src/app/administracion/modelos/Asignacion_Equipo/solicitud-asig-equipo';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { EquipoService } from 'src/app/administracion/servicios/Inventario/Equipo/equipo.service';

@Component({
  selector: 'app-solicitud-equipo',
  templateUrl: './solicitud-equipo.component.html',
  styleUrls: ['./solicitud-equipo.component.scss']
})
export class SolicitudEquipoComponent implements OnInit {

  /* Variables utilizadas para el input del empleado */
  empleado_seleccionado         :   Empleado = new Empleado();
  listaColaboradores            :   Empleado [] = [];
  listaFiltradaColaboradores    :   ReplaySubject<Empleado[]> = new ReplaySubject<Empleado[]>(1);
  protected _onDestroy = new Subject<void>();

  /* Validaciones para la fecha minima del input date*/
  fechaMin:Date = new Date();

  /* Listas a utilizar */
  listaactivos                  :  string   []  = [];
  listaTipos                    :  Tipo     []  = [];
  listaTiposDispoDeRed          :  Tipo     []  = [];
  listaTiposPerifericos         :  Tipo     []  = [];

  /*Constructor */
  constructor( private usuarioService   : UsuarioService ,                   /* Servicio de usuarios y empleados del endpoint*/
               private solicitudAsigEquipoService:SolicitudAsigEquipoService,/* Servicio de solicitu del back*/
               private tipoService:TipoService,                              /* Sevicio del tipo de equipo del back*/
               private equipoService:EquipoService,                          /* Sevicio de la tabla equipo de Glpi*/
               private router:Router                              
             ) { }
          
  /** Form Control */
  input_colaborador         = new FormControl() /*Control del input empleado */
  colaborador               = new FormControl() /*Control del select empleado */
  cargo                     = new FormControl() /*Control Cargo del empleado */
  sucursal                  = new FormControl() /*Control Sucursal del empleado */
  fecha_planeada_asignacion = new FormControl() /*Control fecha lìmite para la asignación */
  equipo_solicitado         = new FormControl() /*Control tipo de equipo solicitado */
  tipo_dispositivo          = new FormControl() /*Control especificacion del tipo de equipo solicitado */
  descripcion               = new FormControl() /*Control descripcion del uso o especificacion del equipo */
  observaciones             = new FormControl() /*Control comentarios y observaciones */

  ngOnInit()
  { 
    /* Bloqueamos los campos de cargo y oficina como no editables */
    this.cargo.disable()
    this.sucursal.disable()

    /* Cargamos las lista de datos */
    this.llenarListaColaboradores()

    this.listaactivos = this.equipoService.listaActivosAsignacion(1)

    /*Cargamos la lista de tipos de periferico y dispositivos de red auxiliares */
    /* Carga la lista de periféricos */
    this.tipoService.allTiposItems("PeripheralType"      ).subscribe(
      val=>{this.listaTiposPerifericos=val;},
      err=>{/* De exxistir un fallo de conexxión retornamos un mensaje de error */
        Swal.fire({
        
          icon: 'error',
          title: 'Sin conexión con el servidor, intentelo más tarde',
          allowEnterKey:false,
          allowEscapeKey:false,
          allowOutsideClick:false,
          showConfirmButton: false,
          timer: 3000,
        })
      }
    )

    /* Carga la lista de equipos de red */
    this.tipoService.allTiposItems("NetworkEquipmentType").subscribe(
      val=>{this.listaTiposDispoDeRed =val;},
      err=>{/* De exxistir un fallo de conexxión retornamos un mensaje de error */
        Swal.fire({
        
          icon: 'error',
          title: 'Sin conexión con el servidor, intentelo más tarde',
          allowEnterKey:false,
          allowEscapeKey:false,
          allowOutsideClick:false,
          showConfirmButton: false,
          timer: 3000,
        })
      }
    )

    /*Comenzamos con el filtro de nombres de empleados */
    this.listaFiltradaColaboradores.next(this.listaColaboradores.slice());
    this.input_colaborador.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterEmpleado();
      });
      
  }

  /* Método que carga la lista de empleados */
  llenarListaColaboradores()
  {
    this.usuarioService.getAllEmpleados().subscribe(lista=>{
      
      /* Realizamos un for each para componer el nombre completo de cada empleado*/
      lista.forEach(empleado=>{
        empleado.nomb_compl = empleado.nombre + " " + empleado.apellidoPat + " " + empleado.apellidoMat
      })
      /* Asignamos el resultado de la consulta a la lista de empelados  */
      this.listaColaboradores = lista
    },
    /*En caso de presentar un problema de conexión, nos va a mostrar una venta emergente 
      con la leyenda "Sin conexión, intentelo más tarde" */
    err=>{
      Swal.fire({
        
        icon: 'error',
        title: 'Error al cargar la lista de empleados, intentelo más tarde',
        allowEnterKey:false,
        allowEscapeKey:false,
        allowOutsideClick:false,
        showConfirmButton: false,
        timer: 3000,
      })
    })
  }
  
  /* Cuando se seleccione un periférico o dispositivo de red, este método retorna un true para visualizar en la pantalla
     un campo en donde se especifique el tipo de dipositivo que se necesite, ademas de cargar una lista con las opciones
     correspondinetes al tipo de equipo solicitado.  */
  clickDispositivos()
  {
    /* De seleccioanrse periférico, bloqueamos el campo descripción, carga la lista con tipos de periféricos y podrá 
       observarse en pantalla */
    if(this.equipo_solicitado.value=="PERIFÉRICO")
    {
      this.listaTipos = this.listaTiposPerifericos;
      this.descripcion.disable();
      return true;
    }
    /* De seleccioanrse Dispositivo de red, bloqueamos el campo descripción, carga la lista con tipos de dispositivos 
       de res y podrá observarse en pantalla */
    if(this.equipo_solicitado.value=="DISPOSITIVO DE RED")
    {
      this.listaTipos = this.listaTiposDispoDeRed;
      this.descripcion.disable();
      return true;
    }
    /*De no ser ninguno d elos casos anteriores habilitamos el campo descripción y ocultamos este campo para especificar */
    else
    {
      this.descripcion.enable();
      return false;
    }
  }

  /* Agrega la descripción del tipo especifico de equipo solicitado seleccionado del campo emergente 
    "Especifique el tipo de dispositivo" 
  */
  tipoDeDispositivoSeleccioando(event:any)
  {
    this.descripcion.setValue("")
    this.descripcion.setValue(this.tipo_dispositivo.value);
  }

  /* Método que carga los datos del colaborador seleccionado */
  clickCargarDatosColaborador( event:any )
  {

    /*
       Cargamos el objeto empleado seleccioando
    */
    this.empleado_seleccionado = this.colaborador.value;

    /* Validamos si el cargo del empleado es nulo o está vacío */
    if(this.empleado_seleccionado.puesto == null)
    { 
      /* En caso de ser un campo vacío colocamos un valor por defecto */
      this.cargo.setValue("Sin información");
    }
    else
    {
      /* En caso de no ser un campo vacío colocamos un valor correspondiente*/
      this.cargo.enable()
      this.cargo.setValue(this.empleado_seleccionado.puesto.nombrePuesto);
      this.cargo.disable()
    }

    /* Validamos si la oficina del empleado es nulo o està vacío */
    if(this.empleado_seleccionado.sucursal == null)
    { 
      /* En caso de ser un campo vacío colocamos un valor por defecto */
      this.sucursal.setValue("Sin información");
    }
    else
    {
      /* En caso de no ser un campo vacío colocamos un valor correspondiente*/
      this.sucursal.enable()
      this.sucursal.setValue(this.empleado_seleccionado.sucursal.nombreSucursal);
      this.sucursal.disable()
    }
  }

  /*Borra el contenido del campo descripcion al cambiarse de tipo de dsipositivo */
  clickSelectTipoEquipo( event:any )
  {
    this.descripcion.setValue("")
  }

  /* Método que guarda la solicitud */
  clickEnviarSolicitud()
  {
    
    /* Agregamos el contenido del formulario a variable slocales*/
    let observaciones  : string = this.observaciones.value
    let descrip_equipo : string = this.descripcion.value
    let tipo_equipo    : string = this.equipo_solicitado.value
    let colaborador    : number = Number(this.empleado_seleccionado.idEmpleado)
    let sustituye_a    : string = " "
    
    /* Comenzamos el proceso reguntando si se está seguro de realizar esta operación */
    Swal.fire({
      title: '¿Quiere realizar esta solicitud a '   + 
              this.empleado_seleccionado.nomb_compl +
              '?',
      icon:'question',
      showConfirmButton:true,
      showCancelButton:true,
      cancelButtonText:'NO',
      confirmButtonText:'SÍ',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
    }).then((result) => {
      
      /* De confirmar, se  crea un objeto de tipo solicitud y se le asignan los datos guardados del formulario */
      if (result.isConfirmed) {
        let solicitudAsigEquipo:SolicitudAsigEquipo = new SolicitudAsigEquipo();
        solicitudAsigEquipo.observaciones           = observaciones;
        solicitudAsigEquipo.descrip_equipo          = descrip_equipo;
        solicitudAsigEquipo.tipo_equipo             = tipo_equipo;
        solicitudAsigEquipo.solicitante_id          = JSON.parse(localStorage.getItem('empleado') || '{}').id;
        solicitudAsigEquipo.colaborador_id          = colaborador
        solicitudAsigEquipo.fecha_planeada_asig     = this.fecha_planeada_asignacion.value
        solicitudAsigEquipo.fecha_solicitud         = new Date().toISOString();
        solicitudAsigEquipo.fecha_mod               = new Date().toISOString();
        solicitudAsigEquipo.status_solicitud        = "ESPERA";

        /* Se guarda la solicitud mediante el servicio de solicitud */
        this.solicitudAsigEquipoService.save(solicitudAsigEquipo).subscribe(data=>{
          
            /*Vemos que nefectivamente se realize la solicitu para retornar un mensaje de confirmación */
            if(data!=null)
            {
              Swal.fire({
                title:'Solicitud realizada',
                icon:'success',
                allowEnterKey:false,
                allowEscapeKey: false,
                allowOutsideClick:false,
                showConfirmButton:false,
                timer:2000
              }).then(()=>{
                this.router.navigate(['layout/solicitudesDeEquipoSolicitante'])
              })
            }
            else
            {
              Swal.fire({
                title:'Error',
                text:'Este empleado ya cuenta con una solicitud de este tipo de equipo sin responder, intentelo más tarde.',
                icon:'error',
                allowEnterKey:false,
                allowEscapeKey: false,
                allowOutsideClick:false,
                showConfirmButton:false,
                timer:2000
              })
            }
          },
          err=>{
            const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 3000,
            })
            Toast.fire({
              icon: 'error',
              title: 'Error de conexión, intentelo más tarde'
            })
          }
        )
      }
    })
  }

  /* Método que despliega una ventana emergente con infrmación sobre la pantalla*/
  clickMasInformacion(event:any)
  {
    Swal.fire({
      title:'Instrucciones',
      html:
      '<p align="justify"> <b>-</b> Ingrese y seleccione el nombre del empleado que necesita un equipo.</p> '          +
      '<p align="justify"> <b>-</b> Seleccione el tipo de equipo (Ya sea una COMPUTADORA,'                             +
                                  ' IMPRESORAS, MONITORES o dispositivos como TECLADOS, MOUSES ect.).</p>'             +
      '<p align="justify"> <b>-</b> Ingrese una pequeña descripción del equipo o del uso planeado.</p> '               +
      '<p align="justify"> <b>-</b> Agregue la fecha límite para reaizar la asignación.</p>'                           +
      '<p align="justify"> <b>-</b> Agregue comentarios u observaciones que se tengan sobre la solicitud (Opcionales).</p>',
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      confirmButtonColor: '#d33',
      confirmButtonText:'Cerrar'
    })
  }

  /* Método que ejecuta el evento del boton cancelar preguntando si queremos salir 
     sin guardar la solicitud yd e confirmarlo, nos dirige a la pantalla de inicio
  */
  clickGoBack(evet:any){
    
    Swal.fire({
      icon: 'question',
      title: '¿Está seguro de salir sin terminar la solicitud?',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'SÍ',
      cancelButtonText:'NO'
    }).then((result) => {

      /* EN CASO DE CONFIRMAR SE REDIRIGE AL HOME DEL PROYECTO */
      if (result.isConfirmed) {
        this.router.navigate(['layout/home/']);
      }
    })
  }

  /*Filtro que busca coincidencias dentro del select search */
  protected filterEmpleado() {
    if (!this.listaColaboradores) {
      return;
    }
    /* Recuperamos el valor del input */
    let search = this.input_colaborador.value;
    if (!search) {
      this.listaFiltradaColaboradores.next(this.listaColaboradores.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    /* Retornamos sla lista de eempleados que cumplen con las coincidencias */
    this.listaFiltradaColaboradores.next(
      this.listaColaboradores.filter(empleado => empleado.nomb_compl.toLowerCase().indexOf(search) > -1)
    );
  }
  
  /* Método que comprueba que los campos necesarios ya se encuentran requisitados para habilitar el botón guardar */
  habilitarBotonSolicitar()
  {
    /* 
        Rrevisa que el empleado, la fecha límite de asignación, el tipo de equipo y la descripción se encuentren requisitados
    */
    if( 
        this.empleado_seleccionado.apellidoPat!= undefined &&
        this.fecha_planeada_asignacion.value  != null &&
        this.equipo_solicitado.value          != null &&
        this.descripcion.value                != null &&

        this.fecha_planeada_asignacion.value  != "" &&
        this.equipo_solicitado.value          != "" &&
        this.descripcion.value                != "" 
      )
    {
      /* Cuando cumple todas las condiciones habilita el botón */
      return true
    }
    /* De no cumplir todas las condiciones no habilita el botón */
    return false
  }
}