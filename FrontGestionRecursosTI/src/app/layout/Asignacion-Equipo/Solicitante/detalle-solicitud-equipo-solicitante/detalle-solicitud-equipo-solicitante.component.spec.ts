import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleSolicitudEquipoSolicitanteComponent } from './detalle-solicitud-equipo-solicitante.component';

describe('DetalleSolicitudEquipoSolicitanteComponent', () => {
  let component: DetalleSolicitudEquipoSolicitanteComponent;
  let fixture: ComponentFixture<DetalleSolicitudEquipoSolicitanteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetalleSolicitudEquipoSolicitanteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleSolicitudEquipoSolicitanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
