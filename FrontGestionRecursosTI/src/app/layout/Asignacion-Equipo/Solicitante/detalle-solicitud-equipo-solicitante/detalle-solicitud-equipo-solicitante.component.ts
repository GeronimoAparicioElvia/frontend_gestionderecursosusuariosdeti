import { Component, OnInit } from '@angular/core';
import { FormBuilder} from '@angular/forms';
import { Router } from '@angular/router';
import {ActivatedRoute,Params} from "@angular/router"
import { UsuarioService } from 'src/app/administracion/servicios';
import { SolicitudAsigEquipoService } from 'src/app/administracion/servicios/Asignacion_Equipo/solicitud-asig-equipo.service';
import { SolicitudAsigEquipo } from 'src/app/administracion/modelos/Asignacion_Equipo/solicitud-asig-equipo';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-detalle-solicitud-equipo-solicitante',
  templateUrl: './detalle-solicitud-equipo-solicitante.component.html',
  styleUrls: ['./detalle-solicitud-equipo-solicitante.component.scss']
})
export class DetalleSolicitudEquipoSolicitanteComponent implements OnInit {

  /* Parámetros */
  id: number = 0                /*Id de la solicitud*/
  habilitarBtnCancelar = false  /*Bandera para habilitar el botón cancelar */
  
  /* Objeto o modelo de la vista */
  solicitud_detalle:SolicitudAsigEquipo = new SolicitudAsigEquipo()
  
  constructor( private usuarioService             : UsuarioService            ,  /* Conexión con el end point usuarios */
               private formBuilder                : FormBuilder               ,  
               private solicitudAsigEquipoService : SolicitudAsigEquipoService,  /* Conexión con el la tabla solicitu de equipo de la base de datos */
               private router                     : Router                    ,  
               private activeRouter               : ActivatedRoute
             ) {}
  
  /*Form control group */
  formsolicitudAsigEq = this.formBuilder.group
  ({
    colaborador               : [{value: '', disabled: true},[]], /* Nombre  del colaborador      */
    colacargo                 : [{value: '', disabled: true},[]], /* Cargo   del colaborador      */
    colasucursal              : [{value: '', disabled: true},[]], /* Oficina del colaborador      */
    solicitante               : [{value: '', disabled: true},[]], /* Nombre  del solicitante      */
    solicargo                 : [{value: '', disabled: true},[]], /* Cargo   del solicitante      */
    solisucursal              : [{value: '', disabled: true},[]], /* Oficina del solicitante      */
    fecha_planeada_asignacion : [{value: '', disabled: true},[]], /* fecha límite de asignación   */
    fecha_de_solicitud        : [{value: '', disabled: true},[]], /* fecha de creación            */
    fecha_ultima_modificacion : [{value: '', disabled: true},[]], /* fecha de última modificación */
    equipo_solicitado         : [{value: '', disabled: true},[]], /* Tipo de equipo solicitado    */
    descripcion               : [{value: '', disabled: true},[]], /* Descripción o uso del equipo */
    observaciones             : [{value: '', disabled: true},[]]  /* Comentarios u observaciones  */
  });

  ngOnInit() 
  { 
    this.cargarDatosSolicitud();
  }

  /* Cargamos la data de la solicitud en especifico en a vista actual */
  cargarDatosSolicitud()
  {
  
    /* Rrecibimos los parámetros enviados */
    this.activeRouter.params.subscribe(params =>{
      this.id = params['id_solicitud']

      /* Cconsultamos la solicitud por medio del id capturado en los parámetros */
      this.solicitudAsigEquipoService.solicitudPorIdSolicitud(this.id).subscribe(
        val=>
        {
          /* Guardamos el resultado de la consulta en el objeto solicitud declarado en la clase */
          this.solicitud_detalle = val

          /* Validamos para saber si se encuentra disponible el boton cancelar solicitu que 
            solo aplica para las solicitudes en "ESPERA" 
          */
          if(val.status_solicitud=="ESPERA")
          {
            this.habilitarBtnCancelar=true
          }

          /* Cargamos los datos del empleado mencionado en la solicitud */
          this.usuarioService.getAnEmpleado(Number(val.colaborador_id)).subscribe
          (
            val=> 
            { 
              /*Cargamos el nombre del colaborador */
              this.formsolicitudAsigEq.controls['colaborador'].setValue ( val.nombre + " " + val.apellidoPat + " " + val.apellidoMat)
              /*validamos si el cargo no se encuentra vacío o nulo*/
              if(val.puesto!=null)
              { this.formsolicitudAsigEq.controls['colacargo'].setValue   ( val.puesto.nombrePuesto)  }
              else /*De ser nulo agregamos una leyenda por defecto a este campo */
              { this.formsolicitudAsigEq.controls['colacargo'].setValue   ( "Sin información")  }
              /*validamos si la oficina no se encuentra vacío o nulo*/
              if(val.sucursal!=null)
              { this.formsolicitudAsigEq.controls['colasucursal'].setValue   ( val.sucursal.nombreSucursal)  }
              else /*De ser nulo agregamos una leyenda por defecto a este campo */
              { this.formsolicitudAsigEq.controls['colasucursal'].setValue   ( "Sin información")  }
            }            
          )
          
          /* Cargamos los datos del solicitante */
          this.usuarioService.getAnEmpleado(Number(this.solicitud_detalle.solicitante_id)).subscribe
          (
            val=> 
            { 
              /*Cargamos el nombre del colaborador */
              this.formsolicitudAsigEq.controls['solicitante'].setValue ( val.nombre + " " + val.apellidoPat + " " + val.apellidoMat)
              /*validamos si el cargo no se encuentra vacío o nulo*/
              if(val.puesto!=null)
              { this.formsolicitudAsigEq.controls['solicargo'].setValue   ( val.puesto.nombrePuesto)  }
              else /*De ser nulo agregamos una leyenda por defecto a este campo */
              { this.formsolicitudAsigEq.controls['solicargo'].setValue   ( "Sin información")  }
              /*validamos si la oficina no se encuentra vacío o nulo*/
              if(val.sucursal!=null)
              { this.formsolicitudAsigEq.controls['solisucursal'].setValue   ( val.sucursal.nombreSucursal)  }
              else /*De ser nulo agregamos una leyenda por defecto a este campo */
              { this.formsolicitudAsigEq.controls['solisucursal'].setValue   ( "Sin información")  }
            }
          )

          /* Cargamos los datos de la solicitud */
          /* Damos formato a la fecha límite de asignación */
          this.formsolicitudAsigEq.controls['fecha_planeada_asignacion'].setValue
          (
            (val.fecha_planeada_asig+"").substring(8,10)  + /*  Día */
            "-"                                           +
            (val.fecha_planeada_asig+"").substring(5,7)   + /*  Mes */
            "-"                                           +
            (val.fecha_planeada_asig+"").substring(0,4)     /*  Año */
          )
          this.formsolicitudAsigEq.controls['equipo_solicitado'].setValue(val.tipo_equipo)  /* Tipo de equipo solicitado */
          this.formsolicitudAsigEq.controls['descripcion'].setValue(val.descrip_equipo)     /* Descripción del uso o del equipo solicitado */
          this.formsolicitudAsigEq.controls['observaciones'].setValue(val.observaciones)    /* Comentarios y observaciones generales */
          /* Damos formato a la fecha en que se creó la solicitud de asignación */
          this.formsolicitudAsigEq.controls['fecha_de_solicitud'].setValue
          ( 
            (val.fecha_solicitud)?.substring(8,10)  + /*  Día */ 
            "-"                                     + 
            (val.fecha_solicitud)?.substring(5,7)   + /*  Mes */  
            "-"                                     + 
            (val.fecha_solicitud)?.substring(0,4)     /*  Año */ 
          )

          /* Damos formato a la última fecha en que se modificó la solicitud de asignación */
          this.formsolicitudAsigEq.controls['fecha_ultima_modificacion'].setValue
          (
            (val.fecha_mod+"").substring(8,10)  + /*  Día */ 
            "-"                                 + 
            (val.fecha_mod+"").substring(5,7)   + /*  Mes */ 
            "-"                                 + 
            (val.fecha_mod+"").substring(0,4))    /*  Año */ 
        },
        err=> /* De existir un error de conexión retornamos un meensaje */
        {
          Swal.fire({
        
            icon: 'error',
            title: 'Sin conexión con el servidor, intentelo más tarde',
            allowEnterKey:false,
            allowEscapeKey:false,
            allowOutsideClick:false,
            showConfirmButton: false,
            timer: 3000,
          })
        }
      );
    })
  }
  
  /*Retornamos a la vista de solicitudes realizadas por el solicitante*/
  clickGoback()
  {
    this.router.navigate(['layout/solicitudesDeEquipoSolicitante']);
  }
  
  /* Método que cancela la solicitud */
  clickCancelararSolicitud()
  {

    /* Comenzamos preguntando si estamos seguros de cancelar la solicitud */
    Swal.fire({
      title: '¿Está seguro de cancelar esta solicitud?',
      text: 'Una vez realizado el cambio, no podrá revertirse',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'SÍ',
      cancelButtonText:'NO'
    }).then((result) => {

      /* En caso de confirmar la cancelaciónn de la solicitud */
      if (result.isConfirmed) {
        
        /*Actualizamos los campos de status y fecha de modificación */
        this.solicitud_detalle.fecha_mod = new Date().toISOString()
        this.solicitud_detalle.status_solicitud = "CANCELADO"
        /*Mandamos la solicitud al servicio de solicitu */
        this.solicitudAsigEquipoService.update(this.solicitud_detalle).subscribe(val=>{
          
          /*Mostramos un mensaje de confirmación */
          Swal.fire({
            icon:'success',
            title:'Solicitud cancelada',
            showConfirmButton: false,
            timer: 2000
          }).then(()=>{
            
            /* Cancelada la solicitud nos redirige nuevamente a ver todas las solicitudes realizadas */
            this.router.navigate(['layout/solicitudesDeEquipoSolicitante']);
          })
        },
        /*En caso surgir un error de conectividad retiorna un mensaje con la explicación del fallo de la operación */
        err=>{
          const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
          })
          Toast.fire({
            icon: 'error',
            title: 'Error de conexión, intentelo más tarde'
          })
        }
        )
      }
    })
  }
}
