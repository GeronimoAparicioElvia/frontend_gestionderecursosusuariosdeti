import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { SolicitudAsigEquipoService } from 'src/app/administracion/servicios/Asignacion_Equipo/solicitud-asig-equipo.service';
import { SolicitudAsigEquipo } from 'src/app/administracion/modelos/Asignacion_Equipo/solicitud-asig-equipo';
import { UsuarioService } from 'src/app/administracion/servicios';

@Component({
  selector: 'app-solicitudes-equipo-realizadas',
  templateUrl: './solicitudes-equipo-realizadas.component.html',
  styleUrls: ['./solicitudes-equipo-realizadas.component.scss']
})
export class SolicitudesEquipoRealizadasComponent implements OnInit {

  /* Encabezados genericos de las tablas */
  encabezadostabla    : String [] = ["OPCIONES","SOLICITANTE","COLABORADOR","CARGO DEL COLABORADOR","EQUIPO SOLICITADO","FECHA DE SOLICITUD"]
  encabezadostabla2   : String [] = ["OPCIONES","STATUS","SOLICITANTE","COLABORADOR","EQUIPO SOLICITADO","FECHA DE ATENCIÓN"]
  encabezadostabla3   : String [] = ["OPCIONES","SOLICITANTE","COLABORADOR","CARGO DEL COLABORADOR","EQUIPO SOLICITADO","FECHA DE ATENCIÓN"]
  
  /* Data de la stablas */
  dataSourceP          = new MatTableDataSource<SolicitudAsigEquipo>([]) /* Pendientes */ 
  dataSourceA          = new MatTableDataSource<SolicitudAsigEquipo>([]) /* Atendidas  */ 
  dataSourceC          = new MatTableDataSource<SolicitudAsigEquipo>([]) /* Canceladas */ 

  /* Paginators */
  @ViewChild('paginatorP', { static: true }) public paginatorP !: MatPaginator; /* Pendientes */ 
  @ViewChild('paginatorA', { static: true }) public paginatorA !: MatPaginator; /* Atendidas  */ 
  @ViewChild('paginatorC', { static: true }) public paginatorC !: MatPaginator; /* Canceladas */ 

  constructor(  private solicitudAsigEquipoService  : SolicitudAsigEquipoService  ,  /* Conexion con la base de datos a la tabla solicitu equipo */
                private empleadoService             : UsuarioService              ,  /* Conexion con el end point usuarios o empleados */
                private router:Router ) {}

  ngOnInit()
  {
    this.llenarDataP()
    this.llenarDataA()
    this.llenarDataC()
  }

  /* Cargar las solicitudes en la tabla pendientes o sin atender */
  llenarDataP()
  {
    /* Mandamos a llamar las solicitudes ene spera pertenecientes a un solicitante */
    this.solicitudAsigEquipoService.todasLasSolicitudesPorSolicitanteAndStatus(1).subscribe(
      lista=>{
      
        /*Recorremos la consulta*/
        lista.forEach(element=>{  

          /*Cargamos el nombre del solicitante */
          this.empleadoService.getAnEmpleado(Number(element.solicitante_id)).subscribe(val=>{element.name_solicitante_id = val.nombre+" "+val.apellidoPat+" "+val.apellidoMat})
          /*Cargamos el nombre y cargo del colaborador */
          this.empleadoService.getAnEmpleado(Number(element.colaborador_id)).subscribe(val=>{element.name_colaborador_id = val.nombre+" "+val.apellidoPat+" "+val.apellidoMat; element.name_cargo_colaborador=val.puesto.nombrePuesto})
          /* Damos formatos a las fechas para visualizarse en la tabla */
          element.fecha_solicitud = element.fecha_solicitud?.substring(8,10)+"-"+element.fecha_solicitud?.substring(5,7)+"-"+element.fecha_solicitud?.substring(0,4);  
        })
        /*Cargamos la data de la tabla y paginator */
        this.dataSourceP.data=lista
        this.dataSourceP.paginator=this.paginatorP
      },
      err=> /* De existir un error de conexión con el servidor retornará un mensaje*/
      {
        console.log(err)
        Swal.fire({
        
          icon: 'error',
          title: 'Sin conexión con el servidor, intentelo más tarde',
          allowEnterKey:false,
          allowEscapeKey:false,
          allowOutsideClick:false,
          showConfirmButton: false,
          timer: 3000,
        })
      }
    )
  }

  /* Cargar las solicitudes en la tabla atendido (APROBADAS Y RECHAZADAS) */
  llenarDataA()
  {

    /* Consultamos las solicitudes atendidas de un solicitante */
    this.solicitudAsigEquipoService.todasLasSolicitudesPorSolicitanteAndStatus(3).subscribe(
      lista=>
      {
        /*Recorremos la consulta */
        lista.forEach(element=>{  

          /* Agregamos el nombre del empleado solicitante */
          this.empleadoService.getAnEmpleado(Number(element.solicitante_id)).subscribe(val=>{element.name_solicitante_id = val.nombre+" "+val.apellidoPat+" "+val.apellidoMat})
          /* Agregamos el nombre y cargo del empleado solicitante */
          this.empleadoService.getAnEmpleado(Number(element.colaborador_id)).subscribe(val=>{element.name_colaborador_id = val.nombre+" "+val.apellidoPat+" "+val.apellidoMat; element.name_cargo_colaborador=val.puesto.nombrePuesto})
          /* Agregamos formato a la fecha de atención */
          element.fecha_mod = element.fecha_mod?.substring(8,10)+"-"+element.fecha_mod?.substring(5,7)+"-"+element.fecha_mod?.substring(0,4);  
        })
        /*Igualamos y agregamos la data a la tabla y el paginator */
        this.dataSourceA.data=lista
        this.dataSourceA.paginator=this.paginatorA
      },
      err=> /* De existir un error de conexión con el servidor retornará un mensaje*/
      {
        Swal.fire({
        
          icon: 'error',
          title: 'Sin conexión con el servidor, intentelo más tarde',
          allowEnterKey:false,
          allowEscapeKey:false,
          allowOutsideClick:false,
          showConfirmButton: false,
          timer: 3000,
        })
      }
    )
  }
  
  /* Cargar las solicitudes en la tabla cancelado */
  llenarDataC()
  {
    /*Realizamos la consulta de solicitudes canceladas de un solicitante */
    this.solicitudAsigEquipoService.todasLasSolicitudesPorSolicitanteAndStatus(2).subscribe(lista=>
    {
   
      /* Recorremos la consulta*/
      lista.forEach(element=>{ 
        /*Agregamos el nombre del solicitante */ 
        this.empleadoService.getAnEmpleado(Number(element.solicitante_id)).subscribe(val=>{element.name_solicitante_id = val.nombre+" "+val.apellidoPat+" "+val.apellidoMat})
        /*Agregamos el nombre y cargo del empleado */
        this.empleadoService.getAnEmpleado(Number(element.colaborador_id)).subscribe(val=>{element.name_colaborador_id = val.nombre+" "+val.apellidoPat+" "+val.apellidoMat; element.name_cargo_colaborador=val.puesto.nombrePuesto})
        /* Agregamos formato a la fecha de cancelación */
        element.fecha_mod = element.fecha_mod?.substring(8,10)+"-"+element.fecha_mod?.substring(5,7)+"-"+element.fecha_mod?.substring(0,4);  
      })

      /* Agregamos la data a la tabla cancelado y al paginator */
      this.dataSourceC.data=lista
      this.dataSourceC.paginator=this.paginatorC
    },
    err=> /* De existir un error de conexión con el servidor retornará un mensaje*/
    {
      Swal.fire({
      
        icon: 'error',
        title: 'Sin conexión con el servidor, intentelo más tarde',
        allowEnterKey:false,
        allowEscapeKey:false,
        allowOutsideClick:false,
        showConfirmButton: false,
        timer: 3000,
      })
    }
    )
  }

  /* Filtro del cuadro de busqueda de la tabla pendientes o en espera*/
  filtroDataP(event: Event) 
  {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceP.filter = filterValue.trim().toLowerCase();
  }
  
  /* Filtro del cuadro de busqueda de la tabla atendido*/
  filtroDataA(event: Event) 
  {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceA.filter = filterValue.trim().toLowerCase();
  }

  /* Filtro del cuadro de busqueda de la tabla cancelado*/
  filtroDataC(event: Event) 
  {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceC.filter = filterValue.trim().toLowerCase();
  }

  /* Método del evento al dar click al boton ver a detalle */
  clickVerDetalle(id_solicitud:number)
  {
    this.router.navigate(['layout/detalleSolicitudEquipoSolicitante/'+id_solicitud]);
  }

  /* Método que despliega una ventana de ayuda */
  clickMasInformacion(event:any)  
  {

    /* Abre una ventana con información sobre la pantalla*/
    Swal.fire({
      title:'Solicitudes de equipo realizadas',
      html:

      '<p align="justify"><b>En espera: </b>Son todas las solicitudes que aún no son atendidas.</p>'  +
      
      '<p align="justify"><b>Atendidas: </b>Son todas las solicitudes que ya fueron atendidas.</p>'   +

      '<p align="justify"><b>Canceladas: </b>Son todas las solicitudes canceladas.</p>'               +
      
      '<p align="justify"> <b>Nota: </b> Al dar click en el botón rosa se puede acceder a visual'     +
      'izar la solicitud a detalle.</p>',

      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      confirmButtonColor: '#d33',
      confirmButtonText:'Cerrar'
    })
  }

  /* Método que detecta que pestaña se ha dado click para cargar la data correspondiente */
  myTabSelectedIndexChange(index: number) 
  { 
        /* Carga la data de la segunda tabla*/
         if(index==1 ){ if(this.dataSourceA.data.length==0){ this.llenarDataA() } }

         /* Carga la data de la tercer tabla*/
    else if(index==2 ){ if(this.dataSourceC.data.length==0){ this.llenarDataC() } }
  }
}