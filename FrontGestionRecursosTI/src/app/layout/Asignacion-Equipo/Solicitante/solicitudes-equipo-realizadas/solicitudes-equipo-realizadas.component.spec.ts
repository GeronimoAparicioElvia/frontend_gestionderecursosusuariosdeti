import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitudesEquipoRealizadasComponent } from './solicitudes-equipo-realizadas.component';

describe('SolicitudesEquipoRealizadasComponent', () => {
  let component: SolicitudesEquipoRealizadasComponent;
  let fixture: ComponentFixture<SolicitudesEquipoRealizadasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SolicitudesEquipoRealizadasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitudesEquipoRealizadasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
