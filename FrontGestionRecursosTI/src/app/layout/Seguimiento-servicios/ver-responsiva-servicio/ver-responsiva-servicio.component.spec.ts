import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VerResponsivaServicioComponent } from './ver-responsiva-servicio.component';

describe('VerResponsivaServicioComponent', () => {
  let component: VerResponsivaServicioComponent;
  let fixture: ComponentFixture<VerResponsivaServicioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VerResponsivaServicioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerResponsivaServicioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
