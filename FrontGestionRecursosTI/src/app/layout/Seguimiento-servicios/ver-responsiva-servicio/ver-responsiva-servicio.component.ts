import { Component, Inject, OnInit} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ExpedientesComponent } from '../../Asignacion-usuario/Administrador/expedientes/expedientes.component';

export interface DataDialog
{
  url:string;
}

@Component({
  selector: 'app-ver-responsiva-servicio',
  templateUrl: './ver-responsiva-servicio.component.html',
  styleUrls: ['./ver-responsiva-servicio.component.scss']
})


export class VerResponsivaServicioComponent implements OnInit {

  urlPdf:string =""
  /**Crea un cuadro de dialogo para poder visualizar la responsiva que se encuentra en la base de datos */
  constructor(  public dialogRef      : MatDialogRef<ExpedientesComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DataDialog,
  ) {}

  ngOnInit(){}

}
