import { Component,  OnInit  } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Empleado } from 'src/app/administracion/modelos/empleado';
import { UsuarioService } from 'src/app/administracion/servicios';
import { ActivatedRoute, Router } from '@angular/router';
import { ServicioUsuario } from 'src/app/administracion/modelos/Control_Usuario/servicio-usuario';
import { ServicioUsuarioService } from 'src/app/administracion/servicios/Control_Usuario/servicio-usuario.service';
import { Perfil } from 'src/app/administracion/modelos/perfil';
import { FormBuilder, FormControl, Validators } from '@angular/forms';

/** PDF */
import * as pdfMake from "pdfmake/build/pdfmake";
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import {  takeUntil } from 'rxjs/operators';
import {  ReplaySubject, Subject } from 'rxjs';
(<any>pdfMake).vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-responsiva-servicio',
  templateUrl: './responsiva-servicio.component.html',
  styleUrls: ['./responsiva-servicio.component.scss']
})
export class ResponsivaServicioComponent implements OnInit {
  /**Campos para la Responsiva */
  idSolicitud   :number=0;
  serUsuario      :ServicioUsuario= new ServicioUsuario();
  colaborador  :Empleado = new Empleado();
  administrador:Empleado = new Empleado();
  hora  !:string
  dia   !:string
  mes   !:string
  anio  !:string
  solicitaSimbanck=false;
  serUsuSimbank : ServicioUsuario= new ServicioUsuario();

  /**Form Control del que suscribe */
  colaboradorFilter = new FormControl();
  inputColaboradorF = new FormControl(); /**input */

  suscriptor :Empleado = new Empleado();

  protected onDestroy  = new Subject<void>();


  cartaResponsiva:boolean=false
  responsiva:boolean=false

  fecha:string = ""
  dataSourcePDF = new MatTableDataSource<string[]>([]);
  dataSource = new MatTableDataSource<ServicioUsuario>([]);
  listaColaboradores : Empleado[] = [];
  listaCargos        : Perfil  [] = [];
  listaMeses          :  string   []=[" DE ENERO DE ", " DE FEBRERO DE ", " DE MARZO DE ", " DE ABRIL DE ", " DE MAYO DE ", " DE JUNIO DE ", " DE JULIO DE ", " DE AGOSTO DE ", " DE SEPTIEMBRE DE ", " DE OCTUBRE DE ", " DE NOVIEMBRE DE ", " DE DICIEMBRE DE ", "ERROR"]

  /**Lista para el select del que suscribe */
  listaFiltradaEmp      :   ReplaySubject<Empleado[]> = new ReplaySubject<Empleado[]>(1);


  constructor(
              private servicioUsuarioService : ServicioUsuarioService,
              private usuarioService: UsuarioService,
              private activatedRoute:ActivatedRoute,
              private formBuilder           : FormBuilder,
              private router                :Router,
              ) {}

  /**Formulario para los datos del que suscribe */
  formResponsiva = this.formBuilder.group({
    puesto_suscribe: ['',[Validators.required]],
  });

  ngOnInit(){
     this.parametros();

     this.llenarListaColab();

    let fechaAct:Date = new Date();
    /**Asigna la fecha actual con el formato correcto a la variable */
    this.fecha= fechaAct.toISOString().substring(8,10)+" "+this.listaMeses[Number(fechaAct.toISOString().substring(5,7))-1]+" "+fechaAct.toISOString().substring(0,4);


    /**Escucha los cambios del valor de búsqueda del colaborador*/
    this.listaFiltradaEmp.next(this.listaColaboradores.slice());
    this.inputColaboradorF.valueChanges.pipe(takeUntil(this.onDestroy)).subscribe(() => {
        this.filterColaborador();
    });
  }

  /**Realiza la búsqueda para el colaborador a quien se le solicitan los servicios */
  protected filterColaborador(){
    if(!this.listaColaboradores){
      return;
    }
    /**Se optiene el valor del input para el colaborador*/
    let search = this.inputColaboradorF.value;
    if (!search) {
      this.listaFiltradaEmp.next(this.listaColaboradores.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    /**Retorna la lista de los colaboradores que cumplen con la busqueda */
    this.listaFiltradaEmp.next( this.listaColaboradores.filter(empleado => empleado.nomb_compl.toLowerCase().indexOf(search) > -1) );
  }

  /**Captura el id de la solicitud recibida y se asignan los valores correspondientes al empleado */
  parametros() {
   let listaSE : ServicioUsuario[] =[];
   let listaStSE : string [][]=[];
    this.activatedRoute.params.subscribe(params =>{
      this.idSolicitud    = params['idSolicitud'  ]
      //this.idSolicitud    = 40;//14 ...93

        this.servicioUsuarioService.todosLosServiciosUsuario().subscribe(servicios=>{
          servicios.forEach(servicio=>{
            this.serUsuario= servicio;
            let sucursalCo = "";//this.colaborador.sucursal?.nombreSucursal+"";//this.serUsuario.nomSucursalCol+"";
            //Obtiene todos los detalles de la solicitud, es decir los servicios que contiene la solicitud
            if(this.serUsuario.detalleSolicitud.solicitud.id_solicitud==this.idSolicitud){
              /**Se hace mension de que la solicitud incluye "SIMBANK" ya que esto conlleva a que se genere una carta responsiva */
              if(this.serUsuario.detalleSolicitud.tipo_servicio.nombre_servicio?.includes("SIMBANK")){
                this.solicitaSimbanck=true;
                this.serUsuSimbank=this.serUsuario;
              }
              this.usuarioService.getAnEmpleado(Number(this.serUsuario.detalleSolicitud.solicitud.colaboradorId)).subscribe(val=>{
                this.colaborador=val;
                this.serUsuario.nomSucursalCol=val?.sucursal?.nombreSucursal;
                sucursalCo=val?.sucursal?.nombreSucursal+"";
              });
              this.usuarioService.getAnEmpleado(Number(this.serUsuario.detalleSolicitud.solicitud.administrador_ti_id)).subscribe(val=>{this.administrador=val;})
              let sistema= this.serUsuario.detalleSolicitud.tipo_servicio.nombre_servicio+"";
              let usuario= this.serUsuario.usuario+"";
              let contrasenia= this.serUsuario.contrasenia+"";
              listaSE.push(this.serUsuario);
              listaStSE.push([sistema,usuario,contrasenia]);//this.serUsuario.nomSucursalCol+""
              this.dataSource.data=listaSE;
              this.dataSourcePDF.data=listaStSE;
            }
          });
        });
    })
    /**Llena la lista de puestos*/
    this.usuarioService .getAllPuesto   ().subscribe( val => { this.listaCargos = val       ;} );
  }
  /**Llena la lista de colaboradores */
  llenarListaColab(){
    //this.listaColaboradores.splice(0,this.listaColaboradores.length);
    this.usuarioService .getAllEmpleados().subscribe( val => {
      val.forEach(emp=>{
        emp.nomb_compl= emp.nombre+" "+ emp.apellidoPat+" "+emp.apellidoMat
      });
      this.listaColaboradores = val;
    } );
  }

  /**Cargar datos del colaborador */
  cargarDatosSuscribe(event : any){
    this.suscriptor = this.colaboradorFilter.value;
  }

  /**Permite habilitar el boton para enviar la solicitud, solo si ya se ha elegido al que sustituye */
  habilitarBotonResponsiva(){
    if(this.colaboradorFilter.value != null ){
       return true}
    return false
  }
  /**Método que genera la responsiva */
  async createPDF(){
    if(this.formResponsiva.valid){
      let colabSuscribe : Empleado= new Empleado();
      colabSuscribe= this.suscriptor;
      const pdfDefinition: any = {
        content: [
          {
            image: await this.getBase64ImageFromURL("/assets/img/sofipa.png"),
            fit: [150, 150],
          },
          {
            alignment: 'right',
            text: 'Oaxaca de Juárez, Oaxaca a '+this.fecha,
          },
          {
            alignment: 'justify',
            bold: true,
            text: '\n\n\n\nC. '+this.colaborador.nombre+" " + this.colaborador.apellidoPat+" " + this.colaborador.apellidoMat
          },
          {//Gerente del área de Tecnologías de la información
            alignment: 'justify',
            text:'\nEl que suscribe '+colabSuscribe.nomb_compl+', con el carácter de '+this.formResponsiva.value.puesto_suscribe+' de Sofipa Corporation, Sociedad Anónima Promotora de la inversión de Capital Variable, SOFOM ENR, ante usted me dirijo para exponer lo siguiente:\n\n'
                +'Por medio del presente escrito y a partir de esta fecha le autorizo para que pueda operar e ingresar al o los sistemas operativos mostrados en la siguiente tabla, con la finalidad de que pueda desarrollar las funciones que su puesto requiere, en consecuencia, se le asigna usuario y contraseña para cada sistema.\n\n\n',
          },
          {
            table: {
              //headerRows: 1,
              widths: [125, 180, 180],
              body: this.buildTableBody(this.dataSourcePDF.data,["SISTEMA","USUARIO","CONTRASEÑA INICIAL"]),
            },
            alignment: 'center',
            layout: {
              fillColor: function (rowIndex : any) {
                return (rowIndex === 0) ? '#bedcff' : null;
              },
              hLineWidth: function (i: any, node:any) {
                return (i === 0 || i === node.table.body.length) ? 0.5 : 1;
              },
              vLineWidth: function (i: any, node:any) {
                return (i === 0 || i === node.table.widths.length) ? 0.5 : 0.2;
              },
              hLineColor: function (i: any, node:any) {
                return (i === 0 || i === node.table.body.length) ? 'gray' : 'black';
              },
              vLineColor: function (i: any, node:any) {
                return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
              },
            }
          },
          {
            alignment: 'justify',
            text: '\n\nAsí mismo se le hace de su conocimiento que con el usuario y la contraseña de cada sistema que se le está confiriendo, usted podrá realizar las operaciones señaladas en la descripción'
                +' de puesto y que se rigen bajo el Perfil de Privilegios de usuario asignado en el sistema: en base a lo anterior el usuario y contraseña de cada sistema que se le está asignando son de carácter confidencial'
                +' y personal que únicamente puede ser operado por usted.'

                +'\n\n\nSin otro particular de momento, quedo de usted.\n\n\n\n\n',
          },
          {
            alignment: 'center',
            columns: [
              {
                text:'OTORGANTE'
                    +'\n\n\n'+colabSuscribe.nombre+' '+colabSuscribe.apellidoPat+' '+colabSuscribe.apellidoMat
                    +'\n________________________________________'
                    +'\n'+this.formResponsiva.value.puesto_suscribe
                    +'\nSOFIPA CORPORATION SAPI DE CV, SOFOM ENR'
              },
              {
                text:'ACEPTANTE'
                    +'\n\n\n'+this.colaborador.nombre+" " + this.colaborador.apellidoPat+" " + this.colaborador.apellidoMat
                    +'\n________________________________________'
              }
            ]
          },

        ]
      }
      const pdf = pdfMake.createPdf(pdfDefinition)
      pdf.download("RESPONSIVA "+this.colaborador.nombre+" " + this.colaborador.apellidoPat+" " + this.colaborador.apellidoMat);
    }
  }

  /**Método el cual genera la carta responsiva en caso de que se solicite simbank */
  async createCartaResponsiva(){
    const pdfDefinition: any = {
      content: [
        {
          image: await this.getBase64ImageFromURL("/assets/img/sofipa.png"),
          fit: [150, 150],
        },
        {
          alignment: 'center',
          bold: true,
          fontSize: 14,
          text: '\nCARTA RESPONSIVA DE USUARIO\nDEL SISTEMA OPERATIVO Y TECNOLÓGICO '+this.serUsuSimbank.detalleSolicitud.tipo_servicio.nombre_servicio,
        },
        {
          alignment: 'justify',
          text: '\nPor medio de la presente que suscribe  '+this.colaborador.nombre+" " + this.colaborador.apellidoPat+" " + this.colaborador.apellidoMat +' declaro bajo protesta de decir la verdad que soy empleado de Sofipa '
                 +'Corporation, Sociedad Anónima Promotora de la inversión de Capital Variable, SOFOM ENR, asignado a la sucursal '+this.colaborador.sucursal?.nombreSucursal
                 +' ubicada en  CALLE '+this.colaborador?.sucursal?.calle+' N°.'+this.colaborador?.sucursal?.numero+', COLONIA '+this.colaborador?.sucursal?.colonia+', '+this.colaborador?.sucursal?.municipio+', '+this.colaborador?.sucursal?.estado+', CP. '+this.colaborador?.sucursal?.codigoPostal
                 +' desempeñando el puesto de  '+this.colaborador?.puesto?.nombrePuesto+' en consecuencia para poder realizar las funciones inherentes '
                 +'a mi puesto, me fue asignado el usuario '+this.serUsuSimbank.usuario+' y la clave '+this.serUsuSimbank.contrasenia+', mediante escrito de fecha '+this.fecha+' con la finalidad de poder ingresar y operar.'
                 +'\n\n'
                 +'En base a lo antes expuesto por medio de esta carta me hago responsable de todos y cada uno de los movimientos y transacciones que se realicen con el usuario que me fue asignado y manifiesto estar de acuerdo como usuario del sistema '+this.serUsuSimbank.detalleSolicitud.tipo_servicio.nombre_servicio+' y al perfil que me fue asignado de las siguientes responsabilidades:'
                 +'\n\n1. El usuario no podrá proporcionar su clave confidencial y bajo ninguna circunstancia puede divulgar a terceros ni aun en ausencias, incapacidades o vacaciones.'
                 +'\n\n2. El usuario es responsable de todos y cada uno de los movimientos y transacciones que se realicen en el sistema '+this.serUsuSimbank.detalleSolicitud.tipo_servicio.nombre_servicio+'.'
                 +'\n\n3. El usuario del sistema tiene la responsabilidad de cambiar la clave de acceso al menos cada 30 días y solicitar su cancelación en caso de rotación del personal.'
                 +'\n\n4. Es responsabilidad total del usuario del sistema, el mal uso que se pueda dar a la clave de acceso al sistema de modo que, será sancionado de acuerdo a la normatividad aplicable dentro de la organización y a las leyes en materia civil y penal aplicables en el estado mexicano.'
                 +'\n\n5. El usuario es responsable de los daños y perjuicios que se ocasionen a Sofipa Corporation Sociedad Anónima Promotora de la Inversión de Capital Variable, SOFOM ENR por el mal uso que se le puede dar a la clave de acceso.'
                 +'\n\n'
                 +'Si bien el presente documento hace mención al usuario y Sucursal/Oficina asignada, sin embargo éste no es limitativo, es decir, no deslinda de las responsabilidades manifestadas, independientemente de la cantidad de usuarios otorgadas al Colaborador.'
        },
        {
          alignment: 'right',
          text: '\n\nOaxaca de Juárez, Oaxaca a '+this.fecha,
        },
        {
          alignment: 'center',
          text: '\n\n\nNombre y firma de Conformidad del Empleado'
              +'\n\n\n'+this.colaborador.nombre+" " + this.colaborador.apellidoPat+" " + this.colaborador.apellidoMat
              +'\n________________________________________'
        },
      ]
     }
    const pdf = pdfMake.createPdf(pdfDefinition);
    pdf.download("CARTA RESPONSIVA "+this.colaborador.nombre+" " + this.colaborador.apellidoPat+" " + this.colaborador.apellidoMat);
  }

  /**Método que convierte a base 64 a la ruta o url de la imagen */
  getBase64ImageFromURL(url: string) {
    return new Promise((resolve, reject) => {
    var img = new Image();
    img.setAttribute("crossOrigin", "anonymous");
          img.onload = () => {
              var canvas = document.createElement("canvas");
              canvas.width = img.width;
              canvas.height = img.height;

              var ctx = canvas.getContext("2d");
              ctx?.drawImage(img, 0, 0);

              var dataURL = canvas.toDataURL("image/png");
              resolve(dataURL);
          };
          img.onerror = error => {
              reject(error);
          };
          img.src = url;
      })
  }

  /**Método que construye la tabla que contiene el pdf */
  buildTableBody(data: string[][], columns: string[]) {
    var body = [];
    body.push(columns);
    data.forEach(function(row) {
        var dataRow :string[]= [];
        for(let i=0;i<columns.length;i++){
          dataRow.push(row[i]);
        }
        //dataRow.push(colab) sin sucursal
        body.push(dataRow);
    });
    return body;
  }

  /**Permite regresar al seguimiento */
  regresar(){
    this.router.navigate(['layout/SeguimientoControlU']);
  }
}
