import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResponsivaServicioComponent } from './responsiva-servicio.component';

describe('ResponsivaServicioComponent', () => {
  let component: ResponsivaServicioComponent;
  let fixture: ComponentFixture<ResponsivaServicioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResponsivaServicioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResponsivaServicioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
