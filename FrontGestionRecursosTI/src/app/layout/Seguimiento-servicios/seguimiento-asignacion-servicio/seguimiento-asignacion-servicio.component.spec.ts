import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeguimientoAsignacionServicioComponent } from './seguimiento-asignacion-servicio.component';

describe('SeguimientoAsignacionServicioComponent', () => {
  let component: SeguimientoAsignacionServicioComponent;
  let fixture: ComponentFixture<SeguimientoAsignacionServicioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SeguimientoAsignacionServicioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SeguimientoAsignacionServicioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
