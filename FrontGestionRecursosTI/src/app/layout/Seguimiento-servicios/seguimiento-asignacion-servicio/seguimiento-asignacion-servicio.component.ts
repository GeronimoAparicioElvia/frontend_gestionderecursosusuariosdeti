import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ServicioUsuario } from 'src/app/administracion/modelos/Control_Usuario/servicio-usuario';
import { Solicitud } from 'src/app/administracion/modelos/Control_Usuario/solicitud';
import { Empleado } from 'src/app/administracion/modelos/empleado';
import { UsuarioService } from 'src/app/administracion/servicios';
import { ServicioUsuarioService } from 'src/app/administracion/servicios/Control_Usuario/servicio-usuario.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-seguimiento-asignacion-servicio',
  templateUrl: './seguimiento-asignacion-servicio.component.html',
  styleUrls: ['./seguimiento-asignacion-servicio.component.scss']
})
export class SeguimientoAsignacionServicioComponent implements OnInit {
  dataSource2         = new MatTableDataSource<ServicioUsuario>([]);
  dataSource          = new MatTableDataSource<Solicitud>([]);
  statusVista : string ="";
   /* Paginators */
   @ViewChild(MatPaginator) paginator  !: MatPaginator

  listaEmpleados       : Empleado[]=[];
  solicitudesxId!      : Solicitud[];
  encabezadostabla     : String [] = ["descargarResp","subirResp","nomColaborador","nomPuesto","name_sucursalCol","status_solicitud","fechaRegistro","fechaSolicitud"]

  constructor(
    private router                : Router,
    private servicioUsuarioService : ServicioUsuarioService,
    private usuarioService        : UsuarioService,
  ) { this.llenarListas();
  }
  ngOnInit(): void {
    this.llenarTablaSeg();
    //this.capturarParametro();
  }
  /**Llena la lista de empleados */
  llenarListas(){
    this.usuarioService.getAllEmpleados().subscribe(val=>{this.listaEmpleados=val;});
  }
  /**Metodo para llenar la tabla de datos */
  llenarTablaSeg(){
    let listaSolicitudes: Solicitud[]=[];
    this.dataSource.data =listaSolicitudes; //vacia la tabla

    this.servicioUsuarioService.todosLosServiciosUsuario().subscribe(servicios=>{
      servicios.forEach(servicio=>{
        if(servicio.archivo_responsiva==null){
           let valor = listaSolicitudes.find(element=>element.id_solicitud==servicio.detalleSolicitud.solicitud.id_solicitud);
          if(valor==undefined){
            /**Creamos el objeto solicitud para agregarle todos los datos para despues añadirlo al arreglo de la tabla */
            let solicitud : Solicitud = new Solicitud();
            solicitud.id_solicitud=servicio.detalleSolicitud.solicitud.id_solicitud;
              this.usuarioService.getAnEmpleado(Number(servicio.detalleSolicitud.solicitud.colaboradorId)).subscribe(val=>{
                solicitud.name_colaborador_id=val?.nombre+" "+val?.apellidoPat+" "+ val?.apellidoMat;
                solicitud.name_cargo_colaborador=val?.puesto?.nombrePuesto;
                solicitud.name_sucursalCol=val?.sucursal?.nombreSucursal;
              });
              solicitud.fechaSolicitud=servicio.detalleSolicitud.solicitud.fechaSolicitud?.substring(0,10);
              solicitud.fecha_planeada_act= servicio.fechaRegistro?.substring(0,10);
              solicitud.status_solicitud=servicio.detalleSolicitud.solicitud.status_solicitud;
              listaSolicitudes.push(solicitud);
              this.dataSource.data=listaSolicitudes;
              this.dataSource.paginator=this.paginator;
            }
          }
      });
    }, err=>{
      Swal.fire({
        position: 'center',
        icon: 'error',
        title: 'Error de conexión',
        text: 'Lo sentimos las solicitudes no se cargaron correctamente debido a que no hay conexión con el servidor, por favor  inténtelo más tarde.',
      })
    });
  }
  /**Método para filtrar los datos de ala tabla por lo que ingresa el usuario */
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  /**Crea una ventana para poder subir la responsiva */
  async subirResponsiva(idSolicitud:number){
    const { value: file } = await Swal.fire({
      title: 'Selecciona el PDF de la responsiva firmada',
      input: 'file',
      inputAttributes: {
        'accept': '.pdf',
        'aria-label': 'Subir responsiva'
      }
    })
    /**Valida que se haya subido un archivo */
    if (file) {
      /**Valida que el archivo subido sea un pdf/ Segunda validación de refuerzo */
      if(file.type=='application/pdf'){
        /**Valida que el peso del archivo no sea superior al permitido 1048576 bytes*/
        if(file.size < 1048576){
          const reader = new FileReader()
          this.servicioUsuarioService.todosLosServiciosUsuario().subscribe(servicios=>{
            servicios.forEach(servicio=>{
              if(servicio.detalleSolicitud.solicitud.id_solicitud== idSolicitud){
                if(servicio.archivo_responsiva==null){
                  this.servicioUsuarioService.cargarResponsiva(Number(servicio.id_servicio_usuario),file).subscribe(val=>{
                    Swal.fire({
                      position: 'center',
                      icon: 'success',
                      title: 'Archivo subido',
                      text: 'La responsiva ha sido guardada correctamente ',
                    })
                    /**Se actualiza la pantalla, y entonces ya  no muestra la solicitud de la responsiva que se acaba de subir */
                    this.llenarTablaSeg();
                  /**Validación en caso de que la consulta no se realice */
                  }, err => {
                    console.log(err);
                      Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'Cargar responsiva',
                        text: 'La responsiva no se cargó correctamente debido a un problema con el servidor, por favor inténtelo más tarde o contacte al área de T.I. ',
                      })
                      this.llenarTablaSeg();
                    }
                  );
                }
              }
            });
          });
          reader.readAsDataURL(file);
          /**Cierre de validación para el tamaño de la responsiva */
        }else{
          Swal.fire({
            position: 'center',
            icon: 'error',
            title: 'Tamaño de archivo no permitido',
            text: 'El archivo que intenta subir supera el tamaño permitido, por favor verifique su documento.',
          })
        }
      /**Cierre de validación para que permita solo pdfs*/
      }else{
        Swal.fire({
          position: 'center',
          icon: 'error',
          title: 'El archivo no es un PDF',
          text: 'Lo sentimos, el archivo que intenta subir no corresponde a un formato PDF ',
        })
      }
    }
  }
  /**Redirecciona a la vista en donde deberá elegir el que suscribe la resonsiva para despues descargarla  */
  descargarResponsiva(idSolicitud:number){
    this.router.navigate(['layout/ResponsivaServicio/'+idSolicitud]);
  }
}
