import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaSelectComponent } from './tabla-select.component';

describe('TablaSelectComponent', () => {
  let component: TablaSelectComponent;
  let fixture: ComponentFixture<TablaSelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TablaSelectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
