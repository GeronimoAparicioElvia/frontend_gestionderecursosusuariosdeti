import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

import { Equipo } from 'src/app/administracion/modelos/Inventario/Equipo/equipo';
import { UsuarioService } from 'src/app/administracion/servicios';
import { EquipoService } from 'src/app/administracion/servicios/Inventario/Equipo/equipo.service';
import { AgendarMantenimientoComponent } from 'src/app/layout/Mantenimiento/agendar-mantenimiento/agendar-mantenimiento.component';


@Component({
  selector: 'app-tabla-select',
  templateUrl: './tabla-select.component.html',
  styleUrls: ['./tabla-select.component.scss']
})
export class TablaSelectComponent implements OnInit {


  /** T A B L A */
  encabezados:string [] =["c1","c2","c3","c4","c5"];
  dataSource          = new MatTableDataSource<Equipo>([]);
  @ViewChild('paginator', { static: true }) public paginator !: MatPaginator;

  src =""


  /** L I S T A S */
  listaDeCarga:Equipo[] = []
  listaSeleccioandos:Equipo[] = []
  listaTablas:string[] = ["Computer","NetworkEquipment","Printer","Monitor","Peripheral"]

  constructor(  private equipoService :EquipoService,
                private usuarioService:UsuarioService
                
             ) {}
  
  ngOnInit()
  {
    this.usuarioService.getFirmaDigital("VMARTINEZ").subscribe(data=>{
      try {
        const base64Data = data;
        const base64Response = `data:data:application/jpg;base64,${base64Data}`;
        this.src = base64Response +""
        console.log(this.src)
      } catch (error) {
        console.log('Error: ', error);
      }
    }) 

  }

}
