import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DetalleAsignacionEquipoUsuarioComponent } from 'src/app/layout/seguimiento/detalle-asignacion-equipo-usuario/detalle-asignacion-equipo-usuario.component';

export interface DataDialog
{
  url:string;
}

@Component({
  selector: 'app-visualizar-responsiva',
  templateUrl: './visualizar-responsiva.component.html',
  styleUrls: ['./visualizar-responsiva.component.scss']
})
export class VisualizarResponsivaComponent implements OnInit {

  urlPdf:string =""

  constructor(  public dialogRef      : MatDialogRef<DetalleAsignacionEquipoUsuarioComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DataDialog,
 ) {}

  ngOnInit(){}

}
