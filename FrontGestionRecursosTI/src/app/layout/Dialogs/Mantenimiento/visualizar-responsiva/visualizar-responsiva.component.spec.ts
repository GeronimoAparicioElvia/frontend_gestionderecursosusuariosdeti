import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizarResponsivaComponent } from './visualizar-responsiva.component';

describe('VisualizarResponsivaComponent', () => {
  let component: VisualizarResponsivaComponent;
  let fixture: ComponentFixture<VisualizarResponsivaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisualizarResponsivaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizarResponsivaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
