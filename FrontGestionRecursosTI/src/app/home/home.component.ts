import { Component, OnInit } from '@angular/core';
import { Empleado } from '../administracion/modelos/empleado';
import { UsuarioService } from '../administracion/servicios';
import { Router, ActivatedRoute } from '@angular/router';
import { Usuario } from '../administracion/modelos';

@Component({templateUrl: 'home.component.html'})
export class HomeComponent implements OnInit {

    empleado!: Empleado; /* Modelo de la clase empleado */
    username: any      ; /* Nombre de usuario           */
    nombre  : any      ; /* Nombre real del empleado    */

    constructor(private usuarioService: UsuarioService) {}
    
    ngOnInit() {
        /* Recuperamos el nombre de usuario actual */
        this.username=JSON.parse(localStorage.getItem('currentUser')!).username;
        /* Consultamos la información del emplelado buscandolo por su nombre de usuario */
        this.usuarioService.getUsernamePrueba(this.username).subscribe
        ( 
            response =>
            {
                let usuario = response as Usuario;
                this.empleado = usuario.empleado
                /* Aquì guardamos sla informaciòn del empleado en el localstorage */
                localStorage.setItem('empleado', JSON.stringify({
                    id              : this.empleado.idEmpleado,
                    nombreEmpleado  : this.empleado.nombre,
                    apellidoPat     : this.empleado.apellidoPat,
                    apellidoMat     : this.empleado.apellidoMat
                }));
                /* Capturamos el nombre completo del empleado */
                this.nombre = this.empleado.nombre + " " + this.empleado.apellidoPat + " " + this.empleado.apellidoMat
            }
        );   
    }
}
