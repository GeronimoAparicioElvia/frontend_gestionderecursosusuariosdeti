import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TipoSolicitud } from '../../modelos/Control_Usuario/tipo-solicitud';


@Injectable({
  providedIn: 'root'
})
export class TipoSolicitudService {
  //Url del back
  private urlApi : string = environment.urlback;

  constructor(private http: HttpClient) { }

  //Método para llamar a todos los tipos de solicitudes
  todosLosTiposSolicitud() :Observable<TipoSolicitud[]>{
      return this.http.get<TipoSolicitud[]>(`${this.urlApi}/TipoSolicitudUsuario/TodosLosTiposSolicitudUsuario`);
  }

  //Un tipo de solicitud por id de la solicitud
  unTipoSolicitud(id: number) :Observable<TipoSolicitud>{
      return this.http.get<TipoSolicitud>(`${this.urlApi}/TipoSolicitudUsuario/UnTipoSolicitudUsuarioPorId/` + id);
  }

  /**Agrega un tipo de solicitud */
  public agregarTipoSolicitud(tipoSolicitud: TipoSolicitud) {
    return this.http.post<TipoSolicitud>( `${this.urlApi}/TipoSolicitudUsuario/AgregarTipoSolicitudUsuario?`, tipoSolicitud);
  }

  /**Actualiza un tipo de solicitud */
  public actualizarTipoSolicitud(tipoSolicitud: TipoSolicitud) {
    return this.http.put<TipoSolicitud>( `${this.urlApi}/TipoSolicitudUsuario/ActualizarTipoSolicitudUsuario?`, tipoSolicitud);
  }
}
