import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TipoServicio } from '../../modelos/Control_Usuario/tipo-servicio';

@Injectable({
  providedIn: 'root'
})
export class TipoServicioService {
  //Url del back
  private urlApi : string = environment.urlback;

  constructor(private http: HttpClient) { }

   //Metodo para llamar a todos los tipos de servicios
  todosLosServicios() :Observable<TipoServicio[]>{
      return this.http.get<TipoServicio[]>(`${this.urlApi}/TiposServicios/TodosLosTiposServicios`);
  }

  /**Retorna un tipo de servicio */
  unTipoServicio(id_tipo_servicio:number) :Observable<TipoServicio>{
    return this.http.get<TipoServicio>(`${this.urlApi}/TiposServicios/TipoServiciosPorId/`+id_tipo_servicio);
  }

  /**Agrega un tipo de servicio */
  public agregarServicio(tipoServicio: TipoServicio) {
    return this.http.post<TipoServicio>( `${this.urlApi}/TiposServicios/AgregarTipoServicio?`, tipoServicio);
  }

  /**Actualiza un tipo de servicio */
  public actualizarServicio(tipoServicio: TipoServicio) {
    return this.http.put<TipoServicio>( `${this.urlApi}/TiposServicios/ActualizarTipoServicio?`, tipoServicio);
  }
}
