import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DetalleSolicitud } from '../../modelos/Control_Usuario/detalle-solicitud';


@Injectable({
  providedIn: 'root'
})
export class DetalleSolicitudService {
  //Url del back
  private urlApi : string = environment.urlback;

  constructor(private http: HttpClient) { }

    /**Añadir un detalle de solicitud */
  generarDetalleSolicitud(  solicitud_id : number, tipo_servicio_id : number, servicio_usuario_id : number):Observable<DetalleSolicitud> {
    return this.http.post<DetalleSolicitud>(`${this.urlApi}/DetalleSolicitud/AgregarNuevoDetalleSolicitud?tipo_servicio_id=`+tipo_servicio_id +`&solicitud_id=`+solicitud_id +`&servicio_usuario_id=`+servicio_usuario_id,{});
  }

  /**Actualizar estado del los detalle de la  solicitud*/
  actualizarStatusDetSolicitud(id_detalle_solicitud: number, status_serv:string){
    return this.http.put<string>(`${this.urlApi}/DetalleSolicitud/ActualizarDetalleSolicitud?id_detalle_solicitud=` + id_detalle_solicitud+`&status_serv=`+status_serv,{}).subscribe(
      val=>{ console.log("realizado");}, err=>{console.log(err)}
    );
  }

  //Lista de los detalles de la solicitud por el id de la solicitud
  listaDetSolicitudPorSolicitudId( solicitud_id: number ) :Observable<DetalleSolicitud[]>{
    return this.http.get<DetalleSolicitud[]>(`${this.urlApi}/DetalleSolicitud/ListaDetalleSolicitudPorIdSolicitud/` + solicitud_id);
  }

}
