import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ServicioUsuario } from '../../modelos/Control_Usuario/servicio-usuario';

@Injectable({
  providedIn: 'root'
})
export class ServicioUsuarioService {
  //Url del back
  private urlApi : string = environment.urlback;

  constructor(private http: HttpClient) { }

    /**Retorna todos los servicios de usuario existentes */
    todosLosServiciosUsuario() :Observable<ServicioUsuario[]>{
        return this.http.get<ServicioUsuario[]>(`${this.urlApi}/ServicioUsuario/ListaServicioUsuario`);
    }
    /**Servicios de usuario por id de solicitud */
    ListaServicioUPorSolicitudId(solicitudId: number) :Observable<ServicioUsuario[]>{
      return this.http.get<ServicioUsuario[]>(`${this.urlApi}/ServicioUsuario/ServUsuarioPorSolicitudId/`+solicitudId);
    }
    /**Servicios de usuario por id de colaborador */
    ListaServicioUPorColaboradorId(colaboradorId: number) :Observable<ServicioUsuario[]>{
      return this.http.get<ServicioUsuario[]>(`${this.urlApi}/ServicioUsuario/ServUsuarioPorColaboradorId/`+colaboradorId);
    }
    /**Retorna un servicio de usuario por su id */
    unServicioUsuario(idServUsuario: number) :Observable<ServicioUsuario>{
      return this.http.get<ServicioUsuario>(`${this.urlApi}/ServicioUsuario/ServUsuarioPorIdServUsuario/`+idServUsuario);
    }

    //Actualizar status de servicio usuario //mensajes
    actualizarStatusSeUsuario(id_servicio_usuario : number, status_reg: string, detalle_solicitud_id: number){
      return this.http.put<string>(`${this.urlApi}/ServicioUsuario/ActualizarServicioUsuario?id_servicio_usuario=` + id_servicio_usuario+`&status_reg=`+status_reg +`&detalle_solicitud_id=`+detalle_solicitud_id,{});
    }

    //GuardarNuevo registro de servicio Usuario
    guardarServicioUsuario(  usuario : string, contrasenia : string,	detalle_solicitud_id : number	) {
      return this.http.post<ServicioUsuario>(`${this.urlApi}/ServicioUsuario/AgregarNuevoServicioUsuario?usuario=`+usuario +`&contrasenia=`+contrasenia+`&detalle_solicitud_id=`+detalle_solicitud_id,{});
    }

    /**Carga la carta responsiva al servicioUsuario*/
    cargarResponsiva(id_servUsuario:number, responsiva: File){
        const headers2 = new HttpHeaders()
        headers2.set("Content-Type", "application/json")
        const formData: FormData = new FormData();
        formData.append('files', responsiva);
        return this.http.put<string>(`${this.urlApi}/ServicioUsuario/ServicioUsuarioCargarResponsiva?id_servicio_usuario=`+id_servUsuario,formData,{reportProgress: true, responseType: 'json'});
    }
}
