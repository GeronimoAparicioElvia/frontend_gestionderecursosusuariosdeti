import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Solicitud } from '../../modelos/Control_Usuario/solicitud';

@Injectable({
  providedIn: 'root'
})
export class SolicitudService {
 //Url del back
  private urlApi : string = environment.urlback;

  constructor(private http: HttpClient) { }

  /**Inserta una solicitud */
  public agregarSolicitud(solicitud: Solicitud, tipo_solicitud_usuario_id : number){
    solicitud.solicitanteId= JSON.parse(localStorage.getItem('empleado') || '{}').id
    return this.http.post<Solicitud>( `${this.urlApi}/Solicitud/AgregarNuevaSolicitud?tipo_solicitud_usuario_id=`+tipo_solicitud_usuario_id , solicitud);
  }

   /** Método del administrador de TI - Get a todas las solicitudes por solicitante **/
  solicitudesPorSolicitante( idSolicitante: number ) :Observable<Solicitud[]>{
      return this.http.get<Solicitud[]>(`${this.urlApi}/Solicitud/ListaSolicitudPorSolicitante/`+idSolicitante);
  }

  /** Método del administrador de TI - Get a todas las solicitudes por colaborador **/
  solicitudesPorColaborador( idColaborador: number ) :Observable<Solicitud[]>{
    return this.http.get<Solicitud[]>(`${this.urlApi}/Solicitud/ListaSolicitudPorColaborador/`+idColaborador);
  }

  /**Retorna todas las solicitudes existentes */
  todasLasSolicitudes() :Observable<Solicitud[]>{
      return this.http.get<Solicitud[]>(`${this.urlApi}/Solicitud/ListaSolicitudes`);
  }

  /**Actualizar estado de la  solicitud y asigna comentarios en caso de ser una solicitud rechazada*/

  actualizarStatusSolicitud(id_solicitud: number, status_solicitud:string, comentarios: string){
    let administrador_id: number = JSON.parse(localStorage.getItem('empleado') || '{}').id;
    return this.http.put<string>(`${this.urlApi}/Solicitud/ActualizarSolicitud?id_solicitud=` + id_solicitud+`&status_solicitud=`+status_solicitud+`&comentarios=`+comentarios+`&administrador_TI_id=`+administrador_id,{}).subscribe(
      val=>{ console.log(val);}
    );
  }

  /** Método para mostrar una solicitud en especifico **/
  unaSolicitud(id_solicitud: number) :Observable<Solicitud>{
    return this.http.get<Solicitud>(`${this.urlApi}/Solicitud/SolicitudPorIdSolicitud/` + id_solicitud);
  }

  /**Regresa l aultima solicitud por el tipo de solicitud y el solicitante que la realizó */
  ultimaSolicitud( id_tipo_solicitud : number): Observable<Solicitud>{
    let solicitanteId : number =JSON.parse(localStorage.getItem('empleado') || '{}').id;
    return this.http.get<Solicitud>(`${this.urlApi}/Solicitud/ultimaSoliSolicitudEsperaPorSolicitanteAndTipo/` + solicitanteId+`/`+id_tipo_solicitud);
  }


}
