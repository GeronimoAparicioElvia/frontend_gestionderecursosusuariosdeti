import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { ElemServicio } from '../../modelos/Control_Usuario/elem_servicio';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class ElemServicioService {
  //Url del back
  private urlApi : string = environment.urlback;

  constructor(private http: HttpClient) { }

    /**Inserta un ElemServicio */
    public agregarElemServicio(servicio_usuario_id   : number, detalle_solicitudAct_id : number, sucursal_id : number ){
      return this.http.post<ElemServicio>( `${this.urlApi}/ElemServicio/AgregarElemServicio?servicio_usuario_id=`+servicio_usuario_id+`&detalle_solicitudAct_id=`+detalle_solicitudAct_id+`&sucursal_id=`+sucursal_id, {});
    }

     /**Actualizar estado del elem_servicio*/
    public actualizarStatusElemServicio(id_elem_servicio: number, status_registro:string, detalle_solicitudSup_id :number){
      return this.http.put<string>(`${this.urlApi}/ElemServicio/ActualizarStatusElemServicio?id_elem_servicio=` + id_elem_servicio+`&status_registro=`+status_registro+`&detalle_solicitudSup_id=`+detalle_solicitudSup_id,{}).subscribe(val=>{
        //Mensaje de confirmación
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Solicitud aprobada',
          text: ' Los servicios fueron suspendidos correctamente',
          })
          //Mensaje de error
        }, err=>{console.log(err);
          Swal.fire({
            position: 'center',
            icon: 'error',
            title: 'Aprobar solicitud',
            text: ' Los servicios no fueron suspendidos correctamente, por favor intentelo más tarde',
            })
        }
      );
    }

    /**Regresa la lista de todos los servicios activados o suspendidos por colaborador y una sucursal en especifico */
    public listaElemServicioForColaboradorSucursal(colaborador_id:number, sucursal_id : number) : Observable<ElemServicio[]>{
      return this.http.get<ElemServicio[]>(`${this.urlApi}/ElemServicio/ListaElemServiciosForColaboradorSucursal/`+colaborador_id+`/`+sucursal_id);
    }

    /**Regresa la lista de todos los servicios activados o suspendidos por colaborador */
    public listaElemServicioForColaborador(colaborador_id:number) : Observable<ElemServicio[]>{
      return this.http.get<ElemServicio[]>(`${this.urlApi}/ElemServicio/ListaElemServiciosForColaborador/`+colaborador_id);
    }

    /**Toda la lista de elemServicio */
    public listaElemServicios() : Observable<ElemServicio[]>{
      return this.http.get<ElemServicio[]>(`${this.urlApi}/ElemServicio/ListaElemServicios`);
    }

    /**Regresa un ElemServicio por su id */
    public elemServicio(id_elem_servicio: number) : Observable<ElemServicio>{
      return this.http.get<ElemServicio>(`${this.urlApi}/ElemServicio/ElemServicioForIdElemServicio/`+id_elem_servicio);
    }
    /**Regresa una lista de los elemento  de servicio activados segun sea el id_solicitud */
    public listaElemServicioIdSolicitudAct(id_solicitud:number) : Observable<ElemServicio[]>{
      return this.http.get<ElemServicio[]>(`${this.urlApi}/ElemServicio/ListaElemServiciosForIdSolicitudAct/`+id_solicitud);
    }
    /**Regresa una lista de los elemento  de servicio suspendidos segun sea el id_solicitud */
    public listaElemServicioIdSolicitudSusp(id_solicitud:number) : Observable<ElemServicio[]>{
      return this.http.get<ElemServicio[]>(`${this.urlApi}/ElemServicio/ListaElemServiciosForIdSolicitudSusp/`+id_solicitud);
    }
}
