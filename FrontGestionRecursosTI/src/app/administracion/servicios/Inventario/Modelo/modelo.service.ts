import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Modelo } from 'src/app/administracion/modelos/Inventario/Modelo/modelo';

@Injectable({
  providedIn: 'root'
})
export class ModeloService {
   //Url del back
   private urlApi : string = environment.urlback
  token: string = JSON.parse(localStorage.getItem('glpiToken') || '{}').token


  constructor(private http: HttpClient) { }

  /* Retorna todos los items de una tabla modelo especificada */
  allModelosItems( tabla:string ) :Observable<Modelo[]>{
      return this.http.get<Modelo[]>(`${this.urlApi}/AllItems?tabla=`+tabla+`&token=` + this.token);
  }

  /*Retorna un modelo de equipo por el nombre de la tabla y su id*/
  anModelo(  tabla:string, id : number){
      return this.http.get<Modelo>(`${this.urlApi}/AnItem?tabla=` + tabla + `+&id=`+ id +`&token=` + this.token);
  }
}
