import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SucursalGlpi } from 'src/app/administracion/modelos/Inventario/Sucursal/sucursal-glpi';

@Injectable({
  providedIn: 'root'
})
export class SucursalGlpiService {
   //Url del back
   private urlApi : string = environment.urlback

  constructor(private http: HttpClient) { }

  /*Retorna todas las sucursales de GLPI*/
  allSucursales( token : string ) :Observable<SucursalGlpi[]>{
      return this.http.get<SucursalGlpi[]>(`${this.urlApi}/AllItems?tabla=Location&token=` + token);
  }

  /*Retorna una sucursal de glpi por su id*/
  anSucursal(  id : number, token : string ){
      return this.http.get<SucursalGlpi>(`${this.urlApi}/AnItem?tabla=Location&id=`+ id +`&token=` + token);
  }
}
