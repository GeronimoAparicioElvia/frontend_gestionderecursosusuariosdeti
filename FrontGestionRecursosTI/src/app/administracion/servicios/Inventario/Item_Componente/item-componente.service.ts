import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { ItemComponente } from 'src/app/administracion/modelos/Inventario/Item_Componente/item-componente';

@Injectable({
  providedIn: 'root'
})
export class ItemComponenteService {
   //Url del back
   private urlApi : string = environment.urlback
  token: string = JSON.parse(localStorage.getItem('glpiToken') || '{}').token


  constructor(private http: HttpClient) { }

  /*Retorna todos los items segun sea el nombre de la tabla que se le pase como parametro */
  allItems( tabla: string) :Observable<ItemComponente[]>{
      return this.http.get<ItemComponente[]>(`${this.urlApi}/AllItems?tabla=`+tabla+`&token=` + this.token);
  }

  /**Retorna todos los sub items de un equipo por su id */
  allSubItems( id: number ,tabla: string) :Observable<ItemComponente[]>{
      return this.http.get<ItemComponente[]>(`${this.urlApi}/allSubItems?id=`+id+`&tabla=`+tabla+`&token=` + this.token);
  }

  /**Retorna todos los sitemas operativos existentes */
  allSistemasOperativos(){
    return this.http.get<ItemComponente[]>(`${this.urlApi}/allOperatingSystems?token=` + this.token);
  }
}
