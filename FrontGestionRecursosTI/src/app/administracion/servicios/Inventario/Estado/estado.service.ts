import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Estado } from 'src/app/administracion/modelos/Inventario/Estado/estado';

@Injectable({
  providedIn: 'root'
})
export class EstadoService {
   //Url del back
   private urlApi : string = environment.urlback

  constructor(private http: HttpClient) { }

  /* Retorna todos los items de una tabla tipo especificada */
  allEstadosItems( token : string ) :Observable<Estado[]>{
      return this.http.get<Estado[]>(`${this.urlApi}/AllItems?tabla=State&token=` + token);
  }
}
