import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { Componente } from 'src/app/administracion/modelos/Inventario/Componente/componente';

@Injectable({
  providedIn: 'root'
})
export class ComponenteService {

  //Url del back
  private urlApi : string = environment.urlback
  token: string = JSON.parse(localStorage.getItem('glpiToken') || '{}').token

  constructor(private http: HttpClient) { }

  /*Retorna todos los componentes segun sea el nombre de la tabla que se le pase como parámetro */
  allComponentes( tabla: string, token : string ) :Observable<Componente[]>{
      return this.http.get<Componente[]>(`${this.urlApi}/AllItems?tabla=`+tabla+`&token=` + token);
  }

  /*Retorna todos los componentes segun sea el nombre de la tabla que se le pase como parámetro */
  anComponente( tabla: string,  id : number ) :Observable<Componente> {
      return this.http.get<Componente>(`${this.urlApi}/AnItem?tabla=`+tabla+`&id=`+ id +`&token=` + this.token);
  }

}
