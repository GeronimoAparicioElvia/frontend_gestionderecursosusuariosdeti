import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UsuarioGlpi } from 'src/app/administracion/modelos/Inventario/Usuario/usuario-glpi';

@Injectable({
  providedIn: 'root'
})
export class UsuarioGlpiService {
  //Url del back
  private urlApi : string = environment.urlback
  token: string = JSON.parse(localStorage.getItem('glpiToken') || '{}').token

  constructor(private http: HttpClient) { }

  //Retorna todos los usuarios
  allUsuarios( token : string ) :Observable<UsuarioGlpi[]> {
      return this.http.get<UsuarioGlpi[]>(`${this.urlApi}/AllItems?tabla=User&token=` + token);
  }

  //Retorna un usuario
  anUsuario(  id : number, token : string ){
      return this.http.get<UsuarioGlpi>(`${this.urlApi}/AnItem?tabla=User&id=`+ id +`&token=` + token);
  }

  //Busca un empleado por su nombre y apellidos
  allUsuariosSearch(nombre: string, apellidos: string) :Observable<UsuarioGlpi[]>{
      return this.http.get<UsuarioGlpi[]>(`${this.urlApi}/AnUser?token=` +this.token+ `&nombre=` + nombre + `&apellidos=`+apellidos);
  }
}
