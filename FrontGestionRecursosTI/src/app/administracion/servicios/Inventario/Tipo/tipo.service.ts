import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Tipo } from 'src/app/administracion/modelos/Inventario/Tipo/tipo';

@Injectable({
  providedIn: 'root'
})
export class TipoService {
   //Url del back
   private urlApi : string = environment.urlback
  token: string = JSON.parse(localStorage.getItem('glpiToken') || '{}').token;

  constructor(private http: HttpClient) { }

  /* Retorna todos los items de una tabla tipo especificada */
  allTiposItems( tabla:string) :Observable<Tipo[]>{
      return this.http.get<Tipo[]>(`${this.urlApi}/AllItems?tabla=`+tabla+`&token=` + this.token);
  }

  /*Retorna un tipo por su id y el nombre de la tabla*/
  anTipo(  tabla:string, id : number){
      return this.http.get<Tipo>(`${this.urlApi}/AnItem?tabla=` + tabla + `+&id=`+ id +`&token=` + this.token);
  }

  /*Actualiza un tipo*/
  updateTipo(tabla: string, id: number, body :any){
    const headers2 = new HttpHeaders()
    .set("Content-Type", "application/json");
    //Realiza el put a Glpi con el nombre de la tabla y el cuerpo del tipo y su id
    return this.http.put<string>(`${this.urlApi}/updateItem?tabla=`+tabla+ `&id=` + id + `&token=`+this.token,body,{headers: headers2})
  }
  /*Inserta un tipo*/
  insertarTipo(tabla: string, body :any) {
    const headers2 = new HttpHeaders();
    headers2.set("Content-Type", "application/json");
    //Realiza el post a Glpi con el nombre de la tabla y el cuerpo del tipo a insertar
    return this.http.post<string>(`${this.urlApi}/addItem?tabla=`+tabla+`&token=`+this.token,body,{headers: headers2});
  }
}
