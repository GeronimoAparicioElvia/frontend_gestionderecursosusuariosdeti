import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2';
import { HttpHeaders } from '@angular/common/http';
import { Equipo } from 'src/app/administracion/modelos/Inventario/Equipo/equipo';

@Injectable({
  providedIn: 'root'
})
export class EquipoService {
   //Url del back
   private urlApi : string = environment.urlback
  //Token de inicio de sesión
  token: string = JSON.parse(localStorage.getItem('glpiToken') || '{}').token


  constructor(private http: HttpClient) { }

  /** Método que retorna todos los equipos de una tabla */
  todosLosEquipos( tabla: string) :Observable<Equipo[]>{
      return this.http.get<Equipo[]>(`${this.urlApi}/AllEquipos?tabla=` + tabla + `&token=` + this.token);
  }

  /** Método que retorna un equipo de una tabla */
  unEquipo( tabla: string, id : number) :Observable<Equipo>{
      return this.http.get<Equipo>(`${this.urlApi}/AnItem?tabla=`+tabla+`&id=`+ id +`&token=` + this.token);
  }

  /** Método que retorna un equipo de una tabla */
  unEquipoExtends( tabla: string, id : number) :Observable<Equipo>{
      return this.http.get<Equipo>(`${this.urlApi}/AnEquipoExtends?tabla=`+tabla+`&id=`+ id +`&token=` + this.token);
  }
  /**Busca la sucursal por su nombre */
  buscarSucursal(sucursal:string): Observable<Equipo[]>
  {
    if(sucursal!= null && sucursal!= undefined )/* Valida primero que la sucursal no se encuentre vacía */
    {
      if(sucursal == "CORPORATIVO"){ sucursal="CAS"}/*Si la sucursal es el coporativo lo busca como CAS */
      return this.http.get<Equipo[]>(`${this.urlApi}/AnSucursal?nombreSucursal=` + sucursal + `&token=` + this.token); /* Retorna la sucursal */
    }
    else
    {
      return this.http.get<Equipo[]>(`${this.urlApi}/AnSucursal?nombreSucursal=CAS&token=` + this.token);/*En caso que esté vacío retorna el cas */
    }
  }

  /* Método para insertar un item */
  insertarItem(tabla: string, body :any){
    const headers2 = new HttpHeaders()
    .set("Content-Type", "application/json");
    //Realiza el post a Glpi con el nombre de la tabla y el cuerpo del item a insertar
    this.http.post<string>(`${this.urlApi}/addItem?tabla=`+tabla+`&token=`+this.token,body,{headers: headers2}).subscribe((data) => {
      console.log(data);
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Alta de equipos',
        text: ' El equipo se registró exitosamente',
      })
    /**Mensaje de error */
    },err => {
      Swal.fire({
        position: 'center',
        icon: 'error',
        text: 'El registro no se realizó correctamente debido a que no hay conexión con el servidor, inténtelo más tarde o contacte al área de T.I. ',
        title: 'Alta de equipos',
      })
    }
    );
    return body;
  }

  /* Método para actualizar un item */
  actualizarItem(tabla: string, id: number, body :any):Observable<string>{
    const headers2 = new HttpHeaders()
    headers2.set("Content-Type", "application/json");
    return this.http.put<string>(`${this.urlApi}/updateItem?tabla=`+tabla+ `&id=` + id + `&token=`+this.token,body,{headers: headers2});
  }

  /**Retorna la lista de activos existentes en glpi */
  listaActivos(): string[]{
    let listaActivos= ["COMPUTADORA","DISP. RED","IMPRESORA","MONITOR","PERIFERICO","RACK","TELEFONO"];
    return listaActivos;
  }
  /**Retorna la lista de opciones de activos a utilizar en el módulo de asignacion de equipo */
  listaActivosAsignacion(indice:number): string[]{
    if(indice==1)
    {
      let listaActivos= ["COMPUTADORA","DISPOSITIVO DE RED","IMPRESORA","MONITOR","PERIFÉRICO","RACK","TELEFONO"];
      return listaActivos;
    }
    else if(indice==2)
    {
      let listaActivos= ["COMPUTADORA","DISPOSITIVO DE RED","IMPRESORA","MONITOR","DISPOSITIVO","RACK","TELEFONO"];
      return listaActivos;
    }
    else
    {
      let listaActivos= ["CPU", "MONITOR","TECLADO","MOUSE","REGULADOR"];
      return listaActivos;
    }
  }
}
