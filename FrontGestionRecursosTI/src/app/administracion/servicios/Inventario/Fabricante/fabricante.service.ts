import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { Fabricante } from 'src/app/administracion/modelos/Inventario/Fabricante/fabricante';

@Injectable({
  providedIn: 'root'
})
export class FabricanteService {
   //Url del back
   private urlApi : string = environment.urlback

  constructor(private http: HttpClient) { }

  //Retorna todos los fabricantes
  allFabricantes( token : string ) :Observable<Fabricante[]>{
      return this.http.get<Fabricante[]>(`${this.urlApi}/AllItems?tabla=Manufacturer&token=` + token);
  }

  //Retorna un fabricante
  anFabricante( id : number, token : string ){
      return this.http.get<Fabricante>(`${this.urlApi}/AnItem?tabla=Manufacturer&id=`+ id +`&token=` + token);
  }
}
