import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { RegistroMantenimiento } from '../../modelos/Mantenimiento/registro-mantenimiento';

@Injectable({
  providedIn: 'root'
})
export class RegistroMantenimientoService {

  private urlApi : string = environment.urlback;                                              /* Ulr del back */
  private administrador_TI:number = JSON.parse(localStorage.getItem('empleado') || '{}').id;  /* Id del administrador */

  constructor(private http: HttpClient) { }   /*Constructor */

  /* Retorna un registro de mantenimiento por su Id  */
  registroDeMantenimientoPorId(id_registro_mantenimiento:number): Observable<RegistroMantenimiento>
  {
    return this.http.get<RegistroMantenimiento>(`${this.urlApi}/RegistroMantenimiento/RegistroMantenimientoForIdRegistroMant/`+id_registro_mantenimiento);
  }

  /* Retorna una lista con los registros de mantenimiento en espera*/
  registrosEspera(): Observable<RegistroMantenimiento[]>
  {
    return this.http.get<RegistroMantenimiento[]>(`${this.urlApi}/RegistroMantenimiento/RegistrosEspera`);
  }

  /* Retorna una lista con los registros de mantenimiento realizados*/
  registrosRealizados(): Observable<RegistroMantenimiento[]>
  {
    return this.http.get<RegistroMantenimiento[]>(`${this.urlApi}/RegistroMantenimiento/RegistrosRealizados`);
  }

  /* Retorna una lista con los registros de mantenimiento cancelados*/
  registrosCancelados(): Observable<RegistroMantenimiento[]>
  {
    return this.http.get<RegistroMantenimiento[]>(`${this.urlApi}/RegistroMantenimiento/RegistrosCancelados`);
  }

  /* Llama todos los mantenimiento para el reporte general  */
  reporteGeneral( fechaI:Date, fechaF:Date): Observable<RegistroMantenimiento[]>
  {
    return this.http.get<RegistroMantenimiento[]>(`${this.urlApi}/RegistroMantenimiento/ReporteGeneral/`+fechaI+`/`+fechaF);
  }

  /* Llama todos los mantenimiento realizados usados para el reporte a detalle  */
  reporteRealizados(descripcion: string, fechaI:Date, fechaF:Date): Observable<RegistroMantenimiento[]>
  {
    return this.http.get<RegistroMantenimiento[]>(`${this.urlApi}/RegistroMantenimiento/ReporteDetalleRealizados/`+descripcion+`/`+fechaI+`/`+fechaF);
  }

  /* Llama todos los mantenimiento en espera usados para el reporte a detalle  */
  reporteEspera(descripcion: string, fechaI:Date, fechaF:Date): Observable<RegistroMantenimiento[]>
  {
    return this.http.get<RegistroMantenimiento[]>(`${this.urlApi}/RegistroMantenimiento/ReporteDetalleEspera&Cancelado/ESPERA/`+descripcion+`/`+fechaI+`/`+fechaF);
  }

  /* Llama todos los mantenimiento cancelados usados para el reporte a detalle  */
  reporteCancelados(descripcion: string, fechaI:Date, fechaF:Date): Observable<RegistroMantenimiento[]>
  {
    return this.http.get<RegistroMantenimiento[]>(`${this.urlApi}/RegistroMantenimiento/ReporteDetalleEspera&Cancelado/CANCELADO/`+descripcion+`/`+fechaI+`/`+fechaF);
    //return this.http.get<RegistroMantenimiento[]>(`${this.urlApi}/RegistroMantenimiento/ReporteDetalleCancelados/`+descripcion+`/`+fechaI+`/`+fechaF);
  }
  
  /* Agrega un nuevo registro de mantenimiento */
  saveRegistrosDeMantenimiento( registroMantenimiento : RegistroMantenimiento )
  {
    return this.http.post<RegistroMantenimiento>(`${this.urlApi}/RegistroMantenimiento/AgregarRegistroMantenimiento`,registroMantenimiento)
  }

  /* Actualiza un registro de mantenimiento */
  updateRegistrosMantenimiento( registroMantenimiento: RegistroMantenimiento )
  {
    return this.http.put<string>(`${this.urlApi}/RegistroMantenimiento/ActualizarRegistroMantenimiento`,registroMantenimiento)
  }
}
