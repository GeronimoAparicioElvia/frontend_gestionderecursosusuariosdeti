import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { MantenimientoEquipo } from '../../modelos/Mantenimiento/mantenimiento-equipo';


@Injectable({
  providedIn: 'root'
})
export class MantenimientoEquipoService {

  private urlApi : string = environment.urlback;  /*Url del back */
  
  constructor(private http: HttpClient) { }       /*Constructor */

  /* Retorna uan lista de los mantenimiento equipos que pertenecen a un registro de mantenimiento */
  mantenimientosEquipoPorIdRegistro(registro_mantenimiento_id:number): Observable<MantenimientoEquipo[]>
  {
    return this.http.get<MantenimientoEquipo[]>(`${this.urlApi}/MantenimientoEquipo/ListaMantEquipoForRegistroMantId/`+registro_mantenimiento_id);
  }

  /* Retorna un  mantenimiento equipo buscandolo por su Id */
  mantenimientoEquipoPorIdRegistroSinEditar(registro_mantenimiento_id:number): Observable<MantenimientoEquipo[]>
  {
    return this.http.get<MantenimientoEquipo[]>(`${this.urlApi}/MantenimientoEquipo/MantenimientoEquipoForIdMantEquipoSinEditar/`+registro_mantenimiento_id);
  }

  /* Agrega un nuevo mantenimeinto equipo */
  saveRegistrosDeMantenimiento( mantenimientoEquipo : MantenimientoEquipo, id_registro_mantenimiento:number )
  {
    return this.http.post<MantenimientoEquipo>(`${this.urlApi}/MantenimientoEquipo/AgregarMantenimientoEquipo?id_registro_mantenimiento=`+id_registro_mantenimiento,mantenimientoEquipo)
  }

  /* Actualiza un mantenimeinto equipo */
  updateMantenimientoEquipo( id_mantenimiento_equipo:number,comentarios_usuario:string, observ_equipo:string, problema_localizado:string )
  {
    return this.http.put<string>(`${this.urlApi}/MantenimientoEquipo/ActualizarObservRegistroMantenimiento?id_mantenimiento_equipo=`+id_mantenimiento_equipo+`&comentarios_usuario=`+comentarios_usuario+`&observ_equipo=`+observ_equipo+`&problema_localizado=`+problema_localizado,{})
  }

}
