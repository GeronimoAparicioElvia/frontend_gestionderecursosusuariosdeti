import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { TipoMan } from '../../modelos/Mantenimiento/tipo-man';

@Injectable({
  providedIn: 'root'
})
export class TipoManService {

  private urlApi : string = environment.urlback;  /*  Url del back */

  constructor(private http: HttpClient) { }       /*  Constructor */

  /* Retorna una lista con todos los tipos de mantenimiento */
  allTiposMantenimiento(): Observable<TipoMan[]>
  {
    return this.http.get<TipoMan[]>(`${this.urlApi}/TipoMant/ListaTipoMant`);
  }

  /* Retorna una lista con todos los tipos de mantenimiento activos */
  allTiposMantenimientoActivos(): Observable<TipoMan[]>
  {
    return this.http.get<TipoMan[]>(`${this.urlApi}/TipoMant/ListaTiposDeMantenimeintosActivos`);
  }

  /* Retorna un tipo de mantenimiento buscando por su Id */
  anTipoMantenimiento(id_tipo_mant:number): Observable<TipoMan>
  {
    return this.http.get<TipoMan>(`${this.urlApi}/TipoMant/TipoMantForIdTipoMant/`+id_tipo_mant);
  }

  /* Método para agregar un nuevo tipo de mantenimiento */
  saveTipoMantenimiento( tipoMantenimiento : TipoMan )
  {
    return this.http.post<TipoMan>(`${this.urlApi}/TipoMant/AgregarTipoMant/`,tipoMantenimiento)
  }

  /* Método para actualizar los datos del tipo de mantenimiento */
  public updateTipoMantenimiento(tipoMant: TipoMan) {
    return this.http.put<TipoMan>( `${this.urlApi}/TipoMant/ActualizarTipoMant/`, tipoMant);
  }
}
