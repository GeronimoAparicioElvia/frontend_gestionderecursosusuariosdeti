import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { ElemMantenimiento } from '../../modelos/Mantenimiento/elem-mantenimiento';


@Injectable({
  providedIn: 'root'
})
export class ElementMantenimientoService {

  private urlApi : string = environment.urlback;  /* Url del back */
  
  constructor(private http: HttpClient) { }       /* Constructor */

  /* Retorna todos los elemento de mantenimiento que pertenecen a un mantenimiento equipo */
  allElemMantenimientoPorIdRegistro(id_registro_mantenimiento:number): Observable<ElemMantenimiento[]>
  {
    return this.http.get<ElemMantenimiento[]>(`${this.urlApi}/ElemMant/ListaElemMantForIdMantenimiento_equipo/`+id_registro_mantenimiento);
  }

  /* Retorna todas las actividades de  mantenimiento que pertenecen a un mantenimiento equipo */
  listaActividadesPorIdMantenimientoEquipo(id_mantenimiento_equipo:number): Observable<ElemMantenimiento[]>
  {
    return this.http.get<ElemMantenimiento[]>(`${this.urlApi}/ElemMant/ListaActividadesDeMantenimeinto/`+id_mantenimiento_equipo);
  }

  /*Guarda un nuevo elemento de mantenimeinto */
  saveElementoMantenimiento(id_registro_mantenimiento:number, id_actividad_mant:number  )
  {
    let elemMantenimiento : ElemMantenimiento = new ElemMantenimiento()
    return this.http.post<ElemMantenimiento>(`${this.urlApi}/ElemMant/AgregarElemMant?id_mantenimiento_equipo=`+id_registro_mantenimiento+`&id_actividad_mant=`+id_actividad_mant,elemMantenimiento)
  }

}
