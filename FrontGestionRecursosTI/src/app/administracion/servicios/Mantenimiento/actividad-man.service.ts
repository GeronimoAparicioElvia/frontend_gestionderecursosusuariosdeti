import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { ActividadMan } from '../../modelos/Mantenimiento/actividad-man';

@Injectable({
  providedIn: 'root'
})
export class ActividadManService {

  private urlApi : string = environment.urlback;  /* Url del back */

  constructor(private http: HttpClient) { }       /* Constructor */

  /* Llama todas las actividades de mantenimeinto */
  allActividadesDeMantenimiento(): Observable<ActividadMan[]>
  {
    return this.http.get<ActividadMan[]>(`${this.urlApi}/ActividadMant/ListaActividadMant`);
  }

  /* Llama todas las actividades de mantenimeinto de un tipo de mantenimeinto*/
  allActividadesDeMantenimientoPorTipoMantenimiento(tipo_mant_id:number): Observable<ActividadMan[]>
  {
    return this.http.get<ActividadMan[]>(`${this.urlApi}/ActividadMant/ListaActividadMantForTipoMantId/`+tipo_mant_id);
  }

  /* Llama todas las actividades de mantenimeinto activas de un tipo de mantenimeinto*/
  allActividadesDeMantenimientoActivasPorTipoMantenimiento(tipo_mant_id:number): Observable<ActividadMan[]>
  {
    return this.http.get<ActividadMan[]>(`${this.urlApi}/ActividadMant/ActividadMantActivasForIdActividadMant/`+tipo_mant_id);
  }

  /* Retorna una actividad de mantenimeinto buscandolo por su Id */
  anTipoMantenimiento(id_actividad_mant:number): Observable<ActividadMan>
  {
    return this.http.get<ActividadMan>(`${this.urlApi}/ActividadMant/ActividadMantForIdActividadMant/`+id_actividad_mant);
  }

  /* Metódo para guardar una actividad de mantenimiento*/
  saveActividadMantenimiento( actividadMan : ActividadMan, tipo_mant_id : number )
  {
    return this.http.post<ActividadMan>(`${this.urlApi}/ActividadMant/AgregarActividadMant?tipo_mant_id=`+tipo_mant_id,actividadMan);
  }

  /* Metódo para actualizar una actividad de mantenimiento*/
  updateActividadMantenimiento( actividadMan : ActividadMan, tipo_mant_id : number )
  {
    return this.http.put<ActividadMan>(`${this.urlApi}/ActividadMant/ActualizarActividadMant?tipo_mant_id=`+tipo_mant_id,actividadMan);
  }
}
