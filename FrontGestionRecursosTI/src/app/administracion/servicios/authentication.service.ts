import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { findIndex, map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { Usuario } from '../modelos';
import { Perfil } from '../modelos/perfil';
import { environment } from 'src/environments/environment.prod';
import { UsuarioService } from '.';
import { Empleado } from '../modelos/empleado';
import { Router } from '@angular/router';


export class GlpiToken
{
    session_token: number | undefined;
}


@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    constructor(private http: HttpClient,
                public router: Router,
               ) { }


    private urlGlpi : string = environment.urlback;
    private urlApi  : string = environment.urlAdmin+'/login';


     private httpHeaders = new HttpHeaders(
         {
             'Content-Type': 'application/json'
         }
     );

     login(usuario :Usuario, sucursal: string) {
        //



         return this.http.post(this.urlApi+"?username="+usuario.username+"&password="+usuario.password+"&sucursal="+sucursal
         , {headers: this.httpHeaders})
            .pipe(map((res:any) => {
                 // login exitoso si se recibe un jwt token
                if (res && res.token) {

                         // se almacena localmente el usario y el jwt token
                     localStorage.setItem('currentUser', JSON.stringify({ username: usuario.username, token: res.token }));
                     localStorage.setItem('roles', JSON.stringify({roles: res.user.detalles.permsos }));

                    //Agregamos el token de glpi al usuario en turno
                     this.retornarToken()
                     return res;



                 }
                 return res;
             }));
     }
    /**Método para cerrar de sesión y redirigir al login*/
     logout() {
         // se elimina localmente el usuario
         localStorage.removeItem('currentUser');
         localStorage.removeItem('empleado');
         localStorage.removeItem('roles');
         localStorage.removeItem('sucursales');
         localStorage.removeItem('region');
         localStorage.removeItem('glpiToken');
         this.router.navigateByUrl("/login");
     }


     /**Genera los tokens para acceder a Glpi */

     generarToken() :Observable<GlpiToken>{
        return this.http.get<GlpiToken>(`${this.urlGlpi}/generateToken`);
    }

    retornarToken(): string
    {
        let cadena: string = "";
        let glpiToken: GlpiToken =  new GlpiToken();
        this.generarToken().subscribe(
            val=>{
                glpiToken = val;
                cadena = glpiToken.session_token + "";
                localStorage.setItem('glpiToken', JSON.stringify({token: cadena }));
            })
        return cadena;
    }
 }
