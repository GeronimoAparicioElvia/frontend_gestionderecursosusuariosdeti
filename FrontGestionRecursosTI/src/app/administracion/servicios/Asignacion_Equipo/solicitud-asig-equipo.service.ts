import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SolicitudAsigEquipo } from '../../modelos/Asignacion_Equipo/solicitud-asig-equipo';
import { UsuarioService } from '..';


@Injectable({
  providedIn: 'root'
})
export class SolicitudAsigEquipoService {

  private urlApi : string = environment.urlback; /*Url del back*/
  
  constructor(private http: HttpClient,private usuarioService:UsuarioService) {} /*Constructor */

  /* Consulta todas las solcilitudes */
  todasLasSolicitudesPorStatus(codigo: number) :Observable<SolicitudAsigEquipo[]>
  {
    if(codigo == 1) /* 1 Para solicitudes en espera */
    {
      return this.http.get<SolicitudAsigEquipo[]>(`${this.urlApi}/SolicitudAsignacionEquipo/ListaSolicitudesEnEspera`);
    }
    else if(codigo == 2) /* 2 Para solicitudes canceladas */
    {
      return this.http.get<SolicitudAsigEquipo[]>(`${this.urlApi}/SolicitudAsignacionEquipo/ListaSolicitudesCanceladas`);
    }
    else /* 3 o cualquier otro valor para solicitudes atendidas */
    {
      return this.http.get<SolicitudAsigEquipo[]>(`${this.urlApi}/SolicitudAsignacionEquipo/ListaSolicitudesAtendidas`);
    }
  }

  /* Consulta todas las solcilitudes realizadas por un empleado */
  todasLasSolicitudesPorSolicitanteAndStatus(codigo: number) :Observable<SolicitudAsigEquipo[]>
  {
    let usuario: number = JSON.parse(localStorage.getItem('empleado') || '{}').id;
    if(codigo == 1) /* 1 Para solicitudes en espera */
    {
      return this.http.get<SolicitudAsigEquipo[]>(`${this.urlApi}/SolicitudAsignacionEquipo/ListaSolicitudesEnEsperaPorIdSolicitante/`+usuario)
    }
    else if(codigo == 2) /* 2 Para solicitudes canceladas */
    {
      return this.http.get<SolicitudAsigEquipo[]>(`${this.urlApi}/SolicitudAsignacionEquipo/ListaSolicitudesCanceladasPorIdSolicitante/`+usuario);
    }
    else  /* 3 o cualquier otro valor para solicitudes atendidas */
    {
      return this.http.get<SolicitudAsigEquipo[]>(`${this.urlApi}/SolicitudAsignacionEquipo/ListaSolicitudesAtendidasPorIdSolicitante/`+usuario);
    }
  }

  /* Retorna una solicitud buscandola por su Id */
  solicitudPorIdSolicitud(id_solicitud_asig_equipo: number) :Observable<SolicitudAsigEquipo>
  {
    return this.http.get<SolicitudAsigEquipo>(`${this.urlApi}/SolicitudAsignacionEquipo/SolicitudPorId/`+id_solicitud_asig_equipo);
  }

  /* Agregar solicitud de equipo */
  public save( solicitudAsigEquipo : SolicitudAsigEquipo )
  {
    return this.http.post<SolicitudAsigEquipo>(`${this.urlApi}/SolicitudAsignacionEquipo/CrearSolicitudAsignacionEquipo`,solicitudAsigEquipo)
  }

  /* Actualizar solicitud de equipo */
  public update( solicitudAsigEquipo : SolicitudAsigEquipo )
  {
    return this.http.put<SolicitudAsigEquipo>(`${this.urlApi}/SolicitudAsignacionEquipo/ActualizarSolicitudAsignacionEquipo`,solicitudAsigEquipo)
  }
}
