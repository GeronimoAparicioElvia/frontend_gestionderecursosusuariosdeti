import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { RegistroAsignacionEquipo } from '../../modelos/Asignacion_Equipo/registro-asignacion-equipo';


@Injectable({
  providedIn: 'root'
})
export class RegistroAsignacionEquipoService {

  /* Variables locales */
  private id_administrador:number = JSON.parse(localStorage.getItem("empleado") || '{}').id /*Id del usuario soporte técnico */
  private nombre_____admin:string =                                                         /*Nombre del usuario soporte técnico */
  JSON.parse(localStorage.getItem("empleado") || '{}').nombreEmpleado + " " + 
  JSON.parse(localStorage.getItem("empleado") || '{}').apellidoPat + " " +
  JSON.parse(localStorage.getItem("empleado") || '{}').apellidoMat 
  private urlApi : string = environment.urlback;                                            /*Url del back */
  
  constructor(private http: HttpClient) { } /*Constructor */


  /* Retorna todos los pendientes de responsiva que tenga un empleado buscandolas por Id*/
  seguimiento()
  {
    return this.http.get<Number[]>(`${this.urlApi}/RegistroAsignacion/SeguimientoPendientes`);
  }

  /* Retorna una lista con todos los registros de asignaciòn de un empleado*/
  todosLosRegistrosDeUnColaborador(id_colaborador:number)
  {
    return this.http.get<RegistroAsignacionEquipo[]>(`${this.urlApi}/RegistroAsignacion/ListaRegistroAsignacionPorIdColaborador/` + id_colaborador);
  }

  /* Busca un registro de asignación buscando por su Id*/
  registroporId(id:number)
  {
    return this.http.get<RegistroAsignacionEquipo>(`${this.urlApi}/RegistroAsignacion/RegistroAsignacionPorId/` + id);
  }

  /* Agrega un nuevo registro de asignación sin solicitud de asignación*/
  public asignacionD(registroAsignacionEquipo: RegistroAsignacionEquipo)
  {
    registroAsignacionEquipo.nombre_empleado_registra = this.nombre_____admin
    registroAsignacionEquipo.administrador_TI = this.id_administrador
    return this.http.post<RegistroAsignacionEquipo>( `${this.urlApi}/RegistroAsignacion/AgregarRegistroAsignacionDirecta` , registroAsignacionEquipo);
  }

  /* Agrega un nuevo registro de asignación con solicitud de asignación*/
  public asignacionI(registroAsignacionEquipo: RegistroAsignacionEquipo, id_solicitud_asig_equipo:number)
  {
    registroAsignacionEquipo.nombre_empleado_registra = this.nombre_____admin
    registroAsignacionEquipo.administrador_TI = this.id_administrador
    return this.http.post<RegistroAsignacionEquipo>( `${this.urlApi}/RegistroAsignacion/AgregarRegistroAsignacionIndirecta?id_solicitud_asig_equipo=`+id_solicitud_asig_equipo , registroAsignacionEquipo);
  }

  /* Actualiza un registro de asignación*/
  public actualizacionRegistro(registroAsignacionEquipo: RegistroAsignacionEquipo)
  {
    return this.http.put<RegistroAsignacionEquipo>( `${this.urlApi}/RegistroAsignacion/ActualizarRegistroAsignacion` , registroAsignacionEquipo);
  }
}
