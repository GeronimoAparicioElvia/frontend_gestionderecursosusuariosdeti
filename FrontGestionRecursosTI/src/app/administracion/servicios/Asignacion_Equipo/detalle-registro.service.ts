import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';
import { DetalleRegistro } from '../../modelos/Asignacion_Equipo/detalle-registro';
import { UsuarioService } from '..';
import { RegistroAsignacionEquipo } from '../../modelos/Asignacion_Equipo/registro-asignacion-equipo';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})

export class DetalleRegistroService {

  private urlApi : string = environment.urlback;                              /* Url del back */
  token: string = JSON.parse(localStorage.getItem('glpiToken') || '{}').token /* Token de Glpi */

  constructor(private http: HttpClient,private router: Router ) { }           /* Constructor */
  

  /* Retorna una lista de todos los detalles que pertenecen a un registro de asignación */
  listaDetallePorIdRegistro( registro_asignacion_id:number): Observable<DetalleRegistro[]>
  {
    return this.http.get<DetalleRegistro[]>(`${this.urlApi}/DetalleRegistro/ListaDetalleRegistroForIdRegistro/`+registro_asignacion_id);
  }

  /*Busca un detalle de asignación por medio del id del equipo y el tipod e equipo*/
  /*detallePorIdEquipoAndTipo(id_equipo:number, tipo:string): Observable<DetalleRegistro>
  {
    return this.http.get<DetalleRegistro>(`${this.urlApi}/DetalleRegistro/DetalleRegistroForIdEquipoTipoEquipoStatus/`+id_equipo+`/`+tipo);
  }*/

  /* Busca un detalle de asignación por medio del nombre del equipo y el tipod e equipo*/
  detallePorNombreEquipo(nombreEquipo:string): Observable<DetalleRegistro>
  {
    return this.http.get<DetalleRegistro>(`${this.urlApi}/DetalleRegistro/DetalleRegistroForNameEquipo/`+nombreEquipo);
  }

  /* Realiza la reaisgnación de todos los equipos enviados en la lista de los nombres de los equipos*/
  reasignarEquipos( nombre_equipo     : string  [],
                    tipo_activo       : string  [],
                    id_equipo         : number  [],
                    comentarios_equipo: string  [],
                    tabla             : string  [],
                    sucursal          : number    ,
                    id_empleado       : string    ,
                    cargo             : string    ,
                    registro          : RegistroAsignacionEquipo 
                  )
  {
    return this.http.post<DetalleRegistro>
    ( 
    `${this.urlApi}/DetalleRegistro/ReasignacionEquipos?`+
    `nombre_equipo=`+ nombre_equipo+
    `&tipo_activo=`+ tipo_activo+
    `&id_equipo=`+ id_equipo+
    `&comentarios_equipo=`+ comentarios_equipo+
    `&tabla=`+ tabla+
    `&token=`+ this.token +
    `&id_empleado=`+ id_empleado+
    '&sucursal='+sucursal+
    `&cargo=`+ cargo, registro
    ).subscribe(data=>
    {
      Swal.fire({
      allowEnterKey:false,
      allowEscapeKey:false,
      allowOutsideClick:false,
      showConfirmButton:false,
      title:'Reasignación realizada',
      icon:'success',
      timer:2000
      }).then(()=>{ 
      /* Enviamos a visitar los pendientes */
      this.router.navigate(['layout/seguimientoAsignacionEquipo']) 
      })
    },
    err=>
    {
    
      Swal.fire({
        icon: 'error',
        title: 'Sin conexión, intentelo más tarde',
        showConfirmButton: false,
        timer: 3000,
        allowEnterKey:false,
        allowEscapeKey:false,
        allowOutsideClick:false
      })
    });
  }

  /* Agrega un nuevo detalle de asignación */
  public save(detalleRegistro:DetalleRegistro, id_registro_asignacion:number)
  {
    return this.http.post<DetalleRegistro>( `${this.urlApi}/DetalleRegistro/agregarNuevoDetalle?registro_asignacion_id=`+id_registro_asignacion , detalleRegistro).subscribe(data=>{console.log(data)});
  }

  /* Agrega un nuevo detalle de asignación sin subscribe*/
  public save2(detalleRegistro:DetalleRegistro, id_registro_asignacion:number)
  {
    return this.http.post<DetalleRegistro>( `${this.urlApi}/DetalleRegistro/agregarNuevoDetalle?registro_asignacion_id=`+id_registro_asignacion , detalleRegistro);
  }

  /* Actualiza un detalle de asignación */
  public update(detalleRegistro:DetalleRegistro, id_registro_asignacion:number)
  {
    return this.http.put<DetalleRegistro>( `${this.urlApi}/DetalleRegistro/ActualizarNuevoDetalle?registro_asignacion_id=`+id_registro_asignacion , detalleRegistro)
  }

  /*Sube la resposniva al detalle de asignación */
  subirResponsiva(id_detalle_asignacion:number, responsiva:File)
  {
    const headers2 = new HttpHeaders()
    headers2.set("Content-Type", "application/json")
    const formData: FormData = new FormData();
    formData.append('files', responsiva);
    return this.http.put<string>(`${this.urlApi}/DetalleRegistro/ActualizarResponsiva?id_detalle_asignacion=`+id_detalle_asignacion,formData,{reportProgress: true, responseType: 'json'});
  }
}
