import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Usuario } from '../modelos/usuario';
import { environment } from 'src/environments/environment.prod';
import { Perfil } from '../modelos/perfil';
import { Empleado } from '../modelos/empleado';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class UsuarioService {

   private urlApi : string = environment.urlAdmin+'/usuarios' ;
   private urlper : string = environment.urlAdmin+'/perfiles' ;
   private urlPue : string = environment.urlAdmin+'/puesto'   ;
   private urlEmp : string = environment.urlAdmin+'/empleados';
   private urlLoginEmpleado : string = environment.urlAdmin+'/usuarios';
   private urlFirma:string = environment.urlFirmasDig;

    private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'})
     rolLocal:any =localStorage.getItem('roles');
    constructor(private http: HttpClient,
                private router: Router) { }

    getFirmaDigital(username: String):Observable<Blob>{
      return this.http.get(`${this.urlFirma}/${username}`,{responseType: 'blob'});
    }

    getUsernamePrueba(username: String): Observable<Usuario> {
      return this.http.get<Usuario>(`${this.urlLoginEmpleado}/${username}/username`);
    }

    getAll() {
        return this.http.get<Usuario[]>(this.urlApi);
    }

    delete(id: number) {
        return this.http.delete(`${this.urlApi}/${id}`, {headers: this.httpHeaders}).subscribe((res) => {

        });
    }

    getUsuario(username :string){
        return this.http.get<Usuario>(`${this.urlApi}/${username}`);
    }

    roleMatch(allowedRoles :string[]): boolean {
      var isMatch = false;

      let userRoles = JSON.parse(this.rolLocal);
      allowedRoles.forEach(element => {
          for(let rol of userRoles.roles)
          {
              if (element == rol.authority) {
                isMatch = true;
                return false;
              }
          }
          return element;
      });
      return isMatch;
    }

    /** Empleados y Cargos */
    getAllPerfiles() {
        return this.http.get<Perfil[]>(this.urlper);
    }

    getAllPuesto() {
        return this.http.get<Perfil[]>(this.urlPue);
    }

    getAnPuesto(id: number) {
        return this.http.get<Perfil>(`${this.urlPue}/${id}`);
    }

    getAllEmpleados() {
        return this.http.get<Empleado[]>(this.urlEmp);
    }

    getAnEmpleado(id: number){
        return this.http.get<Empleado>(`${this.urlEmp}/${id}`);
    }

}
