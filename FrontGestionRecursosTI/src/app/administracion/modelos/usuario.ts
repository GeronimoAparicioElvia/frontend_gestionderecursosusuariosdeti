import { Perfil } from './perfil';
import { Empleado } from './empleado';
import { Sucursal } from './sucursal';

export class Usuario {
    idUsuario: number   | undefined;
    username: string | undefined;
    password: string | undefined;
    empleado: Empleado = new Empleado();
    perfil: Perfil = new Perfil();
    sucursales: Sucursal[] = [];
}
