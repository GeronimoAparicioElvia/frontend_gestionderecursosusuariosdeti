import { Region } from './region';

export class Sucursal {
    idSucursal: number | undefined;
    centroCosto: string | undefined;
    //nombreSucursal : string | undefined;
    nombreSucursal : string =""
    region :Region | undefined;
    calle          : string | undefined;
    numero         : string | undefined;
    colonia        : string | undefined;
    codigoPostal   : string | undefined;
    municipio      : string | undefined;
    estado         : string | undefined;
}
