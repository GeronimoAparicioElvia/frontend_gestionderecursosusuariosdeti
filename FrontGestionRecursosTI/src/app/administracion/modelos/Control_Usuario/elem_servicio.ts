import { DetalleSolicitud } from "./detalle-solicitud";
import { ServicioUsuario } from "./servicio-usuario";

export class ElemServicio{
  idElemServicio     : number | undefined;
  fecha_registro       : string | undefined;
  fecha_modificacion   : string | undefined;
  status_registro      : string | undefined;
  sucursal_id          : string | undefined;
  detalle_solicitud_id : number | undefined;
  servicio_usuario_id  : number | undefined;
  //Detalle de solicitud que realizó la activación
  detalle_solicitudAct	   : DetalleSolicitud= new DetalleSolicitud();
  //Detalle de solicitud que realizó la suspensión
  detalle_solicitudSusp	   : DetalleSolicitud= new DetalleSolicitud();
  servicio_usuario	 : ServicioUsuario = new ServicioUsuario();
}
