import { Sucursal } from "../sucursal";
import { DetalleSolicitud } from "./detalle-solicitud";

export class ServicioUsuario{
  id_servicio_usuario     	: number | undefined;
  archivo_responsiva	      : Blob | undefined;
  contrasenia               : string | undefined;
  fecha_mod_reg	            : string | undefined; //Fecha de modificación
  fechaRegistro	            : string | undefined; //Fecha de registro
  usuario	                  : string | undefined;
  detalle_solicitud_id	    : number | undefined;
  status_serv               : string | undefined; //Estatus del registro
  detalleSolicitud          : DetalleSolicitud = new DetalleSolicitud();
  /**Atributos extras utilizados para las vistas (no se incluyen en la bd) */
  nombre_servicio           : string | undefined;
  nomColaborador            : string | undefined;
  nomPuesto                 : string | undefined;
  nomSucursalCol            : string | undefined;
  fecha_soli                : string | undefined;
  status_reg	              : boolean | undefined;
  /**Permite ingresar la expersión regular ya sea de tipo gmail o algún otro */
  expresionRegular           : string |undefined;
  /**Sucursales en las que se activará el servicio */
  sucursales                : Sucursal[]=[];
}
