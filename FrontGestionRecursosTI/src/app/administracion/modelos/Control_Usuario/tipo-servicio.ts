
export class TipoServicio{
  id_tipo_servicio    : number | undefined;
  descripcion_serv    : string | undefined;
  fechaCreacionServ   : string | undefined; //Fecha de creación
  fecha_mod_serv      : string | undefined; //Fecha de modificación
  nombre_servicio     : string | undefined;
  status_servicio     : string | undefined;
  tiene_privilegio    : boolean | undefined; //Cuenta con privilegio o no
  /**Campo extra, utilizado para la vista (no se incluye en la bd)*/
  privilegio          : string | undefined;
}
