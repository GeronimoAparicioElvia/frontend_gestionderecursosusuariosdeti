
export class TipoSolicitud{
  id_tipo_solicitud_usuario : number | undefined;
  tipo_solicitud            : string | undefined;/**Nombre del tipo de solicitud */
  fecha_creacion            : string | undefined;
  fecha_mod                 : string | undefined;
  status_solicitud          :string  | undefined;
  descripcion               : string | undefined;
}
