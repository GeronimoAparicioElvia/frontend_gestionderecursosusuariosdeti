import { Solicitud } from "./solicitud";
import { TipoServicio } from "./tipo-servicio";

export class DetalleSolicitud{
  id_detalle_solicitud : number | undefined;
  status_serv	         : string | undefined; //Estatus del servicio
  solicitud_id	       : number | undefined; //Id de la solicitud
  tipo_servicio	       : TipoServicio = new TipoServicio();
  solicitud            : Solicitud= new Solicitud();
  servicio_usuario_id  : number | undefined; //Id del servicio_usuario
  /**Campo extra para mostrar en la vista de la tabla (no se incluye en la bd)*/
  nombre_servicio      : string | undefined;
}
