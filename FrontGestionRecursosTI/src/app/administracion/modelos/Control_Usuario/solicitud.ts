import { Empleado } from "../empleado";
import { TipoSolicitud } from "./tipo-solicitud";

export class Solicitud{
  id_solicitud	            : number | undefined;
  administrador_ti_id       : string | undefined; //Id del que atiende la solicitud
  colaboradorId	            : number | undefined; //Id del colaborador al que se le realiza la solictud
  comentarios	              : string | undefined;
  fecha_alta	              : string | undefined; //Fecha de alta en caso de ser temporal
  fecha_atencion	          : string | undefined; //Fecha de atención de la solicitud
  fecha_baja	              : string | undefined; //Fecha de baja en caso de ser temporal
  fechaModificacion	        : string | undefined;
  fecha_planeada_act	      : string | undefined; //Fecha de planeda para su activación en caso de ser solicitud de alta
  fechaSolicitud	          : string | undefined; //Fecha en la que se realizó la solicitud
  puesto_a_cubrir        	  : string | undefined; //En caso de ser solictud de activación
  reemplazo	                : boolean| undefined; //Si cuenta con reemplazo o no
  solicitanteId	            : number | undefined; //Id del que realiza la solicitud
  status_solicitud	        : string | undefined; //Espera, Atendido o Cancelado
  sucursal_id	              : string | undefined; //En caso de ser solictud de activación
  sustituye_a	              : string | undefined;
  tipo	                    : string | undefined; //Temporal o permanente
  nom_supervisor            : string | undefined; //Nombre del supervisor

  tipo_solicitud_usuario_id : string | undefined;
  tipo_solicitud_usuario : TipoSolicitud = new TipoSolicitud();
	/** Datos para la tabla (no se incluye en la bd) **/
	name_solicitante_id          : string | undefined;
	name_colaborador_id          : string | undefined;
	name_cargo_colaborador		   : string | undefined;
	name_administrador_TI        : string | undefined;
  name_tipo_solicitud          : string | undefined;
  name_sucursalCol             : string | undefined;
  banderaResponsiva            : boolean| undefined;

	/** Datos detalle solicitud  (no se incluye en la bd)*/
	colaborador          = new Empleado();
  servicio_usuario_id : number | undefined;
}
