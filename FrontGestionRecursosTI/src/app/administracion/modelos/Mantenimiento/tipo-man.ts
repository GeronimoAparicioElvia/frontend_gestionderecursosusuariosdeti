export class TipoMan 
{
	/* Datos que se llaman directo del backend */
    id_tipo_mant	:	number	|	undefined; /* Identifiacor del tipo de mantenimiento */
	nombre			:	string	|	undefined; /* Nombre del tipo de mantenimiento */
	descripcion		:	string	|	undefined; /* Descripción del tipo de mantenimiento */
	fecha_creacion	:	Date	|	undefined; /* Fecha de creación del tipo de mantenimiento  */
	status_mant		:	string	|	undefined; /* Estado del tipo de mantenimiento */
}
