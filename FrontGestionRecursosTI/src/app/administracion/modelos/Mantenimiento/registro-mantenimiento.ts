import { Empleado } from "../empleado";

export class RegistroMantenimiento 
{
	/* Datos que se llaman directo del backend */
    id_registro_mantenimiento	:	number	|	undefined; /* Identificador único del registro de mantenimeinto */
	observ_generales			:	string	|	undefined; /* Observaciones o comentarios generales */
	status_registro				:	string	|	undefined; /* Estado del registro de mantenimiento (Espera, Realizado Cancelado) */
	fecha_creacion				:	Date	|	undefined; /* Fecha de creacion del registro */
	fecha_mod					:	Date	|	undefined; /* fecha de modificación */
	fecha_agendada				:	Date	|	undefined; /* fecha para la cual se agenda el mantenimeinto */
	id_empleado_registra		:	number	|	undefined; /* Identificador del empleado que agenda o registra el mantenimiento */
	sucursal_id					:	number	| 	undefined; /* Identificador de la sucursal en donde se realiza el mantenimiento */
	id_empleado_realiza 		: 	number	|	undefined; /* Identificador del empleado que realiza o registra el mantenimiento */
	descripcion					: 	string	|	undefined; /* Si el mantenimiento fue eventual o programado */
	nombre_empleado_registra	: 	string	|	undefined; /* Nombre del empleado que agenda o registra el mantenimiento */
	nombre_empleado_realiza		: 	string	|	undefined; /* Nombre del empleado que realiza o registra el mantenimiento */
	nombre_sucursal				: 	string	|	undefined; /* Nombre de la sucursal */
	/* CAMPOS EXTRAS */
	/* CAMPOS PARA MANEJAR EN LAS VISTAS */
	tipo_Mantenimeinto	   : string	| undefined; /* Tipo de mantenimiento (programado o eventual)*/
	sucursal_Tabla		   : string	| undefined; /* Nombre de la sucursal en donde se agendó o realizó el mantenimiento*/
	nombre_empleado_realizo: string | undefined; /* Responsable de programar el mantenimiento */
	nombre_empleado__regist: string | undefined; /* responsable de realizar el mantenimiento */
	fecha_agendad_tabla	   : string	= ""; /* Fecha de programación que se muestra en las tablas de control */
	fecha_modific_tabla    : string	= ""; /* Fecha de modificación  que se muestra en las tablas de control */
	/* MODELOS O CLASES */
	obj_empleado_registra : Empleado = new Empleado() /* Modelo empleado con la info. del responsable  de registrar el mantenimiento */
	obj_empleado_realiza  : Empleado = new Empleado() /* Modelo empleado con la info. del responsable  de realziar el mantenimiento */	
	/* Elementos del reposte de mantenimeinto */
	mantenimiento		   	: string | undefined; /* Si el mantenimiento fue eventual o programado */
	agendado_por		   	: string | undefined; /* Nombre del empleado que agenda o registra el mantenimiento */
	fecha_programada		: string | undefined; /* Fecha de programada */
	comentarios_del_usuario : string | undefined; /* Comentarios del usuario sobre su equipo */
	observaciones_del_equipo: string | undefined; /* Comentarios del responsable de hacer el mantenimeinto sobre el equipo */
	estatus				    : string | undefined; /* Estado del registro de mantenimiento (Espera, Realizado Cancelado) *//* Observaciones o comentarios generales */
	fecha_realizacion		: string | undefined; /* Fecha de realización */
	realizado_por			: string | undefined; /* Responsable de realizar el mantenimiento */
}
