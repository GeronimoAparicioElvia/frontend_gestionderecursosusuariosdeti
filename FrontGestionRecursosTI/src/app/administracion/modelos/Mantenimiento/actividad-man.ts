import { TipoMan } from "./tipo-man";

export class ActividadMan
{
	/* Datos que se llaman directo del backend */
    id_actividad_mant	:	number	|	undefined;  /* Identificador de la actividad de mantenimeinto */
	nombre_act			:	string	|	undefined;  /* Nombre de la actividad de mantenimeinto */
	descripcion			:	string	|	undefined;  /* Descripción de la actividad de mantenimeinto */
	fecha_creacion		:	Date	|	undefined;  /* Fecha de creación de la actividad de mantenimeinto */
	status_actividad	:	string	|	undefined;  /* Fecha de modificación de la actividad de mantenimeinto */
	tipo_mant_id		:	number	|	undefined;  /* Identificador del tipo de mantenimeinto */
  	/*Campo Extra usado para los reportes de mantenimeinto */
  	tipo_mant     		:  TipoMan  |   undefined;	/* Modelo del tipo de mantenimeinto al que pertenece */
	
}
