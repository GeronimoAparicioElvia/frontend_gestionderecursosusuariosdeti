import { ViewChild } from "@angular/core";
import { FormControl } from "@angular/forms";
import { MatPaginator } from "@angular/material/paginator";
import { MatSelect } from "@angular/material/select";
import { Equipo } from "../Inventario/Equipo/equipo";
import { ActividadMan } from "./actividad-man";
import { ElemMantenimiento } from "./elem-mantenimiento";
import { RegistroMantenimiento } from "./registro-mantenimiento";
import { TipoMan } from "./tipo-man";

export class MantenimientoEquipo 
{
	/* Datos que se llaman directo del backend */
    id_mantenimiento_equipo	:	number|undefined; /*Identificador único del mantenimeinto equipo */
	comentarios_usuario		:	string|undefined; /*Comentarios u observaciones del responsable del equipo */
	tipo_activo				:	string|undefined; /*Tipo de activo al que se da mantenimeinto */
	problema_localizado		:	string|undefined; /*De proporcionarle mantenimiento correctivo especifica cual es su falla */
	observ_equipo			:	string|undefined; /*Comentarios u observaciones encontradas en el equipo  */
	equipo_id				:	number|undefined; /* identificador del equipo al que se le proporciono mantenimiento */
	registroMantenimiento 	:	RegistroMantenimiento = new RegistroMantenimiento() /*Registro de mantenimiento al que pertenece */
	/* CAMPOS EXTRAS EXTRAS */
	/*  DATOS PARA EL REPORTE */
	tipo_mantenimiento_rea: string = "" /*Mantenimiento realizado (preventivo o correctivo) */
	actividades_realizadas: string = "" /*Actividades de mantenimiento realizadas */
	/*  ELEMENTOS DE LA VISTA*/
	lista_actividades_mantenimiento: ElemMantenimiento [] = [] /*Lista de actividades realziadas */
	actividades:FormControl = new FormControl();   /*Control de las actividades realizadas al equipo (select) */
	nombre_equipo		:	string	|	undefined; /* Nombre del equipo al que se le dió mantenimineto */
	bandera				:	boolean	|	undefined; /* Determina si ya fue editado el mantenimiento equipo o aún no  */
	tipo_mantenimiento	:	number	|	undefined; /* Tipo de mantenimiento que se le dió (Preventivo o Correctivo)  */
	nombre_tipo_man		: 	string	|	undefined; /* Tipo de mantenimiento que se le dió (Preventivo o Correctivo)  */ 
	equipo:Equipo = new Equipo();                  /* Modelo con los datos del equipo al que se le dió mantenimiento */
	lista_actividades:ActividadMan[]=[]            /* Actividades realizadas al equipo */
	tMantenimineto: TipoMan = new TipoMan()		   /* Tipo de mantenimiento realizado */
}
