import { ActividadMan } from "./actividad-man";
import { MantenimientoEquipo } from "./mantenimiento-equipo";
import { RegistroMantenimiento } from "./registro-mantenimiento";

export class ElemMantenimiento 
{
	/* Datos que se llaman directo del backend */
    id_elem_mantenimiento:number|undefined;								/* Identificado del eleento de mantenimiento*/
	actividad_mant:ActividadMan = new ActividadMan()					/* Obj actividad de mantenimeinto que referencia*/
	mantenimientoEquipo:MantenimientoEquipo = new MantenimientoEquipo() /* Obj mantenimeinto equipo que referencia*/
	/*Elementos para la vista detalle de mantenimiento y reporte */
	descripcion	:	string	|	undefined; /* Descripción de las actividades*/
	nombre_act	:	string	|	undefined; /* Nombre de la activida de mantenimiento */
	nombre		:	string	|	undefined; /* Nombre del tipo de mantenimiento*/
}
