import { Perfil } from "./perfil";
import { Region } from "./region";
import { Regional } from "./regional";
import { Sucursal } from "./sucursal";

export class Empleado {
    idEmpleado  : number | undefined;
    rfc         : string | undefined;
    nombre      : string ="";
    apellidoPat : string | undefined;
    apellidoMat : string | undefined;
    email       : string | undefined;
    folioIne    : string | undefined;
    fechaIngreso: string | undefined;
    nomb_compl  : string ="";
    correoInstitucional: string | undefined;
    puesto      : Perfil   = new Perfil();
    sucursal    : Sucursal = new Sucursal();
    regional    : Regional = new Regional();
    region      : Region   = new Region();

    bandera: boolean = false;
}
