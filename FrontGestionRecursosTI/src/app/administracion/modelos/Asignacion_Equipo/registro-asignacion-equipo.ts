import { Empleado } from "../empleado";
import { SolicitudAsigEquipo } from "./solicitud-asig-equipo";

export class RegistroAsignacionEquipo 
{
	/* Datos que se llaman directo del backend */
    id_registro_asignacion	: number 	| undefined; /*Identificador único del registro de asignación */
	observaciones			: string 	| undefined; /*Observaciones generales del registro de asignación */
	colaborador_id			: number 	| undefined; /*Identificador del empleado que se le asigna el equipo */
	administrador_TI		: number 	| undefined; /*Identificador del empleado que asignó el equipo */
	status_registro			: string 	| undefined; /*Estado del registro de asignación */
	fecha_asignacion		: Date 		| undefined; /*echa en que se realizó la asignación */
	fecha_modificacion		: Date 		| undefined; /*última fecha de modificación */
	solicitud				: SolicitudAsigEquipo = new SolicitudAsigEquipo();
	/* Campos extras para mostrar en las vistas */
	nombre_empleado_asignado: string 	| undefined; /*Nombre del empleado que se le asigna el equipo */
	nombre_empleado_registra: string 	| undefined; /*Nombre del empleado que asignó el equipo */
	razon:string | undefined;						 /*Causa por la cual tiene pendiente de responsiva */
	cuenta_solicitud:string = "NO";					 /*Indica si tiene solicitud */
	fecha_asignacion_cadena		: string | undefined;/*Feca de asignación */
	fecha_modificacion_cadena	: string | undefined;/*Última fecha de modificación */

}