import { Equipo } from "../Inventario/Equipo/equipo";
import { RegistroAsignacionEquipo } from "./registro-asignacion-equipo";

export class DetalleRegistro 
{
	/* Datos que se llaman directo del backend */
    id_detalle_asignacion  : number | undefined; /* identificador único del detalle de asignación */
	status_detalle         : string | undefined; /* Determina si esta Activo o Cancelado  este detalle de asignación*/
	tipo_activo            : string | undefined; /* Tipo de activo asignado */
	observ_equipo          : string | undefined; /* Observaciones o comentarios sobre el equipo */
	archivo_responsiva	   : Blob 	| undefined; /* Espacio para contener el pdf con la responsiva */
	nombre_equipo	       : string | undefined; /* Espacio en donde guardamos el nombre del equipo  */
	equipo_id			   : number | undefined; /* Espacio en donde guardamos el identificador del equipo  */
	registro			   : RegistroAsignacionEquipo = new RegistroAsignacionEquipo() /* Registrpo de asignaciòn al que pertenece el detalle*/
	/* Campos extras para mostrar en las vistas */
	/*Datos para generar la Responsiva, Carta responsiva y Anexo a la carta responsiva */
	direccion_mac			:	string | undefined; /* Dirección física del equipo */
	direccion_ip			:	string | undefined; /* Dirección lógica del equipo */
	version_office			:	string | undefined; /* version del Office instalado en la computadora */
	licencia_office			:	string | undefined; /* licencia del Office instalado en la computadora */
	programas_instalados	:	string | undefined; /* Los otros programas instalados en la computadora */
	direccion_colaborador	:	string | undefined; /* Domicilio del responsable del equipo */
	precio					: 	number | undefined; /* Precio del equipo */
	precio_nombre			:	string | undefined; /* Precio del equipo expresado en valor numérico */
	sistemaOperativoCPU: string | undefined			/* Sistema operativo de la computadora  */
	responsiva				: boolean = false;		/* Indica si el detalle uenta con responsiva o no */
	bandera					: boolean = false		/* Valida que el detalle esté activo*/;
	equipo				    : Equipo = new Equipo();/* Modelo de equipo utilizado en el detalle de asignación*/ 
}
