import { Empleado } from "../empleado";

export class SolicitudAsigEquipo 
{
	/* Datos que se llaman directo del backend */
    id_solicitud_asig_equipo: number | undefined;	/*Identificador único de la solicitud */
	fecha_solicitud         : string | undefined;	/*Fecha de creación de la solicitud */
	observaciones           : string | undefined;	/*Comentarios y observaciones de la solicitud */
	fecha_planeada_asig     : Date   | undefined;	/*Fecha límite para contestar la solicitud */
	fecha_mod               : String | undefined;	/*Fecha de modificación de la solicitud */
	status_solicitud        : string | undefined;	/*Estado actual de la solicitud */
	descrip_equipo          : string | undefined;	/*Descripción o uso del equipo solicitado */
	tipo_equipo             : string | undefined;	/*Tipo de equipo solicitado */
	solicitante_id          : number | undefined;	/*Identificador del solicitante */
	colaborador_id          : number | undefined;	/*Idnetificador del empleado que requiere el equipo */
	administrador_TI        : number | undefined;	/*Idnetificador del empleado que atiende la solicitud */
	/*Campos extras */
	/* Datos para mostrar en la vista de la tabla de solicitudes */
	name_solicitante_id          : string | undefined;	/*Nombre del solicitante */
	name_colaborador_id          : string | undefined;	/*Nombre del empleado que requiere el equipo */
	name_cargo_colaborador		 : string | undefined;	/*Cargo del empleado que requiere el equipo */
	name_administrador_TI        : string | undefined;	/*Nombre del empleado que atiende la solicitud */
	/* Campos para mostrar en la vista de detalle de asignación */
	solicitante          = new Empleado();
	colaborador          = new Empleado();
	administrador        = new Empleado();
}
