export class ItemComponente{
    /**Atributos generales */
    id           : number | undefined;
    items_id     : string | undefined; //Id del equipo
    itemtype     : string | undefined; //Tipo de equipo
    locations_id : number | undefined; //Id de la sucursal
    /*Atributos de un  itemProcessor */
    deviceprocessors_id   : number | undefined;
    frequency             : number | undefined;
    /*Atributos de un itemSoundCard*/
    devicesoundcards_id   : number | undefined;
    /*Atributos de un itemNetworkCard*/
    devicenetworkcards_id : number | undefined;
    mac                   : string | undefined;
    /*Atributos de un itemHardDrive*/
    deviceharddrives_id   : number | undefined;
    capacity              : number | undefined;
    serial                : string | undefined;
    /*Atributos de un itemGraphicCard*/
    devicegraphiccards_id : number | undefined;
    memory                : number | undefined;
    /*Atributos de un itemControls*/
    devicecontrols_id     : number | undefined;
    /*Atributos de un itemFirmwares*/
    devicefirmwares_id    : number | undefined;
    /*Atributos de un itemMemories*/
    devicememories_id     : number | undefined;
    size                  : number | undefined;
    /*Atributos de un itemDrives*/
    devicedrives_id       : number | undefined;
    /*Atributos de un itemSistemaOperativo*/
    operatingsystems_id             : number | undefined;
    operatingsystemversions_id      : number | undefined;
    operatingsystemservicepacks_id  : number | undefined;
    operatingsystemarchitectures_id : number | undefined;
    license_number                  : string | undefined;
    license_id                      : string | undefined;

}


