export class Componente{
  /**Atributos generales de los componentes */
    id              : number | undefined;
    name            : string ="";         //Nombre
    comment         : string | undefined; //Comentarios
    designation     : string | undefined; //Nombre descriptivo
    date_mod        : string | undefined; //Fecha de modificación
    date_creation   : string | undefined; //Fecha de creación
    //Atributos de DeviceMemory
    frequence       : number | undefined; //Frecuencia
    size            : number | undefined; //Tamaño
    //Atributo de DeviceNetworkCard
    bandwidth       : string | undefined; //Velocidad

}
