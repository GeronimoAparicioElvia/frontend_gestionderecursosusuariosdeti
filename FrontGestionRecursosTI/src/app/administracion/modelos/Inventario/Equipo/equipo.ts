export class Equipo
{
    /** ATRIBUTOS GENERALES DEL EQUIPO */
    id                        : number | undefined;
    name                      : string = "";
    serial                    : string | undefined; //Número de serie
    contact                   : string | undefined; //Puesto
    users_id_tech             : number | undefined; //Id del Resp. técnico
    comment                   : string | undefined;
    locations_id              : string | undefined; //Id de la sucursal
    manufacturers_id          : string | undefined; //Id del fabricante
    users_id                  : string | undefined; //Nombre de usuario del Empleado resp.
    states_id                 : string | undefined; //Estatus del equipo
    date_creation             : string | undefined; //Fecha de creacion
    date_mod                  : string | undefined; //Fecha de modificación
    uuid                      : string | undefined;
    number_units              : string | undefined; //Número de unidades de un rack

    /** TIPO Y MODELO DE LOS ACTIVOS */
    /**Computadora */
    computermodels_id         : string | undefined;
    computertypes_id          : string | undefined;
    /**Disp. de red */
    networkequipmenttypes_id  : string | undefined;
    networkequipmentmodels_id : string | undefined;
    /**Impresora */
    printertypes_id           : string | undefined;
    printermodels_id          : string | undefined;
    /**Monitor */
    monitortypes_id           : string | undefined;
    monitormodels_id          : string | undefined;
    /**Periférico */
    peripheraltypes_id        : string | undefined;
    peripheralmodels_id       : string | undefined;
    /**Rack */
    racktypes_id              : string | undefined;
    rackmodels_id             : string | undefined;
    /**Telefono */
    phonetypes_id             : string | undefined;
    phonemodels_id            : string | undefined;
    /** CAMPOS EXXTRAS: FABRICANTE, TIPO, MODELO, ESTADO Y COMENTARIOS EXTRAS PARA MOSTRAR EN LA TABLA
        NOMBRE DEL COLABORADOR Y NOMBRE DE LA SUCURSAL? (no se incluye en la bd)
    */
    fabricante         : string | undefined;
    tipo               : string | undefined;
    modelo             : string | undefined;
    estado             : string | undefined;
    comentariosExtra   : string | undefined;
    tabla              : string | undefined;
    nombre_colaborador : string | undefined;
    nombre_sucursal    : string | undefined;

    bandera:boolean|undefined;
    indice:number|undefined;
    tipo_mantenimeinto:string|undefined;
}

