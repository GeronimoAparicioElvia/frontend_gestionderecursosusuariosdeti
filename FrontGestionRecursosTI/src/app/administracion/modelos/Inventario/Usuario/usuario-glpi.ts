export class UsuarioGlpi{
    id        :number | undefined;
    name      :string | undefined;
    realname  :string | undefined; //Apellidos
    firstname :string | undefined; //Nombre
    usertitles_id :number | undefined;  // Área a la que pertenece el usuario
}
