import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home';
import { LayoutComponent } from './layout/layout.component';
import { LoginComponent } from './login/login.component';
/** INVENTARIO */
import { AltaEquipoComponent } from './layout/Inventario/alta-equipo/alta-equipo.component';
import { GestionEquipoComponent } from './layout/Inventario/gestion-equipo/gestion-equipo.component';
import { DetalleEquipoComponent } from './layout/Inventario/detalle-equipo/detalle-equipo/detalle-equipo.component';
/** ASIGNACIÓN DE EQUIPOS **/
import { SolicitudesEquipoRealizadasComponent } from './layout/Asignacion-Equipo/Solicitante/solicitudes-equipo-realizadas/solicitudes-equipo-realizadas.component';
import { SolicitudEquipoComponent } from './layout/Asignacion-Equipo/Solicitante/solicitud-equipo/solicitud-equipo.component';
import { GestionSolicitudesEquipoSdminComponent } from './layout/Asignacion-Equipo/Administrador/gestion-solicitudes-equipo-sdmin/gestion-solicitudes-equipo-sdmin.component';
import { AsignacionDirectaComponent } from './layout/Asignacion-Equipo/Administrador/Asignaciones/asignacion-directa/asignacion-directa.component';
import { AsignacionIndirectaComponent } from './layout/Asignacion-Equipo/Administrador/Asignaciones/asignacion-indirecta/asignacion-indirecta.component';
import { DetalleSolicitudEquipoAdminComponent } from './layout/Asignacion-Equipo/Administrador/detalle-solicitud-equipo-admin/detalle-solicitud-equipo-admin.component';
import { DetalleSolicitudEquipoSolicitanteComponent } from './layout/Asignacion-Equipo/Solicitante/detalle-solicitud-equipo-solicitante/detalle-solicitud-equipo-solicitante.component';
import { AsignacionComputadoraEscritorioComponent } from './layout/Asignacion-Equipo/Administrador/Asignaciones/asignacion-computadora-escritorio/asignacion-computadora-escritorio.component';
import { ReasignacionEquipoComponent } from './layout/Asignacion-Equipo/Administrador/reasignacion-equipo/reasignacion-equipo.component';
import { ControlSeguimientoAsignacionEquiposComponent } from './layout/seguimiento/control-seguimiento-asignacion-equipos/control-seguimiento-asignacion-equipos.component';
import { AsignacionesEquipoUsuarioComponent } from './layout/seguimiento/asignaciones-equipo-usuario/asignaciones-equipo-usuario.component';
import { DetalleAsignacionEquipoUsuarioComponent } from './layout/seguimiento/detalle-asignacion-equipo-usuario/detalle-asignacion-equipo-usuario.component';
import { RemplazoEquipoComponent } from './layout/seguimiento/remplazo-equipo/remplazo-equipo.component';
/** MANTENIMIENTO */
import { AgendarMantenimientoComponent } from './layout/Mantenimiento/agendar-mantenimiento/agendar-mantenimiento.component';
import { ControlDeMantenimientoComponent } from './layout/Mantenimiento/control-de-mantenimiento/control-de-mantenimiento.component';
import { DetalleMantenimientoComponent } from './layout/Mantenimiento/detalle-mantenimiento/detalle-mantenimiento.component';
import { DetalleMantenimientoEditComponent } from './layout/Mantenimiento/detalle-mantenimiento-edit/detalle-mantenimiento-edit.component';
import { RegistrarMantenimientoComponent } from './layout/Mantenimiento/registrar-mantenimiento/registrar-mantenimiento.component';
/**CONTROL DE USUARIOS */
import { GestionSolicitudesComponent } from './layout/Asignacion-usuario/Solicitante/gestion-solicitudes/gestion-solicitudes.component';
import { SolicitudesComponent } from './layout/Asignacion-usuario/Solicitante/solicitudes/solicitudes.component';
import { DetalleSolicitudComponent } from './layout/Asignacion-usuario/Solicitante/detalle-solicitud/detalle-solicitud.component';
import { GestionSolicitudesAdminComponent } from './layout/Asignacion-usuario/Administrador/gestion-solicitudes-admin/gestion-solicitudes-admin.component';
import { SeguimientoAsignacionServicioComponent } from './layout/Seguimiento-servicios/seguimiento-asignacion-servicio/seguimiento-asignacion-servicio.component';
import { AdministrarUsuarioComponent } from './layout/Asignacion-usuario/Administrador/administrar-usuario/administrar-usuario.component';
import { ExpedientesComponent } from './layout/Asignacion-usuario/Administrador/expedientes/expedientes.component';
import { ResponsivaServicioComponent } from './layout/Seguimiento-servicios/responsiva-servicio/responsiva-servicio.component';
import { TiposEquipoComponent } from './layout/Configuraciones/Gestion-catalogos-inventario/tipos-equipo/tipos-equipo.component';
import { ModelosEquipoComponent } from './layout/Configuraciones/Gestion-catalogos-inventario/modelos-equipo/modelos-equipo.component';
import { TipoServicioComponent } from './layout/Configuraciones/Gestion-catalogos-controlU/tipo-servicio/tipo-servicio.component';
import { TipoSolicitudComponent } from './layout/Configuraciones/Gestion-catalogos-controlU/tipo-solicitud/tipo-solicitud.component';
import { TipoMantenimientoComponent } from './layout/Configuraciones/Gestion-catalogos-mantenimiento/tipo-mantenimiento/tipo-mantenimiento.component';
import { ActividadMantenimientoComponent } from './layout/Configuraciones/Gestion-catalogos-mantenimiento/actividad-mantenimiento/actividad-mantenimiento.component';
import { DetalleAltaComponent } from './layout/Asignacion-usuario/Administrador/detalle-alta/detalle-alta.component';
import { DetalleActivacionComponent } from './layout/Asignacion-usuario/Administrador/detalle-activacion/detalle-activacion.component';
import { DetalleSuspensionComponent } from './layout/Asignacion-usuario/Administrador/detalle-suspension/detalle-suspension.component';

import { AuthGuard } from './_guards/auth.guard';
import { PermisosGuardGuard } from './_guards/permisos-guard.guard';
import { Permisos } from './administracion/permisos/permisos';
import { SidebarComponent } from './layout/components/sidebar/sidebar.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  {
    path: 'layout',
     children: [
       {
        path: '',  component: LayoutComponent , canActivate: [AuthGuard], canActivateChild:[PermisosGuardGuard],
        children: [
          /** HOME */
          { path: '', component: HomeComponent },
          { path: 'home', component: HomeComponent,},
          /** SIDEBAR */
          { path: 'sidebar', component: SidebarComponent },
          /* MÓDULO 1 */
          { path: 'altaequipo'                     , component: AltaEquipoComponent          ,},
          { path: 'gestionequipos'                 , component: GestionEquipoComponent       ,},
          { path: 'detalleEquipo/:id/:nomActivo'   , component: DetalleEquipoComponent       ,},
          { path: 'gestionequipos2/:nomActivo'     , component: GestionEquipoComponent       ,},
          /* MÓDULO 2 */
          { path: 'solicitudEquipo'                                                                     , component: SolicitudEquipoComponent                     ,data:{permiso:Permisos.SOLICITAREQUIPO}},
          { path: 'solicitudesDeEquipoSolicitante'                                                      , component: SolicitudesEquipoRealizadasComponent         ,data:{permiso:Permisos.SOLICITUDESREALIZADAS}},
          { path: 'detalleSolicitudEquipoSolicitante/:id_solicitud'                                     , component: DetalleSolicitudEquipoSolicitanteComponent   ,data:{permiso:Permisos.DETALLESOLICITUDSOLICITANTE}},

          { path: 'solicitudesDeEquipoAdministrador'                                                    , component: GestionSolicitudesEquipoSdminComponent       ,},//,data:{permiso:Permisos.GESTIONSOLICITUDES}},
          { path: 'detalleSolicitudEquipoAdministrador/:id_solicitud/:status_solicitud'                 , component: DetalleSolicitudEquipoAdminComponent         ,},//,data:{permiso:Permisos.DETALLESOLICITUDADMINISTRADOR}},

          { path: 'asignacionDirecta'                                                                   , component: AsignacionDirectaComponent                   ,data:{permiso:Permisos.ASIGNACIONDIRECTA}},
          { path: 'asignacionIndirecta/:id_colaborador/:tipo_activo/:id_solicitud/:descripcion'         , component: AsignacionIndirectaComponent                 ,},//,data:{permiso:Permisos.ASIGNACIONSOLICITUD}},
          { path: 'reasignacionEquipo'                                                                  , component: ReasignacionEquipoComponent                  ,data:{permiso:Permisos.REASIGNACIONDEEQUIPO}},
          { path: 'asignacion_computadora_escitorio/:nombre_empleado/:apellidos_empleado'+
                  '/:id_computadora/:nombre_computadora/:asignacion/:id_empleado/:comentariosCPU'       , component: AsignacionComputadoraEscritorioComponent     ,data:{permiso:Permisos.ASIGNAREQUIPODEESCRITORIO}},
          { path: 'seguimientoAsignacionEquipo'                                                         , component: ControlSeguimientoAsignacionEquiposComponent ,data:{permiso:Permisos.CONTROLASIGNACIONES}},
          { path: 'asignacionesEquipoUsuario/:id_empleado'                                              , component: AsignacionesEquipoUsuarioComponent           ,data:{permiso:Permisos.ASIGNACIONESEMPLEADO}},
          { path: 'detalleAsignacionEquipoUsuario/:id_administrador/:id_colaborador/:id_registro'       , component: DetalleAsignacionEquipoUsuarioComponent      ,data:{permiso:Permisos.DETALLEASIGNACION}},
          { path: 'remplazoEquipo/:id_registro_asignacion/:id_empleado_asignac/:nombre_empleado_asi'    , component: RemplazoEquipoComponent                      ,data:{permiso:Permisos.REMPLAZARCOMPONENTEEQUIPO}},

          /** MÓDULO 3 */
          { path: 'gestionSolicitudes'                        , component: GestionSolicitudesComponent        ,},
          { path: 'solicitanteSolicitudes'                    , component: SolicitudesComponent               ,},
          { path: 'DetalleSolicitudSolicitante/:idSolicitud'  , component: DetalleSolicitudComponent          ,},
          { path: 'gestionSolicitudesAdmin/:tipoSolicitud'    , component: GestionSolicitudesAdminComponent   ,},
          { path: 'gestionSolicitudesAdmin2/:tipoSolicitud'   , component: GestionSolicitudesAdminComponent   ,},
          { path: 'gestionSolicitudesAdmin3/:tipoSolicitud'   , component: GestionSolicitudesAdminComponent   ,},
          { path: 'gestionSolicitudesAdminInit'               , component: GestionSolicitudesAdminComponent   ,},
          { path: 'gestionSolicitudesAdmin4/:tipoSolicitud/:idSolicitudRegElemS' , component: GestionSolicitudesAdminComponent   ,},
          { path: 'SeguimientoControlU/:idSolicitudRegElemS'  , component : SeguimientoAsignacionServicioComponent  ,},
          { path: 'SeguimientoControlU'                       , component : SeguimientoAsignacionServicioComponent  ,},
          { path: 'AdministrarUsuarios'                       , component: AdministrarUsuarioComponent        ,},
          { path: 'Expedientes'                               , component: ExpedientesComponent               ,},
          { path: 'Expedientes2/:idColaborador'               , component: ExpedientesComponent               ,},
          { path: 'ResponsivaServicio/:idSolicitud'           , component: ResponsivaServicioComponent        ,},
          { path: 'GestionTipoEquipo'                         , component: TiposEquipoComponent               ,},
          { path: 'GestionModeloEquipo'                       , component: ModelosEquipoComponent             ,},
          { path: 'GestionTipoSolicitud'                      , component: TipoSolicitudComponent             ,},
          { path: 'GestionTipoServicio'                       , component: TipoServicioComponent              ,},
          { path: 'GestionTipoMantenimiento'                  , component: TipoMantenimientoComponent         ,},
          { path: 'GestionActividadMantenimiento'             , component: ActividadMantenimientoComponent    ,},
          { path: 'DetalleAdminAlta/:idSolicitud'             , component: DetalleAltaComponent               ,},
          { path: 'DetalleAdminActivacion/:idSolicitud'       , component: DetalleActivacionComponent         ,},
          { path: 'DetalleAdminSuspension/:idSolicitud'       , component: DetalleSuspensionComponent         ,},
          { path: 'DetalleAdminAltaExp/:idSolicitud/:banderaE'             , component: DetalleAltaComponent               ,},
          { path: 'DetalleAdminActivacionExp/:idSolicitud/:banderaE'       , component: DetalleActivacionComponent         ,},
          { path: 'DetalleAdminSuspensionExp/:idSolicitud/:banderaE'       , component: DetalleSuspensionComponent         ,},

           /** MÓDULO 4 */
           { path: 'agendarMantenimiento'                                              , component: AgendarMantenimientoComponent      ,data:{permiso:Permisos.PROGRAMARMANTENIMIENTO}},
           { path: 'controlDeMantenimiento'                                            , component: ControlDeMantenimientoComponent    ,data:{permiso:Permisos.CONTROLMANTENIMEINTO}},
           { path: 'detalleMantenimiento/:id_registro_mantenimiento/:status_registro'  , component: DetalleMantenimientoComponent      ,data:{permiso:Permisos.DETALLEMANTENIMEINTO}},
           { path: 'detalleMantenimientoEdit/:id_registro_mantenimiento'               , component: DetalleMantenimientoEditComponent  ,data:{permiso:Permisos.DETALLEEDITMANTENIMEINTO}},
           { path: 'registrarMantenimiento'                                            , component: RegistrarMantenimientoComponent    ,data:{permiso:Permisos.REGISTRARMANTENIMIENTO}},
        ]
      }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
