import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { MatRadioModule } from '@angular/material/radio';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { HomeComponent } from './home';
import { FooterComponent } from './layout/components/footer/footer.component';
import { HeaderComponent } from './layout/components/header/header.component';
import { SidebarComponent } from './layout/components/sidebar/sidebar.component';
import { LayoutComponent } from './layout/layout.component';
import { LoginComponent } from './login/login.component';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { NgxBarcode6Module } from 'ngx-barcode6';
import { MatDialogModule} from '@angular/material/dialog';
import { MatCardModule} from '@angular/material/card';
import { MatTableModule} from '@angular/material/table';
import { MatPaginatorModule} from '@angular/material/paginator';
import { MatFormFieldModule} from '@angular/material/form-field/';
import { MatInputModule} from '@angular/material/input'
import { MatDatepickerModule} from '@angular/material/datepicker';
import { MatIconModule} from '@angular/material/icon'
import { MatGridListModule} from '@angular/material/grid-list';
import { MatButtonModule} from '@angular/material/button';
import { MatSelectModule} from '@angular/material/select';
import { AppRoutingModule } from './app-routing.module';
import { MatNativeDateModule } from '@angular/material/core';
import { MatChipsModule} from '@angular/material/chips';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatTableExporterModule } from 'mat-table-exporter';
import { AltaEquipoComponent } from './layout/Inventario/alta-equipo/alta-equipo.component';
import { GestionEquipoComponent } from './layout/Inventario/gestion-equipo/gestion-equipo.component';
import { MatTabsModule} from '@angular/material/tabs';
import { SolicitudEquipoComponent } from './layout/Asignacion-Equipo/Solicitante/solicitud-equipo/solicitud-equipo.component';
import { GestionSolicitudesEquipoSdminComponent } from './layout/Asignacion-Equipo/Administrador/gestion-solicitudes-equipo-sdmin/gestion-solicitudes-equipo-sdmin.component';
import { DetalleSolicitudEquipoAdminComponent } from './layout/Asignacion-Equipo/Administrador/detalle-solicitud-equipo-admin/detalle-solicitud-equipo-admin.component';
import { SolicitudesEquipoRealizadasComponent } from './layout/Asignacion-Equipo/Solicitante/solicitudes-equipo-realizadas/solicitudes-equipo-realizadas.component';
import { DetalleSolicitudEquipoSolicitanteComponent } from './layout/Asignacion-Equipo/Solicitante/detalle-solicitud-equipo-solicitante/detalle-solicitud-equipo-solicitante.component';
import { AsignacionDirectaComponent } from './layout/Asignacion-Equipo/Administrador/Asignaciones/asignacion-directa/asignacion-directa.component';
import { AsignacionIndirectaComponent } from './layout/Asignacion-Equipo/Administrador/Asignaciones/asignacion-indirecta/asignacion-indirecta.component';
import { DetalleEquipoComponent } from './layout/Inventario/detalle-equipo/detalle-equipo/detalle-equipo.component';
import { AsignacionComputadoraEscritorioComponent } from './layout/Asignacion-Equipo/Administrador/Asignaciones/asignacion-computadora-escritorio/asignacion-computadora-escritorio.component';
import { ReasignacionEquipoComponent } from './layout/Asignacion-Equipo/Administrador/reasignacion-equipo/reasignacion-equipo.component';
import { AgendarMantenimientoComponent } from './layout/Mantenimiento/agendar-mantenimiento/agendar-mantenimiento.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { TablaSelectComponent } from './layout/Dialogs/Mantenimiento/tabla-select/tabla-select.component';
import { VisualizarResponsivaComponent } from './layout/Dialogs/Mantenimiento/visualizar-responsiva/visualizar-responsiva.component';
import { MatMenuModule} from '@angular/material/menu';
import { ControlDeMantenimientoComponent } from './layout/Mantenimiento/control-de-mantenimiento/control-de-mantenimiento.component';
import { DetalleMantenimientoComponent } from './layout/Mantenimiento/detalle-mantenimiento/detalle-mantenimiento.component';
import { DetalleMantenimientoEditComponent } from './layout/Mantenimiento/detalle-mantenimiento-edit/detalle-mantenimiento-edit.component';
import { RegistrarMantenimientoComponent } from './layout/Mantenimiento/registrar-mantenimiento/registrar-mantenimiento.component';
import { MatBadgeModule} from '@angular/material/badge';
import {MatTooltipModule} from '@angular/material/tooltip';
import { ControlSeguimientoAsignacionEquiposComponent } from './layout/seguimiento/control-seguimiento-asignacion-equipos/control-seguimiento-asignacion-equipos.component';
import { AsignacionesEquipoUsuarioComponent } from './layout/seguimiento/asignaciones-equipo-usuario/asignaciones-equipo-usuario.component';
import { DetalleAsignacionEquipoUsuarioComponent } from './layout/seguimiento/detalle-asignacion-equipo-usuario/detalle-asignacion-equipo-usuario.component';
import { RemplazoEquipoComponent } from './layout/seguimiento/remplazo-equipo/remplazo-equipo.component';

/**Modulo servicios y configuraciones */
import { SolicitudesComponent } from './layout/Asignacion-usuario/Solicitante/solicitudes/solicitudes.component';
import { GestionSolicitudesComponent } from './layout/Asignacion-usuario/Solicitante/gestion-solicitudes/gestion-solicitudes.component';
import { DetalleSolicitudComponent } from './layout/Asignacion-usuario/Solicitante/detalle-solicitud/detalle-solicitud.component';
import { GestionSolicitudesAdminComponent } from './layout/Asignacion-usuario/Administrador/gestion-solicitudes-admin/gestion-solicitudes-admin.component';
import { SeguimientoAsignacionServicioComponent } from './layout/Seguimiento-servicios/seguimiento-asignacion-servicio/seguimiento-asignacion-servicio.component';
import { AdministrarUsuarioComponent } from './layout/Asignacion-usuario/Administrador/administrar-usuario/administrar-usuario.component';
import { ExpedientesComponent } from './layout/Asignacion-usuario/Administrador/expedientes/expedientes.component';
import { ResponsivaServicioComponent } from './layout/Seguimiento-servicios/responsiva-servicio/responsiva-servicio.component';
import { VerResponsivaServicioComponent } from './layout/Seguimiento-servicios/ver-responsiva-servicio/ver-responsiva-servicio.component';
import { TiposEquipoComponent } from './layout/Configuraciones/Gestion-catalogos-inventario/tipos-equipo/tipos-equipo.component';
import { ModelosEquipoComponent } from './layout/Configuraciones/Gestion-catalogos-inventario/modelos-equipo/modelos-equipo.component';
import { TipoSolicitudComponent } from './layout/Configuraciones/Gestion-catalogos-controlU/tipo-solicitud/tipo-solicitud.component';
import { TipoServicioComponent } from './layout/Configuraciones/Gestion-catalogos-controlU/tipo-servicio/tipo-servicio.component';
import { TipoMantenimientoComponent } from './layout/Configuraciones/Gestion-catalogos-mantenimiento/tipo-mantenimiento/tipo-mantenimiento.component';
import { ActividadMantenimientoComponent } from './layout/Configuraciones/Gestion-catalogos-mantenimiento/actividad-mantenimiento/actividad-mantenimiento.component';
import { DetalleActivacionComponent } from './layout/Asignacion-usuario/Administrador/detalle-activacion/detalle-activacion.component';
import { DetalleSuspensionComponent } from './layout/Asignacion-usuario/Administrador/detalle-suspension/detalle-suspension.component';
import { DetalleAltaComponent } from './layout/Asignacion-usuario/Administrador/detalle-alta/detalle-alta.component';




@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    LoginComponent,
    HomeComponent,
    AltaEquipoComponent,
    GestionEquipoComponent,
    SolicitudEquipoComponent,
    GestionSolicitudesEquipoSdminComponent,
    DetalleSolicitudEquipoAdminComponent,
    SolicitudesEquipoRealizadasComponent,
    DetalleSolicitudEquipoSolicitanteComponent,
    AsignacionDirectaComponent,
    AsignacionIndirectaComponent,
    DetalleEquipoComponent,
    AsignacionComputadoraEscritorioComponent,
    ReasignacionEquipoComponent,
    AgendarMantenimientoComponent,
    TablaSelectComponent,
    VisualizarResponsivaComponent,
    ControlDeMantenimientoComponent,
    DetalleMantenimientoComponent,
    DetalleMantenimientoEditComponent,
    RegistrarMantenimientoComponent,
    ControlSeguimientoAsignacionEquiposComponent,
    AsignacionesEquipoUsuarioComponent,
    DetalleAsignacionEquipoUsuarioComponent,
    RemplazoEquipoComponent,

    //SERVICIOS Y CONFIGURACIONES
    SolicitudesComponent,
    GestionSolicitudesComponent,
    DetalleSolicitudComponent,
    GestionSolicitudesAdminComponent,
    SeguimientoAsignacionServicioComponent,
    AdministrarUsuarioComponent,
    ExpedientesComponent,
    ResponsivaServicioComponent,
    VerResponsivaServicioComponent,
    TiposEquipoComponent,
    ModelosEquipoComponent,
    TipoSolicitudComponent,
    TipoServicioComponent,
    TipoMantenimientoComponent,
    ActividadMantenimientoComponent,
    DetalleAltaComponent,
    DetalleActivacionComponent,
    DetalleSuspensionComponent

  ],
  imports: [
    MatDialogModule,
    BrowserModule,
    NgxBarcode6Module,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxExtendedPdfViewerModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatCardModule,
    MatGridListModule,
    MatButtonModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    MatIconModule,
    MatNativeDateModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatTableExporterModule,
    MatTabsModule,
    MatCheckboxModule,
    PdfViewerModule,
    MatMenuModule,
    MatBadgeModule,
    MatTooltipModule,
    MatRadioModule,
    NgxMatSelectSearchModule
  ],
  providers: [
    MatDatepickerModule,
    MatNativeDateModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
