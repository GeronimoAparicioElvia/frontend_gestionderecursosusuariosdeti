export const environment = {
  production: true,
  /**Url la cual hace referencia al endpoind de pruebas de la empresa */
  urlAdmin: "http://192.168.1.189:8081/adminPruebas/api",
  /**urlback debe contener la url del back del sistema de SGRUTI */
  urlback: "http://localhost:8090/api",
  //urlback: "http://192.168.1.189:8081/gestion/api",
  /**Url de la cual se hace uso para acceder a las firmas digitales de los usuarios */
  urlFirmasDig: 'http://192.168.1.189:8081/admin/api/usuarios/firmas',
};
